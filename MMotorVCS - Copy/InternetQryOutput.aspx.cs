﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;

public partial class InternetQryOutput : System.Web.UI.Page
{
    public DataTable dv;

    private void Page_Load(object sender, EventArgs e)
    {

        try
        {
     
                string usrn;
                usrn = "";
                lblUpdationDate.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["InternetVisitor"] + " " + Session["ClientAddr"].ToString().Trim();
                lblenqdatetime.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");

                string _nameofquery = Convert.ToString(Session["Nameofquery"]);
                string _datdt1 = System.DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                string _refno = Convert.ToString(Session["InternetVisitor"]);
                string _VehicleTypeDesc = Convert.ToString(Session["VehicleTypeDesc"]);
                string _MakeDesc = Convert.ToString(Session["MakeDesc"]);
                string _Registration = Convert.ToString(Session["Registration"]);
                string _Chasis = Convert.ToString(Session["Chasis"]);
                string _Engine = Convert.ToString(Session["Engine"]);
                lblvectype.Text = _VehicleTypeDesc;
                lblmake.Text = _MakeDesc;
                lblregistrationno.Text = _Registration;
                lblchasisno.Text = _Chasis;
                lblengineno.Text = _Engine;
                Label1.Text = " Status of";
                if (Session["MatchStatus"].ToString() == "1")
                {
                    lblmsg.Text = " is matched with";
                    Label1.Text = " Status of";
                    dv = (DataTable)Session["Data"];

                    int _count = Convert.ToInt32(dv.Rows.Count);

                    if (_count > 0)
                    {
                        Repeater1.DataSource = dv;
                        Repeater1.DataBind();


                    }
                    else
                    {
                        lblmsg.Text = " is not recovered yet.";
                        Label1.Text = " Recovery status of";
                    }
                }
                else if (Session["MatchStatus"].ToString() == "2")
                {
                    lblmsg.Text = " is not recovered yet.";
                    Label1.Text = " Recovery status of";
                }
                else if (Session["MatchStatus"].ToString() == "3")
                {
                    lblmsg.Text = " has not been reported as stolen by police.";
                    Label1.Text = " Status of";
                }

                else
                {
                    lblmsg.Text = " is not found in the database.";
                    Label1.Text = " Status of";
                }
            }

        
        catch
        {
            Response.Redirect("ERROR.aspx");
        }
    }
}