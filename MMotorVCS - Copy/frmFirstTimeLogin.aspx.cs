﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using MMotorVCS;
using System.Text.RegularExpressions;

public partial class frmFirstTimeLogin : System.Web.UI.Page
{

    static int user_id;
    static string _userid;
    DataSet ds = new DataSet();
    ListItemCollection liColl;
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    Class1 myclass = new Class1();
    static int _add, _edit, _delete, _view;
    static int pagecount = 0;
    int PostCount = 0;
    private string vcDistrictCode;
    private string vcTypeCode;
    private string vcStateCode;
    private int _roleId;
    string uniqueCode;
    string strUserType;
    static int randomNumber;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["vcUserName"] == null)
        {

        }
        else
        {

            txtLoginCode.Text = Session["vcUserName"].ToString();
            txtLoginCode.Enabled = false;
        }

       
        {

            if (Session["vcStateCode"] == null)
            {


            }
            else
            {
                vcStateCode = Session["vcStateCode"].ToString();
                _roleId = Convert.ToInt32(Session["inRoleId"]);
                vcTypeCode = Session["vcType"].ToString();
            }

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            bindRoleName();
            bindStateName();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));

            if (Convert.ToInt32(Session["inRoleId"]) == 3 || Convert.ToInt32(Session["inRoleId"]) == 4 || Convert.ToInt32(Session["inRoleId"]) == 6 || Convert.ToInt32(Session["inRoleId"]) == 7 || Convert.ToInt32(Session["inRoleId"]) == 9)
            {
                TableRow4.Visible = false;
            }

            if (Convert.ToInt32(Session["inRoleId"]) != 1)
            {
                txtLoginCode.Enabled = false;
                ddAcDeac.Enabled = false;
                ddUserRole.Enabled = false;
                ddStateName.Enabled = false;
                ddDistrictName.Enabled = false;
                ddPsName.Enabled = false;
            }
            if (!IsPostBack)
            {
                try
                {
                    Random autoRand = new Random();
                    randomNumber = Convert.ToInt32(autoRand.Next(0, 36));
                    String rno = randomNumber.ToString();

                    btnLogin.Attributes.Add("onClick", "return EncryptforgetPassword(" + "'" + rno + "'" + ");");

                    bindQuestion();
                    DataSet dsDetails = mm.getUserDetailsState(Session["vcUserName"].ToString().Trim(), _roleId);
                    if (dsDetails.Tables[0].Rows.Count >= 1)
                    {
                        txtLoginCode.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcUserName"].ToString().Trim());
                        ddStateName.Items.Insert(0, GenericMethods.check(dsDetails.Tables[0].Rows[0]["statename"].ToString().Trim()));
                        ddDistrictName.Items.Insert(0, GenericMethods.check(dsDetails.Tables[0].Rows[0]["districtname"].ToString().Trim()));
                        ddPsName.Items.Insert(0, GenericMethods.check(dsDetails.Tables[0].Rows[0]["Psname"].ToString().Trim()));
                        ddUserRole.SelectedValue = GenericMethods.check(dsDetails.Tables[0].Rows[0]["inRoleId"].ToString().Trim());
                        txtemail.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcMail"].ToString().Trim());
                        txtFileNo.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcFileNumber"].ToString().Trim());
                        txtAmount.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["cashamount"].ToString().Trim());
                        txtAuthority1.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
                        txtAddress1.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcAddress"].ToString().Trim());
                        txtHeader.Text = GenericMethods.check(dsDetails.Tables[0].Rows[0]["vcHeader"].ToString().Trim());
                        Session["inRoleid"] = dsDetails.Tables[0].Rows[0]["inRoleId"].ToString().Trim();

                        if (Convert.ToInt32(Session["inRoleId"]) == 3 || Convert.ToInt32(Session["inRoleId"]) == 4 || Convert.ToInt32(Session["inRoleId"]) == 6 || Convert.ToInt32(Session["inRoleId"]) == 7 || Convert.ToInt32(Session["inRoleId"]) == 9)
                        {
                            TableRow4.Visible = false;
                        }
                    }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
                catch
                {
                }
                finally
                {
                }
                   
            }


            
        }
    }

    public void bindQuestion()
    {
        try
        {

            DataTable dTblQues = clsIncident.GetQues();
            ddQues1.DataSource = dTblQues;
            ddQues1.DataValueField = GenericMethods.check(dTblQues.Columns[0].ColumnName);
            ddQues1.DataTextField = GenericMethods.check(dTblQues.Columns[1].ColumnName);
            ddQues1.DataBind();

            ddQues2.DataSource = dTblQues;
            ddQues2.DataValueField = GenericMethods.check(dTblQues.Columns[0].ColumnName);
            ddQues2.DataTextField = GenericMethods.check(dTblQues.Columns[1].ColumnName);
            ddQues2.DataBind();

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            try
            {
               
                string a, b, c, d;
                a = txtLoginCode.Text;
                a = txtAddress1.Text;
                c = txtAuthority1.Text;
                int flag = 0;
                if (txtLoginCode.Text != "" && txtConPass.Text != "" && txtPassword.Text != "" && ddQues1.SelectedIndex != 0 && ddQues2.SelectedIndex != 0 && txtAns1.Text != "" && txtAns2.Text != "")
                {

                    if (txtPassword.Text == txtConPass.Text)
                    {

                        string pp = myclass.md5(txtPassword.Text.ToString().Trim());
                      
                        if (eform.UpdateUser_Detail(txtLoginCode.Text, txtFileNo.Text, txtAddress1.Text, txtAuthority1.Text, txtemail.Text, txtAmount.Text, txtHeader.Text) > 0)
                        {
                            lblmsg.Text = "<font color=Blue>Updated Successfully !!!</font>";
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again</font>";
                        }



                        if (clsPageBase.InsertQuestion(txtLoginCode.Text.ToString().Trim(), ddQues1.SelectedValue.ToString().Trim(), ddQues2.SelectedValue.ToString().Trim(), txtAns1.Text.ToString().Trim(), txtAns2.Text.ToString().Trim(), pp.Trim()) > 0)
                        {
                            if (clsPageBase.deletealreadyloggedin(txtLoginCode.Text.ToString().Trim()) > 0)
                            {
                            }
                            lblmsg.Text = "<font color=Blue>Updated Successfully !!! Please do Re-login with New Password entered in this form</font>";
                            Session["vcUserName"] = txtLoginCode.Text.ToString().Trim();
                            relogin.Visible = true;
                            relogin.Focus();
                            }

                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Your account is already there.Contact Administrator</font>";
                        }
                    }

                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                    }

                }
                else
                {
                    lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
                }
                if (eform.UpdateUser_Detail(txtLoginCode.Text, txtFileNo.Text, txtAddress1.Text, txtAuthority1.Text, txtemail.Text, txtAmount.Text, txtHeader.Text) > 0)
                {

                }
                else
                {
                
                }
            }
            catch (SqlException)
            { lblmsg.Text = "Occuring Problem in connectivity to database."; }
            catch (HttpCompileException)
            { lblmsg.Text = "System Occuring Problem."; }
            catch (HttpParseException)
            { lblmsg.Text = "Internet Browser Occuring Problem."; }
            catch (Exception ex)
            {
   
            }
        }
    }

    
    protected void refresh()
    {
        txtPassword.Text = "";
        txtLoginCode.Text = "";
        txtConPass.Text = "";
        ddQues1.SelectedIndex = 0;
        ddQues2.SelectedIndex = 0;
        txtAns1.Text = "";
        txtAns2.Text = "";
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void txtLoginCode_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddQues1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    public void bindStateName()
    {
        try
        {

            DataSet dTblState = mm.getStateTypeNew();
            ddStateName.DataSource = dTblState;
            ddStateName.DataTextField = GenericMethods.check(dTblState.Tables[0].Columns["statename"].ColumnName);
            ddStateName.DataValueField = GenericMethods.check(dTblState.Tables[0].Columns["statecode"].ColumnName);
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }

    }

    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {

            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();
            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {

            ddDistrictName.Items.Clear();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));


        }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            string[] temp = ddDistrictName.SelectedValue.Split('-');
            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]));
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = ds.Tables[0].Columns["Psname"].ColumnName;
            ddPsName.DataValueField = ds.Tables[0].Columns["psidpscode"].ColumnName;
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = GenericMethods.check(dt.Columns["districtname"].ColumnName);
            ddDistrictName.DataValueField = GenericMethods.check(dt.Columns["districtcode"].ColumnName);
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }



    public void bindRoleName()
    {
        try
        {
            DataTable dTblRole = clsPageBase.GetRole();
            ddUserRole.DataSource = dTblRole;
            ddUserRole.DataValueField = GenericMethods.check(dTblRole.Columns[0].ColumnName);
            ddUserRole.DataTextField = GenericMethods.check(dTblRole.Columns[1].ColumnName);
            ddUserRole.DataBind();
            ddUserRole.Items.Insert(0, "--Select--");


        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void bindgrid()
    {
        try
        {
          
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected bool checkusername(string UserName)
    {
        try
        {
            if (Convert.ToBoolean((clsPageBase.GetVerifyUserName(UserName))))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected string decrypt(string EncryptValue)
    {
        char[] EncAry = new char[EncryptValue.Length];
        char[] OrgAry = new char[EncryptValue.Length];
        int i;
        EncAry = EncryptValue.ToCharArray();
        for (i = 0; i < EncryptValue.Length; i++)
        {
            OrgAry[i] = (char)((int)EncAry[i] - 63);
        }
        string OriginalString = new string(OrgAry);
        return OriginalString;
    }
    protected string encrypt(string OriginalValue)
    {
        char[] OrgAry = new char[OriginalValue.Length];
        char[] EncAry = new char[OriginalValue.Length];
        int i;
        OrgAry = OriginalValue.ToCharArray();
        for (i = 0; i < OriginalValue.Length; i++)
        {
            EncAry[i] = (char)((int)OrgAry[i] + 63);
        }
        string encryptString = new string(EncAry);
        return encryptString;
    }

    public byte[] EncryptPassword(string username, string password, byte[] salt1, byte[] salt2)
    {
        string tmpPassword = null;
        string rsPwd;
        tmpPassword = Convert.ToBase64String(salt1) + Convert.ToBase64String(salt2) + username.ToLower() + password;

        UTF8Encoding textConvertor = new UTF8Encoding();
        byte[] passBytes = textConvertor.GetBytes(tmpPassword);
        return new SHA384Managed().ComputeHash(passBytes);


    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            byte[] encPassword;

            if (ddStateName.SelectedIndex != 0)
            {
                if (txtLoginCode.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0 &&
                ddStateName.SelectedIndex != 0)
                {
                    if (checkusername(txtLoginCode.Text.Trim().ToUpper()) == false)
                    {
                        if (txtPassword.Text == txtConPass.Text)
                        {

                            string pp = myclass.md5(txtPassword.Text.ToString().Trim());
                            uniqueCode = ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                            if (ddDistrictName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            if (ddPsName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));

                            if (eform.UpdateUser_Detail(txtLoginCode.Text, txtFileNo.Text, txtAddress1.Text, txtAuthority1.Text, txtemail.Text, txtAmount.Text, txtHeader.Text) > 0)
                            {
                                lblmsg.Text = "<font color=Blue>Updated Successfully !!!</font>";
                                string script = "alert('Updated Successfully! Please Re-login with New Password');";
                                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
                            }
                            else
                            {
                                lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again</font>";
                            }

                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                        }
                    }
                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                    }
                }
            }
            else
            {
                if (txtLoginCode.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0)
                {
                    if (checkusername(txtLoginCode.Text.Trim().ToUpper()) == false)
                    {
                        if (txtPassword.Text == txtConPass.Text)
                        {

                            string pp = myclass.md5(txtPassword.Text.ToString().Trim());

                            if (ddStateName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                            }
                            if (ddDistrictName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            if (ddPsName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));
                            if (txtAmount.Text.ToString().Trim() == "")
                            {
                                txtAmount.Text = "0";
                            }
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                        }
                    }
                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                    }
                }

            }
          

        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string encPassword = encrypt(txtPassword.Text);
            string a, b, c, d;
            a = txtLoginCode.Text;
            a = txtAddress1.Text;
            c = txtAuthority1.Text;


            if (eform.UpdateUser_Detail(txtLoginCode.Text, txtFileNo.Text, txtAddress1.Text, txtAuthority1.Text, txtemail.Text, txtAmount.Text, txtHeader.Text) > 0)
            {
                lblmsg.Text = "<font color=Blue>Updated Successfully !!! Please do Re-login with New Password</font>";

            }
            else
            {
                lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again</font>";
            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
        
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
   
    protected void gvUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
 
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void gvUserDetails_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[11].Visible = false;
        }
    }

    protected void btndelete_Click(object sender, EventArgs e)
    {

    }

    protected void relogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
}