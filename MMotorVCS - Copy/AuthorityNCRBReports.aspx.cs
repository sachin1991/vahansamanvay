﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;


public partial class AuthorityNCRBReports : System.Web.UI.Page
{
    public DataTable dv;
    DataSet dt;
    DataSet dtemail;

    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {

                Response.Redirect("LogoutModule.aspx");
            }
            lblUpdated.Text = DateTime.Today.ToShortDateString().Trim();
            lblPrinted.Text = System.DateTime.Now.Date.ToString("ddMMyyyy") + System.DateTime.Now.ToString("HHmmss") + " User ID " + Session["vcUserName"].ToString() + " I.P. " + Session["ClientAddr"].ToString().Trim();
            DateText.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
            string _MakeDesc = Session["MakeDesc"].ToString();
            string _Registration = Session["Registration"].ToString();
            string _Chasis = Session["Chasis"].ToString();
            string _Engine = Session["Engine"].ToString();
            string _color = Session["Color"].ToString();
            string _model = Session["Model"].ToString();
            string _stateCode = Session["vcStateCode"].ToString();
            Entry_FormMethods eform = new Entry_FormMethods();
            DataSet ds = new DataSet();
            MasterMethods mm = new MasterMethods();
            DataSet dtbvisitor = mm.getcountnoc(Session["vcUserName"].ToString());
            if (dtbvisitor != null)
            {
                if (dtbvisitor.Tables[0].Rows.Count > 0)
                {
                    lblFile.Text = dtbvisitor.Tables[0].Rows[0][0].ToString();
                }
            }
            ds = eform.Addcountnoc(lblFile.Text.ToString(), Session["vcUserName"].ToString());
            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;
            lblModel.Text = _model;
            lblColor.Text = _color;
            
            captcha.Text = Session["strRandom"].ToString()+" " + Session["visitor"].ToString();
            string _username = Session["vcusername"].ToString();

            dt = eform.getCounterHeader1(_username);

            if (dt.Tables[0].Rows.Count > 0)
            {
                AuthorityOffice.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcHeader"].ToString().Trim()) + " " + dt.Tables[0].Rows[0]["vcAddress"].ToString().Trim();
                lblFooter.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
            }
            if (Session["MatchStatus"].ToString() == "1")
            {


                lblMsg.Text = " It is certified that the follwing case relating to the theft of the above mentioned motor vehicle has been reported or is pending as per the information available with NCRB till date (based on data received from States/UTs police).  Therefore the above mentioned vehicle is NOT CLEAR from police angle as required under Section 48(5) of the Motor Vehicle Act, 1988.";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();
                    rptAddress.DataSource = dv;
                    rptAddress.DataBind();
                    toaddress.Visible = false;
                    rptAddress.Visible = false;

                    String profilename;
                    String receipients;
                    String subject;
                    String bodytext;
                    profilename = "SQLProfile";
                    subject = "Vahan Samanvay Motor Vehicle Coordination";
                    receipients = "";
                
                    String bt;
                    bt = "";
                    foreach (DataRow dtRow in dv.Rows)
                    {
                        foreach(DataColumn dc in dv.Columns)
                        {
                            if (dc.ColumnName.ToString() == "STCODE")
                            {
                                String std;
                                std = dtRow[dc].ToString();
                                dtemail = eform.getemail(std);
                                receipients = receipients + dtemail.Tables[0].Rows[0]["vcMail"].ToString();
                            }
                            else
                            {
                                bt = bt + dc.ColumnName.ToString() + ": " + dtRow[dc].ToString() + "(.) ";
                            }
                        }
                        
                    }
                    bodytext = AuthorityOffice.Text  + " enquired the particular of Vehicle Type " + Session["VehicleTypedesc"] + "(.)Make " + Session["MakeDesc"] + "(.)Registration " + Session["Registration"] + "(.)Chasis " + Session["Chasis"] + "(.)Engine " + Session["Engine"] + "(.)Color " + Session["Color"] + "(.)Year of Manufacture " + Session["Model"] + " is linked with " + bt;
                    DataSet dsmale = new DataSet();
                    dsmale = eform.sendmail(profilename, receipients, subject, bodytext);

                }
                else
                {
                    toaddress.Visible = false;
                    rptAddress.Visible = false;
                    lblMsg.Text = " has not been reported as stolen by police.";
                }

            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                toaddress.Visible = false;
                rptAddress.Visible = false;
                lblMsg.Text = " is not recovered yet.";
            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                toaddress.Visible = false;
                rptAddress.Visible = false;

                lblMsg.Text = " It is certified that no case relating to the theft of the above mentioned motor vehicle has been reported or is pending as per the infromation available with NCRB till date (based on data received from States/UTs police).";
            }

            else
            {
                toaddress.Visible = false;
                rptAddress.Visible = false;
                lblMsg.Text = " is not found in the database.";
            }
        }
        catch
        {
        }
    }
    public string printsho()
    {
        string strsho ="SHO";
        return strsho;
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {

    }
}