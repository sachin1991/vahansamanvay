<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogoutModule.aspx.cs" Inherits="LogoutModule" Theme="emsTheme"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Logout</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:label ID="lblError" runat="server" Style="position: relative; top: 87px; left: 87px; color: #cc3333;" CssClass="head" Font-Bold="True" Font-Italic="True" Font-Overline="True">You are currently logged out.</asp:label>
    
    </div>
    </form>
</body>
</html>
