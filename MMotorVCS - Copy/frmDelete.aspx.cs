﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using MMotorVCS;
public partial class frmDelete : System.Web.UI.Page
{
    Entry_FormMethods eform = new Entry_FormMethods();
    MasterMethods mobj = new MasterMethods();
    Connection mycon = new Connection();
    SqlDataAdapter da;
    SqlCommand cmd;
    int selind;
    string oldregno;
    string oldchasisno;
    string oldengineno;
    string oldautotype;
    string oldautotypeCode;
    string oldmake;
    string oldmakeCode;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {
                Response.Redirect("LogoutModule.aspx");
            }

            if (!IsPostBack)
            {
                Label1.Visible = false;
            }
            string value = Session["inRoleid"].ToString();
            switch (value)
            {
                case "1":
                    break;
                default:
                    Response.Redirect("LogoutModule.aspx");
                    break;
            }
        }
        private void FillVehicleGrid()
        {

            {
                Label1.Visible = false;
                string vregno, vchasisno, vengineno;
                if (regno.Text == "")
                {
                    vregno = "@";
                }
                else
                {
                    vregno = regno.Text;
                }
                if (chasisno.Text == "")
                {
                    vchasisno = "@";
                }
                else
                {
                    vchasisno = chasisno.Text;
                }
                if (engineno.Text == "")
                {
                    vengineno = "@";
                }
                else
                {
                    vengineno = engineno.Text;
                }

                if (Convert.ToInt32(Session["inRoleId"]) == 1)
                {
                    DataSet ds = new DataSet();
                    ds = eform.getEditDeleteadmin(vregno, vchasisno, vengineno);
                    gvEG.DataSource = null;
                    gvEG.DataBind();
                    gvEG.DataSource = ds;
                    gvEG.DataBind();
                }
                else
                {
                    DataSet ds = new DataSet();
                    ds = eform.getEditDeleteuser(vregno, vchasisno, vengineno, Session["vcUserName"].ToString().Trim());
                    gvEG.DataSource = ds;
                    gvEG.DataBind();
                    gvEG.Visible = true;
                }




            }
        }
        protected void gvEG_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            gvEG.Visible = true;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
            }
            if (e.Row.RowState == DataControlRowState.Edit)
            {

                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                DropDownList ddlStatus = (DropDownList)e.Row.FindControl("ddlStatus");

                ddlStatus.Items.FindByValue((e.Row.FindControl("lblStatus") as Label).Text).Selected = true;
            }
            if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            {
                DropDownList ddlType = (DropDownList)e.Row.FindControl("ddlType");
                if (ddlType != null)
                {
                    ddlType.DataSource = mobj.getVehicleType();
                    ddlType.DataBind();
                }
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
            }
           
        }
        protected void gvEG_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Insert"))
            {
            }
        }
        protected void gvEG_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvEG.EditIndex = e.NewEditIndex;
            FillVehicleGrid();
            gvEG.EnableViewState = true;
            gvEG.Visible = true;
   
          
        }
        protected void gvEG_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            VehicleInfo vInfo = new VehicleInfo();
            vInfo.ID = (gvEG.DataKeys[e.RowIndex].Values[0].ToString());
            oldregno = (gvEG.DataKeys[e.RowIndex].Values[2].ToString());
            oldchasisno = (gvEG.DataKeys[e.RowIndex].Values[3].ToString());
            oldengineno = (gvEG.DataKeys[e.RowIndex].Values[4].ToString());
            vInfo.Status = ((DropDownList)gvEG.Rows[e.RowIndex].FindControl("ddlStatus")).Text;
            vInfo.RegistrationNo = ((TextBox)gvEG.Rows[e.RowIndex].FindControl("txtRegistration")).Text;
            vInfo.ChasisNo = ((TextBox)gvEG.Rows[e.RowIndex].FindControl("txtChasis")).Text;
            vInfo.EngineNo = ((TextBox)gvEG.Rows[e.RowIndex].FindControl("txtEngine")).Text;
            oldautotypeCode = ((Label)gvEG.Rows[e.RowIndex].FindControl("lblAutoTypeCode")).Text;
            oldautotype = ((Label)gvEG.Rows[e.RowIndex].FindControl("lblAutoType")).Text;
            oldmakeCode = ((Label)gvEG.Rows[e.RowIndex].FindControl("lblMakeCode")).Text;
            oldmake = ((Label)gvEG.Rows[e.RowIndex].FindControl("lblMake")).Text;
            vInfo.isDeleted =Convert.ToBoolean(((CheckBox)gvEG.Rows[e.RowIndex].FindControl("chkActive")).Checked);
            if (mobj.updateCrimeDetails(vInfo.ID, vInfo.RegistrationNo, vInfo.ChasisNo, vInfo.EngineNo,vInfo.Status, vInfo.isDeleted, Session["vcUserName"].ToString(),oldregno,oldchasisno,oldengineno,oldautotypeCode,oldmakeCode ))
            {
                lblmsg.Text = "<font color=green>Values updated</font>";
              
            }
            else
            {
                lblmsg.Text = "<font color=red>Values updated</font>";
            }
            gvEG.EditIndex = -1;
            FillVehicleGrid();
            gvEG.EnableViewState = true;
        }
        protected void gvEG_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvEG.EditIndex = -1;
            FillVehicleGrid();
            gvEG.Visible = true;
        }
        protected void gvEG_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
               // Int64 ID = Convert.ToInt64(gvEG.DataKeys[e.RowIndex].Values[0].ToString());
                SqlCommand cmd;
                SqlConnection con = mycon.MakeConnection();
                string msql;
                con.Open();
                oldregno = gvEG.DataKeys[e.RowIndex].Values[2].ToString();
                oldchasisno = gvEG.DataKeys[e.RowIndex].Values[3].ToString();
                oldengineno = gvEG.DataKeys[e.RowIndex].Values[4].ToString();
                msql = "UPDATE Autoentry SET deleted=1 where crimeno = '" + gvEG.DataKeys[e.RowIndex].Values[0].ToString() + "' and (Regnokey = '" + oldregno + "' or chasisnokey = '" + oldchasisno + "' or enginenokey = '" + oldengineno + "')";
                cmd = new SqlCommand(msql, con);
                int i = cmd.ExecuteNonQuery();
                msql = "UPDATE TblTrn_EnterLostRecoveredVehicle SET isdeleted=1 where vcCrime_no = '" + gvEG.DataKeys[e.RowIndex].Values[0].ToString() + "' and (vcRegistrationNo='" + oldregno + "' or vcChasisNo='" + oldchasisno + "' or vcEngineNo='" + oldengineno + "')";
                cmd = new SqlCommand(msql, con);
                i = cmd.ExecuteNonQuery();
                con.Close();
                Label1.Visible = true;
                lblmsg.Text = "<font color=green>Record Deleted</font>";
                FillVehicleGrid();
                //submit_Click(submit, e);
            }
            catch (Exception ex)
            {

            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            Label1.Visible  = false;
            string vregno, vchasisno, vengineno;
            if (regno.Text == "")
            {
                vregno = "@";
            }
            else
            {
            vregno  = regno.Text;
            }
            if (chasisno.Text == "")
            {
                vchasisno = "@";
            }
            else
            {
                vchasisno = chasisno.Text;
            }
            if (engineno.Text == "")
            {
                vengineno = "@";
            }
            else
            {
                vengineno = engineno.Text;
            }
            if (Convert.ToInt32(Session["inRoleId"]) == 1 || Convert.ToInt32(Session["inRoleId"]) == 10)
            {
                DataSet ds = new DataSet();
                ds = eform.getEditDeleteadmin(vregno, vchasisno, vengineno);
                gvEG.DataSource = null;
                gvEG.DataBind();
                gvEG.DataSource = ds;
                gvEG.DataBind();
            }
            else
            {
                DataSet ds = new DataSet();
                ds = eform.getEditDeleteuser(vregno, vchasisno, vengineno, Session["vcUserName"].ToString().Trim());              
                gvEG.DataSource = ds;
                gvEG.DataBind();
                gvEG.Visible = true;
            }
        }


        protected void gvEG_SelectedIndexChanged(object sender, EventArgs e)
        {
            selind = gvEG.SelectedRow.RowIndex;
            oldregno = gvEG.SelectedRow.Cells[1].ToString();
            oldchasisno = gvEG.SelectedRow.Cells[2].ToString();
            oldengineno = gvEG.SelectedRow.Cells[3].ToString();

        }
}
