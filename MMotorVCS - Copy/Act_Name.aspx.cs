using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Act_Name : System.Web.UI.Page
{
    static int atype_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();
    
    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");
        
        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
           
        }
        if (!IsPostBack)
        {
            try
            {                
              
            }
            catch (Exception)
            {
                Response.Redirect("error.aspx");
            }
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindgrid();

        }
        lblmsg.Text = "";
    }
    public void getmaxid()
    {
        try
        {
            atype_id = mm.getActNameMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getActName();
            gvActName.DataSource = ds;
            gvActName.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtActCode.Text = "";
        txtActName.Text = "";
        bindgrid();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
      
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getActName();
            if (txtActCode.Text != "" && txtActName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["actcode"].ToString().ToUpper().Trim()) == txtActCode.Text.ToUpper().Trim())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Act Code is already exists.</font>";
                        break;
                    }
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["actname"].ToString().ToUpper().Trim()) == txtActName.Text.ToUpper().Trim())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Act name is already exists.</font>";
                        break;
                    }
                }
                if (flag != 1)
                {
                    if (mm.AddActName((atype_id), (txtActCode.Text.ToUpper().Trim()), txtActName.Text.ToUpper().Trim(), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values inserted</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not inserted</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
       
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getActName();
            if (txtActCode.Text != "" && txtActName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["actid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["actcode"].ToString().ToUpper().Trim()) == txtActCode.Text.ToUpper().Trim())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Act Code is already exists.</font>";
                            break;
                        }
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["actname"].ToString().ToUpper().Trim()) == txtActName.Text.ToUpper().Trim())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Act name is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    if (mm.updateActName(Convert.ToInt32(lblid.Text), (txtActCode.Text.ToUpper().Trim()), txtActName.Text.ToUpper().Trim(), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values inserted</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not inserted</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void gvActName_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvActName.Rows[Convert.ToInt32(e.CommandName) - (gvActName.PageSize * pagecount)].Cells[1].Text;
                txtActCode.Text = gvActName.Rows[Convert.ToInt32(e.CommandName) - (gvActName.PageSize * pagecount)].Cells[2].Text;
                txtActName.Text = gvActName.Rows[Convert.ToInt32(e.CommandName) - (gvActName.PageSize * pagecount)].Cells[3].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvActName_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvActName.PageIndex = e.NewPageIndex;
        refresh(); 
    }
    protected void gvActName_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
}
