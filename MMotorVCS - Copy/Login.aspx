<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" SmartNavigation="true"
    Inherits="Login" Title="Vahan Samanvay Login Page" ValidateRequest="true" EnableViewState="true"
    EnableViewStateMac="true" ViewStateEncryptionMode="Always"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Vahan Samanvay</title>
    <style type="text/css">
        .style1
        {
            height: 31px;
        }
        .GridHeaderText
        {
            height: 147px;
        }
        .style2
        {
            text-decoration: underline;
        }
        .style3
        {
            font-weight: bold;
        }
    </style>
</head>
<body ondragstart="return false" onselectstart="return false" oncontextmenu="return false"
    autocomplete="off">
    <form id="form1" runat="server" autocomplete="off">
    <script language="javascript">
        document.onmousedown = disableclick;
        status = "Right Click Disabled";
        function disableclick(e) {
            if (event.button == 2) {
                alert("Right Click Disabled");
                return false;
            }
        }
    </script>
    <script language="javascript" type="text/javascript" src="js/md5.js"></script>
    <script language="javascript" type="text/javascript">

        function ValidateTPACode() {
            objLoginCode = document.getElementById("txtLoginCode").value;
            objPasswd = document.getElementById("txtPassword").value;

            if (TrimUsingWhileLoop(objLoginCode).length != 0) {
                if (TrimUsingWhileLoop(objPasswd).length != 0) {
                    return true;
                }


            }
            alert("Please enter a valid UserID or Password");
            return false;

        }


        function TrimUsingWhileLoop(str) {
            while (str.charAt(0) == (" ")) {
                str = str.substring(1);
            }
            while (str.charAt(str.length - 1) == " ") {
                str = str.substring(0, str.length - 1);

            }
            return str;
        }


    </script>
    <script language="javascript" type="text/javascript">
        function openNewWindowscontact() {
            window.open("Contact us.htm");
        }

        function openNewWindowsFAQ() {
            window.open("Frequently asked questions Login.pdf");
        }
        function openNewWindowsHelp() {
            window.open("Vahan Samanvay BrowserSettingsLogin.pdf");
        }

    
    </script>
    <table width="100%">
        <tr>
            <td colspan="4">
                <asp:Image ID="Image1" runat="server" AlternateText="NCRB Vahan Samanvay Image" ImageUrl="~/img/BlueVSHead4.jpg"
                    Width="100%" />
            </td>
        </tr>
    </table>
    <table align="center" class="tabTbl">
        <tr align="center">
            <td align="center">
                <table width="98%" align="right" class="tabTbl">
                    <tr>
                        <td class="style1">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
    <br />
                <table id="tblLogin" class="datatbl" cellspacing="1">
                    <tr>
                        <td class="rA">
                            <strong>User ID</strong>
                        </td>
                        <td class="rB">
                            <asp:TextBox ID="txtLoginCode" runat="server"></asp:TextBox>
                        </td>
            </td>
            <td align="right">
                &nbsp;</td>
            <tr>
                <td class="rA">
                    <strong>Password</strong>
                </td>
                <td class="rB">
                    <asp:TextBox ID="txtPassword" CssClass="combo" runat="server" TextMode="Password"
                        MaxLength="35"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="rA">
                    <strong>Please enter the code</strong>
                </td>
                <td class="rB">
                    <asp:TextBox ID="txtCap" CssClass="combo" runat="server" MaxLength="6"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td class="rB">
                    <asp:Image ID="imgCaptcha" runat="server" ImageUrl="~/captcha.aspx" Height="30px"
                        Width="120px" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="rB">
                    <asp:Button ID="btnLogin" CssClass="btn" Text="Login" runat="server" OnClick="btnLogin_Click" />
                    &nbsp;<asp:Button ID="btnForget" runat="server" Text="Forget Password" OnClick="btnForget_Click" />
                    <asp:CheckBox ID="forcelogin" runat="server" Text="Force Login" Visible="false" />
                </td>
            </tr>
            <tr id="trRrrMsg" visible="false" class="rB" style="height: 25px;" runat="server">
                <td align="center" colspan="2" class="errortext">
                    Invalid UserID or Password, please contact administrator!<br />
                </td>
            </tr>
            <tr id="trRrrMsg1" visible="true" class="rB" style="height: 25px;" runat="server">
                <td align="center" colspan="2" class="errortext">
                    Your Visitor No is :
                    <asp:Label ID="txtVisitor" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="nousersloggedin" runat="server"></asp:Label>
                </td>
            </tr>
            <td colspan="2" align="center">
                <strong><span class="style2">Browser Setting:<br />
                </span>In Windows 7 i.e 10 Please Select the Compatibility View from Tools Menu.<br />
                    Please Turn off the Popup Blocker for this site.
                    <br />
                    Under Internet Options/Tabs Settings/Always open pop ups in a new tab<br />
                    Please Select Print Background colours and images
                    <br />
                    from Internet Options/Advance/Printing</strong></strong>&nbsp;<br />
                <asp:HyperLink ID="Help" runat="server" runat="server" onclick="openNewWindowsHelp()"
                    Style="cursor: pointer; text-decoration: underline; color: Blue">Help Settings</asp:HyperLink>
                &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" runat="server" onclick="openNewWindowsFAQ()"
                    Style="cursor: pointer; text-decoration: underline; color: Blue">FAQ</asp:HyperLink>
                &nbsp;<asp:HyperLink ID="HyperLink2" runat="server" runat="server" onclick="openNewWindowscontact()"
                    Style="cursor: pointer; text-decoration: underline; color: Blue">Contact Us</asp:HyperLink>
            </td>
            </tr> </table> </td>
        </tr>
    </table>
    </td> </tr>
    <tr width="98%" align="justify">
        <td class="copyrights">
        This is the National Portal of India, developed with an objective to enable a single
        window data input and access to vahan samnvay.The information being provided by
        the all Police station across the country.&nbsp; This Application is designed and
        maintained by National Crime Records Bureau (NCRB), Government of India.
    </tr>
    <table class="GridHeaderText" width="98%" align="right">
        <hr />
        <center>For best experience use browser like Mozilla Firefox or Google Chrome.</center>
        <tr align="center" visible="false" runat="server">
            <td>
                <ul style="height: 10px; width: 1024px">
                    <a href='#' onclick="window.open('abouttheapp.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                        About this Application :||: </a><a href='#' onclick="window.open('TermOfUse.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                            Terms of Use :||: </a><a href='#' onclick="window.open('help.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                                Legal Help :||: </a><a href='#' onclick="window.open('speakout.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                                    Your Opinion :||: </a><a href='#' onclick="window.open('SiteMap.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                                        Site Map :||: </a><a href='#' onclick="window.open('Faqs_EIF.htm','','width=650,height=400,resizable=1,scrollbars=1')">
                                            FAQ's :||: </a>
                </ul>
            </td>
        </tr>
       <%-- <tr align="center">
            <td style="width: 80%;">
                <strong>Vahan Samanvay a Re-engineered product of Motor Vehicle Coordination System<br />
                    Re-engineered </strong>by B. Venkatesan <strong>
                        <br />
                        Developed by </strong>B. Venkatesan, Monika Thawani<strong><br />
                            Assisted by:</strong> Mansi Chawla, Shweta Sinha<br />
                <br />
                Please do not use Internet Explorer for this site. Use any other browser like Mozilla
                Firefox or Google Chrome.
            </td>
        </tr>--%>
    </table>
    </table>
    <cc1:TextBoxWatermarkExtender ID="tbwm1" runat="server" TargetControlID="txtLoginCode"
        WatermarkText="Enter User Id">
    </cc1:TextBoxWatermarkExtender>
    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtCap"
        WatermarkText="Enter Captcha">
    </cc1:TextBoxWatermarkExtender>
    </form>
</body>
</html>
