﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;

public partial class rptCounter_Involve_Report_Cash : System.Web.UI.Page
{
    public DataTable dv;
    DataSet dtCounterdata;
    DataSet dtemail;
    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {

                Response.Redirect("LogoutModule.aspx");
            }

            if (Session["inRoleId"].ToString() == "2" || Session["inRoleId"].ToString() == "5" || Session["inRoleId"].ToString() == "1")
            {

                lblDated.Text = System.DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                lblAmount.Text = Session["Amount"].ToString();
                if (lblAmount.Text.ToString() == "0")
                {
                    Original.Text = "Duplicate";
                }
                else
                {
                    Original.Text = "";
                }
                CashIPO.Text = Session["CashIPO"].ToString();
                lblReciept.Text = Session["ReceiptNo"].ToString();
                lblName.Text = Session["Name"].ToString();
                lblUpdationDate.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["vcUserName"].ToString() + " "  + Session["ClientAddr"].ToString().Trim();

                lblenqdatetime.Text = " " + System.DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + Session["vcUserName"].ToString() + " "  + Session["ClientAddr"].ToString().Trim();
                if (Session["MatchStatus"].ToString() == "1")
                {
                    lblmsg.Text = " is matched with";
                    dv = (DataTable)Session["Data"];

                    int _count = Convert.ToInt32(dv.Rows.Count);

                    if (_count > 0)
                    {
                        Repeater1.DataSource = dv;
                        Repeater1.DataBind();
                        String profilename;
                        String receipients;
                        String subject;
                        String bodytext;
                        profilename = "SQLProfile";
                        subject = "Vahan Samanvay Motor Vehicle Coordination";
                        receipients = "";

                        String bt;
                        bt = "";
                        foreach (DataRow dtRow in dv.Rows)
                        {
                            // on all table's columns
                            foreach (DataColumn dc in dv.Columns)
                            {
                                if (dc.ColumnName.ToString() == "STCODE")
                                {
                                    String std;
                                    std = dtRow[dc].ToString();
                                    dtemail = eform.getemail(std);
                                    receipients = receipients + dtemail.Tables[0].Rows[0]["vcMail"].ToString();
                                }
                                else
                                {
                                    bt = bt + dc.ColumnName.ToString() + ": " + dtRow[dc].ToString() + "(.) ";
                                }
                            }

                        }
                        bodytext = lblName.Text + " at " + lblAddress.Text + " " + lblReciept.Text    + " enquired the particular of Vehicle Type " + Session["VehicleTypedesc"] + "(.)Make " + Session["MakeDesc"] + "(.)Registration " + Session["Registration"] + "(.)Chasis " + Session["Chasis"] + "(.)Engine " + Session["Engine"] + "(.)Color " + Session["Color"] + "(.)Year of Manufacture " + Session["Model"] + " is linked with " + bt;
                        DataSet dsmale = new DataSet();
                        dsmale = eform.sendmail(profilename, receipients, subject, bodytext);

                    }
                    else
                    {
                        lblmsg.Text = " is not recovered yet.";
                    }
                }
                else if (Session["MatchStatus"].ToString() == "2")
                {
                    lblmsg.Text = " is not recovered yet.";
                }
                else if (Session["MatchStatus"].ToString() == "3")
                {
                    lblmsg.Text = " has not been reported as stolen by police.";
                }

                else
                {
                    lblmsg.Text = " is not found in the database.";
                }
                string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
                string _MakeDesc = Session["MakeDesc"].ToString();
                string _Registration = Session["Registration"].ToString();
                string _Chasis = Session["Chasis"].ToString();
                string _Engine = Session["Engine"].ToString();
                string _usrnam = Session["vcUserName"].ToString();
                lblvectype.Text = _VehicleTypeDesc;
                lblmake.Text = _MakeDesc;
                lblregistrationno.Text = _Registration;
                lblchasisno.Text = _Chasis;
                lblengineno.Text = _Engine;
                dtCounterdata = eform.getCounterHeader1(_usrnam);
                if (Session["CashIPO"] != "Cash")
                {
                    lbladdressofapp.Visible = true;
                    lbladdressofapp.Text = "To <br><br>" + Session["Name"].ToString() + "<br>"+ Session["Address"].ToString();
                }
                else
                {
                    lbladdressofapp.Visible = false;
                    //lbladdressofapp.Text = "To <br><br>" + Session["Name"].ToString() + "<br>" + Session["Address"].ToString();
                }
                captcha.Text = Session["strRandom"].ToString() + " " + Session["visitor"].ToString();
                if ((dtCounterdata.Tables[0].Rows.Count) > 0)
                {
                    lblAddress.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0][1].ToString().Trim());
                    lblFooter.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
                }
                else
                {
                    lblAddress.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0][1].ToString().Trim());
                    lblFooter.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
                }

            }

        }
        catch
        {
        }
    }
    public string checkpartialandprintPartialNote()
    {
        if (dv.Rows[0]["MatchingParam"].ToString().Contains("Partial"))
        {
            return "<tr><td colspan='2'>Note: In case of partial match, the particulars should be verified physically by the concerned authority</td></tr>";
        }
        else
        {
            return "";
        }
    }
    public string printstoleninfo()
    {
        if (Session["Stolenavailable"].ToString() == "false")
        {
            return "<tr></td>Please request the concerned police station to update the stolen information in Vahan Samanvay System.  NCRB does not accept update request from General Public/Insurance Investigators.</td></tr>";
        }
        else
        {
            return "";
        }
    }
    public string findowner()
    {
        try
        {
            VahanWSRef.VahanInfo serv = new VahanWSRef.VahanInfo();
            String resp;
            String regis, chas, engi;
            regis = dv.Rows[0]["registration"].ToString();
            chas = dv.Rows[0]["chasis"].ToString();
            engi = dv.Rows[0]["engine"].ToString();
            String response = serv.getDetails("DLNCRB", regis);
            response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
            resp = response;
            string rr;
            rr = accesswebservice.stringoutput1(resp);
            if (rr == "")
            {
                response = serv.getChasisDetails("DLNCRB", chas);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            if (rr == "")
            {
                response = serv.getEngineDetails("DLNCRB", engi);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            return rr;
        }
        catch
        {
            return "";
        }
    }    
}

    
 
