<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportVSPerfectMatchView.aspx.vb" Inherits="ReportVSPerfectMatchView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Vahan Samanvay Report</title>
    <link href="style/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">




        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 97px;
        }
        .style4
        {
            width: 75px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="contentmain">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="GOI" runat="server" Font-Bold="True" Text="Government of India" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="MHA" runat="server" Font-Bold="True" Text="Mnistry of Home Affairs"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="NCRB" runat="server" Font-Bold="True" Text="National Crime Records Bureau"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="VS" runat="server" Font-Bold="True" Text="Vahan Samanvay" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="LISTOFSTOLEN" runat="server" Text="List of Stolen Vehicles from "
                        Visible="False"></asp:Label>
                    <asp:TextBox ID="DTFROM" runat="server" Visible="False" BorderStyle="None"></asp:TextBox>
                    <asp:Label ID="LISTTO" runat="server" Text="to" Visible="False"></asp:Label>
                    <asp:TextBox ID="DTTO" runat="server" Visible="False" BorderStyle="None"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <span style="color:Green;font-weight:bold;">
        Total Match Cases:&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
        </span>
        <div align="center">        
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table border='1' cellpadding='0' cellspacing='0' class="report">
   
                    <tr class="heading">
                    <td>
                    S.NO.
                    </td>
                        <td>
                            REGISTRATION
                        </td>
                        <td>
                            ENGINE
                        </td>
                        <td>
                            CHASIS
                        </td>
                        <td>
                            VEHICLETYPE
                        </td>
                        <td>
                            VEHICLEMAKE
                        </td>
                        <td>
                            STATE
                        </td>
                        <td>
                            DISTRICT
                        </td>
                        <td>
                            PS
                        </td>
                        <td>
                            FIRNO
                        </td>
                        <td>
                            FIRDATE
                        </td>
                        <td>
                            STOLEN
                            RECOVERED
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr align="vtop">
                <td>
                  <%Response.Write(serialnumberprint())%>  
                </td>
                    <td >
                        <%# DataBinder.Eval(Container, "DataItem.REGNO")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RREGNO")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container, "DataItem.ENGINENO")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RENGINENO")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container, "DataItem.CHASISNO")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RCHASISNO")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container, "DataItem.autotypedesc")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.Rtype")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container, "DataItem.automakedesc")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.Rmake")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container, "DataItem.STDESC")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RSTDESC")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container, "DataItem.distname")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.Rdistname")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container, "DataItem.psname")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.Rpsname")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container, "DataItem.FIR")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RFIR")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container, "DataItem.FIRDATE")%><br />
                        <%# DataBinder.Eval(Container, "DataItem.RFIRDATE")%>
                    </td>
                    <td>
                        STOLEN<BR />
                        RECOVERED
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <table class="style1">
            <tr>
                <td class="style2">
        <asp:LinkButton ID="FirstPage" runat="server">First Page</asp:LinkButton>
                </td>
                <td class="style3">
        
        <asp:LinkButton ID="Lnkbtnprev" runat="server" Visible="False">Previous Page</asp:LinkButton>
                </td>
                <td class="style4">
        <asp:LinkButton ID="LnkbtnNext" runat="server" Visible="False">Next Page</asp:LinkButton>
                </td>
                <td>
        <asp:LinkButton ID="LastPage" runat="server">Last Page</asp:LinkButton>
                </td>
            </tr>
        </table>
            <br />
        </div>
        
        </div>
    </form>
</body>
</html>
