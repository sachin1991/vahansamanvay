using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class MenuForm_pforms : System.Web.UI.Page
{
    MenuClass mObj = new MenuClass();
    static string _userid;
    ListItemCollection liColl;
    static int _add, _edit, _delete, _view;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
         
        }
        if (!IsPostBack)
        {
            try
            {
            
            }
            catch (Exception)
            {
                Response.Redirect("error.aspx");
            }
            bindRootFormGrid();            
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    private void bindRootFormGrid()
    {
        DataSet ds = new DataSet();
        ds = mObj.getPForms();
        gvForms.DataSource = ds;
        gvForms.DataBind();
        if (ds.Tables[0].Rows.Count == 0)
        {
            DataRow drow = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                drow[i] = DBNull.Value;
            }
            ds.Tables[0].Rows.Add(drow);
            gvForms.DataSource = ds;
            gvForms.DataBind();
            gvForms.Rows[0].Visible = false;
        }
    }
    private void bindChildGrid(int parentId)
    {
        DataSet ds = new DataSet();
        ds = mObj.getPForms(parentId, 1);
        gvChild.DataSource = ds;
        gvChild.DataBind();
        if (ds.Tables[0].Rows.Count == 0)
        {
            DataRow drow = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                drow[i] = DBNull.Value;
            }
            ds.Tables[0].Rows.Add(drow);
            gvChild.DataSource = ds;
            gvChild.DataBind();
            gvChild.Rows[0].Visible = false;
        }
    }
    private void bindSubChildGrid(int parentId)
    {
        DataSet ds = new DataSet();
        ds = mObj.getPForms(parentId, 2);
        gvSubChild.DataSource = ds;
        gvSubChild.DataBind();
        if (ds.Tables[0].Rows.Count == 0)
        {
            DataRow drow = ds.Tables[0].NewRow();
            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
            {
                drow[i] = DBNull.Value;
            }
            ds.Tables[0].Rows.Add(drow);
            gvSubChild.DataSource = ds;
            gvSubChild.DataBind();
            gvSubChild.Rows[0].Visible = false;
        }
    }
    private bool checkInt(string value)
    {
        int blank = 0;
        bool isInt = false;
        isInt = int.TryParse(value, out blank);
        return isInt;
    }

    protected void gvForms_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvForms.EditIndex = -1;
        bindRootFormGrid();
    }
    private Boolean checkillegal(string frmname,string mainid,string frmlink,string sortorder)
    {
        bool illegal;
        illegal = false;
        if (frmname.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (mainid.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (frmlink.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (sortorder.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void gvForms_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Add"))
            {
                ((ImageButton)gvForms.FooterRow.FindControl("lbInsert")).Visible = true;
                ((ImageButton)gvForms.FooterRow.FindControl("lbCancel")).Visible = true;
                ((Button)gvForms.FooterRow.FindControl("lbAdd")).Visible = false;
                ((TextBox)gvForms.FooterRow.FindControl("txtFormName1")).Visible = true;                
                ((TextBox)gvForms.FooterRow.FindControl("txtMainId1")).Visible = true;
                ((TextBox)gvForms.FooterRow.FindControl("txtFormLink1")).Visible = true;
                ((TextBox)gvForms.FooterRow.FindControl("txtSortOrder1")).Visible = true;
                return;
            }
            else
            {
                ((ImageButton)gvForms.FooterRow.FindControl("lbInsert")).Visible = false;
                ((ImageButton)gvForms.FooterRow.FindControl("lbCancel")).Visible = false;
                ((Button)gvForms.FooterRow.FindControl("lbAdd")).Visible = true;
                ((TextBox)gvForms.FooterRow.FindControl("txtFormName1")).Visible = false;                
                ((TextBox)gvForms.FooterRow.FindControl("txtMainId1")).Visible = false;
                ((TextBox)gvForms.FooterRow.FindControl("txtFormLink1")).Visible = false;
                ((TextBox)gvForms.FooterRow.FindControl("txtSortOrder1")).Visible = false;
            }
            if (e.CommandName.Equals("CancelInsert"))
            {
                gvForms.EditIndex = -1;
                bindRootFormGrid();
                return;
            }
            if (e.CommandName.Equals("Insert"))
            {
                int _lblFrmId = mObj.getPFromsMaxId();
                TextBox _txtFrmName = (TextBox)gvForms.FooterRow.FindControl("txtFormName1");                
                TextBox _txtMainId = (TextBox)gvForms.FooterRow.FindControl("txtMainId1");
                TextBox _txtFrmLink = (TextBox)gvForms.FooterRow.FindControl("txtFormLink1");
                TextBox _txtSortOrder = (TextBox)gvForms.FooterRow.FindControl("txtSortOrder1");
                bool illegal = checkillegal(_txtFrmName.Text, _txtMainId.Text, _txtFrmLink.Text, _txtSortOrder.Text);
                if (illegal == false)
                {
                    if (!checkInt(_txtMainId.Text))
                    {
                        lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                        return;
                    }
                    if (!checkInt(_txtSortOrder.Text))
                    {
                        lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                        return;
                    }

                    if (mObj.addPRootForms(_lblFrmId, GenericMethods.check(_txtFrmName.Text.ToString().Trim()), 0, int.Parse(_txtMainId.Text), GenericMethods.check(_txtFrmLink.Text.ToString().Trim()), int.Parse(_txtSortOrder.Text), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values Inserted</font>";
                        bindRootFormGrid();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>values not Inserted</font>";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                }
                return;
            }
            if (checkInt(e.CommandName))
            {
                int _prntFrmId = Convert.ToInt32(((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text);
                lblchldPrntId.Text = _prntFrmId.ToString();
                lblChild.Text = "";
                lblChild.Text += "Parent Form Id: " + ((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text + "&nbsp;&nbsp;&nbsp;&nbsp;Parent Form Name: " + ((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormName")).Text;
                bindChildGrid(_prntFrmId);
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvForms_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Label _lblfrmId = (Label)gvForms.Rows[e.RowIndex].FindControl("lblFormId");
        if (mObj.deletePForms(int.Parse(_lblfrmId.Text), _userid ) > 0)
        {
            lblmsg.Text = "<font color=green>Row Deleted</font>";
            bindRootFormGrid();
        }
        else
        {
            lblmsg.Text = "<font color=red>Row not deleted</font>";
        }
    }
    protected void gvForms_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvForms.EditIndex = e.NewEditIndex;
        bindRootFormGrid();
    }    
    protected void gvForms_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {            
           
            Label _lblFrmId = (Label)gvForms.Rows[e.RowIndex].FindControl("lblFormId");
            TextBox _txtFrmName = (TextBox)gvForms.Rows[e.RowIndex].FindControl("txtFormName");            
            TextBox _txtMainId = (TextBox)gvForms.Rows[e.RowIndex].FindControl("txtMainId");
            TextBox _txtFrmLink = (TextBox)gvForms.Rows[e.RowIndex].FindControl("txtFormLink");
            TextBox _txtSortOrder = (TextBox)gvForms.Rows[e.RowIndex].FindControl("txtSortOrder");
            bool illegal = checkillegal(_txtFrmName.Text, _txtMainId.Text, _txtFrmLink.Text, _txtSortOrder.Text);
            if (illegal == false)
            {
                if (!checkInt(_txtMainId.Text))
                {
                    lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                    return;
                }
                if (!checkInt(_txtSortOrder.Text))
                {
                    lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                    return;
                }

                if (mObj.updatePRootForms(int.Parse(_lblFrmId.Text), GenericMethods.check(_txtFrmName.Text.ToString().Trim()), 0, int.Parse(_txtMainId.Text), GenericMethods.check(_txtFrmLink.Text.ToString().Trim()), int.Parse(_txtSortOrder.Text), _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Updated</font>";
                    gvForms.EditIndex = -1;
                    bindRootFormGrid();
                }
                else
                {
                    lblmsg.Text = "<font color=red>values not updated</font>";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
            }
             
        }
        
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    // Child 1
    
    protected void gvChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChild.EditIndex = -1;
        bindChildGrid(int.Parse(lblchldPrntId.Text));
    }

    private Boolean checkillegalChild(string frmname, string mainid, string frmlink, string sortorder)
    {
        bool illegal;
        illegal = false;
        if (frmname.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (mainid.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (frmlink.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (sortorder.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void gvChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Add"))
            {
                ((ImageButton)gvChild.FooterRow.FindControl("lbInsert")).Visible = true;
                ((ImageButton)gvChild.FooterRow.FindControl("lbCancel")).Visible = true;
                ((Button)gvChild.FooterRow.FindControl("lbAdd")).Visible = false;
                ((TextBox)gvChild.FooterRow.FindControl("txtFormName1")).Visible = true;
                ((TextBox)gvChild.FooterRow.FindControl("txtMainId1")).Visible = true;
                ((TextBox)gvChild.FooterRow.FindControl("txtFormLink1")).Visible = true;
                ((TextBox)gvChild.FooterRow.FindControl("txtSortOrder1")).Visible = true;
                return;
            }
            else
            {
                ((ImageButton)gvChild.FooterRow.FindControl("lbInsert")).Visible = false;
                ((ImageButton)gvChild.FooterRow.FindControl("lbCancel")).Visible = false;
                ((Button)gvChild.FooterRow.FindControl("lbAdd")).Visible = true;
                ((TextBox)gvChild.FooterRow.FindControl("txtFormName1")).Visible = false;
                ((TextBox)gvChild.FooterRow.FindControl("txtMainId1")).Visible = false;
                ((TextBox)gvChild.FooterRow.FindControl("txtFormLink1")).Visible = false;
                ((TextBox)gvChild.FooterRow.FindControl("txtSortOrder1")).Visible = false;
            }
            if (e.CommandName.Equals("CancelInsert"))
            {
                gvChild.EditIndex = -1;
                bindChildGrid(int.Parse(lblchldPrntId.Text));
                return;
            }
            if (e.CommandName.Equals("Insert"))
            {
                int _lblFrmId = mObj.getPFromsMaxId();
                TextBox _txtFrmName = (TextBox)gvChild.FooterRow.FindControl("txtFormName1");
                TextBox _txtMainId = (TextBox)gvChild.FooterRow.FindControl("txtMainId1");
                TextBox _txtFrmLink = (TextBox)gvChild.FooterRow.FindControl("txtFormLink1");
                TextBox _txtSortOrder = (TextBox)gvChild.FooterRow.FindControl("txtSortOrder1");
                bool illegalchild = checkillegalChild(_txtFrmName.Text, _txtMainId.Text, _txtFrmLink.Text, _txtSortOrder.Text);
                if (illegalchild == false)
                {
                    if (!checkInt(_txtMainId.Text))
                    {
                        lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                        return;
                    }
                    if (!checkInt(_txtSortOrder.Text))
                    {
                        lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                        return;
                    }

                    if (mObj.addPForms(_lblFrmId,GenericMethods.check(_txtFrmName.Text.ToString().Trim()), int.Parse(lblchldPrntId.Text), 1, int.Parse(_txtMainId.Text),GenericMethods.check(_txtFrmLink.Text.ToString().Trim()), int.Parse(_txtSortOrder.Text), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values Inserted</font>";
                        bindChildGrid(int.Parse(lblchldPrntId.Text));
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>values not Inserted</font>";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                }
                return;

            }
            if (checkInt(e.CommandName))
            {
                int _prntFrmId = Convert.ToInt32(((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text);
                lblSubchldPrntId.Text = _prntFrmId.ToString();
                lblSubChild.Text = "";
                lblSubChild.Text += "Parent Form Id: " + ((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text + "&nbsp;&nbsp;&nbsp;&nbsp;Parent Form Name: " + ((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormName")).Text;
                bindSubChildGrid(_prntFrmId);
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChild.EditIndex = e.NewEditIndex;
        bindChildGrid(int.Parse(lblchldPrntId.Text));
    }
    protected void gvChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label _lblFrmId = (Label)gvChild.Rows[e.RowIndex].FindControl("lblFormId");
            TextBox _txtFrmName = (TextBox)gvChild.Rows[e.RowIndex].FindControl("txtFormName");
            TextBox _txtMainId = (TextBox)gvChild.Rows[e.RowIndex].FindControl("txtMainId");
            TextBox _txtFrmLink = (TextBox)gvChild.Rows[e.RowIndex].FindControl("txtFormLink");
            TextBox _txtSortOrder = (TextBox)gvChild.Rows[e.RowIndex].FindControl("txtSortOrder");
            bool illegalchild = checkillegalChild(_txtFrmName.Text, _txtMainId.Text, _txtFrmLink.Text, _txtSortOrder.Text);
            if (illegalchild == false)
            {
                if (!checkInt(_txtMainId.Text))
                {
                    lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                    return;
                }
                if (!checkInt(_txtSortOrder.Text))
                {
                    lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                    return;
                }
                if (mObj.updatePForms(int.Parse(_lblFrmId.Text),GenericMethods.check(_txtFrmName.Text.ToString().Trim()), int.Parse(lblchldPrntId.Text), 1, int.Parse(_txtMainId.Text), GenericMethods.check(_txtFrmLink.Text.ToString().Trim()), int.Parse(_txtSortOrder.Text), _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Updated</font>";
                    gvChild.EditIndex = -1;
                    bindChildGrid(int.Parse(lblchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>values not updated</font>";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
            }

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvChild_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[2].Visible = false;
        e.Row.Cells[3].Visible = false;
    }


    // Sub Child
    protected void gvSubChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvSubChild.EditIndex = -1;
        bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
    }
    protected void gvSubChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Add"))
            {
                ((ImageButton)gvSubChild.FooterRow.FindControl("lbInsert")).Visible = true;
                ((ImageButton)gvSubChild.FooterRow.FindControl("lbCancel")).Visible = true;
                ((Button)gvSubChild.FooterRow.FindControl("lbAdd")).Visible = false;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtFormName1")).Visible = true;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtMainId1")).Visible = true;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtFormLink1")).Visible = true;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtSortOrder1")).Visible = true;
                return;
            }
            else
            {
                ((ImageButton)gvSubChild.FooterRow.FindControl("lbInsert")).Visible = false;
                ((ImageButton)gvSubChild.FooterRow.FindControl("lbCancel")).Visible = false;
                ((Button)gvSubChild.FooterRow.FindControl("lbAdd")).Visible = true;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtFormName1")).Visible = false;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtMainId1")).Visible = false;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtFormLink1")).Visible = false;
                ((TextBox)gvSubChild.FooterRow.FindControl("txtSortOrder1")).Visible = false;
            }
            if (e.CommandName.Equals("CancelInsert"))
            {
                gvSubChild.EditIndex = -1;
                bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
                return;
            }
            if (e.CommandName.Equals("Insert"))
            {
                int _lblFrmId = mObj.getPFromsMaxId();
                TextBox _txtFrmName = (TextBox)gvSubChild.FooterRow.FindControl("txtFormName1");
                TextBox _txtMainId = (TextBox)gvSubChild.FooterRow.FindControl("txtMainId1");
                TextBox _txtFrmLink = (TextBox)gvSubChild.FooterRow.FindControl("txtFormLink1");
                TextBox _txtSortOrder = (TextBox)gvSubChild.FooterRow.FindControl("txtSortOrder1");

                if (!checkInt(_txtMainId.Text))
                {
                    lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                    return;
                }
                if (!checkInt(_txtSortOrder.Text))
                {
                    lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                    return;
                }

                if (mObj.addPForms(_lblFrmId, _txtFrmName.Text, int.Parse(lblSubchldPrntId.Text), 2, int.Parse(_txtMainId.Text), _txtFrmLink.Text, int.Parse(_txtSortOrder.Text), _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Inserted</font>";
                    bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>values not Inserted</font>";
                }
                return;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvSubChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvSubChild.EditIndex = e.NewEditIndex;
        bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
    }
    protected void gvSubChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label _lblFrmId = (Label)gvSubChild.Rows[e.RowIndex].FindControl("lblFormId");
            TextBox _txtFrmName = (TextBox)gvSubChild.Rows[e.RowIndex].FindControl("txtFormName");
            TextBox _txtMainId = (TextBox)gvSubChild.Rows[e.RowIndex].FindControl("txtMainId");
            TextBox _txtFrmLink = (TextBox)gvSubChild.Rows[e.RowIndex].FindControl("txtFormLink");
            TextBox _txtSortOrder = (TextBox)gvSubChild.Rows[e.RowIndex].FindControl("txtSortOrder");

            if (!checkInt(_txtMainId.Text))
            {
                lblmsg.Text = "<font color=red>Main Id should be in no.</font>";
                return;
            }
            if (!checkInt(_txtSortOrder.Text))
            {
                lblmsg.Text = "<font color=red>Sort Order should be in no.</font>";
                return;
            }
            if (mObj.updatePForms(int.Parse(_lblFrmId.Text), _txtFrmName.Text, int.Parse(lblSubchldPrntId.Text), 2, int.Parse(_txtMainId.Text), _txtFrmLink.Text, int.Parse(_txtSortOrder.Text), _userid) > 0)
            {
                lblmsg.Text = "<font color=green>Values Updated</font>";
                gvSubChild.EditIndex = -1;
                bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
            }
            else
            {
                lblmsg.Text = "<font color=red>values not updated</font>";
            }

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvSubChild_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[2].Visible = false;
        e.Row.Cells[3].Visible = false;
    }
}
