using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class captcha : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.CacheControl = "no-cache";
        System.Drawing.Bitmap objBmp = new System.Drawing.Bitmap(60, 25);
        System.Drawing.Graphics objGraphics = System.Drawing.Graphics.FromImage(objBmp);
        objGraphics.Clear(System.Drawing.Color.Gray);
        objGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        System.Drawing.Font objFont = new System.Drawing.Font("Blue", 12, System.Drawing.FontStyle.Bold);
        string strRandom = "";
        string[] strArray = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        Random autoRand = new Random();
        int x;
        for (x = 0; x < 5; x++)
        {
            int i = Convert.ToInt32(autoRand.Next(0, 26));
            strRandom += strArray[i].ToString();
        }
        Session.Add("strRandom", strRandom);
        
        objGraphics.DrawString(strRandom, objFont, System.Drawing.Brushes.White, 3, 3);
        Response.ContentType = "image/GIF";
        objBmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg );
        objFont.Dispose();
        objGraphics.Dispose();
        objBmp.Dispose();
    }

}

