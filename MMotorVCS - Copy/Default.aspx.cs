﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using MMotorVCS;
using System.Collections.Specialized;

public partial class _Default : System.Web.UI.Page 
{
    MasterMethods mObj = new MasterMethods();
    Entry_FormMethods eObj = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

    }


    protected void gvForms_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Control ctrl = e.Row.FindControl("ddgAutoType");
            if (ctrl != null)
            {
                DropDownList dd = ctrl as DropDownList;
                DataSet ds = new DataSet();
                ds = mObj.getVehicleType();
                dd.DataSource  = ds;
                dd.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypeid"].ColumnName);
                dd.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
                dd.DataBind();
                dd.Items.Insert(0, new ListItem("--Select--", "0"));

            }
        }
    }

    protected void ddgAutoType_SelectedIndexChanged(object sender, EventArgs e)
{
    DropDownList ddl = sender as DropDownList;
    foreach (GridViewRow row in gvForms.Rows)
    {
        Control ctrl = row.FindControl("ddgAutoType") as DropDownList;
        if (ctrl != null)
        {
            DropDownList ddl1 = (DropDownList)ctrl;
            if (ddl.ClientID == ddl1.ClientID)
            {
                TextBox txt = row.FindControl("txtTest") as TextBox;
                txt.Text = ddl1.SelectedValue;
                break;
            }
        }
    }
}


    protected void Button1_Click(object sender, EventArgs e)
    {
        DataSet dsState = eObj.getRecordEdit_Del(ddAutoType.SelectedValue.ToString().Trim(), ddMake.SelectedValue.ToString().Trim(), txtReg.Text.ToString().Trim(), txtChasis.Text.ToString(), txtEngine.Text.ToString().Trim());

        gvForms.DataSource = dsState;
        gvForms.DataBind();
    }
}