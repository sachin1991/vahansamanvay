using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using System.Security.Principal;
using System.Data.SqlClient;
using MMotorVCS;
public partial class Login : System.Web.UI.Page
{

    private string strLoginCode;
    private string strPasswd;
    private HttpSessionState ss;
    static int randomNumber;
    Class1 myclass = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            IPrincipal User = HttpContext.Current.User;

            string str1 = System.Web.HttpContext.Current.Session.SessionID;

            Session["vcStateCode"] = null;
            Session["inRoleId"] = null;

            txtLoginCode.Focus();
            nousersloggedin.Text = "No of concurrent users = " + Application["noofUserloggedin"].ToString();
            if (!IsPostBack)
            {
                Session.Add("rndNo", 0);
                Random autoRand = new Random();
                randomNumber = Convert.ToInt32(autoRand.Next(1, 35));
                String rno = randomNumber.ToString();
                btnLogin.Attributes.Add("onClick", "return EncryptPassword(" + "'" + rno + "'" + ");");
                MasterMethods mm = new MasterMethods();
                DataSet dtbvisitor = mm.getvisitor();
                txtVisitor.Text = dtbvisitor.Tables[0].Rows[0][0].ToString();
              
            }
        }
        catch (Exception ex)
        {
            Session["Error"] = "3";
            Response.Redirect("Error.aspx");
        }

    }
    
    
    protected void btnLogin_Click(object sender, EventArgs e)
    {

        strLoginCode = SafeSqlLiteral(txtLoginCode.Text.ToString());
        strPasswd = SafeSqlLiteral(txtPassword.Text.ToString().Trim());

        if (txtCap.Text.ToString() == Session["strRandom"].ToString())
        {
            this.ValidateLogin(strLoginCode, strPasswd);
        }
        else
        {
            AlertMessage("Invalid Captcha");
        }
    }

    private void AlertMessage(string strErrMsg)
    {
        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";
        if (!ClientScript.IsClientScriptBlockRegistered("ScpritAlert"))
        {
            ClientScript.RegisterStartupScript(typeof(Login), "ScpritAlert", strScript);
        }
    }
    private void ValidateLogin(string strLoginCode, string strPasswd)
    {
        try
        {
            Boolean blStatus;
            DataTable dtlogindata;
            byte[] strPwd;

            clsPageBase objPage = new clsPageBase();

            //if (clsPageBase.ChkQuestion(txtLoginCode.Text.ToString().Trim()) == 0)
            if (clsPageBase.ChkQuestion(strLoginCode) == 0)
            {
                //Session["vcUserName"] = txtLoginCode.Text.ToString();
                Session["vcUserName"] = strLoginCode;
                string oldp;
                string newp;
                oldp = txtPassword.Text.ToString().ToUpper().ToString().Trim();
                dtlogindata = objPage.ValidateUser(strLoginCode);
                if (dtlogindata.Rows.Count > 0)
                {
                    newp = GenericMethods.check(dtlogindata.Rows[0]["vcPassword"].ToString().Trim());
                    //check for password string
                    if (oldp == newp)
                    {
                        Response.Redirect("frmFirstTimeLogin.aspx");
                    }
                    else
                    {
                        Response.Redirect("Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }

            dtlogindata = objPage.ValidateUser(strLoginCode);

            if (dtlogindata.Rows.Count > 0)
            {
                String k1 = dtlogindata.Rows[0]["vcPassword"].ToString().ToLower().Trim();
                String k2 = txtPassword.Text.Trim();
                if (k1 == k2)
                {
                    objPage.UserName = GenericMethods.check(dtlogindata.Rows[0]["vcUserName"].ToString());
                    objPage.Password = GenericMethods.check(dtlogindata.Rows[0]["vcPassword"].ToString());
                    objPage.StateCode = GenericMethods.check(dtlogindata.Rows[0]["vcStateCode"].ToString());
                    objPage.RoleId = Convert.ToInt32(dtlogindata.Rows[0]["inRoleId"].ToString());
                    objPage.LoginType = GenericMethods.check(dtlogindata.Rows[0]["vcType"].ToString());
                    objPage.StateCode1 = GenericMethods.check(dtlogindata.Rows[0]["vcStateCode1"].ToString());
                    objPage.DistrictCode = GenericMethods.check(dtlogindata.Rows[0]["vcDistrictCode"].ToString());
                    objPage.PSCode = GenericMethods.check(dtlogindata.Rows[0]["vcPSCode"].ToString());
                    //Redirecting user to the respective page.*/
                    Session["vcStateCode"] = objPage.StateCode;
                    Session["vcUserName"] = objPage.UserName;
                    Session["inRoleId"] = objPage.RoleId;
                    Session["vcType"] = objPage.LoginType;
                    Session["vcStateCode1"] = objPage.StateCode1;
                    Session["vcDistrict"] = objPage.DistrictCode;
                    Session["vcPS"] = objPage.PSCode;
                    Session["visitor"] = txtVisitor.Text.ToString();
                    trRrrMsg.Visible = false;
                    Entry_FormMethods eform = new Entry_FormMethods();
                    DataSet ds = new DataSet();
                    ds = eform.AddVisitor(txtVisitor.Text.ToString());
                    HttpCookie myCookie = new HttpCookie("myCookieVahan");
                    Session["rndNo"] = Guid.NewGuid().ToString();
                    myCookie.Value = Session["rndNo"].ToString();
                    myCookie.HttpOnly = true;
                    Response.AppendCookie(myCookie);
                    if (clsPageBase.insertloggedin(txtLoginCode.Text.ToString().Trim()) == 0)
                    {
                    }
                    Response.Write("<script>window.open('Instruction.aspx','_self')</script>");
                }
                else
                {
                    AlertMessage("Invalid Password");
                }
            }
            else
            {
                Session["Error"] = "4";
                Response.Redirect("Error.aspx");
            }

        }
        catch (Exception ex)
        {
            string err = ex.Message.ToString();
            if (err == "Thread was being aborted.")
            {
            }
            else
            {
                Session["Error"] = "3";
                Response.Redirect("Error.aspx");
            }


        }
    }

    public byte[] HashKeyValue(string sessionvalue)
    {


        UTF8Encoding textConvertor = new UTF8Encoding();
        byte[] passBytes = textConvertor.GetBytes(sessionvalue);
        return new SHA384Managed().ComputeHash(passBytes);


    }

    protected void btnForget_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmForgetPassword.aspx");
    }
    private string SafeSqlLiteral(string inputSQL)
    {
        return inputSQL.Replace("'", "''");

    }
}
