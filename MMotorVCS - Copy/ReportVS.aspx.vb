Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS
Imports System.IO

Partial Class ReportVS
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If Me.ListDatefrom.Text > Me.listDateto.Text Then
        '    Msglabel.Text = "Date to is less than date from"
        '    Exit Sub
        'End If
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then
            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""
        preparesql()
        If Me.RadioListofStolen.Checked = True Or Me.RadioListofRecovered.Checked = True Or Me.RadioListofDeleted.Checked = True Then
        Else
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                Response.Write("<script>window.open('ReportVSPerfectMatchview.aspx','_blank')</script>")
                Exit Sub
            End If
        End If
        If Me.Firwise.Checked Then
            Response.Write("<script>window.open('ReportVSview.aspx','_blank')</script>")
        Else
            Response.Write("<script>window.open('Reportqueryfiredpolice.aspx','_blank')</script>")
        End If
    End Sub
    Private Sub preparesql()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = "SELECT autotypedesc AS VEHICLETYPE,automakedesc  AS VEHICLEMAKE, regno AS REGISTRATION,chasisno AS CHASIS,engineno AS ENGINE,model, STDESC AS STATE,distname AS DISTRICT,psname AS PS,FIR AS FIRNO , FIRDATE  , status FROM dbo.vw_Vehicle_Data" '
        Dim whr As String
        Dim ORD As String
        Dim ORDDATE As String
        whr = " where 1=1"
        If Me.RdbPerfectMatch.Checked = True Then
            Session("TYPEOFQUERY") = "LIST OF PERFECT MATCH VEHICLES FROM "
            sql = "Select * from Perfectmatchview"
            Session("reportparameter") = "PerfectMatch"
        End If
        If Me.RdbPartialMatch.Checked = True Then
            Session("TYPEOFQUERY") = "LIST OF PARTIAL MATCH VEHICLES FROM "
            sql = "Select * from PartialMatchview"
            Session("reportparameter") = "PartialMatch"
        End If
        If Me.RdbMatchRegistration.Checked = True Then
            Session("TYPEOFQUERY") = "LIST OF PARTIAL MATCH ON REGISTRATION ONLY FROM "
            sql = "Select * from PartialMatchRegistrationview"

            Session("reportparameter") = "PartialRegistration"
        End If
        If Me.RdbMatchChasis.Checked = True Then
            Session("TYPEOFQUERY") = "LIST OF PARTIAL MATCH VEHICLES CHASIS ONLY FROM "
            sql = "Select * from PartialMatchChasisview"
            Session("reportparameter") = "PartialChasisNo"
        End If
        If Me.RdbMatchEngine.Checked = True Then
            Session("TYPEOFQUERY") = "LIST OF PARTIAL MATCH VEHICLES ENGINE ONLY FROM "
            sql = "Select * from PartialMatchEngineview"
            Session("reportparameter") = "PartialEngineNo"
        End If
        If Me.RadioListofStolen.Checked = True Then
            whr = whr & " and status='L'"
            Session("TYPEOFQUERY") = "LIST OF STOLEN VEHICLES FROM "
        End If

        If Me.RadioListofRecovered.Checked = True Then
            whr = whr & " and status='R'"
            Session("TYPEOFQUERY") = "LIST OF RECOVERED VEHICLES FROM "
        End If
        If Me.RadioListofDeleted.Checked = True Then
            whr = whr & "  and DELETED=1"
            Session("TYPEOFQUERY") = "LIST OF DELETED VEHICLES FROM "
        Else
            whr = whr & " and deleted <>1 "
        End If
        If Me.ddlState.Text <> "ALL STATES" Then
            whr = whr & " AND (STDESC = '" & Trim(ddlState.Text.Trim()) & "'"
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                whr = whr & " OR RSTDESC = '" & Trim(ddlState.Text.Trim()) & "'"
            End If
            whr = whr & ")"
        End If
        If Me.Districtwise.Text <> "ALL DISTRICTS" Then
            whr = whr & " AND (DISTNAME = '" & Trim(Districtwise.Text.Trim()) & "'"
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                whr = whr & " or RDISTNAME = '" & Trim(Districtwise.Text.Trim()) & "'"
            End If
            whr = whr & ")"
        End If
        If Me.PSWise.Text <> "ALL POLICE STATIONS" Then
            whr = whr & " AND (PSNAME = '" & Trim(PSWise.Text.Trim()) & "'"
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                whr = whr & " or RPSNAME = '" & Trim(PSWise.Text.Trim()) & "'"
            End If
            whr = whr & ")"
        End If
        If Me.TypeWise.Text <> "-- Select --" Then
            whr = whr & " AND (AUTOTYPEDESC = '" & Trim(TypeWise.Text.Trim()) & "'"
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                whr = whr & " or RAUTOTYPEDESC = '" & Trim(TypeWise.Text.Trim()) & "'"
            End If
            whr = whr & ")"
        End If
        If Me.MakeWise.Text <> "" And Me.MakeWise.Text <> "-- Select --" Then
            whr = whr & " AND (AUTOMAKEDESC = '" & Trim(MakeWise.Text.Trim()) & "'"
            If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or RdbMatchRegistration.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Then
                whr = whr & " or RAUTOMAKEDESC = '" & Trim(MakeWise.Text.Trim()) & "'"
            End If
            whr = whr & ")"
        End If
        ORD = " ORDER BY "
        If Me.CHKPSWISE.Checked = True Then
            ORD = ORD & "STDESC,DISTNAME,PSNAME,"
        End If
        If Me.Chkmakewise.Checked Then
            ORD = ORD & "autotypedesc,automakedesc,"
        End If

        ORD = ORD & "FIRDATE,FIR"
        Dim mtf As String
        mtf = Month(Me.ListDatefrom.Text.ToString())
        If Len(mtf) = 1 Then
            mtf = "0" & mtf
        End If
        Dim dtf As String
        dtf = Day(Me.ListDatefrom.Text.ToString())
        If Len(dtf) = 1 Then
            dtf = "0" & dtf
        End If
        Dim Mto As String
        Mto = Month(Me.listDateto.Text.ToString())
        If Len(Mto) = 1 Then
            Mto = "0" & Mto
        End If

        Dim dto As String
        dto = Day(Me.listDateto.Text.ToString())
        If Len(dto) = 1 Then
            dto = "0" & dto
        End If
        If Me.Firwise.Checked Then
            If whr <> "" Then
                If Me.RdbPerfectMatch.Checked = True Or RdbPartialMatch.Checked = True Or RdbMatchChasis.Checked = True Or RdbMatchEngine.Checked = True Or RdbMatchRegistration.Checked = True Then
                    If RdbSTOLENdates.Checked Then
                        whr = " " & whr & "  and ((CONVERT(varchar(14),FIRDATE,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "') )"
                        Session("StolenRecoveredDate") = "StolenDate"
                    Else
                        whr = " " & whr & "  and ((CONVERT(varchar(14),RFIRDATE,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'))"
                        Session("StolenRecoveredDate") = "RecoveredDate"
                    End If
                Else
                    whr = "  " & whr & "  and ((CONVERT(varchar(14),FIRDATE,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "') )"
                End If
            Else
                whr = " " & whr & " and CONVERT(varchar(14),FIRDATE,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "' "
        End If
        Else
            sql = "Select *,0 as Netamount from PSEntryView "
            whr = " Where 1 = 1  "
            If Me.ddlState.Text <> "ALL STATES" Then
                whr = whr & " AND (STATENAME = '" & Trim(ddlState.Text.Trim()) & "'"
                whr = whr & ")"
            End If
            If Me.Districtwise.Text <> "ALL DISTRICTS" Then
                whr = whr & " AND (DISTRICTNAME = '" & Trim(Districtwise.Text.Trim()) & "'"
                whr = whr & ")"
            End If
            If Me.PSWise.Text <> "ALL POLICE STATIONS" Then
                whr = whr & " AND (PSNAME = '" & Trim(PSWise.Text.Trim()) & "'"
                whr = whr & ")"
            End If
            If Me.TypeWise.Text <> "-- Select --" Then
                whr = whr & " AND (VEHICLETYPENAME = '" & Trim(TypeWise.Text.Trim()) & "'"
                whr = whr & ")"
            End If
            If Me.MakeWise.Text <> "" And Me.MakeWise.Text <> "-- Select --" Then
                whr = whr & " AND (MAKENAME = '" & Trim(MakeWise.Text.Trim()) & "'"
                whr = whr & ")"
            End If

            If Session("inRoleid") = 3 Then
                whr = whr & " and vcusername = '" & Session("vcUsername") & "'"
            Else
                whr = whr
            End If
            ORD = "order by dtCreated_Date"
            If whr <> "" Then
                whr = "  " & whr & "  and CONVERT(varchar(14),dtCreated_Date,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "' "
            Else
                whr = whr & " CONVERT(varchar(14),dtCreated_Date,111) between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "' "
            End If
        End If
        sql = sql & whr & ORD
        Session("SQLSTRINGSUM") = sql
        Session("SQLSTRING") = sql
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("ListorCount") = "List"
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim cmdState, cmdMake, cmdInvStatus, cmdExp As New SqlCommand

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If

        If Not IsPostBack Then
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
        End If
        Dim dr As SqlDataReader
        Dim cn As New SqlConnection
        If Not Me.IsPostBack Then

            Dim ConStr As ConnectionStringSettings
            ConStr = ConfigurationManager.ConnectionStrings("Conn")
            cn.ConnectionString = MMotorVCS.DTask.BuildConnectionString()

            cn.Open()

            If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Trim(Session("vcStatecode1")) <> "23" Then
                Dim cmdstate1 As New SqlCommand
                cmdstate1.CommandText = "Select Distinct statename From vw_ps Where statecode ='" & Session("vcStateCode1") & "'"
                cmdstate1.Connection = cn
                dr = cmdstate1.ExecuteReader
                If dr.HasRows Then
                    Do While dr.Read
                        Me.ddlState.Items.Add(dr("Statename"))
                    Loop
                End If
                ddlStateSelected()
                dr.Close()
                Me.ddlState.Enabled = False
                If Session("vcDistrict") <> "" And Session("vcDistrict") <> "0" Then
                    Dim cmdDistrict1 As New SqlCommand
                    cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
                    cmdDistrict1.Connection = cn
                    dr = cmdDistrict1.ExecuteReader
                    If dr.HasRows Then
                        Do While dr.Read
                            Me.Districtwise.Items.Add(dr("districtname"))
                        Loop
                    End If
                    dr.Close()
                    DistrictwiseSelected()
                    Me.Districtwise.Enabled = False
                End If
                If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
                    Dim cmdps1 As New SqlCommand
                    cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
                    cmdps1.Connection = cn
                    dr = cmdps1.ExecuteReader
                    If dr.HasRows Then
                        Do While dr.Read
                            PSWise.Items.Add(dr("psname"))
                        Loop
                    End If
                    dr.Close()
                    Me.PSWise.Enabled = False
                End If
            End If
            If (Convert.ToInt32(Session("inRoleId")) = 3) Then
                cmdState.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=1 and code ='" + Session("vcStateCode") + "'  Order By Description" ' where cReportId like ('%" & repId & "')"
            Else
                If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Trim(Session("vcStatecode1")) <> "23" Then
                    cmdState.CommandText = "Select Distinct statename From vw_ps Where statecode ='" & Session("vcStateCode1") & "'"
                Else
                    cmdState.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=1  Order By Description" ' where cReportId like ('%" & repId & "')"
                End If
            End If
            cmdState.Connection = cn
            dr = cmdState.ExecuteReader
            If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Session("vcStateCode1") <> "0" And Trim(Session("vcStatecode1")) <> "23" Then
                If dr.HasRows Then
                    Do While dr.Read
                        ddlState.Items.Add(dr("statename"))
                    Loop
                End If
            Else
                ddlState.Items.Add("ALL STATES")
                If dr.HasRows Then
                    Do While dr.Read
                        ddlState.Items.Add(dr("Description"))
                    Loop
                End If
            End If
            Dim cmdType As New SqlCommand
            Dim drType As SqlDataReader
            Dim cnType As New SqlConnection
            cnType.ConnectionString = MMotorVCS.DTask.BuildConnectionString()
            cnType.Open()
            cmdType.CommandText = "select distinct  vehicletypecode+ '-' +  convert(varchar, vehicletypeid) as vehidvehcode,vehicletypename   from vehicle_type "
            cmdType.Connection = cnType
            drType = cmdType.ExecuteReader

            TypeWise.Items.Add("-- Select --")
            If drType.HasRows Then
                Do While drType.Read
                    TypeWise.Items.Add(drType(1).ToString())
                Loop
            End If
            If Session("vcdistrict") = "" Or Session("vcdistrict") = "0" Then
                Districtwise.Items.Add("ALL DISTRICTS")
            End If
            If Session("vcPS") = "" Or Session("vcPS") = "0" Then
                PSWise.Items.Add("ALL POLICE STATIONS")
            End If
            drType.Close() 'added new
            dr.Close()
            cn.Close()
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "7" 'district
                Exit Select
            Case "8" 'ncrb
                Exit Select
            Case "12" 'ncrbexecutive
                Exit Select
            Case "3" 'police station
                Exit Select
            Case "6" 'state
                Exit Select
            Case "11" 'CR_Executive1
                Exit Select
            Case "22" 'Intern
                Exit Select
            Case "9" 'insurance
                matchbtn.Visible = False
                'Me.EntryDatewise.Checked = True
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect(Request.RawUrl)
    End Sub

    Protected Sub TypeWise_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeWise.SelectedIndexChanged

        Dim cmdMake As New SqlCommand
        Dim cnMake As New SqlConnection
        Dim dr As SqlDataReader

        cnMake.ConnectionString = MMotorVCS.DTask.BuildConnectionString()
        cnMake.Open()
        cmdMake.CommandText = "select distinct  makecode+ '-' +  convert(varchar, makecode) as makeidmakecode,makename from make_of_vehicle where vehicletypeid= (Select vehicletypeid from vehicle_type Where VehicleTypeName = '" & TypeWise.Text.Trim() & "') order by makename"
        cmdMake.Connection = cnMake
        dr = cmdMake.ExecuteReader
        MakeWise.Items.Clear()
        MakeWise.Items.Add("-- Select --")
        If dr.HasRows Then
            Do While dr.Read
                MakeWise.Items.Add(dr("makename"))
            Loop
        End If
        dr.Close()
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
        Dim cmdDistrict As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()
        cmdDistrict.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=2 and ParentKey= (Select Code from TblMst_Location Where Description like '" & ddlState.Text & "' and LocationType=1) Order By Description"
        cmdDistrict.Connection = cn
        dr = cmdDistrict.ExecuteReader
        Districtwise.Items.Clear()
        Districtwise.Items.Add("ALL DISTRICTS")
        If dr.HasRows Then
            Do While dr.Read
                Districtwise.Items.Add(dr("Description"))
            Loop
        End If
        dr.Close()

        If Session("vcDistrict") <> "" Then
            Dim cmdDistrict1 As New SqlCommand
            cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
            cmdDistrict1.Connection = cn
            dr = cmdDistrict1.ExecuteReader
            Districtwise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    Me.Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
            DistrictwiseSelected()
            Me.Districtwise.Enabled = False
        End If
        cn.Close()

    End Sub
    Protected Sub ddlStateSelected()
        Dim cmdDistrict As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()
        If Session("vcDistrict") = "" Then
            cmdDistrict.CommandText = "Select distinct districtname From vw_ps Where statename like '" & ddlState.Text & "' Order By districtname"
            cmdDistrict.Connection = cn
            dr = cmdDistrict.ExecuteReader
            Districtwise.Items.Clear()
            Districtwise.Items.Add("ALL DISTRICTS")
            If dr.HasRows Then
                Do While dr.Read
                    Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
        End If

        If Session("vcDistrict") <> "" Then
            Dim cmdDistrict1 As New SqlCommand
            cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
            cmdDistrict1.Connection = cn
            dr = cmdDistrict1.ExecuteReader
            Me.Districtwise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    Me.Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
            Me.Districtwise.Enabled = False
        End If
        cn.Close()

    End Sub


    Protected Sub Districtwise_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Districtwise.SelectedIndexChanged
        Dim cmdPS As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim StateCode, DistrictCode As String
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        cmdPS.Connection = cn

        If Me.Districtwise.Text <> "ALL DISTRICTS" Then
            cmdPS.CommandText = "Select Distinct psname From vw_ps Where statename like '" & ddlState.Text & "%' and districtname like '" & Districtwise.Text & "%'   Order By psname"
            dr = cmdPS.ExecuteReader
            PSWise.Items.Clear()
            PSWise.Items.Add("ALL POLICE STATIONS")
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
                dr.Close()

            End If
        End If
        If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
            Dim cmdps1 As New SqlCommand
            cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
            cmdps1.Connection = cn
            dr = cmdps1.ExecuteReader
            PSWise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
            End If
            dr.Close()
            Me.PSWise.Enabled = True
        End If
        cn.Close()
    End Sub
    Protected Sub DistrictwiseSelected()
        Dim cmdPS As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim StateCode, DistrictCode As String
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = ConStr.ConnectionString
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        If Session("vcPS") = "" Or Session("vcPS") = "0" Then
            cmdPS.Connection = cn
            If Me.Districtwise.Text <> "ALL DISTRICTS" Then
                cmdPS.CommandText = "Select psname From vw_ps Where  statename like '" & ddlState.Text & "' and districtname like '" & Me.Districtwise.Text & "%'   Order By statename"
                dr = cmdPS.ExecuteReader
                PSWise.Items.Clear()
                PSWise.Items.Add("ALL POLICE STATIONS")
                If dr.HasRows Then
                    Do While dr.Read
                        PSWise.Items.Add(dr("psname"))
                    Loop
                    dr.Close()
                End If
            End If
        End If
        If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
            Dim cmdps1 As New SqlCommand
            cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode like '" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
            cmdps1.Connection = cn
            dr = cmdps1.ExecuteReader
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
            End If
            dr.Close()
            Me.PSWise.Enabled = True
        End If
        cn.Close()

    End Sub

    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        If Me.RdbPerfectMatch.Checked = True Or Me.RdbPartialMatch.Checked = True Or Me.RdbMatchRegistration.Checked = True Or Me.RdbMatchChasis.Checked = True Or Me.RdbMatchEngine.Checked = True Then
            converttoexclmatchcases()
            Exit Sub
        End If
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub

    Protected Sub converttoexclmatchcases()
        preparesql()
         Dim ds As New DataSet()
        ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings("Conn").ToString(), "sp_MatchCount", Session("Datefrom"), Session("Dateto"), Session("reportparameter"), Session("ListorCount"), Session("StolenRecoveredDate").ToString())
        Dim dt As New DataTable()
        dt = ds.Tables(0)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub
    
    Protected Sub EntryDatewise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EntryDatewise.CheckedChanged
        If Me.EntryDatewise.Checked = True Then
            Me.RdbMatchChasis.Visible = False
            Me.RdbMatchEngine.Visible = False
            Me.RdbMatchRegistration.Visible = False
            Me.RdbPartialMatch.Visible = False
            Me.RdbPerfectMatch.Visible = False
            Me.RadioListofStolen.Checked = True
            Me.RdbSTOLENdates.Visible = False
            Me.RdbRecovereddates.Visible = False
        Else
            Me.RdbMatchChasis.Visible = True
            Me.RdbMatchEngine.Visible = True
            Me.RdbMatchRegistration.Visible = True
            Me.RdbPartialMatch.Visible = True
            Me.RdbPerfectMatch.Visible = True
            Me.RadioListofStolen.Checked = True
            Me.RdbSTOLENdates.Visible = True
            Me.RdbRecovereddates.Visible = True
        End If
    End Sub

    Protected Sub Firwise_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Firwise.CheckedChanged
        If Me.Firwise.Checked = True Then
            Me.RdbMatchChasis.Visible = True
            Me.RdbMatchEngine.Visible = True
            Me.RdbMatchRegistration.Visible = True
            Me.RdbPartialMatch.Visible = True
            Me.RdbPerfectMatch.Visible = True
            Me.RadioListofStolen.Checked = True
            Me.RdbSTOLENdates.Visible = True
            Me.RdbRecovereddates.Visible = True
        Else
            Me.RdbMatchChasis.Visible = False
            Me.RdbMatchEngine.Visible = False
            Me.RdbMatchRegistration.Visible = False
            Me.RdbPartialMatch.Visible = False
            Me.RdbPerfectMatch.Visible = False
            Me.RadioListofStolen.Checked = True
            Me.RdbSTOLENdates.Visible = False
            Me.RdbRecovereddates.Visible = False
        End If
    End Sub
End Class
