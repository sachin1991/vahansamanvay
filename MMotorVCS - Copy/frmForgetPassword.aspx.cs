﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

public partial class frmForgetPassword : System.Web.UI.Page
{
    Class1 myclass = new Class1();
    static int randomNumber;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["vcUserName"] == null)
        {
            
        }
        else
        {

            txtLoginCode.Text = Session["vcUserName"].ToString();
            txtLoginCode.Enabled = false;
            
        }

        if (!IsPostBack)
        {
            Random autoRand = new Random();
            randomNumber = Convert.ToInt32(autoRand.Next(0, 36));
            String rno = randomNumber.ToString();

            btnLogin.Attributes.Add("onClick", "return EncryptforgetPassword(" + "'" + rno + "'" + ");");    
            bindQuestion();
        }
    }

    public void bindQuestion()
    {
        try
        {

            DataTable dTblQues = clsIncident.GetQues();
            ddQues1.DataSource = dTblQues;
            ddQues1.DataValueField = GenericMethods.check(dTblQues.Columns[0].ColumnName);
            ddQues1.DataTextField = GenericMethods.check(dTblQues.Columns[1].ColumnName);
            ddQues1.DataBind();

            ddQues2.DataSource = dTblQues;
            ddQues2.DataValueField = GenericMethods.check(dTblQues.Columns[0].ColumnName);
            ddQues2.DataTextField = GenericMethods.check(dTblQues.Columns[1].ColumnName);
            ddQues2.DataBind();

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        
        try
        {
            int flag = 0;
            if (txtPassword.Text.Length >= 12)
            {
                if (txtLoginCode.Text != "" && txtConPass.Text != "" && txtPassword.Text != "" && ddQues1.SelectedIndex != 0 && ddQues2.SelectedIndex != 0 && txtAns1.Text != "" && txtAns2.Text != "")
                {

                    if (txtPassword.Text == txtConPass.Text)
                    {

                        string pp = myclass.md5(txtPassword.Text.ToString().Trim());


                        if (clsPageBase.InsertQuestion(txtLoginCode.Text.ToString().Trim(), ddQues1.SelectedValue.ToString().Trim(), ddQues2.SelectedValue.ToString().Trim(), txtAns1.Text.ToString().Trim(), txtAns2.Text.ToString().Trim(), pp.Trim()) > 0)
                        {
                            lblmsg.Text = "<font color=green>Updated Successfully !!!</font>";
                            Session["vcUserName"] = txtLoginCode.Text.ToString().Trim();
                            Response.Redirect("Login.aspx");

                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Questions & Ans might be wrong.Contact Administrator</font>";
                        }
                    }

                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                    }
                }

                else
                {
                    lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
                }
            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
        }
        

    }

    
    protected void refresh()
    {
        txtPassword.Text = "";
        //txtLoginCode.Text = "";
        txtConPass.Text = "";
        ddQues1.SelectedIndex = 0;
        ddQues2.SelectedIndex = 0;
        txtAns1.Text = "";
        txtAns2.Text = "";
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnLogin_Click1(object sender, EventArgs e)
    {
        btnLogin_Click(sender, e);
    }
}