﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  SmartNavigation="true"  AutoEventWireup="true" CodeFile="CounterEnquiry.aspx.cs" Inherits="CounterEnquiry" Title="Vahan Samanvay Counter Enquiry"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers >
    <asp:PostBackTrigger ControlID="btnSubmit" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
    </Triggers>
<ContentTemplate>
   <script language="javascript" type="text/javascript">
       function GetStateCode() {
           var Statearray = document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').value.split("-");
           document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value = Statearray[0];
           document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value = "";
           document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = "";
       }
    </script>
      <script language="javascript" type="text/javascript">

          function GetMakeCode() {
              var MakeArray = document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').value.split("-");
              document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value = MakeArray[0];
          }
        
    </script>
    <script language="javascript" type="text/javascript">

        function SelectStateddl() {
            document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value = "";
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = "";

            var StateCode = document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value;
            var Stateddl = document.getElementById('ctl00_ContentPlaceHolder1_ddStateName');
            var inc = 0;
            for (var i = 0; i < Stateddl.length; i++) {
                var Statearray = Stateddl.options[i].value.split("-");
                try {
                    if (trim(Statearray[0]) == trim(StateCode)) {
                        inc = 1;
                        document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').options[i].selected = true
                        break;
                    }
                }
                catch (e)
            { }
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value != '' && inc != 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').options[0].selected = true
                document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value = '';
                alert('This State Code is not Exist');
                return false;
            }

        }
    </script>
    
    <script language="javascript" type="text/javascript">

        function GetDistrictCode() {
            var Districtarray = document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value = Districtarray[0];
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = "";
        }
      
    </script>  
    <script language="javascript" type="text/javascript">
        function SelectDistrictddl() {
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = "";
            var DistrictCode = document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value;
            var Districtddl = document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName');
            var inc = 0;
            for (var i = 0; i < Districtddl.length; i++) {
                var Districtarray = Districtddl.options[i].value.split("-");
                try {
                    if (trim(Districtarray[0]) == trim(DistrictCode)) {
                        document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').options[i].selected = true
                        inc = 1;
                        break;
                    }
                }
                catch (e)
            { }
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value != '' && inc != 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').options[0].selected = true
                document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value = '';
                alert('This District Code is not Exist');
                return false;
            }

        }
    </script>
    
    <script language="javascript" type="text/javascript">

        function GetPSCode() {
            var mytool_array = document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = mytool_array[0];
        }
        
    </script>
    <script language="javascript" type="text/javascript">
        function SelectPSddl() {
            var PsCode = document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value;
            var Psddl = document.getElementById('ctl00_ContentPlaceHolder1_ddPsName');
            var inc = 0;
            for (var i = 0; i < Psddl.length; i++) {
                var mytool_array = Psddl.options[i].value.split("-");
                try {
                    if (trim(mytool_array[0]) == trim(PsCode)) {
                        inc = 1;
                        document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').options[i].selected = true
                        break;
                    }
                }
                catch (e)
            { }
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value != '' && inc != 1) {
                document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').options[0].selected = true
                document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value = '';
                alert('This Polic Station Code is not Exist');
                return false;
            }
        }
    </script>

<script language="javascript" type="text/javascript">


    function valid() {

        if (document.getElementById('ctl00_ContentPlaceHolder1_txtAppName').value == "") {
            alert("Enter Applicant Name");
            document.getElementById('ctl00_ContentPlaceHolder1_txtAppName').focus();
            return false;
        }

        
          
       
        if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtresino').value)) {
            alert("Invalid Residence Number !!");
            document.getElementById('ctl00_ContentPlaceHolder1_txtresino').value = "";
            document.getElementById('ctl00_ContentPlaceHolder1_txtresino').focus();
            return false;
        }
      



        //    
        if (document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value == "" && document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value == "" && document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value == "") {
            alert("Enter either the Registration no., Chassis No., Engine No.");
            document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').focus();
            return false;
        }
        if (document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value != "") {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value.length < 5) {
                alert("Registration No. must be alteast 5 digit.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').focus();
                return false;
            }
        }
       
        if (document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value != "") {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value.length < 5) {
                alert("Chassis No. must be alteast 5 digit.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').focus();
                return false;
            }
        }
      
        if (document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value != "") {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value.length < 5) {
                alert("Engine No. must be alteast 5 digit.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').focus();
                return false;
            }
        }

        if (document.getElementById('ctl00_ContentPlaceHolder1_RbtnRec').checked == true) {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value != "") {
                if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value)) {
                    alert("Invalid FIR Number !!");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value = "";
                    document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').focus();
                    return false;
                }
            }
            else {
                alert("Enter the FIR Number !!");
                document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value = "";
                document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').focus();
                return false;
            }

            if (document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').value == "") {
                alert("Enter FIR Date");
                document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').focus();
                return false;
            }

            if (document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').selectedIndex == 0) {
                alert("Select Vehicle Type");
                document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').selectedIndex == 0) {

                if (document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').value == null || document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').value == "-- Select --") 
                {

                    alert("Select the state");
                    document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').focus();
                    return false;
                }
              
            }

            if (document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').value ==0) {
                alert("Select the District");
                document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').selectedIndex == 0) {
                alert("Select the PS");
                document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').focus();
                return false;
            }


        }

        try {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').value == "") {
                alert("Enter Cash Amount.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').focus();
                return false;
            }
            if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').value)) {
                alert("Amount in Digits only !");
                return false;
            }
        }
        catch (e) {
        }
        try {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtIopNo').value == "") {
                alert("Enter IPO Number");
                document.getElementById('ctl00_ContentPlaceHolder1_txtIopNo').focus();
                return false;
            }
        }
        catch (e) {
        }
        try {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').value == "") {
                alert("Enter IPO Amount.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').focus();
                return false;
            }
            if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').value)) {
                alert("Amount in Digits only !");
                return false;
            }
        }
        catch (e) {
        }
        return true;
    }    
    </script>
    
    <script language="javascript" type="text/javascript">
        function validation() {
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtAppName').value == "") {
                alert("Enter Applicant Name");
                document.getElementById('ctl00_ContentPlaceHolder1_txtAppName').focus();
                return false;
            }
            //    
            
                if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtmobno').value)) {
                    alert("Invalid Mobile Number !!");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtmobno').value = "";
                    document.getElementById('ctl00_ContentPlaceHolder1_txtmobno').focus();
                    return false;
                }
            
            //    
            if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtresino').value)) {
                alert("Invalid Residence Number !!");
                document.getElementById('ctl00_ContentPlaceHolder1_txtresino').value = "";
                document.getElementById('ctl00_ContentPlaceHolder1_txtresino').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtAppadd').value == "") {
                alert("Enter Applicant Address");
                document.getElementById('ctl00_ContentPlaceHolder1_txtAppadd').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value != "") {
                if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value)) {
                    alert("Invalid FIR Number !!");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value = "";
                    document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').focus();
                    return false;
                }
            }

            if (document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').value == "") {
                alert("Enter FIR Date");
                document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').selectedIndex == 0) {
                alert("Select Vehicle Type");
                document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').focus();
                return false;
            }
            if (document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value == "" && document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value == "" && document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value == "")
             {
                alert("Enter either the Registration no., Chassis No., Engine No.");
                document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').focus();
                return false;
            }
          
            try {
                if (document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').value == "") {
                    alert("Enter Cash Amount.");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').focus();
                    return false;
                }
                if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtcashamt').value)) {
                    alert("Amount in Digits only !");
                    return false;
                }
            }
            catch (e) {
            }
            try {
                if (document.getElementById('ctl00_ContentPlaceHolder1_txtIopNo').value == "") {
                    alert("Enter IPO Number");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtIopNo').focus();
                    return false;
                }
            }
            catch (e) {
            }
            try {
                if (document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').value == "") {
                    alert("Enter IPO Amount.");
                    document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').focus();
                    return false;
                }
                if (isNaN(document.getElementById('ctl00_ContentPlaceHolder1_txtIPOamt').value)) {
                    alert("Amount in Digits only !");
                    return false;
                }
            }
            catch (e) {
            }
            var val;
            if (document.getElementById('ctl00_ContentPlaceHolder1_RbCash').checked == true) {
                InvokePopRecoveryReportCash("<%=txtAppName.ClientID%>", "<%=txtAppadd.ClientID%>", "<%=txtmobno.ClientID%>", "<%=txtFIRNo.ClientID%>", "<%=txtFIRDt.ClientID%>", "<%=ddVehTypeName.ClientID%>", "<%=ddMakeName.ClientID%>", "<%=txtRegistrationno.ClientID%>", "<%=txtChasisno.ClientID%>", "<%=txtEngineno.ClientID%>", "<%=ddYearofManu.ClientID%>", "<%=hfrecno.ClientID%>", "<%=txtcashamt.ClientID%>");
            }
            else {
                //val="2";
                InvokePopRecoveryReportIPO("<%=txtAppName.ClientID%>", "<%=txtAppadd.ClientID%>", "<%=txtmobno.ClientID%>", "<%=txtFIRNo.ClientID%>", "<%=txtFIRDt.ClientID%>", "<%=ddVehTypeName.ClientID%>", "<%=ddMakeName.ClientID%>", "<%=txtRegistrationno.ClientID%>", "<%=txtChasisno.ClientID%>", "<%=txtEngineno.ClientID%>", "<%=ddYearofManu.ClientID%>", "<%=hfrecno.ClientID%>", "<%=txtIopNo.ClientID%>", "<%=txtIPOamt.ClientID%>");
            }

            return true;
        }
    </script>
    <asp:Panel ID="pnlGeneralDetail" runat="server">
    <contenttemplate>
<table class="contentmain">
    <tr>
    <td> 
        <table width="80%" align="center" class="tablerowcolor">
            <tr class="heading">
                    <td colspan="5">  Counter Enquiry 
                    </td>
                    </tr>
                    <tr> <td colspan="5"></td></tr>
                    <tr>
                        <td colspan="3">
                        <asp:RadioButton id="RbtnRec" runat="server" ForeColor="Black" AutoPostBack="True" Text="Verify recovery status of stolen vehicle" GroupName="enq" OnCheckedChanged="RbtnRec_CheckedChanged"  Checked="True"></asp:RadioButton>
                        <asp:RadioButton id="RbtnInvolve" runat="server" ForeColor="Black" AutoPostBack="True" Text="Verify status of 2nd hand vehicle (Stolen or Not)" GroupName="enq" OnCheckedChanged="RbtnInvolve_CheckedChanged"></asp:RadioButton>
                        <asp:Label id="lblid" runat="server" Visible="False"></asp:Label>
                        </td>
                        <td></td>
                        <td>Cash Receipt No. <asp:Label id="lblreceiptno" runat="server" Width="50px" ForeColor="#000000"></asp:Label>
                        </td>
                    </tr>
             <tr><td colspan="5">
                    <asp:Panel id="pnlAppDetails" runat="server" >
                    <table>
                            <tr>
                            <td colspan="2"> </td>
                            </tr>
                            <tr>
                            <td>Type Of Enquiry</td>
                            <td></td>
                            </tr>
                            <tr>
                                <td style="color: #800000"><strong>Applicant Name</strong></td> 
                                <td> <asp:TextBox id="txtAppName" runat="server" MaxLength="40" Width="148px" 
                                        TabIndex="1" ValidationGroup="ctr"></asp:TextBox></td>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtAppName"></cc1:FilteredTextBoxExtender>
                             </tr>
                             <tr>
                             <td> Contact No.</td>
                             <td> <asp:TextBox id="txtmobno" runat="server" MaxLength="11" Width="148px" TabIndex="2"></asp:TextBox></td>
                             </tr>
                            <tr>
                                <td>Residence Phone No</td>
                                <td> <asp:TextBox id="txtresino" runat="server" MaxLength="12" Width="148px" TabIndex="3"></asp:TextBox></td>
                             </tr>
                            <tr>
                            <td> Applicant Address</td>
                            <td> <asp:TextBox ID="txtAppadd" runat="server" Height="53px" MaxLength="49" 
                                    TextMode="MultiLine" Width="287px" TabIndex="4"></asp:TextBox> </td>
                            </tr>
                          </table>
                    </asp:Panel>
                 </td>
              </tr>
                  
          
             <tr>
                 <td colspan="5">
                     <asp:Panel ID="pnlFIR" runat="server" Visible="True">
                         <table>
                             <tr>
                                 <td style="color: #800000">
                                     <b>State Name</b></td>
                                 <td style="width: 130px">
                                <asp:TextBox ID="txtStateCode" runat="server" AutoPostBack="true" 
                                         Font-Bold="True" MaxLength="8" OnTextChanged="ddStateName_SelectedIndexChanged" 
                                          Width="51px" visible="false" ></asp:TextBox>
<%--                                 </td>
                                 <td>--%>
                                     <asp:DropDownList ID="ddStateName" runat="server" AutoPostBack="true" 
                                         CssClass="dropdownlistCss" 
                                         OnSelectedIndexChanged="ddStateName_SelectedIndexChanged"  
                                         Width="300px" TabIndex="5" >
                                     </asp:DropDownList>
                                 </td>
                                 <td>
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                             <tr>
                                 <td style="color: #800000">
                                     <b>District Name</b></td>
                                 <td style="width: 130px">
                                     <asp:TextBox ID="txtDistrictCode" runat="server" AutoPostBack="true" 
                                         Font-Bold="True" MaxLength="8" OnTextChanged="txtDistrictCode_TextChanged" 
                                         Width="51px"  visible="false" Enabled="False"></asp:TextBox>
<%--                                 </td>
                                 <td>--%>
                                     <asp:DropDownList ID="ddDistrictName" runat="server" AutoPostBack="True" 
                                         OnSelectedIndexChanged="ddDistrictName_SelectedIndexChanged" 
                                         Width="300px">
                                     </asp:DropDownList>
                                 </td>
                                 <td>
                                 </td>
                                 <td>
                                 </td>
                             </tr>
                             <tr>
                                 <td style="color: #800000">
                                     <b>Police Station Name</b></td>
                                 <td style="width: 130px">
                                     <asp:TextBox ID="txtPsCode" runat="server" Font-Bold="True" MaxLength="10" 
                                          Width="51px" AutoPostBack="True"  visible="false"></asp:TextBox>
<%--                                 </td>
                                 <td>--%>
                                     <asp:DropDownList ID="ddPsName" runat="server" Width="300px" TabIndex="7">
                                     </asp:DropDownList>
                                 </td>
                                 <td>
                                 </td>
                                 <td>
                                     <asp:TextBox ID="datetoday" runat="server" Visible="False"></asp:TextBox>
                                 </td>
                             </tr>
                             <tr>
                                 <td style="color: #800000">
                                     <strong>FIR No.</strong></td>
                                 <td style="width: 130px">
                                     <asp:TextBox ID="txtFIRNo" runat="server" MaxLength="5" Width="148px" TabIndex="8"></asp:TextBox>
                                     <cc1:TextBoxWatermarkExtender ID="tbwm1" runat = "server" TargetControlID="txtFIRno" WatermarkText="Enter FIR No"></cc1:TextBoxWatermarkExtender>
                                     <cc1:filteredtextboxextender ID="Filteredtextboxextender1" runat="server" 
                                  FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                                  TargetControlID="txtFIRNo"></cc1:filteredtextboxextender>
                                 </td>
                                 <td>

                                 </td>
                                 <td style="color: #800000; font-weight: 700">
                                     FIR Date</td>
                                 <td>
                                     <asp:TextBox ID="txtFIRDt" runat="server" CssClass="date" Width="148px" 
                                         TabIndex="9" ValidationGroup="ctr" ontextchanged="txtFIRDt_TextChanged"></asp:TextBox>
                                     <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat = "server" TargetControlID="txtFIRdt" WatermarkText="Enter FIR Date"></cc1:TextBoxWatermarkExtender>
                                     <asp:Button ID="btnFIRDt" runat="server" CssClass="button" Text="..." 
                                         Visible="false" Width="26px" onclick="btnFIRDt_Click" />
                                     (dd/mm/yyyy)<br />
                                     <cc1:CalendarExtender 
                    ID="firdtcalendar" 
                    runat="server" 
                    TargetControlID="txtFIRdt" 
                    Format="dd/MM/yyyy" 
                     PopupPosition="Right"  />
                                     </td>
                                 <tr>
                                     <td colspan="5">
                                     </td>
                                 </tr>
                             </tr>
                         </table>
                     </asp:Panel>
                 </td>
            </tr>
            <tr>
                <td style="color: #800000; font-weight: 700">
                    Vehicle Type</td>
                <td>
                    <asp:TextBox ID="txtVehTypeCode" runat="server" AutoPostBack="True" 
                        OnTextChanged="txtVehTypeCode_TextChanged" Width="51px" Visible="False"></asp:TextBox>
<%--                </td>
                <td>--%>
                    <asp:DropDownList ID="ddVehTypeName" runat="server" AutoPostBack="True" 
                        OnSelectedIndexChanged="ddVehTypeName_SelectedIndexChanged" Width="300px" 
                        TabIndex="10">
                    </asp:DropDownList>
                </td>
                <td style="color: #800000">
                    <b>Registration No.</b></td>
                <td>
                    <asp:TextBox ID="txtRegistrationno" runat="server" CssClass="textareacss" 
                        MaxLength="25" Width="148px" TabIndex="12"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat = "server" TargetControlID="txtRegistrationno" WatermarkText="Enter Registration No"></cc1:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Make Of Vehicle</td>
                <td>
                    <asp:TextBox ID="txtMakeCode" runat="server" AutoPostBack="True" 
                        OnTextChanged="txtMakeCode_TextChanged" Width="51px"  Visible="False"></asp:TextBox>
<%--                </td>
                <td>--%>
                    <asp:DropDownList ID="ddMakeName" runat="server" AutoPostBack="True" 
                        OnSelectedIndexChanged="ddMakeName_SelectedIndexChanged" Width="300px" 
                        TabIndex="11">
                    </asp:DropDownList>
                </td>
                <td style="color: #800000">
                    <b>Chassis No.</b></td>
                <td>
                    <asp:TextBox ID="txtChasisno" runat="server" MaxLength="25" Width="148px" TabIndex="13"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat = "server" TargetControlID="txtChasisno" WatermarkText="Enter Chasis No"></cc1:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td>
                    Colour Of Vehicle
                </td>
                <td>
                    <asp:TextBox ID="txtColCode" runat="server" AutoPostBack="True" 
                        OnTextChanged="txtColCode_TextChanged" Width="51px"  Visible="False"></asp:TextBox>
<%--                </td>
                <td>--%>
                    <asp:DropDownList ID="ddColName" runat="server" AutoPostBack="True" 
                        OnSelectedIndexChanged="ddColName_SelectedIndexChanged"  Width="300px">
                    </asp:DropDownList>
                </td>
                <td style="color: #800000">
                    <b>Engine No.</b></td>
                <td>
                    <asp:TextBox ID="txtEngineno" runat="server" CssClass="textareacss" 
                        MaxLength="25" Width="148px" TabIndex="14"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat = "server" TargetControlID="txtEngineno" WatermarkText="Enter Engine No"></cc1:TextBoxWatermarkExtender>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:RadioButton ID="RbCash" runat="server" AutoPostBack="True" 
                        GroupName="rbcashipo" OnCheckedChanged="RbCash_CheckedChanged" Text="Cash" />
                    <asp:RadioButton ID="RbIPO" runat="server" AutoPostBack="True" 
                        GroupName="rbcashipo" OnCheckedChanged="RbIPO_CheckedChanged" Text="IPO" 
                        ValidationGroup="ctr" CausesValidation="true" TabIndex="15" />
                        
                </td>
                <td>
                    Year of Manufacture</td>
                <td>
                    <asp:DropDownList ID="ddYearofManu" runat="server" Width="148px" TabIndex="16">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr runat="server" visible="false">
                <td>
                    Enquiry Date</td>
                <td>
                </td>
                <td>
                    <asp:TextBox ID="txtEnqDt" runat="server" CssClass="date" Width="148px"></asp:TextBox>
                    <asp:Button ID="btnEnqDt" runat="server" CssClass="button" Text="..." 
                        Visible="false" Width="26px" />
                    <asp:DropDownList ID="ddModelName" runat="server" AutoPostBack="True" 
                        OnSelectedIndexChanged="ddModelName_SelectedIndexChanged" Visible="false">
                    </asp:DropDownList>
                    <br />
                    (dd/mm/yyyy)<br /></td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:Panel ID="pnlcash" runat="server" Width="100%">
                        <table>
                            <tr>
                                <td style="color: #800000; font-weight: 700">
                                    Cash Amount</td>
                                <td>
                                    <asp:TextBox ID="txtcashamt" runat="server" Width="148px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlipo" runat="server" Width="100%">
                        <table>
                            <tr>
                                <td style="color: #800000; font-weight: 700">
                                    IPO No.</td>
                                <td>
                                    <asp:TextBox ID="txtIopNo" runat="server" Width="148px" TabIndex="16"></asp:TextBox>
                                </td>
                            </tr>
                              <tr>
                                <td style="color: #800000; font-weight: 700">
                                    IPO Date.</td>
                                <td>
                                    <asp:TextBox ID="Txtipodate" runat="server" Width="148px" AutoPostBack="True" 
                                        TabIndex="17" ></asp:TextBox>
                                    <cc1:CalendarExtender 
                    ID="CalendarExtender1" 
                    runat="server" 
                    TargetControlID="txtipodate" 
                    Format="dd/MM/yyyy" 
                     PopupPosition="Right"  />
                     <asp:RequiredFieldValidator ID="iporequired" runat="server" ValidationGroup="ctr" ErrorMessage="Enter IPO Date" ControlToValidate="Txtipodate"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #800000; font-weight: 700">
                                    IPO Amount</td>
                                <td>
                                    <asp:TextBox ID="txtIPOamt" runat="server" Width="148px" Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" 
                        ForeColor="Black"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="button" 
                        onclick="btnSubmit_Click" Text="Submit" Width="76px" TabIndex="18" 
                        ValidationGroup="ctr" />
                    <asp:Button ID="btnReset" runat="server" CssClass="button" 
                        onclick="btnReset_Click" Text="Next Receipt" Width="95px" TabIndex="18" />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <cc1:FilteredTextBoxExtender ID="fetxtRegistrationno" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                        TargetControlID="txtRegistrationno">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="fetxtChasisno" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                        TargetControlID="txtChasisno">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="fetxtEngineno" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                        TargetControlID="txtEngineno">
                    </cc1:FilteredTextBoxExtender>
                     <asp:RequiredFieldValidator ID="rqdtxtAppName"  runat="server"  
                        ControlToValidate="txtAppName" ErrorMessage="Please enter the Applicant Name." 
                        SetFocusOnError="True" ValidationGroup="ctr"></asp:RequiredFieldValidator> 
           <asp:RequiredFieldValidator ID="rqdddVehTypeName"  runat="server" 
                        InitialValue="--Select--" ControlToValidate="ddVehTypeName" 
                        ErrorMessage="Please select the Vehicle Type." SetFocusOnError="True" 
                        ValidationGroup="ctr"></asp:RequiredFieldValidator>
                       <asp:RequiredFieldValidator ID="rqdtxtRegistrationno"  runat="server"  
                        ControlToValidate="txtRegistrationno" 
                        ErrorMessage="Please enter the Registration No." SetFocusOnError="True" 
                        ValidationGroup="ctr"></asp:RequiredFieldValidator> 
            <asp:RequiredFieldValidator ID="rqdtxtChasisno"  runat="server"  
                        ControlToValidate="txtChasisno" ErrorMessage="Please enter the Chasis No." 
                        SetFocusOnError="True" ValidationGroup="ctr"></asp:RequiredFieldValidator> 
        <asp:RequiredFieldValidator ID="rqdtxtEngineno"  runat="server"  
                        ControlToValidate="txtEngineno" ErrorMessage="Please enter the Engine No." 
                        SetFocusOnError="True" ValidationGroup="ctr"></asp:RequiredFieldValidator>
                    
                    <asp:RequiredFieldValidator ID="Rqdfirdate" runat="server" 
                        ControlToValidate="txtFIRDt" ErrorMessage="Fir dateReguired" 
                        SetFocusOnError="True" ValidationGroup="ctr"></asp:RequiredFieldValidator>
                   <asp:HiddenField ID="hfrecno" runat="server" />
                </td>
            </tr>
              </tr>
              <tr>
              <td colspan="5">
              <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="txtFIRDt" 
                        ErrorMessage="Date Should be less than or Equal to Current Date" 
                        SetFocusOnError="True" ValidationGroup="ctr"  Operator="LessThanEqual"      ControlToCompare="datetoday" Type="Date" ></asp:CompareValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" 
                        ControlToValidate="txtFIRDt" 
                        ErrorMessage="Date type" 
                        SetFocusOnError="True" ValidationGroup="ctr" Operator="DataTypeCheck" Type="Date"       ></asp:CompareValidator>
              </td>
              </tr>
                <tr>
                    <td colspan="5" style="text-align: center">
                        <asp:HyperLink ID="HelpCounter" runat="server" 
                            NavigateUrl="Vahan Samanvay Counters.pdf">Help</asp:HyperLink>
                    </td>
    </tr>
                </table>

</contenttemplate>            
        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="InvalidChars"
            InvalidChars="!+=-/\ @#$%^&*(){}[].,: " TargetControlID="txtFIRNo">
        </cc1:FilteredTextBoxExtender>      
    </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

