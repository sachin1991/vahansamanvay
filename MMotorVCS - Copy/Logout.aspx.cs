﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;


public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (clsPageBase.deletealreadyloggedin(Session["vcUserName"].ToString()) > 0)
            {
            }
            Session.Abandon();
            FormsAuthentication.SignOut();
            Session.Clear();
            Response.Redirect("Login.aspx");
        }
        catch      
{
}
    }
    
}