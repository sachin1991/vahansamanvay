using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Colour_Name : System.Web.UI.Page
{
    static int cname_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();
    ListItemCollection liColl;
    static int _add, _edit, _delete, _view;
    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");
        //_userid = "1";
        if (Session[Session.SessionID] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            liColl = (ListItemCollection)(Session[Session.SessionID]);
            _userid = liColl.FindByText("UserID").Value;
        }

        

        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["frmid"] != null)
                {
                    string _frmId = Request.QueryString["frmid"].ToString();
                    formpermission fpermission = new formpermission(int.Parse(liColl.FindByText("frm" + _frmId + "").Value));
                    _add = fpermission.Add;
                    _edit = fpermission.Edit;
                    _delete = fpermission.Delete;
                    _view = fpermission.View;
                    if (_view == 0)
                    {
                        Response.Redirect("error.aspx");
                    }
                    if (_add != 1)
                    {
                        btnAdd.Visible = false;
                    }
                    else
                    {
                        btnAdd.Visible = true;
                    }
                }
                else
                {
                    Response.Redirect("error.aspx");
                }
            }
            catch
            {
                Response.Redirect("error.aspx");
            }
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindgrid();
        }
        lblmsg.Text = "";
    }
    public void getmaxid()
    {
        try
        {
            cname_id = mm.getColourMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourName();
            gvColName.DataSource = ds;
            gvColName.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtColCode.Text = "";
        txtColName.Text = "";
        
        bindgrid();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourName();
            int flag = 0;
            if (txtColCode.Text != "" && txtColName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["colourcode"].ToString().ToUpper().Trim()) == txtColCode.Text.ToUpper().Trim())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Colour Code is already exists.</font>";
                        break;
                    }
                    if (ds.Tables[0].Rows[i]["colourname"].ToString().ToUpper().Trim() == txtColName.Text.ToUpper().Trim())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Colour name is already exists.</font>";
                        break;
                    }
                }
                if (flag != 1)
                {
                    if (mm.AddColour((cname_id),(txtColCode.Text.ToUpper().Trim()), txtColName.Text.ToUpper().Trim(), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values inserted</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not inserted</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourName();
            int flag = 0;
            if (txtColCode.Text != "" && txtColName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["colourid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["colourcode"].ToString().ToUpper().Trim()) == txtColCode.Text.ToUpper().Trim())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Colour Code is already exists.</font>";
                            break;
                        }
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["colourname"].ToString().ToUpper().Trim()) == txtColName.Text.ToUpper().Trim())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Colour name is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    if (mm.updateColour(Convert.ToInt32(lblid.Text),(txtColCode.Text.ToUpper().Trim()), txtColName.Text.ToUpper().Trim(), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values inserted</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not inserted</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvColName_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvColName.Rows[Convert.ToInt32(e.CommandName) - (gvColName.PageSize * pagecount)].Cells[1].Text;
                txtColCode.Text = gvColName.Rows[Convert.ToInt32(e.CommandName) - (gvColName.PageSize * pagecount)].Cells[2].Text;
                txtColName.Text = gvColName.Rows[Convert.ToInt32(e.CommandName) - (gvColName.PageSize * pagecount)].Cells[3].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvColName_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvColName.PageIndex = e.NewPageIndex;
        refresh(); 
    }
    protected void gvColName_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
}
