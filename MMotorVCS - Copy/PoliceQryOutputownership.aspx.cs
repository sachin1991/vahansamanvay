﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PoliceQryOutputownership : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Label1.Text = Convert.ToString(Session["Registration"]);
            Label2.Text = Convert.ToString(Session["Chasis"]);
            Label3.Text = Convert.ToString(Session["Engine"]);
        }
    }
    public string findowneralone1()
    {
        try
        {
            string rr;
            string rrf;
            string rrl;
            rr = "Ownership Details Not found()";
            string q;
            q = "1";
            if (q != "0")
            {
                VahanWSRef.VahanInfo serv = new VahanWSRef.VahanInfo();
                String resp;
                String regis, chas, engi;
                regis = Session["Registration"].ToString();
                chas = Session["Chasis"].ToString();
                engi = Session["Engine"].ToString();
                String response = serv.getDetails("DLNCRB", regis);
                //Response.Write(response);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                //Response.Write(response);
                resp = response;
                //return resp;
                if (regis != "")
                {
                    rr = accesswebservice.stringoutput1(resp);
                }
                if (rr == "" && chas != "")
                {
                    response = serv.getChasisDetails("DLNCRB", chas);
                    response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                    rr = accesswebservice.stringoutput1(response);
                }
                if (rr == "" && engi != "")
                {
                    response = serv.getEngineDetails("DLNCRB", engi);
                    response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                    rr = accesswebservice.stringoutput1(response);
                }
                rrf = "<table>";
                rrl = "</table>";
                return rrf + rr + rrl;
            }
            else
            {
                return rr;
            }
        }
        catch (Exception ex)
        {
            //return ex.StackTrace;
            return "<tr><td colspan=2><b>Presently NIC Service Not available</td></tr>";
        }
    }

}