﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for VehicleInfo
/// </summary>
namespace MMotorVCS
{
public class VehicleInfo
{
        private string strCrimeNo;
        private string strRegistrationNo;
        private string strChasisNo;
        private string strEngineNo;
        private string strState;
        private string strAutoType;
        private string  strMake;
        private string strStatus;
        private Boolean boDeleted;

        public VehicleInfo()
        {
            if (HttpContext.Current.Session["VehicleInfo"] != null)
            {
                VehicleInfo obj = (VehicleInfo)HttpContext.Current.Session["VehicleInfo"];

                strCrimeNo = Convert.ToString(obj.ID );
                strRegistrationNo = obj.RegistrationNo;
                strChasisNo = obj.ChasisNo;
                strEngineNo = obj.EngineNo;
                strState = obj.State;
                strAutoType = obj.AutoType;
                strMake = obj.Make;
                strStatus = obj.Status;
                boDeleted = obj.isDeleted;
            }
        }

        public string ID
        {
            set { strCrimeNo = value; }
            get { return strCrimeNo; }
        }

        public string RegistrationNo
        {
            set { strRegistrationNo = value; }
            get { return strRegistrationNo; }
        }

        public string ChasisNo
        {
            set { strChasisNo = value; }
            get { return strChasisNo; }
        }


        public string EngineNo
        {
            set { strEngineNo = value; }
            get { return strEngineNo; }
        }


        public string State
        {
            set { strMake = value; }
            get { return strMake; }
        }

        public string AutoType
        {
            set { strAutoType = value; }
            get { return strAutoType; }
        }

        public string Make
        {
            set { strMake = value; }
            get { return strMake; }
        }

        public string Status
        {
            set { strStatus = value; }
            get { return strStatus; }
        }

        public Boolean isDeleted
        {
            set { boDeleted = value; }
            get { return boDeleted; }
        }

    
}
}