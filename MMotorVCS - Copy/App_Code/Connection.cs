using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

/// <summary>
/// Summary description for Connection
/// </summary>
public class Connection
{
    private string conStr;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter Ad;
    DataSet ds;
	public Connection()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public SqlConnection MakeConnection()
    {
        conStr = WebConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        con = new SqlConnection(conStr);
        return (con);
    }
}
