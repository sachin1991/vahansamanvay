﻿using System;
using System.Web;

/// <summary>
/// Summary description for PasswordSetting
/// </summary>
public class PasswordSetting
{
	public PasswordSetting()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    //password age , 80, 180, 360 days
    private int _myvalue;
    private string _mystrvalue; 
    public int Duration 
    {

        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        } 
 
    }
    
    //password minimum length
    public int MinLength
    {
        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        }
    }

    //password maximum length
    public int MaxLength
    {
        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        }
    }

    //password Numbers length
    public int NumsLength
    {
        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        }
    }

    //password Upper letter length
    public int UpperLength
    {
        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        }
    }

    //password Special character length
    public int SpecialLength
    {
        get
        {

            return _myvalue;
        }
        set
        {

            _myvalue = value;
        }
    }

    //password valid special characters
    public string SpecialChars
    {
        get
        {

            return _mystrvalue;
        }
        set
        {

            _mystrvalue = value;
        }
    }



}
