using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for MenuClass
/// </summary>
public class MenuClass
{
    Connection mycon = new Connection();
    SqlDataAdapter da;
    SqlCommand cmd;

	public MenuClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}    
    public DataSet getRoleDet(int prRoleId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("Select * from P_ROLES where ROLE_ID="+ prRoleId +"", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getTopMenu(int prRoleId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select P_Forms.FORM_ID,P_Forms.FORM_NAME,P_Forms.PARENT_ID,P_Forms.MAIN_ID,P_Forms.FORM_LINK,P_Role_Rights.FORM_ADD,P_Role_Rights.FORM_EDIT,P_Role_Rights.FORM_DELETE,P_Role_Rights.FORM_VIEW,P_Role_Rights.ISATTACH from P_Forms inner join P_Role_Rights on P_Role_Rights.FORM_ID=P_Forms.FORM_ID where P_Forms.Level_ID=0 and P_Forms.ISDeleted=0 and P_Role_Rights.ROLE_ID="+ prRoleId +" order by P_Forms.Sort_Order", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getSubMenu(int prRoleId, int prLevelId,int prParentId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select P_Forms.FORM_ID,P_Forms.FORM_NAME,P_Forms.PARENT_ID,P_Forms.MAIN_ID,P_Forms.FORM_LINK,P_Role_Rights.FORM_ADD,P_Role_Rights.FORM_EDIT,P_Role_Rights.FORM_DELETE,P_Role_Rights.FORM_VIEW,P_Role_Rights.ISATTACH from P_Forms inner join P_Role_Rights on P_Role_Rights.FORM_ID=P_Forms.FORM_ID where P_Forms.PARENT_ID="+ prParentId +" and P_Forms.Level_ID="+ prLevelId +" and P_Forms.ISDeleted=0 and P_Role_Rights.ROLE_ID="+ prRoleId +" order by P_Forms.Sort_Order", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    #region P_Forms
    //public int getPFromsMaxId()
    //{
    //    int maxId = 0;
    //    SqlConnection con = mycon.MakeConnection();
    //    da = new SqlDataAdapter("select max(FORM_ID) MAX1 from P_Forms", con);
    //    DataSet ds = new DataSet();
    //    da.Fill(ds);
    //    if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX1"].ToString() != "")
    //    {
    //        maxId = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX1"].ToString());
    //        maxId = maxId + 1;
    //    }
    //    else
    //    {
    //        maxId = maxId + 1;
    //    }
    //    return maxId;
    //}
    public int getPFromsMaxId()
    {
        int maxId = 0;
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select max(inMenuId) MAX1 from [TblMst_Menu]", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX1"].ToString() != "")
        {
            maxId = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX1"].ToString());
            maxId = maxId + 1;
        }
        else
        {
            maxId = maxId + 1;
        }
        return maxId;
    }
    /// <summary>
    /// Child Forms of Parent Forms
    /// </summary>
    /// <param name="prParentId"></param>
    /// <returns></returns>
    public DataSet getPForms(int prParentId, int prLevelId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select * from TblMst_Menu where boEnabled=1 and inParentId=" + prParentId + " and inLevelId=" + prLevelId + " order by inMainId,inParentId,inLevelId,inSortOrder", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    /// <summary>
    /// Root Level Forms
    /// </summary>
    /// <param name="prParentId"></param>
    /// <returns></returns>
    public DataSet getPForms()
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select * from dbo.TblMst_Menu where boEnabled=1 and inParentId =0 and inLevelId=0 order by inParentId,inLevelId,inSortOrder", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int addPRootForms(int prFromId, string prFormName, int prLevelId, int prMainId, string prFormLink, int prSortOrder, string prUserId)
    {
        Regex re = new Regex("!+=-/\\@$%^&amp;*(){},:'<>");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("INSERT INTO TblMst_Menu(inMenuId,vcMenuText,inParentId,inLevelId,inMainId,vcMenuLink,inSortOrder,boEnabled) VALUES (" + prFromId + ",'" +re.Replace(prFormName,"") + "',0," + prLevelId + ",'" + prMainId + "','" +re.Replace(prFormLink,"") + "'," + prSortOrder + " ,"+ '1'+ ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updatePRootForms(int prFromId, string prFormName, int prLevelId, int prMainId, string prFormLink, int prSortOrder, string prUserId)
    {
        Regex re = new Regex("!+=-/\\@#$%^&amp;*(){}[],:'<>");

        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("UPDATE TblMst_Menu SET FORM_NAME='" + re.Replace(prFormName,"") + "',LEVEL_ID=" + prLevelId + ",MAIN_ID=" + prMainId + ",FORM_LINK='" + re.Replace(prFormLink,"") + "',SORT_ORDER=" + prSortOrder + " where FORM_ID=" + prFromId + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int addPForms(int prFromId, string prFormName, int prParentId, int prLevelId, int prMainId, string prFormLink, int prSortOrder, string prUserId)
    {
        Regex re = new Regex("!+=-/\\@#$%^&amp;*(){}[],:'<>");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("INSERT INTO TblMst_Menu(inMenuId,vcMenuText,inParentId,inLevelId,inMainId,vcMenuLink,inSortOrder,boEnabled) VALUES (" + prFromId + ",'" + re.Replace(prFormName,"") + "'," + prParentId + "," + prLevelId + ",'" + prMainId + "','" +re.Replace(prFormLink,"") + "'," + prSortOrder + " ," + '1' + ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updatePForms(int prFromId, string prFormName, int prParentId, int prLevelId, int prMainId, string prFormLink, int prSortOrder, string prUserId)
    {
        Regex re = new Regex("!+=-/\\@#$%^&amp;*(){}[],:'<>");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("UPDATE TblMst_Menu SET FORM_NAME='" +re.Replace(prFormName,"") + "',LEVEL_ID=" + prLevelId + ",MAIN_ID=" + prMainId + ",FORM_LINK='" +re.Replace(prFormLink,"") + "',SORT_ORDER=" + prSortOrder + " where FORM_ID=" + prFromId + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int deletePForms(int prFromId, string prUserId)
    {
        SqlConnection con = mycon.MakeConnection();
        //cmd = new SqlCommand("UPDATE TblMst_Menu SET ISDELETED=1,MODIFIED_DATE=getdate(),RECENT_USER=" + int.Parse(prUserId) + " where FORM_ID=" + prFromId + "", con);
        cmd = new SqlCommand("UPDATE TblMst_Menu SET boEnabled=0 where inMenuId=" + prFromId + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    #endregion

    #region P_Roles
    public int getPRoleMaxId()
    {
        int maxId = 0;
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select max(inRoleId) MAX1 from TblMst_UserRole", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX1"].ToString() != "")
        {
            maxId = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX1"].ToString());
            maxId = maxId + 1;
        }
        else
        {
            maxId = maxId + 1;
        }
        return maxId;
    }
    public DataSet getPRoles()
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select * from  dbo.TblMst_UserRole where boenabled = 1 order by vcFullDesc", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int addPRoles(int prRoleId, string shortrole, string prRoleName, int prActive, string vcUserTpe)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("INSERT INTO TblMst_UserRole(inRoleId,vcRoleDesc,vcFullDesc,vcUserType,boEnabled) VALUES (" + prRoleId + ",'" +re.Replace(shortrole,"") + "','" +re.Replace(prRoleName,"") + "','" + re.Replace(vcUserTpe.Trim(),"") + "'," + prActive + ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updatePRoles(int prRoleId,string shortName, string prRoleName, int prActive, string vcUserTpe, int lblLoginId)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("UPDATE TblMst_UserRole SET vcRoleDesc='" +re.Replace(shortName,"") + "',vcFullDesc='" +re.Replace(prRoleName,"") + "',boEnabled=" + prActive + " where inLoginId=" + lblLoginId + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    #endregion

    #region P_RoleRigts
    public int getPRoleRightMaxId()
    {
        int maxId = 0;
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select max(inMenuRightsId) MAX1 from TblMst_MenuRights", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX1"].ToString() != "")
        {
            maxId = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX1"].ToString());
            maxId = maxId + 1;
        }
        else
        {
            maxId = maxId + 1;
        }
        return maxId;
    }
    public DataSet getPActiveRoles()
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select * from  dbo.TblMst_UserRole where boenabled = 1 order by vcFullDesc", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    /// <summary>
    /// For Root Level Menu
    /// </summary>
    /// <returns></returns>
    public DataSet getPRoleRights(int prRoleId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select mr.inFormId,mr.inRoleId,m.boEnabled inForm_View,m.boEnabled inForm_Add,m.boEnabled inForm_Edit,m.boEnabled[inForm_Delete], m.boEnabled from TblMst_MenuRights mr inner join TblMst_Menu m on mr.inFormId=m.inMenuId where mr.inRoleId =" + prRoleId + " and  m.inLevelId=0  order by m.inMainId,m.inParentId,m.inLevelId,m.inSortOrder", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    /// <summary>
    /// For Child Level Menu
    /// </summary>
    /// <param name="prParentId"></param>
    /// <param name="prLevelId"></param>
    /// <returns></returns>
    public DataSet getPRoleRights(int prParentId, int prLevelId, int prRoleId)
    {
        SqlConnection con = mycon.MakeConnection();
        da = new SqlDataAdapter("select mr.inFormId,mr.inMenuRightsId,m.boEnabled inForm_View, m.boEnabled inForm_Add,m.boEnabled inForm_Add,m.boEnabled inForm_Add,m.boEnabled,mr.inRoleId from TblMst_MenuRights mr inner join TblMst_Menu m on mr.inFormId=m.inMenuId where m.boEnabled=1 and m.inParentId=" + prParentId + " and m.inLevelId=" + prLevelId + " and mr.inRoleId=" + prRoleId + " order by m.inMainId,m.inParentId,m.inLevelId,m.inSortOrder", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int addPRoleRights(int prRightsId, int prRoleId, int prFromId, int prFormView, int prFormAdd, int prFormEdit, int prFormDelete, int prIsAttach, string prUserId)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("INSERT INTO TblMst_MenuRights(inRoleId,inFormId,inForm_View,inForm_Edit,inForm_Add) VALUES (" + prRoleId + "," + prFromId + "," + prFormView + "," + prFormView + "," + prFormView + ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updatePRoleRights(int prRightsId, int prRoleId, int prFromId, int prFormView, int prFormAdd, int prFormEdit, int prFormDelete, int prIsAttach, string prUserId)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        SqlConnection con = mycon.MakeConnection();
        cmd = new SqlCommand("DELETE FROM TblMst_MenuRights WHERE inRoleId=" + prRoleId + " and inFormId=" + prFromId + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    #endregion
}
