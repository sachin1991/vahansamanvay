using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for formpermission
/// </summary>
public class formpermission
{
    //1 for add, 3 for edit, 5 for delete
    private int _add; 
    private int _edit;
    private int _delete;
    private int _view;
    public int Add
    {
        get
        {
            return _add;
        }
    }
    public int Edit
    {
        get
        {
            return _edit;
        }
    }
    public int Delete
    {
        get
        {
            return _delete;
        }
    }
    public int View
    {
        get
        {
            return _view;
        }
    }
	public formpermission(int val)
	{
        switch (val)
        {
            case 0: _add = 0;
                    _edit = 0;
                    _delete = 0;
                    _view = 1;
                    break;
            case 1: _add = 1;
                    _edit = 0;
                    _delete = 0;
                    _view = 1;
                    break;
            case 3: _add = 0;
                    _edit = 1;
                    _delete = 0;
                    _view = 1;
                    break;
            case 5: _add = 0;
                    _edit = 0;
                    _delete = 1;
                    _view = 1;
                    break;
            case 4: _add = 1;
                    _edit = 1;
                    _delete = 0;
                    _view = 1;
                    break;
            case 6: _add = 1;
                    _edit = 0;
                    _delete = 1;
                    _view = 1;
                    break;
            case 8: _add = 0;
                    _edit = 1;
                    _delete = 1;
                    _view = 1;
                    break;
            case 9: _add = 1;
                    _edit = 1;
                    _delete = 1;
                    _view = 1;
                    break;
           default: _add = 0;
                    _edit = 0;
                    _delete = 0;
                    _view = 0;
                    break;
        }
	}
}
