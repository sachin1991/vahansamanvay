using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for MasterMethods
/// </summary>
public class MasterMethods
{
    private string conStr;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    DataSet ds;
    Connection Mycon = new Connection();
    public MasterMethods()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    // Function for State Type
    public int AddState(int id, string _scode, string _sname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into TblMst_Location(LocationKey,code,Description,LocationType) values(" + id + ",'" + re.Replace(_scode, "") + "','" + re.Replace(_sname, "") + "','1')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con = Mycon.MakeConnection();
        return i;
    }
    public int AddStatealpha(string _scode, string statecodealpha)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into statecodeinalpha(statecodenumeric,statecodealpha) values(" + re.Replace(_scode, "") + ",'" + re.Replace(statecodealpha, "") + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con = Mycon.MakeConnection();
        return i;
    }
    public int updateStateType(int id, string _scode, string _sname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update TblMst_Location set  LocationKey='" + re.Replace(_scode, "") + "',code='" + re.Replace(_scode, "") + "',Description='" + re.Replace(_sname, "") + "' where LocationType='1' and  code=" + id + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getStateType()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select [Code] stateid,[Code]statecode,[Description] statename from TblMst_Location where LocationType='1' order by Code", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getStateType1()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select stateid,statecode,statename from vw_state order by statename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getStateTypeNew()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  statecode,statename from vw_state ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getStatecode(string statecode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  statecode,statename from vw_state where statecode=" + statecode + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getUserDetailsState(string statecode, int sess)
    {
        DataSet ds = new DataSet();
        if (sess == 3)

        //*(Session["inRoleId"] == 3)
        {
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_getUserAccountDetailsPS", statecode);
        }
        else
        {
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "dbo.usp_getUserAccountDetails", statecode);
        }
        return ds;
    }

    public DataSet getUserDetailsState(string statecode)
    {
        DataSet ds = new DataSet();
        {
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "dbo.usp_getUserAccountDetails", statecode);
        }
        return ds;
    }

    public DataSet getStateNameByCode(string stateCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select stateid,statecode,statename from vw_state where statecode='" + stateCode + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }


    public int getStateMaxId()
    {

        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(StateId) MAX from vw_state", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;

    }
    // End for function State type

    // Function for District Type



    public int AddDistrict(string id, string sid, string _dcode, string _dname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into TblMst_Location(LocationKey,Code,Description,LocationType,ParentKey) values('" + id + "','" + sid + "','" + re.Replace(_dcode, "") + "','" + re.Replace(_dname, "") + "','" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateDistrictType(int id, int sid, string _dcode, string _dname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update TblMst_Location set Code='" + re.Replace(_dcode, "") + "',Description='" + re.Replace(_dname, "") + "' where LocationType ='2' and Code='" + _dcode + "'", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }



    public Boolean updateCrimeDetails(string id, string Registration, string Chasis, string Engine, string Status, Boolean isdeleted, string strUserName, string oldregno, string oldchasisno, string oldengineno, string autotypecode, string makecode)
    {
        try
        {
            Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
            Boolean boResult;
            boResult = Convert.ToBoolean(MMotorVCS.DTask.ExecuteScalar(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_UpdateVehicleDetails", id, re.Replace(Registration, ""), re.Replace(Chasis, ""), re.Replace(Engine, ""), re.Replace(Status, ""), isdeleted, strUserName, oldregno, oldchasisno, oldengineno, autotypecode, makecode));

            return boResult;
        }
        catch (Exception ex)
        {
            return false;
        }

    }
    public DataSet getDistrictType()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("SELECT stateid,statecode,statename,districtid,districtcode,districtname FROM vw_district order by districtcode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getDistrictTypeNamewise()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("SELECT stateid,statecode,statename,districtid,districtcode,districtname FROM vw_district order by districtname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getDistrictType(string stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("SELECT stateid,statecode,statename,districtid,districtcode,districtname FROM vw_district where stateid=" + stateid + " order by districtname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getDistrictType1(string lblstate)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("SELECT stateid,statecode,statename,districtid,districtcode,districtname FROM vw_district where stateid=" + lblstate + " order by districtcode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataTable getDistrictType_accstatetype(int stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  districtcode+ '-' +  districtid as districtcode,districtname from vw_district where stateid=" + stateid + " order by districtname", con);
        //da = new SqlDataAdapter("select distinct  districtcode as districtcode,districtname from vw_district where stateid='" + stateid + "' order by districtname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getDistrictType_accstatetypestring(string stateid)
    {
        con = Mycon.MakeConnection();
        //da = new SqlDataAdapter("select distinct  districtcode+ '-' +  convert(varchar, districtid) as districtcode,districtname from vw_district where statecode='" + stateid + "' order by districtname", con);
        da = new SqlDataAdapter("select distinct  districtcode as districtcode,districtname from vw_district where stateid='" + stateid + "' order by districtname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getDistrictname(string districtCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  districtcode as districtcode,districtname from vw_district where districtcode=" + districtCode + " order by districtname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }


    public DataTable getDistrictType_accstatetypeRecUpd(int stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  districtcode, districtid ,districtname from vw_district where stateid=" + stateid + " order by districtname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataSet getDistrictType_accstatetypeNew()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  districtcode as districtcode,districtname from vw_district ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getDistrictMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(DistrictId) MAX from vw_district", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    public DataSet getDistrictcodeList(int statecode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtcode,districtname,stateid,statecode from vw_district where statecode=" + statecode + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getDistrictcode(int districtid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtcode,districtname,stateid,statecode from vw_district where districtid=" + districtid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getDistrictNameByCode(string districtCode, int stateId)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtcode,districtname,districtid from vw_district where districtcode='" + districtCode + "' and StateId=" + stateId + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    // End for Function District Type


    // Function for Police Station Type

    public int AddPsType(string locationkey, string code, string description, string locationtype, string parentkey, string updatedby)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Tblmst_location(Langid,LocationKey,Code,Description,LocationType,ParentKey,updatedby) values(409,'" + re.Replace(locationkey, "") + "','" + re.Replace(code, "") + "','" + re.Replace(description, "") + "','" + re.Replace(locationtype, "") + "','" + re.Replace(parentkey, "") + "','" + updatedby + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }

    private string getdate()
    {
        throw new NotImplementedException();
    }

    public DataTable getPSname(string psCode, string distCode, string statecode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  pscode+ '-' +  convert(varchar, psid) as psidpscode,Psname from dbo.vw_Ps where Pscode='" + psCode + "' and districtcode = '" + distCode + "' and statecode = '" + statecode + "' order by Psname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getInsuranceCompany()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  Inscompcode from InsuranceComp_detail ", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getInsCompCodefromsetup(string usname)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select insurancecode from TblMst_Logindetails where vcUserName = '" + usname + "'", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getInsuranceAddress(string inscode)
    {
        con = Mycon.MakeConnection();
        string em;
        em = "select top 1 InscopmName,InscompAdd from InsuranceComp_detail where InsCompCode = '" + inscode + "'";
        da = new SqlDataAdapter(em, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }
    public DataTable getPSname_districtWise(string statecode, string distCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  pscode+ '-' +  convert(varchar, psid) as psidpscode,Psname from dbo.vw_Ps where  districtcode = " + distCode + " and statecode = " + statecode + "order by Psname", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }


    public int updatePsType(string _Psid, int _did, string _Pscode, string _Psname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update TblMst_Location set [Description]= '" + re.Replace(_Psname, "") + "' where LocationKey= '" + _Psid + "'", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public DataSet getPsType()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtid,districtcode,districtname,Psid,Pscode,Psname from dbo.vw_Ps order by pscode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getPsTypenew(String stcode, String dtcode)
    {

        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtid,districtcode,districtname,Psid,Pscode,Psname from dbo.vw_Ps Where statecode='" + stcode + "' and districtcode ='" + dtcode + "' order by Pscode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsType(string distcode, string stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtid,districtcode,districtname,Psid,Pscode,Psname from dbo.vw_Ps where statecode='" + stateid + "' and districtcode=" + distcode + "  order by Pscode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsType1(string distcode, string stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtid,districtcode,districtname,Psid,Pscode,Psname from dbo.vw_Ps where statecode='" + stateid + "' and districtcode=" + distcode + "  order by Psname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsType_accDistricttype(int disid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select districtid,districtcode,districtname,Psid,Pscode,Psname from dbo.vw_Ps where districtid=" + disid + " order by psname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsType_accDistricttypeNew(int disid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  pscode+ '-' +  convert(varchar, psid) as psidpscode,Psname from vw_Ps where districtid=" + disid + " order by psname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsType_accDistricttypeNew(int disid, int statecode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  pscode+ '-' +  convert(varchar, psid) as psidpscode,Psname from vw_Ps where districtid=" + disid + " and statecode=" + statecode + " order by psname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPscode(int Psid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Pscode,Psname from vw_Ps where Psid=" + Psid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getPsNameByCode(string PsCode, int districtId)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Pscode,Psname,Psid from vw_Ps where PsCode='" + PsCode + "' and districtid=" + districtId + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getPsMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(PsId) MAX from vw_Ps", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    // End for function Police Station Type

    // Function for Insurance Company Details
    public int AddInsComp(int id, string _compcode, string _compname, string compadd, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into InsuranceComp_Detail(inscompid,inscompcode,InsCopmName,inscompadd,created_date,recent_user) values(" + id + ",'" + re.Replace(_compcode, "") + "','" + re.Replace(_compname, "") + "','" + re.Replace(compadd, "") + "',getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateInsComp(int id, string _compcode, string _compname, string compadd, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update InsuranceComp_Detail set inscompcode='" + re.Replace(_compcode, "") + "',InsCopmName='" + re.Replace(_compname, "") + "',inscompadd='" + re.Replace(compadd, "") + "',modified_date=getdate(),modified_user='" + _userid + "' where inscompid=" + id + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getInsCompDetail()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select inscompid,inscompcode,InsCopmName,inscompadd from InsuranceComp_Detail", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getInsCompCode(int InsCompid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select InsCompcode,InsCopmName from InsuranceComp_Detail where InsCompid=" + InsCompid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getInsCompDetailMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(InscompId) MAX from InsuranceComp_Detail", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    // End for Function Insurance Company Details

    // Function For Vehicle Type Details

    public int AddVehicleType(int vid, string _vcode, string _vname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Vehicle_Type(vehicletypeid,vehicletypecode,vehicletypename,created_date,recent_user) values(" + vid + ",'" + re.Replace(_vcode, "") + "','" + re.Replace(_vname, "") + "',getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateVehicleType(int vid, string _vcode, string _vname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update vehicle_type set vehicletypecode='" + re.Replace(_vcode, "") + "',vehicletypename='" + re.Replace(_vname, "") + "',modified_date=getdate(),modified_user= '" + _userid + "' where vehicletypeid=" + vid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public DataSet getVehicleType()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicletypeid,vehicletypecode,vehicletypename  from vehicle_type order by vehicletypecode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehicleType1()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicletypeid,vehicletypecode,vehicletypename  from vehicle_type order by vehicletypename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehicleTypeNew()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  vehicletypecode+ '-' +  convert(varchar, vehicletypeid) as vehidvehcode,vehicletypename   from vehicle_type ", con);

        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getStatus()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  vcStatusCode+ '-' +  convert(varchar, vcStatusCode) as vcStatusCode,vcStatusDescription from dbo.TblMst_Status where boEnabled=1", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getVehiclecode(int vehicleid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicletypecode,vehicletypename from vehicle_type where vehicletypeid=" + vehicleid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehiclecodecounter(int vehicleid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicletypecode,vehicletypename from vehicle_type where vehicletypecode=" + vehicleid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getVehicleMaxId()
    {

        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(VehicleTypeId) MAX from Vehicle_type", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    public DataSet getVehicleTypeNameByCode(string vehicletypeCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicletypeid,vehicletypecode,vehicletypename from vehicle_type where vehicletypecode='" + vehicletypeCode.Trim() + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    // End for Function Vehicle Type Details

    // Function for Make of Vehicle
    public int AddVehMake(int mid, int vid, string _mcode, string _mname, string _userid, string vehcode)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Make_of_vehicle(makeid,vehicletypeid,makecode,makename,created_date,recent_user,vehicletypecode) values(" + mid + "," + vid + ",'" + re.Replace(_mcode, "") + "','" + re.Replace(_mname, "") + "',getdate(),'" + _userid + "','" + re.Replace(vehcode, "") + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateVehMake(int vid, string _mcode, string _mname, string _userid, string vehcode)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update make_of_vehicle set vehicletypecode='" + re.Replace(vehcode, "") + "',vehicletypeid=" + vid + ",makecode='" + re.Replace(_mcode, "") + "',makename='" + re.Replace(_mname, "") + "',modified_date=getdate(),modified_user='" + _userid + "' where makecode='" + _mcode + "' and vehicletypeid=" + vid, con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getVehMake()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Vehicle_Type.VehicleTypeId,Vehicle_Type.VehicleTypeCode,Vehicle_Type.VehicleTypeName,Make_of_Vehicle.makeid,Make_of_Vehicle.Makecode,Make_of_Vehicle.makename from vehicle_type inner join Make_of_Vehicle on vehicle_type.vehicletypeid=Make_of_Vehicle.vehicletypeid order by vehicletypename,makecode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMakecode(String mak)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Vehicle_Type.VehicleTypeId,Vehicle_Type.VehicleTypeCode,Vehicle_Type.VehicleTypeName,Make_of_Vehicle.makeid,Make_of_Vehicle.Makecode,Make_of_Vehicle.makename from vehicle_type inner join Make_of_Vehicle on vehicle_type.vehicletypeid=Make_of_Vehicle.vehicletypeid where vehicle_type.vehicletypecode='" + mak + "' order by vehicletypename,makecode", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMakename(String mak)
    {
        con = Mycon.MakeConnection();
        String sq = "select Vehicle_Type.VehicleTypeId,Vehicle_Type.VehicleTypeCode,Vehicle_Type.VehicleTypeName,Make_of_Vehicle.makeid,Make_of_Vehicle.Makecode,Make_of_Vehicle.makename from vehicle_type inner join Make_of_Vehicle on vehicle_type.vehicletypeid=Make_of_Vehicle.vehicletypeid where vehicle_type.vehicletypecode='" + mak + "' order by vehicletypename,makename";

        da = new SqlDataAdapter("select Vehicle_Type.VehicleTypeId,Vehicle_Type.VehicleTypeCode,Vehicle_Type.VehicleTypeName,Make_of_Vehicle.makeid,Make_of_Vehicle.Makecode,Make_of_Vehicle.makename from vehicle_type inner join Make_of_Vehicle on vehicle_type.vehicletypeid=Make_of_Vehicle.vehicletypeid where vehicle_type.vehicletypecode='" + mak + "' order by vehicletypename,makename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMake_accVehType(int vehid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Makecode,makename from typeandmakeofvehicle where vehicletypecode='" + vehid + "' order by makename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMake_accVehTypeNew1(int vehid)
    {
        con = Mycon.MakeConnection();

        da = new SqlDataAdapter("select  makecode,makename from vw_make_Equi where vehicletypecode=" + vehid + " order by makename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMake_accVehTypeNew2(int vehid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select  makecode,makename from typeandmakeofvehicle where vehicletypecode='" + vehid + "' order by makename ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMake_accVehTypeNew(int vehid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  makecode+ '-' +  convert(varchar, makecode) as makeidmakecode,makename from make_of_vehicle where vehicletypeid=" + vehid + " order by makename", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehMakecode(int makeid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Makecode,makename from Make_of_Vehicle where makecode=" + makeid + " ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public int getVehMakeMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(MakeId) MAX from Make_of_Vehicle", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }

    public DataSet getMakeNameByCode(string MakeCode, int VehId)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Makecode,Makename,Makeid from Make_of_Vehicle where MakeCode='" + MakeCode + "' and Vehicletypeid=" + VehId + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    // End for function Make of Vehicle

    // Function for Model of Vehicle

    public int AddVehModel(int modid, int vid, int mid, string _modname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Model_name(modelid,vehicletypeid,makeid,modelname,created_date,recent_user) values(" + modid + "," + vid + ",'" + mid + "','" + re.Replace(_modname, "") + "',getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateVehModel(int modid, int vid, int mid, string _modname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update model_name set vehicletypeid=" + vid + ",makeid='" + mid + "',modelname='" + re.Replace(_modname, "") + "',modified_date=getdate(),modified_user ='" + _userid + "'  where modelid=" + modid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getVehModelname(int modelid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select modelname from model_name where modelid=" + modelid + " ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getVehModel()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Vehicle_Type.VehicleTypeId,Vehicle_Type.VehicleTypeCode,Vehicle_Type.VehicleTypeName,Make_of_Vehicle.makeid,Make_of_Vehicle.Makecode,Make_of_Vehicle.makename,Model_name.modelid,Model_name.modelname from vehicle_type inner join Model_name on vehicle_type.vehicletypeid=Model_Name.vehicletypeid inner join make_of_vehicle on make_of_vehicle.makeid=Model_name.makeid", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getVehModel_accMake(int makeid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("Select modelid,modelname from Model_name where makeid=" + makeid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public int getVehModelMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(ModelId) MAX from Model_name", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }

    // End for Function Model of Vehicle


    // Function for Colour Name

    public int AddColour(int cid, string _ccode, string _cname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into colour_name(colourid,colourcode,colourname,created_date,recent_user) values(" + cid + ",'" + re.Replace(_ccode, "") + "','" + re.Replace(_cname, "") + "',getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateColour(int cid, string _ccode, string _cname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update colour_name set colourcode='" + re.Replace(_ccode, "") + "',colourname='" + re.Replace(_cname, "") + "',modified_date=getdate(),modified_user='" + _userid + "'  where colourid=" + cid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getColourName()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select colourid,colourcode,colourname from colour_name ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getColourNameNew()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  colourcode+ '-' +  convert(varchar, colourid) as colidcolcode,colourname from colour_name ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getColourCode(int colid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select colourcode from colour_name where colourcode=" + colid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getColourMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(ColourId) MAX from Colour_name", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    public DataSet getColourNameByCode(string ColourCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Colourid,Colourcode,Colourname from Colour_name where Colourcode='" + ColourCode + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    // End of Function Colour Name

    // Function for Act Name
    public int AddActName(int aid, string _acode, string _aname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into act_name(actid,actcode,actname,created_date,recent_user) values(" + aid + ",'" + re.Replace(_acode, "") + "','" + re.Replace(_aname, "") + "',getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateActName(int aid, string _acode, string _aname, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update act_name set actcode='" + re.Replace(_acode, "") + "',actname='" + re.Replace(_aname, "") + "',modified_date=getdate(),modified_user ='" + _userid + "'  where actid=" + aid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public DataSet getActName()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select actid,actcode,actname from act_name order by actname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getActNameNew()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select distinct  actcode+ '-' +  convert(varchar, actid) as actidactcode,actname from act_name order by actname", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getvisitor()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Visitor from Visitor", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet internetgetvisitor()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Visitor from InternetVisitor", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getcountnoc(string usr)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select cnt from countnoc where usr='" + usr + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getActNameByCode(string actCode)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select actid,actcode,actname from act_name where actcode='" + actCode + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getActCode(int actid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select actcode from act_name where actid=" + actid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getActNameMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(actId) MAX from act_name", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    // End For Function Act Name

    // Function for Section No.

    public int AddSection(int secid, int actid, string _secno, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Section_no(sectionid,actid,sectionno,created_date,modified_date,recent_user) values(" + secid + "," + actid + ",'" + re.Replace(_secno, "") + "',getdate(),getdate(),'" + _userid + "')", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int updateSectionNo(int secid, int actid, string _secno, string _userid)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^#]");
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update section_no set actid=" + actid + ",sectionno='" + re.Replace(_secno, "") + "',modified_date=getdate(),recent_user='" + _userid + "'  where sectionid=" + secid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;

    }
    public DataSet getSectionNo()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select act_name.actid,act_name.actcode,act_name.actname,Section_no.Sectionid,Section_no.sectionno from act_name inner join Section_no on act_name.actid=Section_no.actid", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getSecNo_accActtype(int actid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select Sectionid,sectionno from Section_no where actid=" + actid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    //public DataSet getSectionCode(int secid)
    //{
    //    con = Mycon.MakeConnection();
    //    da = new SqlDataAdapter("select sectioncode from state_type where stateid=" + stateid + "", con);
    //    DataSet ds = new DataSet();
    //    da.Fill(ds);
    //    return ds;
    //}
    public int getSectionNoMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(SectionId) MAX from section_no", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }

    // End for Function Section No.

    // Function for converting date in mm/dd/yyyy
    /// <summary>
    /// Validate Date for check, it is in dd/MM/yyyy.
    /// </summary>
    /// <param name="date"></param>
    /// <param name="format"></param>
    /// <returns></returns>
    public bool ValidateDate(String date, String format)
    {
        try
        {
            System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
            dtfi.ShortDatePattern = format;
            DateTime dt = DateTime.ParseExact(date, "d", dtfi);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    /// <summary>
    /// Returns Date into MM/dd/yyyy
    /// </summary>
    /// <param name="date"></param>
    /// <param name="format"></param>
    /// <returns></returns>
    public DateTime ValidateDate(String date)
    {
        try
        {
            System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            DateTime dt = DateTime.ParseExact(date, "d", dtfi);
            return dt;
        }
        catch (Exception)
        {
            return DateTime.MinValue;
        }
    }
    //End for this function

}
