﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Security.Cryptography;
using System.Xml;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

/// <summary>
/// Summary description for accesswebservice
/// </summary>
public class accesswebservice
{
	public accesswebservice()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    
    public static string stringoutput1(string resp)
    {
        string lbl;
        lbl = "";
        using (XmlReader reader = XmlReader.Create(new StringReader(resp)))
        {
            reader.Read();
            reader.MoveToContent();
            
            while (reader.Read())
            {
                if (reader.Name == "rc_regn_dt" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Registration Date " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_regn_no" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Registration Number " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_owner_name" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Owner_name " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_f_name" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Fathers Name " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_present_address" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Present Address " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_permanent_address" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Permanent Address " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_vh_class_desc" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Vehicle Type " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }

                if (reader.Name == "rc_chasi_no" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Chasis No " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_eng_no" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Engine No " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_maker_desc" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Make " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_maker_model" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Model " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_body_type_desc" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Body Type " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_fuel_desc" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Fuel Type " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_color" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Color " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_financer" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Financer " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_insurance_comp" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Insurance from " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_insurance_policy_no" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Insurance Policy No " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_insurance_upto" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Insurance Upto " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_manu_month_yr" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Manufacturing Month/Year " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_registered_at" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Registered at " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_registered_at" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Registered at " + "</td><td>" + reader.ReadString() + "</tr><tr>";
                }
                if (reader.Name == "rc_status_as_on" && reader.MoveToContent() == XmlNodeType.Element)
                {
                    lbl = lbl + "<tr><td>";
                    lbl = lbl + " <b>Registration Status on " + "</td><td>" + reader.ReadString() + "</tr><tr></table>";
                }
            }
        }
        if (lbl != "")
        {
            lbl = "<tr><td colspan=2 align=center><b>Ownership Details from Transport Authority</td></tr>" + lbl;
        }
        else
        {
            lbl = "";
        }

        return lbl;
    }

    public static string decrypt(string Text, string secretkey)
    {
        try
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            rijndaelCipher.Mode = CipherMode.CBC;
            rijndaelCipher.Padding = PaddingMode.PKCS7;
            rijndaelCipher.KeySize = 128;
            rijndaelCipher.BlockSize = 128;
            byte[] encryptedData = Convert.FromBase64String(Text);
            byte[] pwdBytes = System.Text.Encoding.UTF8.GetBytes(secretkey);
            byte[] keyBytes = new byte[16]; int len = pwdBytes.Length;
            if (len > keyBytes.Length) len = keyBytes.Length;
            System.Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes; rijndaelCipher.IV = keyBytes;
            ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
            byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            return Encoding.UTF8.GetString(plainText);
        }
        catch
        {
            return "";
        }

    }
       
    
}