﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
using MMotorVCS;


public partial class InsuranceTabulated : System.Web.UI.Page
{
    public DataTable dv;
    DataSet dt;
    DataSet dtemail;
    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {

                Response.Redirect("LogoutModule.aspx");
            }

            lblUpdated.Text = DateTime.Today.ToShortDateString().Trim() ;
            lblPrinted.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy") + " by " + Session["vcUserName"].ToString() + " "  + Session["ClientAddr"].ToString().Trim();
            DateText.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
            string _MakeDesc = Session["MakeDesc"].ToString();
            string _Registration = Session["Registration"].ToString();
            string _Chasis = Session["Chasis"].ToString();
            string _Engine = Session["Engine"].ToString();
            string _color = Session["Color"].ToString();
            string _model = Session["Model"].ToString();
            Entry_FormMethods eform = new Entry_FormMethods();
            DataSet ds = new DataSet();
            MasterMethods mm = new MasterMethods();
            DataSet dtbvisitor = mm.getcountnoc(Session["vcUserName"].ToString());

            if (dtbvisitor != null)
            {
                if (dtbvisitor.Tables[0].Rows.Count > 0)
                {
                    lblFile.Text = dtbvisitor.Tables[0].Rows[0][0].ToString();
                }
            }
         
            ds = eform.Addcountnoc(lblFile.Text.ToString(), Session["vcUserName"].ToString());
            lblPS.Text = Session["PSDesc"].ToString();
            lblSate.Text = Session["StateDesc"].ToString();
            companyname.Text = Session["companyname"].ToString();
            InsuranceName.Text = Session["InsuranceName"].ToString();
            InsuranceAddress.Text = Session["InsuranceAddress"].ToString();
            string _stateCode = Session["vcStateCode"].ToString();
            captcha.Text = Session["strRandom"].ToString() + " " + Session["visitor"].ToString();
            dt = eform.getCounterHeader1(Session["vcUserName"].ToString());

            if (dt.Tables[0].Rows.Count > 0)
            {
                lblFooter.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
            }

            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;
            lblModel.Text = _model;
            lblColor.Text = _color;

            if (Session["MatchStatus"].ToString() == "1")
            {
                lblMsg.Text = " is linked with vehicle having following description";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();

                    rptAddress.DataSource = dv;
                    rptAddress.DataBind();
                    toaddress.Visible = false;
                    String profilename;
                    String receipients;
                    String subject;
                    String bodytext;
                    profilename = "SQLProfile";
                    subject = "Vahan Samanvay Motor Vehicle Coordination";
                    receipients = "";

                    String bt;
                    bt = "";
                    foreach (DataRow dtRow in dv.Rows)
                    {
                        // on all table's columns
                        foreach (DataColumn dc in dv.Columns)
                        {
                            if (dc.ColumnName.ToString() == "STCODE")
                            {
                                String std;
                                std = dtRow[dc].ToString();
                                dtemail = eform.getemail(std);
                                receipients = receipients + dtemail.Tables[0].Rows[0]["vcMail"].ToString();
                            }
                            else
                            {
                                bt = bt + dc.ColumnName.ToString() + ": " + dtRow[dc].ToString() + "(.) ";
                            }
                        }

                    }
                    bodytext = companyname.Text +" "+ InsuranceName.Text +" " + InsuranceAddress.Text + " enquired the particular of Vehicle Type " + Session["VehicleTypedesc"] + "(.)Make " + Session["MakeDesc"] + "(.)Registration " + Session["Registration"] + "(.)Chasis " + Session["Chasis"] + "(.)Engine " + Session["Engine"] + "(.)Color " + Session["Color"] + "(.)Year of Manufacture " + Session["Model"] + " is linked with " + bt;
                    DataSet dsmale = new DataSet();
                    dsmale = eform.sendmail(profilename, receipients, subject, bodytext);

                }
                else
                {
                    toaddress.Visible = false;
                    lblMsg.Text = Session["Reportedstolen"] + " This vehicle is not recovered yet.";
                }
            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                toaddress.Visible = false;
                lblMsg.Text = Session["Reportedstolen"] + " is not recovered yet.";
            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                toaddress.Visible = false;
                lblMsg.Text = " has not been reported as stolen by police.";
            }

            else
            {
                toaddress.Visible = false;
                lblMsg.Text = " is not found in the database.";
            }
        }
        catch
        {
            Response.Redirect("ERROR.aspx");
        }
    }
    public string printsho()
    {
        string strsho ="SHO";
        return strsho;
    }

    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }

  //this gets the ip address of the server pc



}