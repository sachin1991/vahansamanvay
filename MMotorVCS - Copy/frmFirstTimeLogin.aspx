﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmFirstTimeLogin.aspx.cs" SmartNavigation="true" Inherits="frmFirstTimeLogin" Title="Vahan Samanvay First Time Login Page" EnableViewState="true"  EnableViewStateMac="true"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<head id="Head1" runat="server">
    <title>Vahan Samanvay:- First Time Login Password</title>
     <link href="style/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 27px;
        }
        .combo
        {}
        .style3
        {
            height: 27px;
            text-align: left;
        }
        .style5
        {
            height: 30px;
        }
        .style6
        {
            text-align: left;
            height: 31px;
        }
        .style7
        {
            height: 31px;
        }
        .style8
        {
            height: 29px;
            text-align: left;
        }
        .style9
        {
            height: 29px;
        }
        .style10
        {
            text-align: left;
            height: 35px;
        }
        .style11
        {
            height: 35px;
        }
        .style12
        {
            text-align: center;
        }
    </style>
</head>
<body ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
    <form id="form1" runat="server" autocomplete="off">
<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(e) {
        if (event.button == 2) {
            alert("Right Click Disabled");
            return false;
        }
    }
</script>
<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.password-strength.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var myPlugin = $("[id$='txtPassword']").password_strength();

        $("[id$='btnSubmit']").click(function () {
            return myPlugin.metReq(); //return true or false
        });

        $("[id$='passwordPolicy']").click(function (event) {
            var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
            window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
            event.preventDefault();
            return false;
        });

    });
</script>
<SCRIPT language="javascript" type="text/javascript">
    function ValidateTPACode() {
        if (document.getElementById("<%=ddQues1.ClientID%>").selectedIndex == 0) {
            alert("Select Question 1");
            document.getElementById("<%=ddQues1.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=ddQues2.ClientID%>").selectedIndex == 0) {
            alert("Select Question 2");
            document.getElementById("<%=ddQues2.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtAns1.ClientID%>").value == "") {
            alert("Please enter Ans 1");
            document.getElementById("<%=txtAns1.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtAns2.ClientID%>").value == "") {
            alert("Please enter Ans 2");
            document.getElementById("<%=txtAns2.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
            alert("Enter User Name");
            document.getElementById("<%=txtLoginCode.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
            alert("Enter Password");
            document.getElementById("<%=txtPassword.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtConPass.ClientID%>").value == "") {
            alert("Enter Password for Confirmation");
            document.getElementById("<%=txtConPass.ClientID%>").focus();
            return false;
        }

        return true;
    }    
    </SCRIPT>
<script language="javascript" type="text/javascript">
    function valid() {

        if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
            alert("Enter User Name");
            document.getElementById("<%=txtLoginCode.ClientID%>").focus();
            return false;
        }



        if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
            alert("Enter Password");
            document.getElementById("<%=txtPassword.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtConPass.ClientID%>").value == "") {
            alert("Enter Password for Confirmation");
            document.getElementById("<%=txtConPass.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtPassword.ClientID%>").value != "" || document.getElementById("<%=txtConPass.ClientID%>").value != "") {
            if (document.getElementById("<%=txtPassword.ClientID%>").value != document.getElementById("<%=txtConPass.ClientID%>").value) {
                alert("Password and Confirm Password Doesn't match");
                document.getElementById("<%=txtPassword.ClientID%>").focus();
                return false;
            }
        }


        if (document.getElementById("<%=ddUserRole.ClientID%>").selectedIndex == 0) {
            alert("Select User Role");
            document.getElementById("<%=ddUserRole.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
            alert("Enter Logon Name");
            document.getElementById("<%=txtLoginCode.ClientID%>").focus();
            return false;
        }

        if (document.getElementById("<%=ddStateName.ClientID%>").selectedIndex == 0) {
            alert("Select State Name");
            document.getElementById("<%=ddStateName.ClientID %>").focus();
            return false;
        }
        return true;
    }    
    </script>
    <script language="javascript" type="text/javascript">

        function SelectDropDown() {

            if (document.getElementById("<%=ddUserRole.ClientID%>").value == 4 || document.getElementById("<%=ddUserRole.ClientID%>").value == 5 || document.getElementById("<%=ddUserRole.ClientID%>").value == 6 || document.getElementById("<%=ddUserRole.ClientID%>").value == 6 || document.getElementById("<%=ddUserRole.ClientID%>").value == 7 || document.getElementById("<%=ddUserRole.ClientID%>").value == 3) {

                document.getElementById("<%=tblRow6.ClientID%>").style.display = '';
                document.getElementById("<%=tblRow7.ClientID%>").style.display = '';
                document.getElementById("<%=tblRow8.ClientID%>").style.display = '';

                return false

            }
            else {

                document.getElementById("<%=tblRow6.ClientID%>").style.display = 'none';
                document.getElementById("<%=tblRow7.ClientID%>").style.display = 'none';
                document.getElementById("<%=tblRow8.ClientID%>").style.display = 'none';

                return true
            }

            if (document.getElementById("<%=ddUserRole.ClientID%>").value == 2 || document.getElementById("<%=ddUserRole.ClientID%>").value == 5) {

                document.getElementById("<%=TableRow4.ClientID%>").style.display = '';
                return false

            }
            else {

                document.getElementById("<%=TableRow4.ClientID%>").style.display = 'none';
                return true
            }


        }

        function validation() {

            if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
                alert("Enter User Name");
                document.getElementById("<%=txtLoginCode.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtPassword.ClientID%>").value != document.getElementById("<%=txtConPass.ClientID%>").value) {
                alert("Password and Confirm Password Doesn't match");
                document.getElementById("<%=txtPassword.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=ddUserRole.ClientID%>").selectedIndex == 0) {
                alert("Select User Role");
                document.getElementById("<%=ddUserRole.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
                alert("Enter Logon Name");
                document.getElementById("<%=txtLoginCode.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=ddStateName.ClientID%>").selectedIndex == 0) {
                alert("Select State Name");
                document.getElementById("<%=ddStateName.ClientID %>").focus();
                return false;
            }
            return true;
        }

        function validationsetup() {


            return true;
        }    



    </script>   
       <table class="contentmain">
            <tr> <td>
             <table class="tablerowcolor" align="center" width="100%">
             <tr>
                <td colspan="4" > <asp:Image ID="Image1" runat="server" 
                        AlternateText="NCRB CCTNS Image" ImageUrl="~/img/BlueVSHead3.jpg" Width="100%"/>
               </td> </tr>

                    <tr>
                        <td style="height: 10px" class="style12" >
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                            <strong>First Time Login Details and Registration and Password Setting</strong></td>
                    </tr>

                    </table>
                    <table class="tablerowcolorcenter" align="center" width="60%">
<asp:TableRow  ID="TableRow16"  runat="server" Width="70%" >
<asp:TableCell Width="24%" ID="TableCell31" runat="server" align="right" >User ID :
</asp:TableCell>
<asp:TableCell ID="TableCell32" runat="server" align="center" >&nbsp;
<asp:TextBox ID="txtLoginCode" runat="server" MaxLength="12"  Width="245px" 
ontextchanged="txtLoginCode_TextChanged"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow51" runat="server" Width="70%">
<asp:TableCell Width="24%" align="right" ID="TableCell51a" runat="server" >User Role :
</asp:TableCell>
<asp:TableCell ID="TableCell51c" runat="server" >&nbsp;
<asp:DropDownList id="ddUserRole" runat="server" align="center" Width="245px" ></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow4" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell4a" runat="server" align="right">Activate/Deactivate :
</asp:TableCell>
<asp:TableCell ID="TableCell4c" runat="server" >&nbsp;
<asp:DropDownList id="ddAcDeac" runat="server" align="center" Width="245px">
<asp:ListItem Value="1">Active</asp:ListItem>
<asp:ListItem Value="0">Deactive</asp:ListItem>
</asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow5" runat="server" Width="70%" Visible = "false" >
<asp:TableCell Width="24%" ID="TableCell9" runat="server" align="right" >User Role :
</asp:TableCell>
<asp:TableCell ID="TableCell10" runat="server" >&nbsp;
<asp:DropDownList id="DropDownList1" runat="server" align="center" Width="245px"></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow6" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell6a" runat="server" align="right" >State :
</asp:TableCell>
<asp:TableCell ID="TableCell6c" runat="server" >&nbsp;
<asp:DropDownList id="ddStateName" runat="server" align="center" Width="245px" AutoPostBack="true" OnSelectedIndexChanged="ddStateName_SelectedIndexChanged"></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow7" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell7a" runat="server" align="right">District :
</asp:TableCell>
<asp:TableCell ID="TableCell7c" runat="server" >&nbsp;
<asp:DropDownList id="ddDistrictName" runat="server" AutoPostBack="true" align="center" Width="245px" OnSelectedIndexChanged="ddDistrictName_SelectedIndexChanged"></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow8" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell8a" runat="server" align="right" >Police Station :
</asp:TableCell>
<asp:TableCell ID="TableCell8c" runat="server" >&nbsp;
<asp:DropDownList id="ddPsName" runat="server" AutoPostBack="true" align="center" Width="245px"></asp:DropDownList></asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow9" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell9a" runat="server" align="right" >E-Mail :
</asp:TableCell>
<asp:TableCell ID="TableCell9c" runat="server" >&nbsp;
<asp:TextBox id="txtemail" runat="server" align="center" Width="245px"></asp:TextBox> <br />
<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ErrorMessage="Email is not in proper format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail" SetFocusOnError="true" ValidationGroup="ctr"></asp:RegularExpressionValidator>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow1" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell1" runat="server" align="right" >File Number :
</asp:TableCell>
<asp:TableCell ID="TableCell2" runat="server" >&nbsp;
<asp:TextBox id="txtFileNo" runat="server" align="center" Width="245px"></asp:TextBox> <br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please enter File No" ControlToValidate="txtFileNo"></asp:RequiredFieldValidator>
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="InvalidChars" InvalidChars="!+=\@#$%^&amp;*{}[].,:'<>" TargetControlID="txtFileNo">
                    </cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow6" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell11" runat="server" align="right" style="color: #800000">Office Name :
</asp:TableCell>
<asp:TableCell ID="TableCell12" runat="server" >&nbsp;
<asp:TextBox id="txtHeader" runat="server" align="center" Width="245px" Rows=4 Textmode="MultiLine" ClientIdMode="static"></asp:TextBox><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please enter Office Name" ControlToValidate="txtHeader"></asp:RequiredFieldValidator>
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=\@#$%^&amp;*{}[].,:'<>" TargetControlID="txtHeader">
                    </cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow2" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell3" runat="server" align="right"  style="color: #800000">Address :
</asp:TableCell>
<asp:TableCell ID="TableCell4" runat="server" >&nbsp;
<asp:TextBox id="txtAddress1" runat="server" align="center" Width="245px" Rows=4 Textmode="MultiLine" ClientIdMode="static"></asp:TextBox><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter Address" ControlToValidate="txtAddress1"></asp:RequiredFieldValidator>
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="!+=\@#$%^&amp;*{}[].,:'<>" TargetControlID="txtAddress1">
                    </cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow7" runat="server" Width="70%" Visible = "false">
<asp:TableCell Width="24%" ID="TableCell13" runat="server" align="right" >Company Office :
</asp:TableCell>
<asp:TableCell ID="TableCell14" runat="server" >&nbsp;
<asp:DropDownList id="insurancecompany" runat="server" AutoPostBack="true" align="center" Width="245px"></asp:DropDownList></asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow8" runat="server" Width="70%" Visible = "false">
<asp:TableCell Width="24%" ID="TableCell15" runat="server" align="right" >Company Address :
</asp:TableCell>
<asp:TableCell ID="TableCell16" runat="server" >&nbsp;
<asp:TextBox id="insuranceaddress" runat="server" align="center" Width="245px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>


<asp:TableRow  ID="TableRow3" runat="server" Width="70%" Visible = "true" >
<asp:TableCell Width="24%" ID="TableCell5" runat="server" align="right">Authority :
</asp:TableCell>
<asp:TableCell ID="TableCell6" runat="server" >&nbsp;
<asp:TextBox id="txtAuthority1" runat="server" align="center" Width="245px" Text="Duty Officer"></asp:TextBox><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter Authority" ControlToValidate="txtAuthority1"></asp:RequiredFieldValidator>
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars" InvalidChars="!+=\@#$%^&amp;*{}[].,:'<>" TargetControlID="txtAuthority1">
                    </cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow4" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell7" runat="server" align="right">Counter Charges :
</asp:TableCell>
<asp:TableCell ID="TableCell8" runat="server" >&nbsp;
<asp:TextBox id="txtAmount" runat="server" align="center" Width="245px" Text="50"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow9" runat="server" Width="70%" Visible="false" >
<asp:TableCell Width="24%" ID="TableCell17" runat="server" align="right">Signature :
</asp:TableCell>
<asp:TableCell ID="TableCell18" runat="server" >&nbsp;
<asp:TextBox id="txtSignature" runat="server" align="center" Width="245px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<tr>
<td colspan=2>Use these Question and answer to recover your password at a later date when you forget password</td>
</tr>

<asp:TableRow  ID="TableRow10" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell19" runat="server" align="right" style="color: #800000">Security Question1 :
</asp:TableCell>
<asp:TableCell ID="TableCell20" runat="server" >&nbsp;
<asp:DropDownList ID="ddQues1" CssClass="combo" runat="server" Height="23px" 
    style="margin-left: 0px" align="center" Width="245px" 
    onselectedindexchanged="ddQues1_SelectedIndexChanged">
</asp:DropDownList> 
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow11" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell21" runat="server" align="right" style="color: #800000">Security Answer :
</asp:TableCell>
<asp:TableCell ID="TableCell22" runat="server" >&nbsp;
<asp:TextBox ID="txtAns1" CssClass="combo" runat="server"  align="center" Width="245px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow12" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell23" runat="server" align="right" style="color: #800000">Security Question2 :
</asp:TableCell>
<asp:TableCell ID="TableCell24" runat="server" >&nbsp;
<asp:DropDownList ID="ddQues2" CssClass="combo" runat="server" Height="23px" 
    style="margin-left: 0px" align="center" Width="245px">
</asp:DropDownList>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow13" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell25" runat="server" align="right" style="color: #800000" >Security Answer2 :
</asp:TableCell>
<asp:TableCell ID="TableCell26" runat="server" >&nbsp;
<asp:TextBox ID="txtAns2" CssClass="combo" runat="server"  align="center" Width="245px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow14" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell27" runat="server" align="right" style="color: #800000">Password :
</asp:TableCell>
<asp:TableCell ID="TableCell28" runat="server" >&nbsp;
<asp:TextBox ID="txtPassword" CssClass="combo" runat="server" 
TextMode="Password" MaxLength="20"  align="center" Width="245px" autocomplete="off"  ValidationGroup="valid"></asp:TextBox>

</asp:TableCell>
</asp:TableRow> 
<asp:TableRow  ID="TableRow15" runat="server" Width="70%"  >
<asp:TableCell ColumnSpan="2">
<asp:RegularExpressionValidator ID="Regex4" runat="server" ControlToValidate="txtPassword"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red"  ValidationGroup="valid"/>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow15a" runat="server" Width="70%"  >
<asp:TableCell Width="24%" ID="TableCell29" runat="server" align="right" style="color: #800000">Confirm Password :
</asp:TableCell>
<asp:TableCell ID="TableCell30" runat="server" >&nbsp;
<asp:TextBox ID="txtConPass" CssClass="combo" runat="server" 
TextMode="Password"  MaxLength="20"  align="center" Width="245px" ValidationGroup="valid"></asp:TextBox>

</asp:TableCell>
</asp:TableRow> 
<asp:TableRow  ID="TableRow15b" runat="server" Width="70%"  >
<asp:TableCell ColumnSpan="2"><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtConPass"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red"  ValidationGroup="valid"/></asp:TableCell>
</asp:TableRow>
                                <tr>
                                    <td align="center" colspan="2" class="rB" >
                                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" class="rB" >
                                        <asp:Button ID="btnLogin" CssClass="btn" Text="Update" OnClientClick="javascript:return ValidateTPACode();"
                                            runat="server" onclick="btnLogin_Click" Causesvalidation="true"  ValidationGroup="valid" />
                                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" 
                                            onclick="btnRefresh_Click" Visible="False" />
                                        <asp:Button ID="relogin" runat="server" onclick="relogin_Click" Text="Re-Login" 
                                            Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" class="rB" >
                                        &nbsp;</td>
                                </tr>
                                
                                <tr id="trRrrMsg"   class="rB" style="height: 25px;" runat="server">
                                    <td align="center" colspan="2" class="errortext">
                                         <cc1:PasswordStrength ID="ps" TargetControlID="txtPassword" runat = "server"  PreferredPasswordLength="13"></cc1:PasswordStrength>
                                         <cc1:PasswordStrength ID="PasswordStrength1" TargetControlID="txtConPass" runat = "server"  PreferredPasswordLength="13"></cc1:PasswordStrength>                                        
                                        <br />

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ErrorMessage="Office Name is required" ControlToValidate="txtHeader" validationGroup="ctr"></asp:RequiredFieldValidator>

                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                            ControlToValidate="txtAddress1" ErrorMessage="Address Required" validationGroup="ctr"></asp:RequiredFieldValidator>
                                         <cc1:ConfirmButtonExtender ID="cbtn" runat="server" ConfirmText="Please Re-login with the new password" TargetControlID="btnLogin" Enabled="false" ></cc1:ConfirmButtonExtender>
                                    </td>
                                </tr>
                                

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        </form>
</body>
</html>