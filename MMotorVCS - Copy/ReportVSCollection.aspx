﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="false" AutoEventWireup="true" CodeFile="ReportVSCollection.aspx.vb" Inherits="ReportVSCollection" Title="Vahan Samanvay Report" Theme="emsTheme"  EnableViewState="true"  EnableViewStateMac="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <Triggers> 
<asp:PostBackTrigger ControlID="Button1" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 
    <ContentTemplate>
            <table class="contentmain">
                <tr>
                <td><table class="tablerowcolor" align="center" width="80%">
                <tr class="heading">
                    
                    <td colspan="3" >
                        <asp:Label ID="Label3" runat="server" Text="Collection Report" Font-Bold="True"></asp:Label></td>
                   
                </tr>
                <tr>
                    <td style="width: 100px">
                        User</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="Usernam" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="ListDatefrom" runat="server" ValidationGroup="Date"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender runat="server" ID="datefrom" 
                            TargetControlID="Listdatefrom" Format="dd/MM/yyyy" 
                     PopupPosition="Right"   ></ajaxtoolkit:calendarextender>
                        <asp:RadioButton ID="IPOList" runat="server" GroupName="CASHIPO" 
                            Text="IPO List" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="listDateto" runat="server"  ValidationGroup="Date"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender ID="dateto" runat="server" 
                            TargetControlID="ListDateTo" Format="dd/MM/yyyy" 
                     PopupPosition="Right"  ></ajaxtoolkit:calendarextender>
                        <asp:RadioButton ID="IPOAddressList" runat="server" GroupName="CASHIPO" 
                            Text="IPO Address List" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    <asp:RadioButton ID="Cash" Text ="Cash" runat="server" groupname ="CASHIPO" Checked="true"  />
                    <asp:RadioButton ID="IPO" Text = "IPO" runat="server" groupname="CASHIPO"/>
                        <asp:Button ID="Button1" runat="server" Text="View" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" />
                        </td>
                    <td style="width: 100px">
                        <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Msglabel" runat="server"></asp:Label>
                    </td>
                    <td style="width: 100px">
                    <asp:Button ID="Converttoexecl" runat="server" visible="false" 
                            Text="Generate to excel (.csv) format file" />
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
            </td> </tr> </table>
 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
