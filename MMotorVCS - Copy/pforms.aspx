<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="pforms.aspx.cs" Inherits="MenuForm_pforms" Title="Vahan Samanvay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <table class="contentmain"> <tr> <td> 
<table width="80%" align="center" class="tablerowcolor">
<TBODY>
<TR>
<TD colspan="2" class="heading">
Menu Forms 
</TD>
</TR>
<TR>
<TD style="HEIGHT: 17px; TEXT-ALIGN: left" colspan="2">
<asp:Label id="lblchldPrntId" runat="server" Visible="False"></asp:Label> 
<asp:Label id="lblSubchldPrntId" runat="server" Visible="False"></asp:Label>
</TD>
</TR>
<TR>
<TD style="HEIGHT: 17px; TEXT-ALIGN: left" colspan="2">
<asp:Label id="lblmsg" runat="server" __designer:wfdid="w100"></asp:Label>
</TD>
</TR>
<TR>
<TD colspan="2"><h4>Root Menu</h4></TD>
</TR>
<TR><TD colspan="2">
<asp:GridView id="gvForms" runat="server" Width="97%" 
        OnRowDeleting="gvForms_RowDeleting" GridLines="Vertical" ForeColor="Black" 
        CellPadding="4" ShowFooter="True" OnRowUpdating="gvForms_RowUpdating" 
        OnRowEditing="gvForms_RowEditing" OnRowCommand="gvForms_RowCommand" 
        OnRowCancelingEdit="gvForms_RowCancelingEdit" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
                    <Columns>                        
                        <asp:TemplateField HeaderText="FORM&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFormName" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtFormName"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFormName1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11a" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtFormName1"></cc1:FilteredTextBoxExtender>                   
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="MAIN&#160;ID">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMainId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtMainId"></cc1:FilteredTextBoxExtender>                    
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMainId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtMainId1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1a" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtMainId1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;LINK">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1b" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@$%^&amp;*(){}[],:'<>" TargetControlID="txtFormLink"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFormLink1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1c" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@$%^&amp;*(){}[],:'<>" TargetControlID="txtFormLink1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SORT&#160;ORDER">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1d" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtSortOrder"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtSortOrder1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1e" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtSortOrder1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit / Delete"
                            ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="lbInsert" runat="server" CommandName="Insert" ImageUrl="~/img/updt_icon.gif"
                                    ToolTip="Insert" Visible="false" />
                                <asp:Button ID="lbAdd" runat="server" CausesValidation="False" CommandName="Add"
                                    CssClass="button" Text="Add" ToolTip="Add New" />
                                <asp:ImageButton ID="lbCancel" runat="server" CausesValidation="False" CommandName="CancelInsert"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" Visible="false" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />&nbsp;&nbsp;
                                <asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False" CommandName="Delete"
                                    ImageUrl="~/img/delete_icon.jpg" ToolTip="Delete" />    
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="Child">
                    <ItemTemplate>
                    <asp:LinkButton  runat="server" ID="snum"  CommandName='<%# Container.DataItemIndex %>' ForeColor="Green" Text="Child&nbsp;Forms"></asp:LinkButton>        
                                         
                    </ItemTemplate>
                            
                    </asp:TemplateField>                        
                    </Columns>                    
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> &nbsp; </TD></TR>
           
<TR> 
<TD style="HEIGHT: 15px" colspan="2"></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: left" colspan="2"><asp:Label id="lblChild" runat="server"></asp:Label>
</TD>
</TR>
<TR>
<TD colSpan=2><asp:GridView id="gvChild" runat="server" Width="97%" 
        GridLines="Vertical" ForeColor="Black" CellPadding="4" ShowFooter="True" 
        OnRowUpdating="gvChild_RowUpdating" OnRowEditing="gvChild_RowEditing" 
        OnRowCommand="gvChild_RowCommand" OnRowCancelingEdit="gvChild_RowCancelingEdit" 
        AutoGenerateColumns="False" OnRowCreated="gvChild_RowCreated" BackColor="White" 
        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
                <Columns>
                    <asp:TemplateField HeaderText="FORM&#160;ID">
                        <ItemTemplate>
                            <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FORM&#160;NAME">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtFormName1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PARENT&#160;ID">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtParentId" runat="server" Text='<%# Eval("inParentId") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblParentId" runat="server" Text='<%# Eval("inParentId") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtParentId1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="LEVEL&#160;ID">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLevelId" runat="server" Text='<%# Eval("inLevelId") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLevelId" runat="server" Text='<%# Eval("inLevelId") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtLevelId1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MAIN&#160;ID">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMainId" runat="server" Text='<%# Eval("inMainId") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMainId" runat="server" Text='<%# Eval("inMainId") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtMainId1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FORM&#160;LINK">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtFormLink1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SORT&#160;ORDER">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtSortOrder1" runat="server" Visible="false"></asp:TextBox>
                        </FooterTemplate>
                        <ItemStyle Width="160px" />
                        <FooterStyle Width="160px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit / Delete"
                            ShowHeader="False">
                        <EditItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                            <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="lbInsert" runat="server" CommandName="Insert" ImageUrl="~/img/updt_icon.gif"
                                    ToolTip="Insert" Visible="false" />
                            <asp:Button ID="lbAdd" runat="server" CausesValidation="False" CommandName="Add"
                                    CssClass="button" Text="Add" ToolTip="Add New" />
                            <asp:ImageButton ID="lbCancel" runat="server" CausesValidation="False" CommandName="CancelInsert"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" Visible="false" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />
                            &nbsp;
                            <asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False" CommandName="Delete"
                                    ImageUrl="~/img/delete_icon.jpg" ToolTip="Delete" />
                        </ItemTemplate>
                        <ItemStyle Width="100px" />
                        <FooterStyle Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Child">
                        <ItemTemplate>
                            <asp:LinkButton ID="snum" runat="server" CommandName='<%# Container.DataItemIndex %>' ForeColor="Green"
                                Text="Child&nbsp;Forms"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <RowStyle BackColor="#F7F7DE" />
                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView> </TD></TR>
            
            
<TR>
<TD style="HEIGHT: 17px" colSpan=2>
</TD>
</TR>

<TR>
<TD style="TEXT-ALIGN: left" colSpan=2><asp:Label id="lblSubChild" runat="server"></asp:Label>
</TD>
</TR>

<TR>
<TD colSpan=2><asp:GridView id="gvSubChild" runat="server" Width="97%" 
        GridLines="Vertical" ForeColor="Black" CellPadding="4" ShowFooter="True" 
        OnRowUpdating="gvSubChild_RowUpdating" OnRowEditing="gvSubChild_RowEditing" 
        OnRowCommand="gvSubChild_RowCommand" 
        OnRowCancelingEdit="gvSubChild_RowCancelingEdit" AutoGenerateColumns="False" 
        OnRowCreated="gvSubChild_RowCreated" BackColor="White" BorderColor="#DEDFDE" 
        BorderStyle="None" BorderWidth="1px" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField HeaderText="FORM&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFormName1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PARENT&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtParentId" runat="server" Text='<%# Eval("inParentId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblParentId" runat="server" Text='<%# Eval("inParentId") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtParentId1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LEVEL&#160;ID">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLevelId" runat="server" Text='<%# Eval("inLevelId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLevelId" runat="server" Text='<%# Eval("inLevelId") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtLevelId1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MAIN&#160;ID">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtMainId" runat="server" Text='<%# Eval("inMainId") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMainId" runat="server" Text='<%# Eval("inMainId") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtMainId1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;LINK">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblFormLink" runat="server" Text='<%# Eval("vcMenuLink") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFormLink1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SORT&#160;ORDER">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSortOrder" runat="server" Text='<%# Eval("inSortOrder") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtSortOrder1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit / Delete"
                            ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="lbInsert" runat="server" CommandName="Insert" ImageUrl="~/img/updt_icon.gif"
                                    ToolTip="Insert" Visible="false" />
                                <asp:Button ID="lbAdd" runat="server" CausesValidation="False" CommandName="Add"
                                    CssClass="button" Text="Add" ToolTip="Add New" />
                                <asp:ImageButton ID="lbCancel" runat="server" CausesValidation="False" CommandName="CancelInsert"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" Visible="false" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />
                                &nbsp;
                                <asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False" CommandName="Delete"
                                    ImageUrl="~/img/delete_icon.jpg" ToolTip="Delete" />
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>                        
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> 
                </TD>
                </TR>
                <TR>
                <TD colspan="2"></TD>
                </TR>
                <TR>
                <TD style="TEXT-ALIGN: left" align=center></TD>
                </TR>
                <TR><TD align="left"><IMG alt="Edit" src="img/edit_icon.gif" />&nbsp; Edit Data&nbsp; 
                                    <IMG alt="Update" src="img/updt_icon.gif" />&nbsp; Submit Data&nbsp;
                                     <IMG alt="Cancel" src="img/cancel_icon.gif" />&nbsp; Cancel &nbsp; 
                                     <IMG alt="Delete" src="img/delete_icon.jpg" />&nbsp; Delete &nbsp; 
                </TD>
                </TR>
                </TBODY>
                </TABLE>

     </td></tr></table>
</ContentTemplate>
    </asp:UpdatePanel></center>
</asp:Content>

