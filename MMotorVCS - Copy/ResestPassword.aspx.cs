using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

public partial class ResestPassword : System.Web.UI.Page
{
    static int user_id;
    static string _userid;
    DataSet ds = new DataSet();
 
    static int pagecount = 0;
    int PostCount = 0;
    private string vcStateCode;
    private string vcStateDesc;
    private int _roleId;
    CommonFunctions eform = new CommonFunctions();
    static int randomNumber;
    Class1 myclass = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            
            Response.Redirect("LogoutModule.aspx");
        }

        if (Session["vcStateCode"] == null)
        {


        }
        else
        {
            vcStateCode = Session["vcStateCode"].ToString();
            _roleId = Convert.ToInt32(Session["inRoleId"]);

        
        }
        string a;
        a = Session["inRoleid"].ToString();
        if ( a != "1")
        {
            Response.Redirect("LogoutModule.aspx");
        }

       

        lblmsg.Text = "";
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

       
        if (!IsPostBack)
        {
            //btnReset.Visible = true;

            //Random autoRand = new Random();
            //randomNumber = Convert.ToInt32(autoRand.Next(0, 36));
            //String rno = randomNumber.ToString();
            //btnUpdate.Attributes.Add("onClick", "return EncryptResetPassword(" + "'" + rno + "'" + ");");
            
        }
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "10":
                break;
            case "12":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }


    }

    public void bindUserName()
    {
        try
        {


        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }

     
    protected void btnReset_Click(object sender, EventArgs e)
    {

       refresh();
    }
    protected void refresh()
    {
        txtPassword.Text = "";
        txtConPass.Text = "";
        ddUsername.Text = "";
        
    }

    protected void refresh1()
    {
        txtPassword.Text = "";
        txtConPass.Text = "";
        ddUsername.Text = "";
       
    }
    protected void TextValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (args.Value.Length >= 12);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (IsValid)
       {
           try
           {
               Random autoRand = new Random();
               randomNumber = Convert.ToInt32(autoRand.Next(0, 36));
               String rno = randomNumber.ToString();              
               int flag = 0;
               string p1 = myclass.md5(txtPassword.Text.ToString().Trim());
               string p2 = myclass.md5(txtConPass.Text.ToString().Trim());
               {
                   if (chkEnable.Checked == true)
                   {
                       if (eform.ResetUser_Password(ddUsername.Text.ToString(), p1.ToString(), true, Session["vcUserName"].ToString()) > 0)
                       {

                           lblmsg.Text = "<font color=green>Updated Successfully !!!</font>";
                           //refresh1();

                       }
                       else
                       {

                           lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again.Continue after Pressing CLEAR button.</font>";


                       }
                   }
                   else
                   {
                       if (eform.ResetUser_Password(ddUsername.Text.ToString(), txtPassword.Text.ToString().ToUpper().Trim(), false, Session["vcUserName"].ToString()) > 0)
                       {

                           lblmsg.Text = "<font color=green>Updated Successfully !!!</font>";
                           //refresh1();
                       }
                       else
                       {

                           lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again After Pressing CLEAR button.</font>";


                       }
                   }

               }
           }
           catch (SqlException)
           { lblmsg.Text = "Occuring Problem in connectivity to database."; }
           catch (HttpCompileException)
           { lblmsg.Text = "System Occuring Problem."; }
           catch (HttpParseException)
           { lblmsg.Text = "Internet Browser Occuring Problem."; }
           catch (Exception ex)
           {
               lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
           }
    }
    }

    protected bool checkusername(string name)
    {
        try
        {
            ds = eform.Login_User(name);
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void gvUserDetails_RowdataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {



        }
    }
}