﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="AuthorityNCRBReports.aspx.cs" Inherits="AuthorityNCRBReports" Theme="emsTheme"  ValidateRequest="true"%>
<html>

<head id="Head1" runat="server">
    <title></title>

    <script language="javascript" type="text/javascript">
        function PrintReport() {
            document.getElementById("btnPrint").style.display = "hidden"
            window.print();

        }
        

    </script>
    <style type="text/css">
        .style5
        {
        }
        .style9
        {
        }
        .style11
        {
        }
        .style12
        {
        }
        .style13
        {
        }
        .style15
        {
            text-decoration: underline;
        }
        .style16
        {
            font-size: medium;
            text-align: center;
        }
       
       
        .style17
        {
            text-align: right;
        }
               
       
        .style18
        {
            text-align: center;
            text-decoration: underline;
        }
               
       
        .style19
        {
            text-align: center;
        }
               
       
        </style>
</head>
<body onunload="return window_onunload()" ">
    <form id="form1" runat="server">
    <table width=100%>
    <tr>
    <td>
    <div align="center" >
       <asp:Panel ID="Panel1" runat="server"  Width="80%" style="background-position:center; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg" >
            <br />
            <table align="center" width="100%">
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                        Reference No.
                        <asp:Label ID="lblFile" runat="server" Text="1"></asp:Label>
                    </td>
                    <td style="text-align: right; font-size: 12pt; font-family=Arial">
                        Date:<asp:Label ID="DateText" runat="server"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td style="text-align: center; font-size: 12pt; font-family=Arial" colspan="2">
                            <strong><span class="style16">National Crime Records Bureau</span></strong><br /> (Ministry of Home Affairs)<br />
                            <br />
                            <span class="style15"><strong>Motor Vehicle Coordincation Report </strong></span></td>
                </tr>
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial" colspan="2">
                        1.&nbsp;&nbsp; Particular of Vehicle</td>
                </tr>
              
                <tr>
                
                
                    <td style="text-align: left; font-size: 8pt; height: inherit " colspan="2">
                        &nbsp;</td>
                             
                </tr>
                <tr>
                    <td style="text-align: left; width:100%; font-size: 12pt; font-family=Arial"" 
                        colspan="2">
                        <table border="1" cellpadding="0" cellspacing="0" width="100%" >
                            <tr>
                                <td >
                                    <strong>Registration No</strong></td>
                                <td>
                                    <asp:Label ID="lblregistrationno" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Chassis No</strong></td>
                                <td>
                                    <asp:Label ID="lblchasisno" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Engine No</strong></td>
                                <td>
                                    <asp:Label ID="lblengineno" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Manufacturing Year</strong></td>
                                <td>
                                    <asp:Label ID="lblModel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Vehicle</strong></td>
                                <td>
                                    <asp:Label ID="lblvectype" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Make</strong></td>
                                <td>
                                    <asp:Label ID="lblmake" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Color</strong></td>
                                <td>
                                    <asp:Label ID="lblColor" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                <tr>
                
                
                    <td style="text-align: left; font-size: 8pt; height: inherit " colspan="2">
                        &nbsp;</td>
                             
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" 
                        class="style5" colspan="2">
                        <strong>
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </strong></td>
                </tr>
                <tr>
                    <td class="style11" 
                        style="text-align: justify;  font-size: 12pt; font-family=Arial;" 
                        colspan="2">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; width: 100%; font-size: 12pt; font-family=Arial" 
                        colspan="2"  >
                        <asp:Repeater ID="Repeater1" runat="server">
                        <HeaderTemplate>
                        <table border="1"  cellpadding="0" cellspacing="0">
                        <tr>
                        <td valign="top"><b>Registraiton number</b></td>
                        <td valign="top"><b>Chasis number</b></td>
                        <td valign="top"><b>Engine number</b></td>
                        <td valign="top"><b>Model</b></td>
                        <td valign="top"><b>Manufacturing Year</b></td>
                        <td valign="top"><b>color</b></td>
                        <td valign="top"><b>Matched on</b></td>
                        <td valign="top"><b>Matched detail</b></td>
                        </tr>
                        </HeaderTemplate>
                            <ItemTemplate>
                                    <tr align="justify" height="25" valign="top">
                                    <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Registration")%>
                                            </td>
                                        <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Chasis")%>
                                     </td>
                                     <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Engine")%>
                                       </td>
                                       <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Vehicle")%> &nbsp;&nbsp;
                                            <%# DataBinder.Eval(Container, "DataItem.Make")%>&nbsp;
                                        </td>
                                       <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Year")%> &nbsp;
                                      </td>
                                       <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Color")%>&nbsp;
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container, "DataItem.Status")%>
                                             <%# DataBinder.Eval(Container, "DataItem.MatchingParam")%>
                                        </td>
                                        <td>
                                                <%--<%# DataBinder.Eval(Container, "DataItem.Source")%>--%>
                                                    <%#DataBinder.Eval(Container, "DataItem.FIR")%>
                                                 <%# DataBinder.Eval(Container, "DataItem.StateName") %>
                                            </td>  
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                            </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                    <tr>
                        <td class="style11" style="text-align: justify;  font-size: 8pt" colspan="2">
                            <span class="style12">Accessed Information Ref. </span><strong><span class="style12"> </span>
                            </strong>
                            <asp:Label ID="lblPrinted" runat="server" CssClass="style12" Font-Size="8pt"></asp:Label>
                            &nbsp;Address:
                            <asp:Label ID="AuthorityOffice" runat="server"></asp:Label>
                        </td>
                    </tr>
                <tr>
                    <td class="style11" style="text-align: justify;  font-size: 8pt" colspan="2">
                        <span class="style13">Data Bank Maintained by National Crime Records Bureau, New 
                        Delhi
                        <asp:Label ID="captcha" runat="server" Visible="False"></asp:Label>
                        &nbsp;Last updated </span>
                        <asp:Label ID="lblUpdated" runat="server" CssClass="style12" Font-Size="8pt"></asp:Label>
                        <strong><span class="style12">&nbsp;</span></strong></td>
                </tr>
                <tr>
                    <td class="style19" style="font-size: 8pt" colspan="2">
                        <span class="style13">
                        <br />
                        <strong>-------------------End of Report-------------</strong></span></td>
                </tr>
                <tr>
                    <td class="style18" colspan="2" >
                        Certificate by printing authority or authorized 
                        officer/official</span></td>
                </tr>
                <tr>
                    <td style="text-align: justify" colspan="2">
                        <br />
                        It is certified that the above mentioned report has been printed by me on the 
                        date and time mentioned above from the NCRB Vahansamanvay portal.</td>
                </tr>
                <tr>
                    <td class="style17" colspan="2">
                        <br />
                        (Signature)<br />
                        <br />
                        Name............................................<br /> 
                        Designation....................................</td>
                </tr>
                <tr>
                    <td class="style9" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td alsign="right" style="text-align: right" colspan="2">
                        <strong style="text-align: right">
                        <br />
                        <asp:Label ID="lblFooter" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                        </strong></td>
                </tr>
                </table>

                <table>
                <tr>
                    <td style="height: 19px;">
                  
                    </td>
                </tr>
                </table>
          
            </asp:Panel>
        <asp:Button ID="btnPrint" runat="server" Text="Print" Visible="false" 
            onclick="btnPrint_Click"/></div>
            </td></tr></table>
            <table align="center">
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial">
                        <asp:Label ID="toaddress" runat="server" style="font-weight: 700" Text="To" 
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial">
                        <asp:Repeater ID="rptAddress" runat="server">
                            <ItemTemplate>
                                <b>SHO:</b> <%# DataBinder.Eval(Container, "DataItem.statename")%>
                                <br />
                                <b>SCRB:</b> <%# DataBinder.Eval(Container, "DataItem.STDESC")%>
                            </ItemTemplate>
                        </asp:Repeater>
                        <br />
                    </td>
                </tr>
                    <tr>
                        <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                            &nbsp;</td>
                    </tr>
              
                </table>

    </form>
    </form>
</body>
</html>
