﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using MMotorVCS;
using System.Net.NetworkInformation;

public partial class PoliceEnquiry : System.Web.UI.Page
{
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    int PostCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {

        }

    
        if (!IsPostBack)
        {
            try
            {
               
            }
            catch
            {
                Response.Redirect("error.aspx");
            }


            bindVehicleType();
            bindStateName();
            bindColName();
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "7": //district
                break;
            case "8": //ncrb
                break;
            case "3": //police station
                break;
            case "6": //state
                break;
            case "10": //Crime Records
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }



    private bool SaveForm(bool blSave)
    {
        try
        {


            if (ddVehTypeName.SelectedIndex == 0)
            {
                Alert("Please select the Vehicle Type to be entered");
                ddVehTypeName.Focus();
                return false;
            }


            return true;

        }
        catch (Exception ex)
        {
            return false;
        }

        finally
        {
        }
    }

    private void Alert(string strErrMsg)
    {

        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";

        if (!ClientScript.IsClientScriptBlockRegistered("ScriptAlert"))
        {

            ClientScript.RegisterStartupScript(typeof(PoliceEnquiry), "ScriptAlert", strScript);

        }

    }
    
    protected void btnSearch_Click2(object sender, EventArgs e)
    {
        try
        {

                      
            bool blnIsSaved = SaveForm(false);
            if (blnIsSaved == true)
            {
                string strStateCode = Session["vcStateCode"].ToString().Trim();
                string strUserName = Session["vcUserName"].ToString().Trim();
                string strHostName = System.Net.Dns.GetHostName();
                string clientName = HttpContext.Current.Request.UserHostAddress ;
                string typeOfQuery;
                string strsource = strStateCode;
                string[] MakeCode = ddMakeName.SelectedValue.ToString().Split('-');
                string Color;
                string VehicleNameDesc;
                string MakeDecs;

                if (ddVehTypeName.SelectedIndex == 0)
                {
                    VehicleNameDesc = "";
                }
                else
                {
                    VehicleNameDesc = ddVehTypeName.SelectedItem.Text.ToString().Trim();
                }

                if (ddMakeName.SelectedIndex == 0)
                {
                    MakeDecs = "";
                }
                else
                {
                    MakeDecs = ddMakeName.SelectedItem.Text.ToString().Trim();
                }
                
                if (ddColName.SelectedIndex == 0)
                {
                    Color = "0";
                }
                else
                {
                    Color = ddColName.SelectedValue.ToString().Split('-')[1];
                }

                Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
                string opRegistration = re.Replace(txtRegistration.Text.ToString().Trim(), "");
                string opChasis = re.Replace(txtChasis.Text.ToString().Trim(), "");
                string opEngine = re.Replace(txtEngine.Text.ToString().Trim(), "");

                {
                    typeOfQuery = "SH";
                    DataSet ds = new DataSet();
                    ds = eform.AddEntry_PoliceEnquiry((ddVehTypeName.SelectedValue.Split('-')[0]), (MakeCode[0]), txtYear.Text.ToString(), Color, GenericMethods.check(opRegistration), GenericMethods.check(opChasis), GenericMethods.check(opEngine), strsource, strHostName, clientName, typeOfQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                     
                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "1";

                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";

                        
                        Response.Write("<script>window.open('PoliceQryOutput.aspx','_blank')</script>");



                    }
                    else
                    {
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;


                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";

                        Session["MatchStatus"] = "3";
                        
                        Response.Write("<script>window.open('PoliceQryOutput.aspx','_blank')</script>");

                    }
                }

            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void btnrefresh_Click(object sender, EventArgs e)
     {
         Response.Redirect(Request.RawUrl);
     }
    


    protected void ddVehTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectVehicleType();
            PostCount++;
        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }
    }

    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }
    }

    public void bindColName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourNameNew();
            ddColName.DataSource = ds;
            ddColName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["colourname"].ColumnName);
            ddColName.DataValueField =GenericMethods.check(ds.Tables[0].Columns["colidcolcode"].ColumnName);
            ddColName.DataBind();
            ddColName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    private void afterSelectState()
    {

    }

    private void afterSelectDistrict()
    {

    }

    private void afterSelectVehicleType()
    {
        if (ddVehTypeName.SelectedIndex != 0)
        {
            bindMakeName();
        }
        else
        {

            ddMakeName.Items.Clear();
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
    }

    public void bindStateName()
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindDistrictName()
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindPsName()
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleTypeNew();
            ddVehTypeName.DataSource = ds;
            ddVehTypeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehTypeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehidvehcode"].ColumnName);
            ddVehTypeName.DataBind();
            ddVehTypeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindMakeName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake_accVehTypeNew(Convert.ToInt32(ddVehTypeName.SelectedValue.Split('-')[1]));
            ddMakeName.DataSource = ds;
            ddMakeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["makename"].ColumnName);
            ddMakeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["makeidmakecode"].ColumnName);
            ddMakeName.DataBind();
            ddMakeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    protected void TextValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (txtRegistration.Text.Length > 0 || txtChasis.Text.Length > 0 || txtEngine.Text.Length > 0);
    }
    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }
    protected void ownershipdetails_Click(object sender, EventArgs e)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
        string opRegistration = re.Replace(txtRegistration.Text.ToString().Trim(), "");
        string opChasis = re.Replace(txtChasis.Text.ToString().Trim(), "");
        string opEngine = re.Replace(txtEngine.Text.ToString().Trim(), "");
        Session["Registration"] = opRegistration;
        Session["Chasis"] = opChasis;
        Session["Engine"] = opEngine;
        Response.Write("<script>window.open('PoliceQryOutputownership.aspx','_blank')</script>");

    }
}