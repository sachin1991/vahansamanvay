<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StateType.aspx.cs" Inherits="StateType"  MasterPageFile="~/MasterPage.master" Theme="emsTheme" MaintainScrollPositionOnPostback="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
<ContentTemplate >
<script language="javascript" type="text/javascript">
    function valid() {
        if (document.getElementById("<%=txtStateCode.ClientID%>").value == "") {
            alert("Enter State Code");
            document.getElementById("<%=txtStateCode.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtStateName.ClientID%>").value == "") {
            alert("Enter State Name");
            document.getElementById("<%=txtStateName.ClientID%>").focus();
            return false;
        }
        return true;
    }
    </script>

 <table class="contentmain"><tr> <td> 
<table class="tablerowcolor" width="80%" align="center">
        <tr>
            <td colspan="2" style="height: 15px">
                <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2" class="heading">
                  State
            </td>
        </tr>
         <tr>
            <td colspan="2">
            </td>
        </tr>

        <tr>
            <td style="width: 108px">
                State Code</td>
            <td style="width: 415px">
                <asp:TextBox ID="txtStateCode" runat="server" MaxLength="6" Width="82px"></asp:TextBox></td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtStateCode"></cc1:FilteredTextBoxExtender>
        </tr>
        <tr>
            <td style="width: 108px">
                State Code in alpha</td>
            <td style="width: 415px">
                <asp:TextBox ID="txtStateCodealpha" runat="server" MaxLength="6" Width="82px"></asp:TextBox></td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtStateCodealpha"></cc1:FilteredTextBoxExtender>
        </tr>
        <tr>
            <td style="width: 108px">
                State Name</td>
            <td style="width: 415px">
                <asp:TextBox ID="txtStateName" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtStateName"></cc1:FilteredTextBoxExtender>
                </td>
        </tr>
        <tr>
            <td style="width: 108px">
            </td>
            <td style="width: 415px">
            </td>
        </tr>
        <tr align="center">
            <td colspan="2" >
                <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add"
                    Width="76px" /><asp:Button ID="btnUpdate" runat="server" CssClass="button" OnClick="btnUpdate_Click"
                        Text="Update" Width="71px" /><asp:Button ID="btnReset" runat="server" CssClass="button"
                            OnClick="btnReset_Click" Text="Reset" Width="76px" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblmsg" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2">
                <h4>
                    State Details</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">                
                    <asp:GridView ID="gvStateType" runat="server" AutoGenerateColumns="False" 
                        OnRowCommand="gvStateType_RowCommand" AllowPaging="True" PageSize="15" OnPageIndexChanging="gvStateType_PageIndexChanging"
                        OnRowCreated="gvStateType_RowCreated" Width="97%" CssClass="Grid" 
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="4" EnableModelValidation="True" ForeColor="Black" 
                        GridLines="Vertical">
                        <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />
                        <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                        <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                            ForeColor="White" />
                        <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                            HorizontalAlign="Right"  />
                        <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                            ForeColor="White" />
                        <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                            PreviousPageText="Previous" />
                        <Columns>
                            <asp:TemplateField HeaderText="S.No.">
                                <ItemTemplate>
                                    <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                        Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Stateid" HeaderText="Id " />
                            <asp:BoundField DataField="statecode" HeaderText="State Code" />
                            <asp:BoundField DataField="statename" HeaderText="State Name" >
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>                
            </td>
        </tr>
    </table>
    </td></tr></table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
