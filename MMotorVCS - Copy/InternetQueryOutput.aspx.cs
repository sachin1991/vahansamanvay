﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;
using System.Security.Cryptography;
using System.Xml;
using System.IO;
using System.Text;

public partial class internetqueryoutput1 : System.Web.UI.Page
{
    public DataTable dv;
    private void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string usrn;
            usrn = "";
            lblUpdationDate.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["InternetVisitor"] + " " + Session["ClientAddr"].ToString().Trim();
            lblenqdatetime.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            string _nameofquery = Session["Nameofquery"].ToString();
            string _datdt1 = System.DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            string _refno = Session["InternetVisitor"].ToString();
            string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
            string _MakeDesc = Session["MakeDesc"].ToString();
            string _Registration = Session["Registration"].ToString();
            string _Chasis = Session["Chasis"].ToString();
            string _Engine = Session["Engine"].ToString();
            Session["newcha"] = "";
            Session["neweng"] = "";
            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;
            Label1.Text = " Status of";
            this.Nameofq.Text = _nameofquery;
            this.Refnoq.Text = _refno;
            this.Datofq.Text = _datdt1.ToString();


            if (Session["MatchStatus"].ToString() == "1")
            {
                lblmsg.Text = " is matched with";
                Label1.Text = " Status of";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();
                }
                else
                {
                    //lblmsg.Text = " is not recovered yet.";
                    lblmsg.Text = " is not recovered yet. ";
                    Label1.Text = " Recovery status of";
                }
            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                //lblmsg.Text = " is not recovered yet.";
                lblmsg.Text = " is not recovered yet. ";
                Label1.Text = " Recovery status of";

            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                lblmsg.Text = " has not been reported as stolen by police.";
                Label1.Text = " Status of";
            }

            else
            {
                lblmsg.Text = " is not found in the database.";
                Label1.Text = " Status of";
            }
        }
        catch
        {
            Response.Redirect("ERROR.aspx");
        }


    }
    public string checkpartialandprintPartialNote()
    {
        if (dv.Rows[0]["MatchingParam"].ToString().Contains("Partial"))
        {
            return "<tr><td colspan='2'>Note: In case of partial match, the particulars should be verified physically by the concerned authority</td></tr>";
        }
        else
        {
            return "";
        }
    }
    public string printstoleninfo()
    {
        if (Session["Stolenavailable"].ToString() == "false")
    {
        return "Please request the concerned police station to update the stolen information in Vahan Samanvay System.  NCRB does not accept update request from General Public/Insurance Investigators.";
    }
        else
    {
        return "";
    }
    }
    public string findowner()
    {
        try
        {
            VahanWSRef.VahanInfo serv = new VahanWSRef.VahanInfo();
            String resp;
            String regis, chas, engi;
            regis = dv.Rows[0]["registration"].ToString();
            chas = dv.Rows[0]["chasis"].ToString();
            engi = dv.Rows[0]["engine"].ToString();
            String response = serv.getDetails("DLNCRB", regis);
            response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
            resp = response;
            string rr;
            rr = accesswebservice.stringoutput1(resp);
            if (rr == "")
            {
                response = serv.getChasisDetails("DLNCRB", chas);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            if (rr == "")
            {
                response = serv.getEngineDetails("DLNCRB", engi);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            if (rr == "Vehicle Data Not Found")
            {
                return "";
            }
            else
            {
                return rr;
            }
        }
        catch
        {
            return "";
        }
    }
}