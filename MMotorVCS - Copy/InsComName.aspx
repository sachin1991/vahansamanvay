<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" Theme="emsTheme" AutoEventWireup="true" CodeFile="InsComName.aspx.cs" Inherits="InsComName" Title="Vahan Samanvay Master Insurance Company" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <script language="javascript" type="text/javascript">
    function valid()
    {    
    if(document.getElementById("<%=txtInsCompCode.ClientID%>").value == "")
    {
        alert("Enter Insurance Company Code");
        document.getElementById("<%=txtInsCompCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtCompName.ClientID%>").value == "")
    {
        alert("Enter Insurance Company Name");
        document.getElementById("<%=txtCompName.ClientID%>").focus();
        return false;
    }  
    return true;
    }
    </script>
        <table class="contentmain">
        <tr> <td> 
        <table class="tablerowcolor" align="center" width="80%">
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" class="heading">
                    Insurance Company</td>
            </tr>
            <tr>
                <td colspan="3" >
                    </td>
            </tr>
            <tr>
                <td style="width: 175px" style="color: #800000">
                    <strong>Insurance Company Code</strong></td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtInsCompCode" runat="server" width="148px"></asp:TextBox></td>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtInsCompCode"></cc1:FilteredTextBoxExtender>
                <td style="width: 176px">
                </td>
            </tr>
            <tr>
                <td style="width: 175px"  style="color: #800000">
                    <strong>Insurance Company Name</strong></td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtCompName" runat="server" width="148px"></asp:TextBox></td>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtCompName"></cc1:FilteredTextBoxExtender>
                <td style="width: 176px">
                </td>
            </tr>
            <tr>
                <td style="width: 175px; height: 40px" style="color: #800000">
                    <strong>Company Address</strong></td>
                <td style="width: 100px; height: 40px">
                    <asp:TextBox ID="txtaddress" runat="server" Height="68px" TextMode="MultiLine" Width="148px"></asp:TextBox></td>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtaddress"></cc1:FilteredTextBoxExtender>
                <td style="width: 176px; height: 40px">
                </td>
            </tr>
            <tr>
                <td style="width: 175px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 176px">
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" Width="76px" CssClass="button" /><asp:Button
                        ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="71px" CssClass="button" /><asp:Button
                            ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" Width="76px" CssClass="button" /></td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#000000"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3">
                    <h4>Insurance Company Details</h4></td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:GridView ID="gvInsCompType" runat="server" AutoGenerateColumns="False" 
                        OnRowCommand="gvInsCompType_RowCommand" AllowPaging="True" PageSize="15" 
                        OnPageIndexChanging="gvInsCompType_PageIndexChanging" 
                        OnRowCreated="gvInsCompType_RowCreated" CssClass="gridText" BackColor="White" 
                        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                        EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" Width="97%">
                        <Columns>
                            <asp:TemplateField HeaderText="S No.">
                                <ItemTemplate>
                                    <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                        Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="InsCompid" HeaderText="InsCompId " SortExpression="InsCompid" />
                            <asp:BoundField DataField="inscompcode" HeaderText="Ins Comp Code" SortExpression="inscompcode" />
                            <asp:BoundField DataField="InsCopmName" HeaderText="Insurance Company Name" SortExpression="InsCopmName" >
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="InsCompadd" HeaderText="Ins Comp Address" SortExpression="InsCompadd" >
                                <ItemStyle HorizontalAlign="Left" Wrap="True" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />                    
                    <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />                    
                    <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                            ForeColor="White" />
                    <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                            HorizontalAlign="Right" />
                    <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                            ForeColor="White" />
                    <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                            PreviousPageText="Previous" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    </td>
            </tr>
        </table>
    </td></tr> </table>
    <%--</div>
    </form>
</body>
</html>--%>
</ContentTemplate>
    </asp:UpdatePanel></center>
</asp:Content>