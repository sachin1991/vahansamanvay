<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResestPassword.aspx.cs" Inherits="ResestPassword"  MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" Theme="emsTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.password-strength.js" type="text/javascript"></script>
 <script language="javascript" type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
       
            var myPlugin = $("[id$='txtPassword']").password_strength();

            $("[id$='btnUpdate']").click(function () {
                return myPlugin.metReq(); //return true or false
            });

            $("[id$='passwordPolicy']").click(function (event) {
                var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
                window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
                event.preventDefault();
                return false;
            });

        });
</script>
<script type="text/javascript">
    function hello() {

        var myPlugin = $("[id$='txtPassword']").password_strength();

        $("[id$='btnUpdate']").click(function () {
            return myPlugin.metReq(); //return true or false
        });

        $("[id$='passwordPolicy']").click(function (event) {
            var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
            window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
            event.preventDefault();
            return false;
        });

    }
 </script>
<SCRIPT language="javascript" type="text/javascript">
    function valid()
    {     
    if(document.getElementById("<%=ddUsername.ClientID%>").selectedIndex==0)
    {
        alert("Select State Name");
        document.getElementById("<%=ddUsername.ClientID %>").focus();        
        return false;
    }      

    if(document.getElementById("<%=txtPassword.ClientID%>").value == "")
    {
        alert("Enter Password");
        document.getElementById("<%=txtPassword.ClientID%>").focus();
        return false;
    }  
    if(document.getElementById("<%=txtConPass.ClientID%>").value == "")
    {
        alert("Enter Password for Confirmation");
        document.getElementById("<%=txtConPass.ClientID%>").focus();
        return false;
    }   
    
    return true;
    }    
    </SCRIPT>
     <table class="contentmain">
    <tr> <td> 
<asp:Table style="WIDTH: 80%" id="tblIncidents" runat="server" class="tablerowcolor" CellSpacing="1" CellPadding="0" HorizontalAlign="Center">
<asp:TableRow >
<asp:TableCell ColumnSpan="6" ID="ErrorDisplayCell" class="heading">
<asp:Label ID="lblmsg" CssClass="tblAppealCase" ForeColor="White"  runat="server"></asp:Label>
<asp:Label ID="lblHeader"   runat="server">Reset User Password</asp:Label>
<asp:Label ID="Label1" CssClass="tblAppealCase" ForeColor="White"  runat="server"></asp:Label>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow >
<asp:TableCell ColumnSpan="6" >
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow1" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="tblcell1" runat="server" ><strong>User Name</strong>
</asp:TableCell>
<asp:TableCell ID="tblCell1a" runat="server" >&nbsp;
<asp:TextBox ID="ddUsername" runat="server" Width = "255px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow1" runat="server" Width="70%" Visible="false">
<asp:TableCell Width="24%" ID="TableCell3" runat="server" >Old Password
</asp:TableCell>
<asp:TableCell ID="TableCell4" runat="server" >&nbsp;
<asp:TextBox id="txtOldPassword"  runat="server" TextMode="Password" Width="250px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow11" runat="server" Width="70%" >
<asp:TableCell Width="24%" ID="TableCell1b" runat="server" >New Password
</asp:TableCell>
<asp:TableCell ID="TableCell2b" runat="server" >&nbsp;
<asp:TextBox id="txtPassword"  runat="server" TextMode="Password" Width="250px" CausesValidation="true" ValidationGroup="rst"></asp:TextBox>
<asp:CustomValidator id="CustomValidator1" runat="server" 
  OnServerValidate="TextValidate" 
  ControlToValidate="TxtPassword" 
  ErrorMessage="Text must be 12 or more characters." ValidationGroup="rst" > </asp:CustomValidator> 
<asp:RegularExpressionValidator ID="Regex4" runat="server" ControlToValidate="txtPassword"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red"/>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow3" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell4b" runat="server" >Confirm Password
</asp:TableCell>
<asp:TableCell ID="TableCell3b" runat="server" >&nbsp;
<asp:TextBox id="txtConPass" runat="server"  TextMode="Password" Width="250px"  CausesValidation="true" ValidationGroup="rst"></asp:TextBox>
<asp:CustomValidator id="CustomValidator2" runat="server" 
  OnServerValidate="TextValidate" 
  ControlToValidate="TxtConPass" 
  ErrorMessage="Text must be 12 or more characters." Validationgroup="rst"><</asp:CustomValidator> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtConPass"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red"/>

</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="TableRow5" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell1" runat="server" >Enable User
</asp:TableCell>
<asp:TableCell ID="TableCell2" runat="server" >&nbsp;
<asp:CheckBox Checked="true" ID="chkEnable" runat="server" />
</asp:TableCell>
</asp:TableRow>

<asp:TableRow HorizontalAlign="center">
<asp:tableCell HorizontalAlign="center" runat="server" id="tblrow22" colspan="3"><asp:Button id="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="button" Width="120px" Text="Reset Password" ValidationGroup="rst" CausesValidation="true" onClientClick="hello();"></asp:Button>
<asp:Button id="btnReset" onclick="btnReset_Click" runat="server" CssClass="button" Width="71px" Text="Clear"></asp:Button>
<%--onClientClick="hello();--%>"
<asp:Button id="btndelete" runat="server" CssClass="button" Width="71px" Text="Delete"  Visible="False"></asp:Button>
</asp:tableCell></asp:TableRow>


</asp:Table> 

</td> </tr> </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
