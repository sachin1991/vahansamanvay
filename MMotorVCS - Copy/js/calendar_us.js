
function checkdate(objName) {
var datefield = objName;
if (chkdate(objName) == false) {
datefield.select();
//alert(objName);
//alert("That date is invalid.  Please try again.");
datefield.focus();
return false;
}
else {
return true;
   }
}
function chkdate(objName) {
var strDatestyle = "US"; //United States date style
//var strDatestyle = "EU";  //European date style
var strDate;
var strDateArray;
var strDay;
var strMonth;
var strYear;
var intday;
var intMonth;
var intYear;
var booFound = false;
var datefield = objName;
var strSeparatorArray = new Array("-"," ","/",".");
var intElementNr;
var err = 0;
var strMonthArray = new Array(12);
strMonthArray[0] = "Jan";
strMonthArray[1] = "Feb";
strMonthArray[2] = "Mar";
strMonthArray[3] = "Apr";
strMonthArray[4] = "May";
strMonthArray[5] = "Jun";
strMonthArray[6] = "Jul";
strMonthArray[7] = "Aug";
strMonthArray[8] = "Sep";
strMonthArray[9] = "Oct";
strMonthArray[10] = "Nov";
strMonthArray[11] = "Dec";
strDate = datefield.value;
if (strDate.length < 1) {
return true;
}
for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
strDateArray = strDate.split(strSeparatorArray[intElementNr]);
if (strDateArray.length != 3) {
err = 1;
return false;
}
else {
strDay = strDateArray[1];
strMonth = strDateArray[0];
strYear = strDateArray[2];
}
booFound = true;
   }
}
if (booFound == false) {
if (strDate.length>5) {
strDay = strDate.substr(0, 2);
strMonth = strDate.substr(2, 2);
strYear = strDate.substr(4);
   }
else
{
return false;
}
}
if (strYear.length != 4) {
return false;
}
// US style
if (strDatestyle == "US") {
strTemp = strDay;
strDay = strMonth;
strMonth = strTemp;
}
intday = parseInt(strDay, 10);
if (isNaN(intday)) {
err = 2;
return false;
}
intMonth = parseInt(strMonth, 10);
if (isNaN(intMonth)) {
for (i = 0;i<12;i++) {
if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
intMonth = i+1;
strMonth = strMonthArray[i];
i = 12;
   }
}
if (isNaN(intMonth)) {
err = 3;
return false;
   }
}
intYear = parseInt(strYear, 10);
if (isNaN(intYear)) {
err = 4;
return false;
}
if (intMonth>12 || intMonth<1) {
err = 5;
return false;
}
if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
err = 6;
return false;
}
if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
err = 7;
return false;
}
if (intMonth == 2) {
if (intday < 1) {
err = 8;
return false;
}
if (LeapYear(intYear) == true) {
if (intday > 29) {
err = 9;
return false;
}
}
else {
if (intday > 28) {
err = 10;
return false;
}
}
}
if (strDatestyle == "US") {
datefield.value = intday + "/" + intMonth+"/" + strYear;
}
else {
datefield.value = intday + "/" + intMonth+"/" + strYear;;
}
return true;
}
function LeapYear(intYear) {
if (intYear % 100 == 0) {
if (intYear % 400 == 0) { return true; }
}
else {
if ((intYear % 4) == 0) { return true; }
}
return false;
}

function doDateSt2FCheck() {
var FirDate=document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt');
var StolenDate=document.getElementById('ctl00_ContentPlaceHolder1_txtStolenDt');
if (Date.parse(FirDate.value) >= Date.parse(StolenDate.value)) {
}
else {
if (FirDate.value != "" && StolenDate.value != "") 
    {
//    alert("FIR date must be Gerater than or Equal to Stolen date.");
//    document.getElementById('ctl00_ContentPlaceHolder1_txtStolenDt').focus();
    return false;
    }
   }
}
function doDateRe2FCheck() {
var FirDate=document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt');
var StolenDate=document.getElementById('ctl00_ContentPlaceHolder1_txtStolenDt');
if (Date.parse(FirDate.value) >= Date.parse(StolenDate.value)) {
}
else {
if (FirDate.value != "" && StolenDate.value != "") 
    {
//    alert("GD date must be Gerater than or Equal to Recovered date.");
    document.getElementById('ctl00_ContentPlaceHolder1_txtStolenDt').focus();
    return false;
    }
   }
}
function doDateS2ECheck() {
var FirDate=document.getElementById('ctl00_ContentPlaceHolder1_txtEndDate');
var StolenDate=document.getElementById('ctl00_ContentPlaceHolder1_txtstartDate');
if (Date.parse(FirDate.value) >= Date.parse(StolenDate.value)) {
}
else {
if (FirDate.value != "" && StolenDate.value != "") 
    {
//    alert("End Date must be Gerater than or Equal to Start Date.");
    document.getElementById('ctl00_ContentPlaceHolder1_txtEndDate').focus();
    return false;
    }
   }
}

function doDateF2TCheck() {
var FirDate=document.getElementById('ctl00_ContentPlaceHolder1_txtToDt');
var StolenDate=document.getElementById('ctl00_ContentPlaceHolder1_txtFromDt');
if (Date.parse(FirDate.value) >= Date.parse(StolenDate.value)) {
}
else {
if (FirDate.value != "" && StolenDate.value != "") 
    {
//    alert("To Date must be Gerater than or Equal to From Date.");
    document.getElementById('ctl00_ContentPlaceHolder1_txtToDt').focus();
    return false;
    }
   }
}
function CheckCurrentDate(CtlID) {
var FirDate= document.getElementById(CtlID);
//var StolenDate=document.getElementById('ctl00_ContentPlaceHolder1_txtStolenDt');
var objDate; 
var CurrentDate;
objDate = new Date();

CurrentDate=(eval(objDate.getDate())+eval(1))+'/'+(eval(objDate.getMonth())+eval(1))+'/'+objDate.getFullYear();
//alert(firdate.value)
//alert(CurrentDate)
 if (FirDate.value != "") 
    {
        if(FirDate.value >= CurrentDate)
        {
          //  alert('Date must be less than or equal to current date')
//            alert("firdate"+Firdate.value+"")
//            alert("currentdate"+CurrentDate+"")
            document.getElementById(CtlID).focus();;
            return false;
            
        }
    }
}
 function trim(str)
    {    
        if(!str || typeof str != 'string')   
        return null;    
        return str.replace(/^[\s]+/,'').replace(/[\s]+$/,'').replace(/[\s]{2,}/,' ');
    }
    

