<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IPONoListView.aspx.vb" Inherits="IPONoListView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="style/main.css" rel="stylesheet" type="text/css" />
    <title>Untitled Page</title>
    <style type="text/css">

        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 97px;
        }
        .style4
        {
            width: 75px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="contentmain">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="GOI" runat="server" Font-Bold="True" Text="Government of India" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="MHA" runat="server" Font-Bold="True" Text="Mnistry of Home Affairs"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="NCRB" runat="server" Font-Bold="True" Text="National Crime Records Bureau"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3"><asp:Label ID="lblMoney" runat="server" Font-Bold="True" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="VS" runat="server" Font-Bold="True" Text="Vahan Samanvay" Visible="False"></asp:Label>
                    <br />
                    <strong>Report Login Log</strong><br />
                    <br />
                </td>
            </tr>
           
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="LISTOFSTOLEN" runat="server" Text="Login Log Report"
                        Visible="False"></asp:Label>
                    <asp:TextBox ID="DTFROM" runat="server" Visible="False" BorderStyle="None"></asp:TextBox>
                    <asp:Label ID="LISTTO" runat="server" Text="to" Visible="False"></asp:Label>
                    <asp:TextBox ID="DTTO" runat="server" Visible="False" BorderStyle="None"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <div align="center">
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table border='0' cellpadding='0' cellspacing='0' class="report">
   
                    <tr class="heading">
                        <td>
                            Receipt No.
                        </td>
                    
                        <td>
                            S.No.
                        </td>
                        <td>
                            IPO No.
                        </td>
                        <td>
                        Amount
                        </td>
                    </tr>
                    
                  
            </HeaderTemplate>
            <ItemTemplate>
                <td valign="top" >
                    <%# DataBinder.Eval(Container, "DataItem.inReceiptId")%>
                    </td>
                    <td valign="top" >
                    <%Response.Write(serialnumberprint())%>                  
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.IPONo")%><br />
                    </td>
                    <td align="right" >
                    <%# DataBinder.Eval(Container, "DataItem.IPOAmt")%><br />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    <%Response.Write("<tr><td></td><td></td><td>" & totalprint() & "</td></tr>")%>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        
        Report Generated on
        <asp:Label ID="toda" runat="server" Text="Label"></asp:Label>
        <br />
        
        <br />
        <table class="style1">
            <tr>
                <td class="style2">
        <asp:LinkButton ID="FirstPage" runat="server">First Page</asp:LinkButton>
                </td>
                <td class="style3">
        
        <asp:LinkButton ID="Lnkbtnprev" runat="server" Visible="False">Previous Page</asp:LinkButton>
                </td>
                <td class="style4">
        <asp:LinkButton ID="LnkbtnNext" runat="server" Visible="False">Next Page</asp:LinkButton>
                </td>
                <td>
        <asp:LinkButton ID="LastPage" runat="server">Last Page</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
