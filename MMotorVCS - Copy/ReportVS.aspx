﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" SmartNavigation="true"  AutoEventWireup="true" CodeFile="ReportVS.aspx.vb" Inherits="ReportVS" Title="Vahan Samanvay Report" Theme="emsTheme"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <Triggers> 
<asp:PostBackTrigger ControlID="Button1" /> 
<asp:PostBackTrigger ControlID="Converttoexecl" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 

    <ContentTemplate>
            <table class="contentmain">
                <tr>
                    <td >
                    <table class="tablerowcolor" align="center" width="80%">
                    <tr class="heading"> <td colspan="4"> Vehicle List</td></tr>
                    <tr> <td>  
                                           <asp:RadioButton ID="RadioListofStolen" runat="server" Checked="True" GroupName="STOLENRECOVEREDDELETED"
                            Text="List of Stolen" AutoPostBack="True" /></td>
                    <td >
                        <asp:RadioButton ID="RadioListofRecovered" runat="server" GroupName="STOLENRECOVEREDDELETED"
                            Text="List of Recovered" AutoPostBack="True" /></td>
                    <td >
                        <asp:RadioButton ID="RadioListofDeleted" runat="server" GroupName="STOLENRECOVEREDDELETED"
                            Text="List of Deleted" AutoPostBack="True" /></td>
                            <td runat="server" visible="false">  <asp:RadioButton ID="RadioCoordinatedCases" runat="server" GroupName="STOLENRECOVEREDDELETED"
                            Text="List of Coordinated Cases" /></td>
                </tr>
                    <tr> <td id ="matchbtn" runat="server">  
                                           <asp:RadioButton ID="RdbPerfectMatch" runat="server" 
                                               GroupName="STOLENRECOVEREDDELETED" Text="Perfect Match Cases List" 
                                               AutoPostBack="True" /><br />
                                               <asp:RadioButton ID="RdbPartialMatch" runat="server" 
                            GroupName="STOLENRECOVEREDDELETED" Text="Partial Match Cases List" 
                            AutoPostBack="True" /><br />
                            <asp:RadioButton ID="RdbMatchRegistration" runat="server" 
                            GroupName="STOLENRECOVEREDDELETED" 
                            Text="Partial Match Only on Registration List" AutoPostBack="True" /><br />
                            <asp:RadioButton ID="RdbMatchChasis" runat="server" 
                            GroupName="STOLENRECOVEREDDELETED" Text="Partial Match Only on Chasis List" 
                            AutoPostBack="True" /><br />
                            <asp:RadioButton ID="RdbMatchEngine" runat="server" 
                            GroupName="STOLENRECOVEREDDELETED" Text="Partial Match Only on Engine List" 
                            AutoPostBack="True" />
                        </td>
                        </td>
                    <td >
                        
                        <asp:RadioButton ID="RdbSTOLENdates" runat="server" GroupName="STOLENRECOVERED" 
                            Text="Between Stolen FIR dates" Checked="True" />
                        <br />
                        <asp:RadioButton ID="RdbRecovereddates" runat="server" GroupName="STOLENRECOVERED" 
                            Text="Between Recovered FIR Dates" />
                        
                    <td >
                        
                        
                        
                        </td>
                            <td runat="server" visible="false">  &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:CheckBox ID="CHKPSWISE" runat="server" Text="State District PS wise" />
                        </td>
                    <td >
                            <asp:CheckBox ID="Chkmakewise" runat="server" Text="Type and Make wise" />
                    </td>
                    <td >
                    </td>
                     <td></td>
                </tr>
                <tr>
                    <td >
                        </td>
                    <td >
                    </td>
                    <td>
                    </td>
                     <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label3" runat="server" Text="State"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                            
                        </asp:DropDownList></td>
                    <td>
                    </td>
                     <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label4" runat="server" Text="District"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="Districtwise" runat="server" OnSelectedIndexChanged="Districtwise_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                           
                        </asp:DropDownList></td>
                    <td >
                    </td>
<td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label5" runat="server" Text="PS"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="PSWise" runat="server" Width="250px">
                           
                        </asp:DropDownList></td>
                    <td >
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label6" runat="server" Text="Type"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="TypeWise" runat="server" OnSelectedIndexChanged="TypeWise_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                                       </asp:DropDownList></td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label7" runat="server" Text="Make"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="MakeWise" runat="server" Width="250px">
                        
                        </asp:DropDownList></td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label></td>
                    <td >
                       
                        <asp:TextBox ID="ListDatefrom" runat="server" ></asp:TextBox></td>
                    <td >
                         <ajaxtoolkit:calendarextender 
                    ID="ChargeSheetCalendarExtender" 
                    runat="server" 
                    TargetControlID="ListDatefrom" 
                    Format="dd-MM-yyyy" 
                     PopupPosition="Right"  /></td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="listDateto" runat="server"></asp:TextBox>
                         
                        </td>
                    <td >
                         <ajaxtoolkit:calendarextender 
                    ID="CalendarExtender1" 
                    runat="server" 
                    TargetControlID="listDateto" 
                    Format="dd-MM-yyyy" 
                     PopupPosition="Right"  />
                         <asp:RadioButton ID="Firwise" runat="server" Checked="True" 
                             GroupName="entryfirdate" Text="Between FirDate" AutoPostBack="True" />
                         <asp:RadioButton ID="EntryDatewise" runat="server" GroupName="entryfirdate" 
                             Text="Between EntryDate" AutoPostBack="True" />
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                    </td>
                    <td >
                        <asp:Button ID="Button1" runat="server" Text="View" />
                         <asp:Button ID="btnReset" runat="server" Text="Reset" />
                        </td>
                    <td >
                        <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Msglabel" runat="server"></asp:Label>
                    </td>
                    <td >
                    <asp:Button ID="Converttoexecl" runat="server" 
                            Text="Generate to excel (.csv) format file" />
                    </td>
                    <td >
                        
                    </td> <td></td>
                </tr>
            </table>
            </td></tr></table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

