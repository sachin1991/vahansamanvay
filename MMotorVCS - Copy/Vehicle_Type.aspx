<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Vehicle_Type.aspx.cs" Theme="emsTheme" Inherits="Vehicle_Type" Title="Vehicle Name" EnableViewState="true"  EnableViewStateMac="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<script language="javascript" type="text/javascript">
    function valid()
    {    
    if(document.getElementById("<%=txtVehCode.ClientID%>").value == "")
    {
        alert("Enter Vehicle Type Code");
        document.getElementById("<%=txtVehCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtVehName.ClientID%>").value == "")
    {
        alert("Enter Vehicle Type Name");
        document.getElementById("<%=txtVehName.ClientID%>").focus();
        return false;
    }  
    return true;
    }
    </script>
    <table class="contentmain"> 
    <tr>
    <td>
    
    <table width="80%" align="center" class="tablerowcolor">
        <tr>
            <td colspan="3"> <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" class="heading">Vehicle Type</td>
        </tr>
        <tr> <td colspan="3"></td></tr>
        <tr>
            <td style="width: 127px"> Vehicle Type Code</td>
            <td style="width: 100px"> <asp:TextBox ID="txtVehCode" runat="server" Width="148px"></asp:TextBox></td>
            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<> " TargetControlID="txtVehCode"></cc1:FilteredTextBoxExtender>
            <td style="width: 100px"> 
                <asp:CheckBox ID="vehiclenamewise" runat="server" Text="Vehicle Name wise" 
                    AutoPostBack="True" oncheckedchanged="vehiclenamewise_CheckedChanged"  />
            </td>
        </tr>
        <tr>
            <td style="width: 127px"> Vehicle Type Name</td>
            <td style="width: 100px"> <asp:TextBox ID="txtVehName" runat="server" Width="148px"></asp:TextBox></td>
            <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<> " TargetControlID="txtVehName"></cc1:FilteredTextBoxExtender>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 127px">   </td>
            <td style="width: 100px">   </td>
            <td style="width: 100px">   </td>
        </tr>
        <tr>
            <td colspan="3" align="center"> <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" Width="76px" CssClass="button" />
                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="71px" CssClass="button" />
                            <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" Width="76px" CssClass="button" /></td>
        </tr>
        <tr>
            <td colspan="3"> <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#000000"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3"> <h4>Vehicle Details</h4></td>
        </tr>
        <tr>
            <td colspan="3">           
                <asp:GridView ID="gvVehType" runat="server" AutoGenerateColumns="False" 
                    Width="97%" OnRowCommand="gvVehType_RowCommand" 
                    PageSize="15" OnPageIndexChanging="gvVehType_PageIndexChanging" 
                    OnRowCreated="gvVehType_RowCreated" CssClass="gridText" BackColor="White" 
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" >
                    <Columns>
                        <asp:TemplateField HeaderText="S No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                    Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="VehicleTypeid" HeaderText="Id " SortExpression="VehicleTypeid" />
                        <asp:BoundField DataField="vehicletypecode" HeaderText="Vehicle Code" SortExpression="vehicletypecode" />
                        <asp:BoundField DataField="vehicletypename" HeaderText="Vehicle Name" SortExpression="vehicletypename" >
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />                    
                    <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />                    
                    <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                        ForeColor="White" />
                    <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                        HorizontalAlign="Right" />
                    <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                        ForeColor="White" />
                    <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                        PreviousPageText="Previous" />
                </asp:GridView>
               
            </td>
        </tr>
        <tr>
            <td colspan="3">
                </td>
        </tr>
    </table>
    </td></tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

