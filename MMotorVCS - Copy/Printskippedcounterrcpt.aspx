﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="Printskippedcounterrcpt.aspx.cs" Inherits="Printskippedcounterrcpt" Theme="emsTheme"  ValidateRequest="true"%>
<head id="Head1" runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function PrintReport() {
            document.getElementById("btnPrint").style.display = "hidden"
            window.print();
            return false;
        }
    </script>
    <style type="text/css">
        .style1
        {
            color: black;
        }
        .style2
        {
            height: 81px;
            width: 119px;
        }
        .style4
        {
            width: 469px;
        }
        .style5
        {
            width: 131px;
        }
        .style6
        {
            text-align: left;
        }
        .style8
        {
            font-size: medium;
        }
        .style9
        {
            color: #C0C0C0;
        }
        .style11
        {
            height: 81px;
            width: 661px;
        }
        .style12
        {
            width: 119px;
        }
        .style13
        {
            height: 23px;
        }
        </style>
</head>
<body style="background-position:center; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg">
    <form id="form1" runat="server">
    <div align="center">
       <asp:Panel ID="Panel1" runat="server"  Width="700px" style="background-position:top; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg">
            &nbsp;<table style="width: 593px; height: 128px" align="center">
                <tr>
                    <td class="style2">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logo.jpg" Height="82px" Width="87px" /></td>
     
                    <td align="center" class="style11">
                        <asp:Label ID="lblHeader" runat="server"  > <strong> <span style="font-size: 14pt">
                                 Government of India
                        <%--<br />
                            &nbsp;भारत सरकार&nbsp;<br />--%>
                            <br />
                      Ministry of Home Affairs
                      <%--<br />
                            गृह मंत्रालय--%>
                            <br />
                    National Crime Records Bureau
                            <%--<br />
                            राष्ट्रीय अपराध रिकार्ड ब्यूरो--%>
                            <br />
                            </span>
                                                  </strong></asp:Label>
                         </td>
                         
                    <td style="width: 100px; height: 81px;">
                    </td>
                </tr>
              <tr>
                    <td style="text-align: center; font-size: 12pt; font-weight: 700;" colspan="3" 
                        class="style13" >
                        Motor Vehicle Verification Counter</td>
               
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center; font-size: 12pt; font-weight: 700;" 
                        class="style13">
                        </td>
                </tr>
                <tr>
                    <td style="text-align: right; font-size: 12pt; font-family=Arial" 
                        class="style12">
                        &nbsp;
                    </td>
                    <td colspan="2" style="text-align: right; font-size: 12pt; font-family=Arial">
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" 
                        style="text-align: right; font-size: 12pt; font-family=Arial""  >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right; font-size: 12pt; font-family=Arial"">
                        Dated:<asp:Label ID="lblDated" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6" colspan="3" 
                        style="text-align: right;font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="Original" runat="server"></asp:Label>
                        &nbsp;Receipt No:<asp:Label ID="lblReciept" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style6" colspan="3" 
                        style="text-align: right;font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="CashIPO" runat="server" Text="Cash"></asp:Label>
                        &nbsp;Amount:<asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: center;font-size: 12pt; font-family=Arial"">
                        <strong><span class="style8">Vehicle Enquiry Report</span><br class="style9" /> </span>
                        Only For Information For General Public<br /> (Not Valid For Insurance 
                        Claims/Any other Legal Purposes)</strong></td>
                </tr>
            </table>
                <table align="center">
                    <tr>
                        <td colspan="2" style="text-align: left; font-size: 12pt; font-family=Arial"">
                            Refrence enquiry recieved from Mr./Mrs. :<asp:Label ID="lblName" runat="server"></asp:Label>
                            &nbsp;regarding</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size: 12pt; font-family=Arial"">
                            &nbsp;</td>
                    </tr>
                <tr>
                    <td style="text-align: justify; font-size: 12pt; font-family=Arial"  class="style5">
                        <strong>Vehicle Type:</strong></td>
                    <td class="style4" style="text-align: justify; font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="lblvectype" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" class="style5">
                        <strong> Make :</strong></td>
                    <td style="text-align: justify; font-size: 12pt; font-family=Arial"" class="style5">
                        <asp:Label ID="lblmake" runat="server" Font-Bold="True"></asp:Label>
                        <strong>&nbsp; </strong>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify ;  font-size: 12pt; font-family=Arial"  class="style5">
                        <strong> Registration No.:</strong></td>
                    <td class="style4" style="text-align: justify; font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="lblregistrationno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" class="style5">
                       <strong> Chassis No.:</strong></td>
                    <td class="style4" style="text-align: justify; font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="lblchasisno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" class="style5">
                        <strong>Engine No.:</strong></td>
                    <td class="style4" style="text-align: justify; font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="lblengineno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td class="style5" style="text-align: justify;  font-size: 12pt; font-family=Arial"">
                            &nbsp;</td>
                        <td class="style4" style="text-align: justify; font-size: 12pt; font-family=Arial"">
                            &nbsp;</td>
                    </tr>
                <tr>
                    <td colspan='2' style="text-align: left;  font-size: 12pt; font-family=Arial"" >
                        As per the information avaliable with NCRB till date (based on data recieved 
                        from States/UTs police) the above mentioned vehicle 
                        <asp:Label ID="lblmsg" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            &nbsp;</td>
                    </tr>
                </table>

                <table>
                <tr>
                    <td style="height: 19px;">
                        <asp:Panel ID="Panel2" runat="server" Width="100%">
                        
                        <asp:repeater id="Repeater1" runat="server">
                        <ItemTemplate>
														<table 

cellpadding="4" cellspacing="0" border="1" bordercolor="#000000" class="dataTbl"
															

width="100%">

<tr valign="top" align="justify"  height="25">
                                                                <td nowrap ID="VehicleType" runat="Server"><b>Vehicle 

Type</b></td><td><%# DataBinder.Eval(Container, "DataItem.matVehicle")%></td></tr>
															

   <tr valign="top" align="justify"  height="25"><td nowrap ID="Make" runat="Server"><b>matMake</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.matMake")%></td></tr>
	<tr valign="top" align="justify"  height="25"><td nowrap ID="Registration" runat="Server"><b>Registration 

No</b></td><td><%# DataBinder.Eval(Container, "DataItem.matRegistration")%></td></tr>
															

    <tr valign="top" align="justify"  height="25"><td nowrap ID="Chasis" runat="Server"><b>Chasis No</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.matChasis")%></td></tr>
															

	<tr valign="top" align="justify"  height="25"><td nowrap ID="Engine" runat="Server"><b>Engine No</b></td>
    <td><%# DataBinder.Eval(Container, "DataItem.matEngine")%></td></tr>														


															

	
															

	
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Year" runat="Server"><b>Yr of manufacture</b></td><td><%# DataBinder.Eval(Container, "DataItem.matYear")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Color" runat="Server"><b>Color</b></td><td><%# DataBinder.Eval(Container, "DataItem.matColor")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Status" runat="Server"><b>Status</b></td> <td><%# DataBinder.Eval(Container, "DataItem.matstatus")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="MatchingParam" runat="Server"><b>Match Status</b></td><td><%# DataBinder.Eval(Container, 

"DataItem.matMatchingParam")%></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Source" runat="Server"><b>Source</b></td><td><%# DataBinder.Eval(Container, "DataItem.matSource")%></tr>
<tr valign="top" align="justify"  height="25">
															

	<td nowrap ID="State" runat="Server"><b>State /<br>District / <br>PS :</b></td>
                                                                <td align="center"><%# DataBinder.Eval(Container, 

"DataItem.matstateName") %></td>
                                                             </tr>
															

<tr valign="top" align="justify"  height="25">
                                                            <td nowrap ID="FIR" runat="Server"><b>FIR</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.matFIR")%></td></tr>
															
													</ItemTemplate>

													
														
													
													<FooterTemplate>
													</FooterTemplate>
												</asp:repeater>

                               </asp:Panel>
                    </td>
                </tr>
                </table><table>
                  <tr>
                    <td style="text-align: justify;  font-size: 7pt">
                        Databank maintained by NCRB
                        <asp:Label ID="captcha" 
                            runat="server" Font-Size="7pt" ></asp:Label>
                        updated as on&nbsp;&nbsp;<asp:Label ID="lblenqdatetime" runat="server" Font-Bold="False" 
                            Font-Size="7pt"></asp:Label>
                      </td>
                </tr>
                  <tr>
                      <td style="text-align: right;  font-size: 8pt">
                          &nbsp;<br />
                          <br />
                          <asp:Label ID="lblFooter" runat="server" Font-Bold="True"></asp:Label>
                      </td>
                  </tr>
                <tr>
                    <td style="height: 21px;" align="left">
                        <font color="#ffffff"><font color="#ff0000" style="COLOR:blue">
                        <font color="#ffffff" style="COLOR: white"><font color="#ffffff" 
                            style="color: blue"><font color="#ff0000" style="COLOR:blue">
                        <font color="#000000" size="2" style="color: white"><strong>
                        <span class="style1" style="text-align: justify;  font-size: 12pt; font-family=Arial"">&nbsp; </span>
                        <asp:Label ID="lblUpdationDate" runat="server" Text="Label" Visible="false" ></asp:Label>
                        </strong></font></font></font></font></font></font></td>
                </tr>
                
                  <tr>
                      <td align="left" style="height: 21px;">
                          This is a computer generated output. No signature required.</td>
                  </tr>
                  <tr>
                      <td align="left" style="height: 21px;">
                      <b>Save your precious time by visiting NCRB website ncrb.nic.in Vehicle Enquiry to get this report.</b>
                  </td>
                  </tr>
                
            </table>
          
            </asp:Panel>
        <asp:Button ID="btnPrint" runat="server" Text="Print" onClientclick="PrintReport()" Visible="false"  /></div>


    </form>
</body>
</html>

