Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Partial Class ReportVSMatchedStatus
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Private Sub preparesql()
        Dim sql As String
        Dim cmd As New SqlCommand
        If Session("inRoleid") = 4 Then
            sql = "SELECT '' as rcptid,[inInsuranceId]  ,[vcRTOCode] ,[vcType] ,[vcMake] ,[vcRegistrationNo]      ,[vcChasisNo]      ,[vcEngineNo]  ,[vcColorCode]      ,[vcYearOfManu]      ,[vcCompanyTpe]      ,[vcResult],[vcCapcha]      ,[vcUserName]      ,[dtCreatedOn]      ,[vcHostAddr]      ,[vcClientAddr]      ,[vcModel]      ,[matstatename]      ,[matpsname]      ,[matstdesc],[matfir]      ,[matvehicle]      ,[matmake]      ,[matregistration]      ,[matchasis]      ,[matengine]      ,[matyear]      ,[matcolor]      ,[matstatus]      ,[matmatchingparam]      ,[matsource], vehicle_type.vehicletypename,  typeandmakeofvehicle.makename "
            sql = sql + " FROM [dbo].[TblTrn_Authority_Enquiry] "
            sql = sql + " left join vehicle_type  on [vcType] = vehicle_type.vehicletypecode "
            sql = sql + " left join dbo.typeandmakeofvehicle  on [vcType] = dbo.typeandmakeofvehicle.vehicletypecode   and vcMake = dbo.typeandmakeofvehicle.makecode"
        End If
        If Session("inRoleid") = 9 Then
            sql = "SELECT '' as rcptid,[vcType] ,[vcMake] ,[vcRegistrationNo]      ,[vcChasisNo]      ,[vcEngineNo]  ,[vcColorCode]      ,[vcYearOfManu]      ,[vcCompanyTpe]      ,[vcResult],[vcCapcha]      ,[vcUserName]      ,[dtCreatedOn]      ,[vcHostAddr]      ,[vcClientAddr]      ,[matstatename]      ,[matpsname]      ,[matstdesc],[matfir]      ,[matvehicle]      ,[matmake]      ,[matregistration]      ,[matchasis]      ,[matengine]      ,[matyear]      ,[matcolor]      ,[matstatus]      ,[matmatchingparam]      ,[matsource], vehicle_type.vehicletypename,  typeandmakeofvehicle.makename "
            sql = sql + " FROM [dbo].[TblTrn_Insurance_Enquiry] "
            sql = sql + " left join vehicle_type  on [vcType] = vehicle_type.vehicletypecode "
            sql = sql + " left join dbo.typeandmakeofvehicle  on [vcType] = dbo.typeandmakeofvehicle.vehicletypecode   and vcMake = dbo.typeandmakeofvehicle.makecode"
        End If
        If Session("inRoleid") = 5 Then
            sql = "SELECT [inReceiptId] as rcptid ,[vcStateCode] ,[vcDistCode] ,[vcPSCode],[vcAppName],[vcMobno] ,[vcResino],[vcType] ,[vcMake],[vcRegistrationNo] ,[vcChasisNo] ,[vcEngineNo] ,[vcColorCode] ,[vcYearOfManu] ,[vcAppAdd] ,[dtEnqDt] dtCreatedOn ,[CashAmt] ,[IpoNo] ,[IpoAmt] ,[inLostId] ,[inRecoveryId] ,[dtCreated_Date] ,[vcCounterCode] ,[vcUserName] ,[vcQueryStatus] ,[vcFIRDate] ,[vcFIRNo] ,[vcipodate] ,[matstatename] ,[matpsname] ,[matstdesc] ,[matfir] ,[matvehicle] ,[matmake] ,[matregistration] ,[matchasis] ,[matengine] ,[matyear] ,[matcolor] ,[matstatus] ,[matmatchingparam] ,[matsource], vehicle_type.vehicletypename,  typeandmakeofvehicle.makename "
            sql = sql + " FROM [dbo].[TblTran_Counter_Enquiry] "
            sql = sql + " left join vehicle_type  on [vcType] = vehicle_type.vehicletypecode "
            sql = sql + " left join dbo.typeandmakeofvehicle  on [vcType] = dbo.typeandmakeofvehicle.vehicletypecode   and vcMake = dbo.typeandmakeofvehicle.makecode"
        End If
        If Session("inRoleid") = 1 Then
            sql = "SELECT [inReceiptId] as rcptid ,[vcStateCode] ,[vcDistCode] ,[vcPSCode],[vcAppName],[vcMobno] ,[vcResino],[vcType] ,[vcMake],[vcRegistrationNo] ,[vcChasisNo] ,[vcEngineNo] ,[vcColorCode] ,[vcYearOfManu] ,[vcAppAdd] ,[dtEnqDt] dtCreatedOn ,[CashAmt] ,[IpoNo] ,[IpoAmt] ,[inLostId] ,[inRecoveryId] ,[dtCreated_Date] ,[vcCounterCode] ,[vcUserName] ,[vcQueryStatus] ,[vcFIRDate] ,[vcFIRNo] ,[vcipodate] ,[matstatename] ,[matpsname] ,[matstdesc] ,[matfir] ,[matvehicle] ,[matmake] ,[matregistration] ,[matchasis] ,[matengine] ,[matyear] ,[matcolor] ,[matstatus] ,[matmatchingparam] ,[matsource], vehicle_type.vehicletypename,  typeandmakeofvehicle.makename "
            sql = sql + " FROM [dbo].[TblTran_Counter_Enquiry] "
            sql = sql + " left join vehicle_type  on [vcType] = vehicle_type.vehicletypecode "
            sql = sql + " left join dbo.typeandmakeofvehicle  on [vcType] = dbo.typeandmakeofvehicle.vehicletypecode   and vcMake = dbo.typeandmakeofvehicle.makecode"
        End If
        If Session("inRoleid") = 2 Then
            sql = "SELECT [inReceiptId] as rcptid ,[vcStateCode] ,[vcDistCode] ,[vcPSCode],[vcAppName],[vcMobno] ,[vcResino],[vcType] ,[vcMake],[vcRegistrationNo] ,[vcChasisNo] ,[vcEngineNo] ,[vcColorCode] ,[vcYearOfManu] ,[vcAppAdd] ,[dtEnqDt] dtCreatedOn ,[CashAmt] ,[IpoNo] ,[IpoAmt] ,[inLostId] ,[inRecoveryId] ,[dtCreated_Date] ,[vcCounterCode] ,[vcUserName] ,[vcQueryStatus] ,[vcFIRDate] ,[vcFIRNo] ,[vcipodate] ,[matstatename] ,[matpsname] ,[matstdesc] ,[matfir] ,[matvehicle] ,[matmake] ,[matregistration] ,[matchasis] ,[matengine] ,[matyear] ,[matcolor] ,[matstatus] ,[matmatchingparam] ,[matsource], vehicle_type.vehicletypename,  typeandmakeofvehicle.makename "
            sql = sql + " FROM [dbo].[TblTran_Counter_Enquiry] "
            sql = sql + " left join vehicle_type  on [vcType] = vehicle_type.vehicletypecode "
            sql = sql + " left join dbo.typeandmakeofvehicle  on [vcType] = dbo.typeandmakeofvehicle.vehicletypecode   and vcMake = dbo.typeandmakeofvehicle.makecode"
        End If
        Dim whr As String
        Dim ORD As String
        Dim mtf As String
        mtf = Month(Me.ListDatefrom.Text.ToString())
        If Len(mtf) = 1 Then
            mtf = "0" & mtf
        End If
        Dim dtf As String
        dtf = Day(Me.ListDatefrom.Text.ToString())
        If Len(dtf) = 1 Then
            dtf = "0" & dtf
        End If
        Dim Mto As String
        Mto = Month(Me.listDateto.Text.ToString())
        If Len(Mto) = 1 Then
            Mto = "0" & Mto
        End If

        Dim dto As String
        dto = Day(Me.listDateto.Text.ToString())
        If Len(dto) = 1 Then
            dto = "0" & dto
        End If
        whr = ""
        If Session("inRoleId") = 1 Then
            whr = " CONVERT(varchar(10),dtCreated_date,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreated_date,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
        Else
            If Session("inRoleId") = 5 Then
                whr = " vcUserName ='" & Session("vcUserName").ToString().Trim() & "' and CONVERT(varchar(10),dtEnqDt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtEnqDt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
            Else
                If Session("inRoleId") = 2 Then

                    whr = " vcUserName ='" & Session("vcUserName").ToString().Trim() & "' and CONVERT(varchar(10),dtCreated_date,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreated_date,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                Else
                    whr = " vcUserName ='" & Session("vcUserName").ToString().Trim() & "' and CONVERT(varchar(10),dtCreated_date,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreated_date,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                End If

            End If
        End If
        ORD = " ORDER BY "
        ORD = ORD & "matfir"
        If Me.OnlyMatchedCases.Checked = True Then
            whr = whr & " and (matstatus<>'' or matstatus is not null)"
        End If
        If whr <> "" Then
            whr = " WHERE " & whr
        End If
        sql = sql & whr '& ORD
        Session("SQLSTRING") = sql
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If Me.ListDatefrom.Text > Me.listDateto.Text Then
        '    Msglabel.Text = "Date to is less than date from"
        '    Exit Sub
        'End If
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then
            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""
        preparesql()
        Response.Write("<script>window.open('ReportVSMatchedStatusview.aspx','_blank')</script>")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If
        If Not IsPostBack Then
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "4" 'Authority
                Exit Select
            Case "9" 'Insurance
                Exit Select
            Case "2" 'national counter
                Exit Select
            Case "5" 'state counter
                Exit Select
            Case "12" 'ncrbexecutive
                Exit Select
            Case "11" 'CR_Executive1
                Exit Select
            Case "22" 'Intern
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
