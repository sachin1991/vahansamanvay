﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;
using System.Web;
using System.Web.UI;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

public partial class WirelessMessage : System.Web.UI.Page
{
    public DataTable dv;
    DataSet dt;
    DataSet dtemail;
    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {

                Response.Redirect("LogoutModule.aspx");
            }

            
            lblUpdated.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["vcUserName"].ToString() +  " " + Session["ClientAddr"].ToString().Trim();
            lblPrinted.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            DateText.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
            string _VehicleTypeDesc = re.Replace(Session["VehicleTypeDesc"].ToString(),"");
            string _MakeDesc = re.Replace(Session["MakeDesc"].ToString(),"");
            string _Registration = re.Replace(Session["Registration"].ToString(),"");
            string _Chasis = re.Replace(Session["Chasis"].ToString(),"");
            string _Engine = re.Replace(Session["Engine"].ToString(),"");
            string _color = re.Replace(Session["Color"].ToString(),"");
            string _model = re.Replace(Session["Model"].ToString(),"");
            lblPS.Text = re.Replace(Session["PSDesc"].ToString(),"");
            lblSate.Text = re.Replace(Session["StateDesc"].ToString(),"");
            captcha.Text = Session["strRandom"].ToString() + " " + Session["visitor"].ToString();
            int rol = (Convert.ToInt32(Session["inRoleId"].ToString()));


            if ((rol == 6) || (rol == 7))
            {

                lblPS.Visible = true;
                lblSate.Visible = true;

            }

            string _username = Session["vcusername"].ToString();

            dt = eform.getCounterHeader1(_username);

            if (dt.Tables[0].Rows.Count > 0)
            {
                lblFile.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcFileNumber"].ToString().Trim());
                lblFrom.Text = "<strong>From: </strong>" + dt.Tables[0].Rows[0]["vcHeader"].ToString().Trim() + " " + dt.Tables[0].Rows[0]["vcAddress"].ToString().Trim();
                lblFooter.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
            }

            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;
            lblModel.Text = _model;
            lblColor.Text = _color;

            if (Session["MatchStatus"].ToString() == "1")
            {
                lblMsg.Text = " is linked with";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();

                    rptAddress.DataSource = dv;
                    rptAddress.DataBind();

                    lblTo.Visible = true;

                    String profilename;
                    String receipients;
                    String subject;
                    String bodytext;
                    profilename = "VahanSamanvayProfile";
                    subject = "Vahan Samanvay Motor Vehicle Coordination";
                    receipients = "";

                    String bt;
                    bt = "";
                    foreach (DataRow dtRow in dv.Rows)
                    {
                        // on all table's columns
                        foreach (DataColumn dc in dv.Columns)
                        {
                            if (dc.ColumnName.ToString() == "STCODE")
                            {
                                String std;
                                std = dtRow[dc].ToString();
                                dtemail = eform.getemail(std);
                                receipients = receipients + dtemail.Tables[0].Rows[0]["vcMail"].ToString();
                            }
                            else
                            {
                                bt = bt + dc.ColumnName.ToString() + ": " + dtRow[dc].ToString() + "(.) ";
                            }
                        }

                    }
                    bodytext = lblPS.Text + " "+ lblSate.Text + " enquired the particular of Vehicle Type " + Session["VehicleTypedesc"] + "(.)Make " + Session["MakeDesc"] + "(.)Registration " + Session["Registration"] + "(.)Chasis " + Session["Chasis"] + "(.)Engine " + Session["Engine"] + "(.)Color " + Session["Color"] + "(.)Year of Manufacture " + Session["Model"] + " is linked with " + bt;
                    DataSet dsmale = new DataSet();
                    dsmale = eform.sendmail(profilename, receipients, subject, bodytext);
                }
                else
                {
                    lblTo.Visible = false;
                    lblMsg.Text = " is not recovered yet.";
                }
            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                lblTo.Visible = false;
                lblMsg.Text = " is not recovered yet.";
            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                lblTo.Visible = false;
                lblMsg.Text = " has not been reported as stolen by police.";
            }
            else if (Session["MatchStatus"].ToString() == "4")
            {
                lblTo.Visible = false;
                lblMsg.Text = "The FIR for this vehicle is not recorded and is not reported stolen or involved in crime.";
            }
            else
            {
                lblTo.Visible = false;
                lblMsg.Text = " is not found in the database.";
            }
        }
        catch
        {
            Response.Redirect("ERROR.aspx");
        }
    }
    public string printsho()
    {
        string strsho ="SHO";
        return strsho;
    }
    public string findowner()
    {
        try
        {
            VahanWSRef.VahanInfo serv = new VahanWSRef.VahanInfo();
            String resp;
            String regis, chas, engi;
            regis = dv.Rows[0]["registration"].ToString();
            chas = dv.Rows[0]["chasis"].ToString();
            engi = dv.Rows[0]["engine"].ToString();
            String response = serv.getDetails("DLNCRB", regis);
            response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
            resp = response;
            string rr;
            rr = accesswebservice.stringoutput1(resp);
            if (rr == "")
            {
                response = serv.getChasisDetails("DLNCRB", chas);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            if (rr == "")
            {
                response = serv.getEngineDetails("DLNCRB", engi);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                rr = accesswebservice.stringoutput1(response);
            }
            return rr;
        }
        catch
        {
            return "";
        }
    }
}