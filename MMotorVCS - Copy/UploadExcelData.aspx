﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="true" CodeFile="UploadExcelData.aspx.cs" Theme="emsTheme" Inherits="UploadExcelData"
    Title="Data Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table align="center" class="contentmain">
        <tr>
            <td>
                <table class="tablerowcolor" align="center" width="80%">
                    <tr class="heading">
                        <td colspan="2">
                            Import
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:FileUpload ID="FileUpload1" runat="server" />
                            <br />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </td>
                        <td align="center" valign="top">
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Import From IRDA Transaction File" />
                        </td>
                    </tr>
                    <tr>
                    <td colspan="3"><asp:Label ID="Label2" runat="server" Text=""></asp:Label></td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
