<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="frmChangePassword.aspx.cs" Inherits="frmChangePassword" Theme="emsTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.password-strength.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/md5.js"></script>
<%--<script type="text/javascript">
    $(document).ready(function() {
        var myPlugin = $("[id$='txtPassword']").password_strength();

        $("[id$='btnUpdate']").click(function() {
            return myPlugin.metReq(); //return true or false
        });

        $("[id$='passwordPolicy']").click(function(event) {
            var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
            window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
            event.preventDefault();
            return false;
        });

    });
</script>
    <SCRIPT language="javascript" type="text/javascript">
        function ValidateTPACode() {
            if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
                alert("Enter Password");
                document.getElementById("<%=txtPassword.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtConPass.ClientID%>").value == "") {
                alert("Enter Password for Confirmation");
                document.getElementById("<%=txtConPass.ClientID%>").focus();
                return false;
            }

            return true;
        }    
    </SCRIPT>--%>
    <table class="contentmain">
    <tr> <td> 
<asp:Table style="WIDTH: 80%" id="tblIncidents" runat="server" class="tablerowcolor" CellSpacing="1" CellPadding="0" HorizontalAlign="Center">
<asp:TableRow class="heading" >
<asp:TableCell ColumnSpan="6" ID="ErrorDisplayCell">
<asp:Label ID="lblmsg" CssClass="tblAppealCase" ForeColor="White"  runat="server"></asp:Label>
<asp:Label ID="lblHeader"   runat="server">Change Password </asp:Label>
<asp:Label ID="Label1" CssClass="tblAppealCase" ForeColor="White"  runat="server"></asp:Label>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow >
<asp:TableCell ColumnSpan="6">
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow6" runat="server" Width="70%" visible=false>
<asp:TableCell Width="24%" ID="TableCell6a" runat="server" >State
</asp:TableCell>
<asp:TableCell ID="tblState" runat="server" >&nbsp;
<asp:DropDownList ID="ddStateName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddStateName_SelectedIndexChanged"
                    Width="250px">
                </asp:DropDownList>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow1" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="tblcell1" runat="server" ><strong>User Name</strong>
</asp:TableCell>
<asp:TableCell ID="tblCell1a" runat="server" >&nbsp;
<asp:TextBox id="txtUsername" runat="server" Width="250px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow1" runat="server" Width="70%" >
<asp:TableCell Width="24%" ID="TableCell3" runat="server" >Old Password
</asp:TableCell>
<asp:TableCell ID="TableCell4" runat="server" >&nbsp;
<asp:TextBox id="txtOldPassword" TextMode="Password" runat="server" Width="250px" ></asp:TextBox>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow11" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell1b" runat="server" >New Password
</asp:TableCell>
<asp:TableCell ID="TableCell2b" runat="server" >&nbsp;
<asp:TextBox id="txtPassword"  runat="server" TextMode="Password"  Width="250px" ValidationGroup="ctr1"></asp:TextBox>
<asp:RegularExpressionValidator ID="Regex4" runat="server" ControlToValidate="txtPassword" 
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="'" TargetControlID="txtPassword"></cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow3" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell4b" runat="server" >Confirm Password
</asp:TableCell>
<asp:TableCell ID="TableCell3b" runat="server" >&nbsp;
<asp:TextBox id="txtConPass" runat="server" TextMode="Password"  Width="250px" ValidationGroup="ctr1"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtConPass"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}" 
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="'" TargetControlID="txtConPass"></cc1:FilteredTextBoxExtender>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow HorizontalAlign="center">
<asp:tableCell HorizontalAlign="center" runat="server" id="tblrow22" colspan="3">
<asp:Button id="btnUpdate" onclick="btnUpdate_Click" runat="server"  CssClass="button" Width="76px" Text="Update" ValidationGroup="ctr1"  ></asp:Button>
<asp:Button id="btnReset" onclick="btnReset_Click" runat="server" CssClass="button" Width="71px" Text="Reset" CausesValidation="false" ></asp:Button>
<asp:Button id="btndelete" runat="server" CssClass="button" Width="71px" Text="Delete"  Visible="False" ></asp:Button>
</asp:tableCell></asp:TableRow>

</asp:Table>
<asp:GridView id="gvUserDetails" runat="server" CssClass="grid" Width="80%" align="center"
            PageSize="15" OnRowCreated="gvUserDetails_RowCreated" 
            OnRowCommand="gvUserDetails_RowCommand" 
            OnPageIndexChanging="gvUserDetails_PageIndexChanging" 
            AutoGenerateColumns="False" AllowPaging="True" 
            OnRowDataBound="gvUserDetails_RowdataBound" BackColor="White" 
            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
            EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
                    <PagerSettings FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" />
                    <FooterStyle CssClass="gridfooter" BackColor="#CCCC99" />
                    <Columns>
                        <asp:TemplateField HeaderText="S No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                    Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="inRoleId" HeaderText="Role Id " SortExpression="inRoleId" />
                        <asp:BoundField DataField="vcUserName" HeaderText="User Name" SortExpression="vcUserName" />
                         <asp:TemplateField HeaderText="Password" visible="false">
                            <ItemTemplate>
                            <asp:Label id="lblPassword" runat="server" Text='<%# Eval("vcPassword") %>'></asp:Label>                              
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                    <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                    <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                        ForeColor="White" />
                    <PagerStyle CssClass="gridpager" ForeColor="Black" BackColor="#F7F7DE" 
                        HorizontalAlign="Right" />
                    <HeaderStyle CssClass="gridheader" BackColor="#6B696B" Font-Bold="True" 
                        ForeColor="White" />
                    <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                </asp:GridView> 
    </td></tr></table>
    <asp:HiddenField ID="hdnFieldOldPassword" runat="server" visible="true" />
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
