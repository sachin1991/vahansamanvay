﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="InternetQryOutput.aspx.cs" Inherits="InternetQryOutput" Theme="emsTheme"  ValidateRequest="true"%>


<head id="Head1" runat="server">
    <title>Vahan Samanvay</title>
    <script language="javascript" type="text/javascript">
        function PrintReport() {
            document.getElementById("btnPrint").style.display = "hidden"
            window.print();
          
        }
    </script>
    <style type="text/css">
        .style1
        {
           
        }
        .style2
        {
           
        }
        .style4
        {
           
        }
        .style5
        {
           
        }
        .style8
        {
           
        }
        .style9
        {
           
        }
        .style10
        {
           
        }
        .style11
        {            font-size: medium;
        }
        .style12
        {
           
        }
        .style13
        {
            height: 81px;
            width: 1213px;
        }
        .style14
        {
            font-size: small;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
       <asp:Panel ID="Panel1" runat="server"  Width="700px" style="background-position:center; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg">
            &nbsp;<table style="width: 593px; height: 128px" align="center">
                <tr>
                    <td class="style2">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/img/logo.jpg" Height="82px" 
                            Width="87px" CssClass="style14" /></td>
                    <td align="center" class="style13" >
                        
                        
                        <strong> <span class="style11">
                        Government of India<br />
                           <%-- &nbsp;भारत सरकार&nbsp;<br />--%>
                      Ministry of Home Affairs
                  <%--    <br />
                            गृह मंत्रालय--%>
                            <br />
                    National Crime Records Bureau
                           <%-- <br />
                            राष्ट्रीय अपराध रिकार्ड ब्यूरो--%>
                            <br />
                        <br />
                        Motor Vehicle Verificatio</span><span class="style14">n<br /> </span>
                                                  </strong>
                         </td>
                         
                </tr>
              
                <tr>
                    <td style="text-align: right; font-size: 10pt" colspan="2">
                        <strong><span class="style14">National Highway - 8, Service Road, <br />Mahipalpur, New Delhi - 110037<br />
                        <br />
                        </span> </strong>
                        <span class="style14">Dated:&nbsp;<asp:Label ID="Datofq" runat="server"></asp:Label>
                        <strong>
                        <br />
                        </strong>Reference No.
                        <asp:Label ID="Refnoq" runat="server"></asp:Label>
                        <strong>&nbsp;</strong></span><strong><br class="style14" /><span class="style14">&nbsp;</span></strong></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;font-size: 10pt">
                        <strong><span class="style8"><span class="style11">Vehicle Enquiry Report</span><br class="style11" /> </span>
                        <span class="style11">Only For Information For General Public</span><br class="style11" />
                        <span class="style11">(Not Valid For Insurance Claims/Any other Legal Purposes)</span></strong></td>
                </tr>
            </table>
                <table align="center">
                <tr>
                    <td style="text-align: center; font-size: 10pt" colspan="2">
                        &nbsp;</td>
                </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size: 10pt">
                            <b>
                            <asp:Label ID="Label1" runat="server" Visible="false" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left; " class="style14">
                            Reference enquiry received from Mr./Mrs.&nbsp;<asp:Label ID="Nameofq" 
                                runat="server"></asp:Label>
                            &nbsp;regarding</td>
                    </tr>
                <tr>
                    <td style="text-align: justify; font-size: 10pt ;" class="style5">
                        &nbsp;</td>
                    <td class="style4" style="text-align: justify; font-size: 10pt">
                        &nbsp;</td>
                </tr>
                    <tr>
                        <td class="style14" style="text-align: justify; ">
                            <strong>Vehicle Type :</strong></td>
                        <td class="style14" style="text-align: justify; ">
                            <asp:Label ID="lblvectype" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                <tr>
                    <td style="text-align: justify;  " class="style14">
                        <strong> Make :</strong></td>
                    <td style="text-align: justify; font-size: 10pt" class="style10">
                        <asp:Label ID="lblmake" runat="server" Font-Bold="True" CssClass="style14"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  " class="style14">
                        <strong> Registration No. :</strong></td>
                    <td class="style14" style="text-align: justify; ">
                        <asp:Label ID="lblregistrationno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  " class="style14">
                       <strong> Chassis No.:</strong></td>
                    <td class="style14" style="text-align: justify; ">
                        <asp:Label ID="lblchasisno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  " class="style14">
                        <strong>Engine No. :</strong></td>
                    <td class="style14" style="text-align: justify; ">
                        <asp:Label ID="lblengineno" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td class="style5" style="text-align: justify;  font-size: 10pt">
                            &nbsp;</td>
                        <td class="style4" style="text-align: justify; font-size: 10pt">
                            &nbsp;</td>
                    </tr>
                <tr>
                    <td colspan="2" class="style9">
                        <span class="style14">As per the information avaliable with NCRB till date 
                        (based on data recieved from States/UTs police) the above mentioned vehicle
                        </span> 
                        <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="style14"></asp:Label>
                    </td>
                </tr>
                </table>

                <table>
                <tr>
                    <td style="height: 19px;">
                        <asp:Panel ID="Panel2" runat="server" Width="100%">
                        
                        <asp:repeater id="Repeater1" runat="server">
                        <ItemTemplate>
														<table 

cellpadding="4" cellspacing="0" border="1" bordercolor="#000000" class="dataTbl"
															

width="100%">
															

															

	<tr valign="top" align="justify"  height="25">
                                                                <td nowrap ID="VehicleType" runat="Server"><b>Vehicle 

Type</b></td><td><%# DataBinder.Eval(Container, "DataItem.Vehicle")%></td></tr>
															

   <tr valign="top" align="justify"  height="25"><td nowrap ID="Make" runat="Server"><b>Make</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.Make")%></td></tr>
															

	<tr valign="top" align="justify"  height="25"><td nowrap ID="Registration" runat="Server"><b>Registration 

No</b></td><td><%# DataBinder.Eval(Container, "DataItem.Registration")%></td></tr>
															

    <tr valign="top" align="justify"  height="25"><td nowrap ID="Chasis" runat="Server"><b>Chasis No</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.Chasis")%></td></tr>
															

	<tr valign="top" align="justify"  height="25"><td nowrap ID="Engine" runat="Server"><b>Engine No</b></td>
    <td><%# DataBinder.Eval(Container, "DataItem.Engine")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Year" runat="Server"><b>Yr of manufacture</b></td><td><%# DataBinder.Eval(Container, "DataItem.Year")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Color" runat="Server"><b>Color</b></td><td><%# DataBinder.Eval(Container, "DataItem.Color")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Status" runat="Server"><b>Status</b></td> <td><%# DataBinder.Eval(Container, "DataItem.Status")%></td></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="MatchingParam" runat="Server"><b>Match Status</b></td><td><%# DataBinder.Eval(Container,

                                                                  "DataItem.MatchingParam")%></tr>
                                                               <tr valign="top" align="justify"  height="25"><td nowrap 

ID="Source" runat="Server"><b>Source</b></td><td><%# DataBinder.Eval(Container, "DataItem.Source")%></tr>


<tr valign="top" align="justify"  height="25">
															

	<td nowrap ID="State" runat="Server"><b>State /<br>District / <br>PS :</b></td>
                                                                <td align="center"><%# DataBinder.Eval(Container, 

"DataItem.StateName") %></td>
                                                             </tr>
															

<tr valign="top" align="justify"  height="25">
                                                            <td nowrap ID="FIR" runat="Server"><b>FIR</b></td><td><%# 

DataBinder.Eval(Container, "DataItem.FIR")%></td></tr>
<tr><td colspan='2'>&nbsp;</td></tr>
															
													</ItemTemplate>

													
														
													
													<FooterTemplate>
													</FooterTemplate>
												</asp:repeater>

                               </asp:Panel>
                    </td>
                </tr>
                </table><table>
                  <tr>
                    <td style="text-align: justify;  font-size: 10pt">
                        <asp:Label ID="lblusername" runat="server" Font-Bold="True"></asp:Label>
                        <br />
                       
                        &nbsp;
                        </td>
                    <td style="width: 194px">
                    </td>
                    <td style="width: 176px">
                    </td>
                </tr>
                <tr>
                    <td style="height: 21px;" align="left" colspan="3">
                        <%--<font color="#ffffff">--%>
                        &nbsp;</td>
                </tr>
                
                  <tr>
                      <td align="left" colspan="3" style="height: 21px;">
                          <%--<font color="#ffffff">--%><span class="style1" 
                              style="text-align: justify;  font-size: 8pt">Databank maintained by NCRB 
                          updated as on </span></font><span class="style12">
                          <%--                 <font>--%>
                          <asp:Label ID="lblUpdationDate" runat="server" Font-Size="8pt"></asp:Label>
                          </font>
                          <%-- <strong><font><span class="style12">&nbsp;</span></font>
                        </strong>--%><span class="style12" style="font-size: 8pt">printed on</span></span><font 
                              color="#ffffff">
                          <asp:Label ID="lblenqdatetime" runat="server" Font-Size="8pt" 
                              style="color: #000000"></asp:Label>
                          </font>
                      </td>
                  </tr>
                  <tr>
                      <td align="left" colspan="3" style="height: 21px;">
                          &nbsp;</td>
                  </tr>
                
            </table>
          
            </asp:Panel>
        <asp:Button ID="btnPrint" runat="server" Text="Print" Visible="false"/></div>
    <p>
        &nbsp;</p>
    </form>
</body>
</html>
