﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" SmartNavigation="true"
    AutoEventWireup="true" CodeFile="frmEdit.aspx.cs" Inherits="frmEdit" EnableViewState="true"
    EnableViewStateMac="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
    <asp:Label ID="lblmsg" runat="server"></asp:Label>
    Registration No
    <asp:TextBox ID="regno" runat="server" AutoPostBack="True"></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="InvalidChars"
        InvalidChars="!+=-/\@#$%^&amp;*(){}[],.:'<>" TargetControlID="regno">
    </cc1:FilteredTextBoxExtender>
    Chasis No
    <asp:TextBox ID="chasisno" runat="server" AutoPostBack="True"></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars"
        InvalidChars="!+=-/\@#$%^&amp;*(){}[],.:'<>" TargetControlID="chasisno">
    </cc1:FilteredTextBoxExtender>
    Engine No
    <asp:TextBox ID="engineno" runat="server" AutoPostBack="True"></asp:TextBox>
    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars"
        InvalidChars="!+=-/\@#$%^&amp;*(){}[],.:'<>" TargetControlID="engineno">
    </cc1:FilteredTextBoxExtender>
    <asp:Button ID="submit" runat="server" Text="Search" OnClick="submit_Click"></asp:Button>
    <asp:GridView ID="gvEG" runat="server" AutoGenerateColumns="False" CssClass="grid"
        AlternatingRowStyle-CssClass="gridAltRow" RowStyle-CssClass="gridRow" ShowFooter="True"
        EditRowStyle-CssClass="gridEditRow" FooterStyle-CssClass="gridFooterRow" OnRowCancelingEdit="gvEG_RowCancelingEdit"
        OnRowCommand="gvEG_RowCommand" OnRowDataBound="gvEG_RowDataBound" OnRowDeleting="gvEG_RowDeleting"
        OnRowEditing="gvEG_RowEditing" OnRowUpdating="gvEG_RowUpdating" DataKeyNames="ID,crimeno,regnokey,chasisnokey,enginenokey"
        OnSelectedIndexChanged="gvEG_SelectedIndexChanged">
        <Columns>
            <asp:TemplateField HeaderText="Crime No" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="50px">
                <EditItemTemplate>
                    <asp:Label ID="txtCrimeNo" runat="server" Text='<%# Bind("crimeno") %>' MaxLength="6"
                        Width="50px"></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <%# Eval("crimeno")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Registration No" HeaderStyle-HorizontalAlign="Left"
                ControlStyle-Width="90px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtRegistration" runat="server" Text='<%# Bind("regnokey") %>' Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRegistration" ValidationGroup="Update" runat="server"
                        ControlToValidate="txtRegistration" ErrorMessage="Please Enter Registration No"
                        ToolTip="Please Enter Registration Number" SetFocusOnError="true" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars"
                        InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtRegistration">
                    </cc1:FilteredTextBoxExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <%# Eval("regnokey")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Chasis No" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="90px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtChasis" runat="server" Text='<%# Bind("chasisnokey") %>' Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvChasis" ValidationGroup="Update" runat="server"
                        ControlToValidate="txtChasis" ErrorMessage="Please Enter Chasis No" ToolTip="Please Chasis No"
                        SetFocusOnError="true" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fetxtChasis" runat="server" FilterMode="InvalidChars"
                        InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtChasis">
                    </cc1:FilteredTextBoxExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <%# Eval("chasisnokey")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Engine No" HeaderStyle-HorizontalAlign="Left" ControlStyle-Width="90px">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEngine" runat="server" Text='<%# Bind("enginenokey") %>' Width="250px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEngine" ValidationGroup="Update" runat="server"
                        ControlToValidate="txtEngine" ErrorMessage="Please Enter Engine No" ToolTip="Please Engine No"
                        SetFocusOnError="true" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="fetxtEngineno" runat="server" FilterMode="InvalidChars"
                        InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtEngine">
                    </cc1:FilteredTextBoxExtender>
                </EditItemTemplate>
                <ItemTemplate>
                    <%# Eval("enginenokey")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Auto Type" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblAutoTypeCode" Visible="false" runat="server" Text='<%# Bind("[DepartmentId]") %>'></asp:Label>
                    <asp:Label ID="lblAutoType" runat="server" Text='<%# Bind("autotypedesc") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Make" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:Label ID="lblMakeCode" Visible="false" runat="server" Text='<%# Bind("automake") %>'></asp:Label>
                    <asp:Label ID="lblMake" runat="server" Text='<%# Bind("automakedesc") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>                
                 <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                    <%--<%# Eval("Status")%>--%>
                </ItemTemplate>
                <EditItemTemplate>
                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' Visible="false"></asp:Label>
                <asp:DropDownList ID="ddlStatus" runat="server">
                <asp:ListItem>L</asp:ListItem>
                <asp:ListItem>R</asp:ListItem>
                </asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Deleted">
                <ItemTemplate>
                    <asp:Label ID="lblDeleted" runat="server" Text='<%# Eval("isDeleted") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit" ShowHeader="False" HeaderStyle-HorizontalAlign="Left">
                <EditItemTemplate>
                    <asp:LinkButton ID="lnkUpdate" runat="server" CausesValidation="True" CommandName="Update"
                        Text="Update" OnClientClick="return confirm('Update?')" ValidationGroup="Update"></asp:LinkButton>
                    <asp:ValidationSummary ID="vsUpdate" runat="server" ShowMessageBox="true" ShowSummary="false"
                        ValidationGroup="Update" Enabled="true" HeaderText="Validation Summary..." />
                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                        Text="Cancel"></asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="Edit"
                        Text="Edit"></asp:LinkButton>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <table>
                <tr class="gridRow">
                    <td colspan="8">
                        No Records found...
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
</asp:Content>
