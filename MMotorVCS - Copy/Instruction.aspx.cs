using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;

public partial class Instruction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            if (clsPageBase.deletealreadyloggedin(Session["vcUserName"].ToString()) > 0)
            {
            }
            Response.Redirect("LogoutModule.aspx");
        }
        
        if (!IsPostBack)
        {
            FillMenu();
            GetDashBoardCount();
        }
        string captcha;
        captcha=Session["strRandom"].ToString();
   
    }

    private void FillMenu()
    {

        // the top and bottom menu is filled

        DataTable dTblConfigMenu = new DataTable();
        DataTable dTblMenuDetails = new DataTable();

        try
        {

            Menu mnuMaster;
            mnuMaster = (Menu)Master.FindControl("mnuMaster");
            mnuMaster.Style.Clear();
            mnuMaster.StaticMenuItemStyle.CssClass = "topMenuNav";
            mnuMaster.DynamicMenuItemStyle.CssClass = "topMenuNavsubItem1";
            mnuMaster.DynamicMenuStyle.CssClass = "topMenuNavsubItem";

            FormatManager objFM = new FormatManager();
            int mon = 0;
            dTblMenuDetails = objFM.GetURLConfigData(mon);


            MenuItem mnuItem;
            MenuItem mnuSepItem;
            MenuItem mnuDrpItem2, mnuDrpItem3, mnuDrpItem4, mnuDrpItem5, mnuDrpItem6;

            for (int i = 0; i < dTblMenuDetails.Rows.Count; i++)
            {
                mnuItem = new MenuItem();
                mnuItem.NavigateUrl = GenericMethods.check(dTblMenuDetails.Rows[i].ItemArray[1].ToString());
                mnuItem.Text = GenericMethods.check(dTblMenuDetails.Rows[i].ItemArray[0].ToString());
                

                switch (GenericMethods.check(dTblMenuDetails.Rows[i].ItemArray[2].ToString().Trim()))
                {

                    case "1":
                        break;
                    case "2":
                        mnuItem.Target = "_blank";
                        break;
                    case "3":
                        mnuItem.Target = "Window1";
                        break;

                    default:
                        break;

                }

                mnuMaster.Items.Add(mnuItem);

                if (i != dTblMenuDetails.Rows.Count - 1)
                {
                    mnuSepItem = new MenuItem();
                    mnuSepItem.SeparatorImageUrl = "~/IMAGES/spacer.gif";
                    mnuSepItem.Text = "|";
                    mnuSepItem.Enabled = false;
                    mnuMaster.Items.Add(mnuSepItem);

                }

            }

        }

        catch (Exception exGeneral)
        {

        }
        finally
        {
            dTblConfigMenu = null;
            dTblMenuDetails = null;

        }
    }
    public void GetDashBoardCount()
    {
        try
        {
            DateTime dt = DateTime.Now;
            //string dt = "2015-12-23";
            string strcon = ConfigurationManager.ConnectionStrings["Conn"].ToString();
            SqlConnection conn = new SqlConnection(strcon);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "sp_DashBoardCount";
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            cmd.Parameters.AddWithValue("@dtCreated", dt);
            cmd.ExecuteNonQuery();
            SqlDataAdapter adr = new SqlDataAdapter(cmd);
            DataSet dscount = new DataSet();
            adr.Fill(dscount);
            //For Today
            Enteredtoday.Text = dscount.Tables[0].Rows[0]["endereddailycount"].ToString();
            CounterQuery.Text = dscount.Tables[1].Rows[0]["countercount"].ToString();
            Authoritiesquery.Text = dscount.Tables[2].Rows[0]["authoritycount"].ToString();
            Publicquery.Text = dscount.Tables[3].Rows[0]["internetcount"].ToString();
            //For Month
            Enteredtoday0.Text = dscount.Tables[4].Rows[0]["enderedmonthcount"].ToString();
            CounterQuery0.Text = dscount.Tables[5].Rows[0]["countermonthcount"].ToString();
            Authoritiesquery0.Text = dscount.Tables[6].Rows[0]["authoritymonthcount"].ToString();
            Publicquery0.Text = dscount.Tables[7].Rows[0]["internetmonthcount"].ToString();
            //For Year
            Enteredtoday1.Text = dscount.Tables[8].Rows[0]["endereddailyyearcount"].ToString();
            CounterQuery1.Text = dscount.Tables[9].Rows[0]["counteryearcount"].ToString();
            Authoritiesquery1.Text = dscount.Tables[10].Rows[0]["authorityyearcount"].ToString();
            Publicquery1.Text = dscount.Tables[11].Rows[0]["internetyearcount"].ToString();
            //For Total
            Totalquery.Text = Convert.ToString(Convert.ToInt32(Enteredtoday.Text) + Convert.ToInt32(CounterQuery.Text) + Convert.ToInt32(Authoritiesquery.Text) + Convert.ToInt32(Publicquery.Text));
            Totalquery0.Text = Convert.ToString(Convert.ToInt32(Enteredtoday0.Text) + Convert.ToInt32(CounterQuery0.Text) + Convert.ToInt32(Authoritiesquery0.Text) + Convert.ToInt32(Publicquery0.Text));
            Totalquery1.Text = Convert.ToString(Convert.ToInt32(Enteredtoday1.Text) + Convert.ToInt32(CounterQuery1.Text) + Convert.ToInt32(Authoritiesquery1.Text) + Convert.ToInt32(Publicquery1.Text));
            conn.Close();
            //countcctns();
        }
        catch (Exception ex)
        {
        }


    }
    //protected void countcctns()
    //{
    //    try
    //    {
    //        string strconcctns = ConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString();
    //        SqlCommand cmdcctns = new SqlCommand();
    //        SqlConnection conncctns = new SqlConnection(strconcctns);
    //        cmdcctns.Connection = conncctns;
    //        cmdcctns.CommandText = "sp_countrecords";
    //        cmdcctns.CommandType = CommandType.StoredProcedure;
    //        conncctns.Open();
    //        cmdcctns.ExecuteNonQuery();
    //        SqlDataAdapter adrcctns = new SqlDataAdapter(cmdcctns);
    //        DataSet dscountcctns = new DataSet();
    //        adrcctns.Fill(dscountcctns);
    //        cctnscount.Text = dscountcctns.Tables[0].Rows[0]["cctnscount"].ToString();
    //        conncctns.Close();
    //    }
    //    catch
    //    {
    //    }
    //}
}
