﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="AuthorityNOC.aspx.cs" Inherits="AuthorityNOC" Theme="emsTheme"  ValidateRequest="true"%>


<head id="Head1" runat="server">
    <title></title>

    <script language="javascript" type="text/javascript">
        function PrintReport() {
            document.getElementById("btnPrint").style.display = "hidden"
            window.print();

        }
        

    </script>
    <style type="text/css">
        .style5
        {
        }
        .style9
        {
        }
        .style11
        {
        }
        .style12
        {
        }
        .style13
        {
        }
        .style14
        {
            text-align: center;
        }
        .style15
        {
            height: 23px;
        }
        </style>
</head>
<body onunload="return window_onunload()">
    <form id="form1" runat="server">
    <div align="center">
       <asp:Panel ID="Panel1" runat="server"  Width="80%" style="background-position:center; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg">
            &nbsp;<table align="center">
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"" colspan="2" 
                        class="style15">
                        <asp:Label ID="lblNo" runat="server" Text="No."></asp:Label>
                        <asp:Label ID="lblFile" runat="server"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td colspan="2" style="text-align: left; font-size: 12pt; font-family=Arial">
                            <asp:Label ID="lblDate" runat="server" style="text-align: right" Text="Dated"></asp:Label>
                            <asp:Label ID="DateText" runat="server"></asp:Label>
                        </td>
                </tr>
                    <tr>
                        <td style="text-align: center; font-size: 12pt; font-family=Arial"" colspan="2">
                            <%# DataBinder.Eval(Container, "DataItem.statename")%>
                            <asp:Label ID="AuthorityOffice" runat="server" style="text-align: center"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12pt; font-family=Arial" colspan="2" class="style14">
                            No Objection Certificate</td>
                    </tr>
              
                <tr>
                
                
                    <td style="text-align: left; font-size: 8pt; height: inherit " colspan="2">
                        The Owner of following vehicle has applied for no objection certificate to 
                        register this vehicle out of 
                        <asp:Label ID="AuthorityOffice0" runat="server"></asp:Label>
                    </td>
                             
                </tr>
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                        &nbsp;</td>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                        &nbsp;</td>
                </tr>
                <tr>
                
                
                    <td style="text-align: left; width:100%; font-size: 12pt; font-family=Arial"">
                        <table border="1" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <strong>Registration No</strong></td>
                                <td>
                                    <strong>Chassis No</strong></td>
                                <td>
                                    <strong>Engine No</strong></td>
                                <td>
                                    <strong>Manufacturing Year</strong></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblregistrationno" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblchasisno" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblengineno" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblModel" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Vehicle</strong></td>
                                <td>
                                    <strong>Make&nbsp;</strong></td>
                                <td>
                                    <strong>Color </strong>
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblvectype" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblmake" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblColor" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                             
                </tr>
                <tr>
                    <td style="text-align: justify; font-size: 12pt; font-family=Arial"  class="style5" 
                        colspan="2">
                        
                        <strong>
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </strong>
                </tr>
                <tr>
                   <td>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <HeaderTemplate>
                                <table border="1" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top">
                                            <b>Registraiton number</b></td>
                                        <td valign="top">
                                            <b>Chasis number</b></td>
                                        <td valign="top">
                                            <b>Engine number</b></td>
                                        <td valign="top">
                                            <b>Model</b></td>
                                        <td valign="top">
                                            <b>Manufacturing Year</b></td>
                                        <td valign="top">
                                            <b>color</b></td>
                                        <td valign="top">
                                            <b>Matched on</b></td>
                                        <td valign="top">
                                            <b>Matched detail</b></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr >
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Registration")%>
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Chasis")%>
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Engine")%>
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Vehicle")%>&nbsp;&nbsp;
                                        <%# DataBinder.Eval(Container, "DataItem.Make")%>&nbsp;
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Year")%>&nbsp;
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Color")%>&nbsp;
                                    </td>
                                    <td>
                                        <%# DataBinder.Eval(Container, "DataItem.Status")%>
                                        <%# DataBinder.Eval(Container, "DataItem.MatchingParam")%>
                                    </td>
                                    <td>
                                        <%--<%# DataBinder.Eval(Container, "DataItem.Source")%>--%>
                                        <%#DataBinder.Eval(Container, "DataItem.FIR")%>
                                        <%# DataBinder.Eval(Container, "DataItem.StateName") %>
                                    </td>
                                </td>
                                </tr>
                                
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td class="style11" style="text-align: justify;  font-size: 8pt">
                        <span class="style12">Accessed on</span><strong><span class="style12"> </span>
                        </strong>
                        &nbsp;<span class="style13"><asp:Label ID="lblUpdated" runat="server" 
                            CssClass="style12" Font-Size="8pt"></asp:Label>
                        <asp:Label ID="captcha" runat="server" Visible="False"></asp:Label>
                        &nbsp;</span>user:
                        <asp:Label ID="AuthorityOffice1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style11" style="text-align: justify;  font-size: 8pt">
                        <span class="style13">Data Bank Maintained by National Crime Records Bureau, New 
                        Delhi &nbsp;Last updated </span>
                        <span class="style12">&nbsp;<asp:Label ID="lblPrinted" runat="server" 
                            CssClass="style12" Font-Size="8pt"></asp:Label>
                        </span></td>
                </tr>
                <tr>
                    <td class="style11" colspan="2" style="text-align: justify;  font-size: 8pt">
                        <span class="style13">
                        <br />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td class="style9" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td alsign="right" colspan="2" style="text-align: right">
                        <strong style="text-align: right">
                        <br />
                        <asp:Label ID="lblFooter" runat="server" Font-Bold="True"></asp:Label>
                        </strong>
                    </td>
                </tr>
                </table>

                <table>
                <tr>
                    <td style="height: 19px;">
                        <asp:Panel ID="Panel2" runat="server" Width="100%">
                        
                               </asp:Panel>
                    </td>
                </tr>
                </table>
          
            </asp:Panel>
        <asp:Button ID="btnPrint" runat="server" Text="Print" Visible="false"/>
        <br />
    </div>
    <p>
                            <asp:Repeater ID="rptAddress" runat="server" 
            Visible="False">
                            <ItemTemplate>
                            <b>SHO:</b>
                            
                            <%# DataBinder.Eval(Container, "DataItem.statename")%>
                            <br />
                             <b>SCRB:</b>
                            <%# DataBinder.Eval(Container, "DataItem.STDESC")%>
                            </ItemTemplate>

                            </asp:Repeater>
                            </p>
    <p>
        <asp:Label ID="toaddress" runat="server" Text="To"></asp:Label>
    </p>
    </form>
</body>
</html>
