﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PopupBlockers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                lblError.Text = "Please Turn off the Popup Blocker for this site.<br> Under Internet Options/Tabs Settings/Always open pop ups in a new tab";

            }
            catch (Exception ex)
            {
                //lblError.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
                    lblError.Text = "Error";
            }
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
}