<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Instruction.aspx.cs" Inherits="Instruction"
    Title="Vahan Samanvay" EnableEventValidation="false"  EnableViewState="true" EnableViewStateMac="true"  ViewStateEncryptionMode="Always"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"
    Visible="true">
    
                                <table border="1" cellspacing="0" cellpadding="0" style="background:white">
                                <tr style="background-color: skyblue;">
                                    <td class="style3">
                                        Entry/Query
                                    </td>
                                    <td class="style3">
                                        Today
                                    </td>
                                    <td class="style3">
                                        Current Month
                                    </td>
                                    <td class="style3">
                                        Current Year
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td class="rA">
                                        Entered Records
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Enteredtoday" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Enteredtoday0" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Enteredtoday1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rA">
                                        Counter
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="CounterQuery" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="CounterQuery0" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="CounterQuery1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rA">
                                        Authorities
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Authoritiesquery" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Authoritiesquery0" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Authoritiesquery1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="rA">
                                        Public
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Publicquery" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Publicquery0" runat="server"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Publicquery1" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="background-color: skyblue;">
                                    <td class="rA">
                                        <strong>Total Usage</strong>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Totalquery" runat="server" Style="font-weight: 700"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Totalquery0" runat="server" Style="font-weight: 700"></asp:Label>
                                        </td>
                                        <td align="right">
                <asp:Label ID="Totalquery1" runat="server" Style="font-weight: 700"></asp:Label>
                </td>
                                </tr>
                               <%-- <tr>
                                <td colspan="3"><b>
                                  Motor Vehicle Data fetched from CCTNS</b>
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="cctnscount" runat="server" Style="font-weight: 700"></asp:Label>
                                    </td>
                                </tr>--%>
                        </td>
                </table>


    </asp:Content>