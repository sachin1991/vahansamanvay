﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" SmartNavigation="true"  AutoEventWireup="true" CodeFile="ReportStatistics.aspx.vb" Inherits="ReportStatistics" Title="Vahan Samanvay Statistics Report" Theme="emsTheme"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <Triggers> 
<asp:PostBackTrigger ControlID="Button1" /> 
<asp:PostBackTrigger ControlID="Converttoexecl" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 
    <ContentTemplate>
            <table class="contentmain">
                <tr>
                    <td >
                    <table class="tablerowcolor" align="center" width="80%">
                    <tr class="heading"> <td colspan="4"> Vehicle Statistics</td></tr>
                    <tr> <td>  
                                           <br />
                                           <br />
                                           <br />
                                           <asp:RadioButton ID="RdType" runat="server" GroupName="stg" Text="Type" 
                                               Visible="False" />
                                           <br />
                                           <asp:RadioButton ID="RdMake" runat="server" GroupName="stg" Text="Make" 
                                               Visible="False" />
                        </td>
                    <td >
                                           <asp:RadioButton ID="RdState" runat="server" GroupName="stg" 
                            Text="Statewise" />
                                           <br />
                                           <asp:RadioButton ID="RdStateDistrict" runat="server" GroupName="stg" 
                                               Text="Statewise, Districtwise" />
                                           <br />
                                           <asp:RadioButton ID="RdStateDistrictPS" runat="server" 
                                               GroupName="stg" 
                            Text="Statewise, Distrcitwise, PS wise" />
                                           <br />
                        <asp:RadioButton ID="RdStateType" runat="server" GroupName="stg" 
                            Text="Statewise, Typewise" />
                        <br />
                        <asp:RadioButton ID="RdStateDistrictType" runat="server" GroupName="stg" 
                            Text="Statewise, Districtwise, Typewise" />
                        <br />
                        <asp:RadioButton ID="RdStateDistrictPSType" runat="server" GroupName="stg" 
                            Text="Statewise, Districtwise, PS wise, Typewise" />
                        <br />
                        <asp:RadioButton ID="RdStateTypeMake" runat="server" GroupName="stg" 
                            Text="Statewise, Typewise, Makewise" />
                        <br />
                        <asp:RadioButton ID="RdStateDistrictTypeMake" runat="server" GroupName="stg" 
                            Text="Statewise, Districtwise, Typewise, Makewise" />
                        <br />
                        <asp:RadioButton ID="RdStateDistrictPSTypeMake" runat="server" GroupName="stg" 
                            Text="Statewise, Districtwise, PS wise, Typewise, Makewise" 
                            Checked="True" />
                        </td>
                    <td >
                        &nbsp;</td>
                            <td runat="server" visible="false">  &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        </td>
                    <td >
                    </td>
                    <td>
                    </td>
                     <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label3" runat="server" Text="State"></asp:Label></td>
                    <td>
                        <asp:DropDownList ID="ddlState" runat="server" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                            
                        </asp:DropDownList></td>
                    <td>
                    </td>
                     <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label4" runat="server" Text="District"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="Districtwise" runat="server" OnSelectedIndexChanged="Districtwise_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                           
                        </asp:DropDownList></td>
                    <td >
                    </td>
<td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label5" runat="server" Text="PS"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="PSWise" runat="server" Width="250px">
                           
                        </asp:DropDownList></td>
                    <td >
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label6" runat="server" Text="Type"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="TypeWise" runat="server" OnSelectedIndexChanged="TypeWise_SelectedIndexChanged" AutoPostBack="True" Width="250px">
                                       </asp:DropDownList></td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label7" runat="server" Text="Make"></asp:Label></td>
                    <td >
                        <asp:DropDownList ID="MakeWise" runat="server" Width="250px">
                        
                        </asp:DropDownList></td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label></td>
                    <td >
                       
                        <asp:TextBox ID="ListDatefrom" runat="server" ></asp:TextBox></td>
                    <td >
                         <ajaxtoolkit:calendarextender 
                    ID="ChargeSheetCalendarExtender" 
                    runat="server" 
                    TargetControlID="ListDatefrom" 
                    Format="dd-MM-yyyy" 
                     PopupPosition="Right"  /></td> <td></td>
                </tr>
                <tr>
                    <td style="height: 25px" >
                        <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></td>
                    <td style="height: 25px" >
                        <asp:TextBox ID="listDateto" runat="server"></asp:TextBox>
                         
                        </td>
                    <td style="height: 25px" >
                         <ajaxtoolkit:calendarextender 
                    ID="CalendarExtender1" 
                    runat="server" 
                    TargetControlID="listDateto" 
                    Format="dd-MM-yyyy" 
                     PopupPosition="Right"  /></td> <td style="height: 25px"></td>
                </tr>
                <tr>
                    <td >
                        &nbsp;</td>
                    <td >
                        <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                        </td>
                    <td >
                        &nbsp;</td> <td>&nbsp;</td>
                </tr>
                <tr>
                    <td >
                    </td>
                    <td >
                     <asp:Label ID="Msglabel" runat="server"></asp:Label>
                        <asp:Button ID="Button1" runat="server" Text="View" />
                         <asp:Button ID="btnReset" runat="server" Text="Reset" />
                        </td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                    </td>
                    <td >
                        <asp:Button ID="databasesize" runat="server" Text="Database Size" />
                        <asp:Label ID="lbldatabasesize" runat="server"></asp:Label>
                    </td>
                    <td >
                    </td> <td></td>
                </tr>
                <tr>
                    <td >
                        &nbsp;</td>
                    <td >
                    <asp:Button ID="Converttoexecl" runat="server" 
                            Text="Generate to excel (.csv) format file" />
                    </td>
                    <td >
                        &nbsp;</td> <td>&nbsp;</td>
                </tr>
            </table>
            </td></tr></table>

</ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>

