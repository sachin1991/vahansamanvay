﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="WirelessMessageAuthority.aspx.cs" Inherits="WirelessMessageAuthority" Theme="emsTheme"  ValidateRequest="true"%>


<head id="Head1" runat="server">
    <title></title>

    <script language="javascript" type="text/javascript">
        function PrintReport() {
            document.getElementById("btnPrint").style.display = "hidden"
            
            window.print();

        }
        

    </script>
    <style type="text/css">
        .style5
        {
        }
        .style9
        {
        }
        .style11
        {
        }
        .style12
        {
        }
        .style13
        {
        }
        </style>
</head>
<body onunload="return window_onunload()">
    <form id="form1" runat="server">
    <div align="center">
       <asp:Panel ID="Panel1" runat="server"  Width="700px" style="background-position:center; background-repeat:no-repeat;" BackImageUrl="~/img/NCRBimg.jpg">
            &nbsp;<table align="center">
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"" colspan="2">
                        <asp:Label ID="toaddress" runat="server" style="font-weight: 700" Text="To"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td style="text-align: left; font-size: 12pt; font-family=Arial"" colspan="2">
                            <asp:Repeater ID="rptAddress" runat="server">
                            <ItemTemplate>
                            <b>SHO:</b>
                            
                            <%# DataBinder.Eval(Container, "DataItem.statename")%>
                            <br />
                             <b>SCRB:</b>
                            <%# DataBinder.Eval(Container, "DataItem.STDESC")%>
                            </ItemTemplate>

                            </asp:Repeater>
                            <br />
                            <strong>From: </strong><%# DataBinder.Eval(Container, "DataItem.statename")%>
                            <asp:Label ID="AuthorityOffice" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-size: 12pt; font-family=Arial"" colspan="2">
                            <strong>
                            Info: </strong>Director General National Crime Records Bureau, <br />
                            National Highway - 8, Service Road, <br />Mahipalpur, New Delhi - 110037<br /></td>
                    </tr>
              
                <tr>
                
                
                    <td style="text-align: left; font-size: 8pt; height: inherit " colspan="2">
                        <hr /></td>
                             
                </tr>
                <tr>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                        <asp:Label ID="lblNo" runat="server" Text="No."></asp:Label>
                        <asp:Label ID="lblFile" runat="server"></asp:Label>
                        
                    </td>
                    <td style="text-align: left; font-size: 12pt; font-family=Arial"">
                   <asp:Label ID="lblDate" runat="server" style="text-align: right" Text="Dated"></asp:Label>
                        <asp:Label ID="DateText" runat="server"></asp:Label>
                        </td>
                </tr>
                <tr>
                
                
                    <td style="text-align: left; font-size: 8pt; height: inherit " colspan="2">
                        <hr /></td>
                             
                </tr>
                <tr>
                    <td style="text-align: justify; font-size: 12pt; font-family=Arial"  class="style5" 
                        colspan="2">
                        <strong>Vehicle: </strong><asp:Label ID="lblvectype" runat="server" ></asp:Label>
                        &nbsp;(.) <strong>Make&nbsp;:</strong>
                        <asp:Label ID="lblmake" runat="server"></asp:Label>
                        &nbsp;(.) <strong>Registration No.&nbsp; :</strong><asp:Label ID="lblregistrationno" 
                            runat="server" ></asp:Label>
                        &nbsp;(.)
                        <strong> &nbsp;Chassis No. :</strong><asp:Label ID="lblchasisno" runat="server" 
                           ></asp:Label>
                        &nbsp;(.)<strong>  Engine No.&nbsp;:</strong><asp:Label ID="lblengineno" runat="server" 
                            ></asp:Label>
                        (.)
                        <strong> &nbsp;Color :</strong><asp:Label ID="lblColor" runat="server" 
                            ></asp:Label>
                          &nbsp;(.) <strong>Year of Manufacture:</strong><asp:Label ID="lblModel" runat="server" 
                            ></asp:Label>
                        (.)</td>
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" class="style5" colspan="2">
                        <strong>
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </strong></td>
                </tr>
                <tr>
                    <td class="style11" style="text-align: justify;  font-size: 12pt; font-family=Arial"" colspan="2">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify;  font-size: 12pt; font-family=Arial"" class="style5" colspan="2">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <table border="0" bordercolor="#000000" cellpadding="4" cellspacing="0" 
                                    class="dataTbl" width="60%">
                                    <tr align="justify" height="25" valign="top">
                                        
                                            <b>Vehicle Type:</b>
                                       
                                            <%# DataBinder.Eval(Container, "DataItem.Vehicle")%>
                                        
                                      
                                            <b>(.)Make:</b>
                                       
                                            <%# 

DataBinder.Eval(Container, "DataItem.Make")%>
                                        
                                        
                                            <b>(.)Registration No:</b>
                                       
                                            <%# DataBinder.Eval(Container, "DataItem.Registration")%>
                                        
                                        
                                            <b>(.)Chasis No:</b>
                                        
                                            <%# DataBinder.Eval(Container, "DataItem.Chasis")%>
                                     
                                    </tr>
                                    <tr align="justify" height="25" valign="top">
                                      
                                            <b>(.)Engine No"</b>
                                       
                                            <%# DataBinder.Eval(Container, "DataItem.Engine")%>
                                       
                                       
                                            <b>(.)Model:</b>
                                        
                                            <%# DataBinder.Eval(Container, "DataItem.Year")%>
                                      
                                       
                                            <b>(.)Color:</b>
                                        
                                            <%# DataBinder.Eval(Container, "DataItem.Color")%>
                                        
                                       
                                            <b>(.)Status:</b>
                                        
                                            <%# DataBinder.Eval(Container, "DataItem.Status")%>
                                        
                                    </tr>
                                    <tr align="justify" height="25" valign="top">
                                        
                                            <b>(.)Match Status:</b>
                                        
                                            <%# DataBinder.Eval(Container, "DataItem.MatchingParam")%>
                                           
                                                <b>(.)Source:</b>
                                         
                                                <%# DataBinder.Eval(Container, "DataItem.Source")%>
                                                 <b>(.)FIR:</b>
                                               
                                                    <%#DataBinder.Eval(Container, "DataItem.FIR")%>
                                                    <b>(.)PS/Distric/State :
                                                    
                                                    </b>
                                              
                                                   
                                                 <%# DataBinder.Eval(Container, "DataItem.StateName") %>
                                              
                                                                    <br />
                                               <br />
                                           <br />
                                               
                                           
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                    <tr>
                        <td class="style11" style="text-align: justify;  font-size: 8pt" colspan="2">
                            &nbsp;</td>
                    </tr>
                <tr>
                    <td class="style11" style="text-align: justify;  font-size: 8pt" colspan="2">
                        <span class="style13">Data Bank Maintained by NCRB
                        <asp:Label ID="captcha" runat="server"></asp:Label>
                        updated on </span>
                        <asp:Label ID="lblUpdated" runat="server" CssClass="style12" Font-Size="8pt"></asp:Label>
                        <strong><span class="style12">&nbsp;</span></strong><span class="style12">printed on</span><strong><span 
                            class="style12"> </span></strong>
                        <asp:Label ID="lblPrinted" runat="server" Font-Size="8pt" CssClass="style12"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style9" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td alsign="right" style="text-align: right" colspan="2">
                        <strong style="text-align: right">
                        <br />
                        <asp:Label ID="lblFooter" runat="server" Font-Bold="True"></asp:Label>
                        </strong></td>
                </tr>
                </table>

                <table>
                <tr>
                    <td style="height: 19px;">
                        <asp:Panel ID="Panel2" runat="server" Width="100%">
                        
                               </asp:Panel>
                    </td>
                </tr>
                </table>
          
            </asp:Panel>
        <asp:Button ID="btnPrint" runat="server" Text="Print" Visible="false"/></div>
    </form>
</body>
</html>
