Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS

Partial Class ReportLoginlog
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Private Sub preparesql()
        Dim sql, sqlSumTotal As String
        Dim cmd As New SqlCommand
        Dim whr As String
        Dim ORD As String

        If Me.currentlylogged.Checked = True Then
            Session("currentlylogged") = True
            sql = "Select vcUserName,vcHeader,vcAddress,statename,datetm,logoutdatetm,clientadr from AlreadyLoggedinView"
        Else
            If Me.UnusedLogins.Checked = True Then
                Session("currentlylogged") = False
                sql = "Select UserName,vcHeader,vcAddress,statename from unusedlogins"
            Else
                If Me.Loginlist.Checked = True Then
                    Session("currentlylogged") = False
                    sql = "Select vcUserName,vcHeader,vcAddress,statename from LoginAddress"
                Else
                    Session("currentlylogged") = False
                    sql = "Select vcUserName,vcHeader,vcAddress,statename,datetm,logoutdatetm,clientadr from loggedinlogview"
                End If
            End If
        End If

        sqlSumTotal = "select SUM(CAST(CashAmt AS INT)) as netamount FROM TblTrn_Counter_Enquiry "

        whr = ""
        If Me.Loginlist.Checked Then
            ORD = ""
        Else
            If Me.UnusedLogins.Checked Then
            Else
                ORD = " ORDER BY "
                ORD = ORD & "datetm"
            End If

        End If
        Dim mtf As String
        mtf = Month(Me.ListDatefrom.Text.ToString())
        If Len(mtf) = 1 Then
            mtf = "0" & mtf
        End If
        Dim dtf As String
        dtf = Day(Me.ListDatefrom.Text.ToString())
        If Len(dtf) = 1 Then
            dtf = "0" & dtf
        End If
        Dim Mto As String
        Mto = Month(Me.listDateto.Text.ToString())
        If Len(Mto) = 1 Then
            Mto = "0" & Mto
        End If

        Dim dto As String
        dto = Day(Me.listDateto.Text.ToString())
        If Len(dto) = 1 Then
            dto = "0" & dto
        End If
        If Me.Loginlist.Checked = True Then
        Else
            If Me.UnusedLogins.Checked = True Then
            Else
                whr = whr & " CONVERT(varchar(10),datetm,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),datetm,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
            End If
        End If
        If Me.Loginlist.Checked = True Then
            If ddlState.Text <> "ALL STATES" Then
                whr = whr & " statename ='" & ddlState.Text.Trim() & "'"
            End If
        Else
            If Me.UnusedLogins.Checked = True Then
                If ddlState.Text <> "ALL STATES" Then
                    whr = whr & " statename ='" & ddlState.Text.Trim() & "'"
                End If
            Else
                If ddlState.Text <> "ALL STATES" Then
                    whr = whr & " and statename ='" & ddlState.Text.Trim() & "'"
                End If
            End If
        End If
        If whr <> "" Then
            whr = " WHERE " & whr
        End If
        sql = sql & whr & ORD
        sqlSumTotal = sqlSumTotal ' & whr
        Session("SQLSTRING") = sql
        Session("SQLSTRINGSUM") = sqlSumTotal
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then
            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""
        preparesql()
        If Me.Loginlist.Checked = True Then
            Response.Write("<script>window.open('ReportLoginlist.aspx','_blank')</script>")
        Else
            If Me.UnusedLogins.Checked = True Then
                Response.Write("<script>window.open('ReportLoginUnusedview.aspx','_blank')</script>")
            Else
                Response.Write("<script>window.open('ReportLoginlogview.aspx','_blank')</script>")
            End If
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If

        If Not IsPostBack Then
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
        End If
        Dim dr As SqlDataReader
        Dim cn As New SqlConnection

        If Not IsPostBack Then
            Dim ConStr As ConnectionStringSettings
            ConStr = ConfigurationManager.ConnectionStrings("Conn")
            cn.ConnectionString = MMotorVCS.DTask.BuildConnectionString()
            cn.Open()
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
            Dim cmdstate1 As New SqlCommand
            cmdstate1.CommandText = "Select Distinct statename From vw_ps"
            cmdstate1.Connection = cn
            dr = cmdstate1.ExecuteReader
            ddlState.Items.Add("ALL STATES")
            If dr.HasRows Then
                Do While dr.Read
                    Me.ddlState.Items.Add(dr("Statename"))
                Loop
            End If
            dr.Close()
            cn.Close()
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
                'Case "10" 'crime records
                '    Exit Select
            Case "12" 'ncrbexecutive
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Response.Redirect(Request.RawUrl)
    End Sub
    Protected Sub btnreset_click1()
        Response.Redirect(Request.RawUrl)
    End Sub



    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
