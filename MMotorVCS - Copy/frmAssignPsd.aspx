<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="frmAssignPsd.aspx.cs" Inherits="frmAssignPsd" Title="Vahan Samanvay Assign Password" Theme="emsTheme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<asp:Label Id="lblResult" runat="server" Visible="false"></asp:Label>

<table class="contentmain"><tr> <td>
<asp:Table id="tblAccused" runat="server"  HorizontalAlign="Center"  class="tablerowcolor" width="80%" CellPadding="0" CellSpacing="1">
<asp:TableRow class="heading">
<asp:TableCell ColumnSpan="6" ID="ErrorDisplayCell">
<asp:Label ID="lblHeader"  runat="server">Create User Form</asp:Label>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow1" runat="server" Width="100%">
<asp:TableCell Width="24%" ID="tblcell1" runat="server" CssClass="newInit"><strong>New User</strong>
</asp:TableCell>
<asp:TableCell ID="tblCell1a" runat="server" CssClass="second">
<asp:RadioButtonList AutoPostBack="true"  id="chknew" CssClass="Objectdrp" runat="server"></asp:RadioButtonList>
</asp:TableCell>
<asp:TableCell Width="24%" ID="TableCell1b" runat="server" CssClass="newInit"><strong>New User</strong>
</asp:TableCell>
<asp:TableCell ID="TableCell2b" runat="server" CssClass="second">
<asp:RadioButtonList AutoPostBack="true" id="chkexists" CssClass="Objectdrp" runat="server"></asp:RadioButtonList>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow ID="tblRow2" runat="server" Width="100%">
<asp:TableCell Width="24%" ID="TableCell1u" runat="server" CssClass="newInit"></asp:TableCell>
<asp:TableCell Width="24%" ID="drpState" runat="server" CssClass="Objectdrp"></asp:TableCell>
</asp:TableRow>
</asp:Table>
</td></tr></table>

</ContentTemplate>
</asp:UpdatePanel>                                                     
</asp:Content>
