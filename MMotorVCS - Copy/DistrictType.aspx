<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Theme="emsTheme" CodeFile="DistrictType.aspx.cs" Inherits="DistrictType"
Title="Vahan Samanvay Master District" EnableViewState="true"  EnableViewStateMac="true"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <table class="contentmain"> 
<tr> <td > 
    <table align="center" width="80%" class="tablerowcolor">

     <tr class="heading">
            <td colspan="4">
               List of Districts</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4">
                <b>
                    District Name </b>
            </td>
        </tr>
        <tr>
            <td style="width: 100px;">
                Select State
            </td>
            <td style="width: 77px;">
                <asp:TextBox ID="txtStateCode" runat="server" Width="64px" AutoPostBack="True" MaxLength="6"
                    OnTextChanged="txtStateCode_TextChanged"></asp:TextBox></td>
            <td colspan="2" >
                <asp:DropDownList ID="ddStateCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddStateCode_SelectedIndexChanged"
                    Width="300px">
                </asp:DropDownList></td>
                
        </tr>
        <tr>
            <td style="width: 100px">
                District Code</td>
            <td style="width: 77px">
            </td>
            <td style="width: 160px">
                <asp:TextBox ID="txtDistrictCode" runat="server" MaxLength="6" Width="64px"></asp:TextBox></td>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtDistrictCode"></cc1:FilteredTextBoxExtender>
                
            <td style="width: 295px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100px;">
                District Name</td>
            <td style="width: 77px;">
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtDistrictName" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                <asp:CheckBox ID="districtbyname" runat="server" 
                    Text="Order by District Name" AutoPostBack="True" 
                    oncheckedchanged="districtbyname_CheckedChanged" />
                    <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtDistrictName"></cc1:FilteredTextBoxExtender>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 77px">
            </td>
            <td style="width: 160px">
            </td>
            <td style="width: 295px">
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center"">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add"
                    Width="76px" /><asp:Button ID="btnUpdate" runat="server" CssClass="button" OnClick="btnUpdate_Click"
                        Text="Update" Width="71px" /><asp:Button ID="btnReset" runat="server" CssClass="button"
                            OnClick="btnReset_Click" Text="Reset" Width="76px" /></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblmsg" runat="server" ForeColor="#000000"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4">
                <h4>
                    District Details</h4>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="gvDisType" runat="server" AutoGenerateColumns="False" OnRowCommand="gvDisType_RowCommand"
                    OnRowCreated="gvDisType_RowCreated" Width="97%" AllowPaging="False"
                    OnPageIndexChanging="gvDisType_PageIndexChanging" CssClass="gridText" 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="4" EnableModelValidation="True" ForeColor="Black" 
                    GridLines="Vertical">
                    <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />
                    <Columns>
                        <asp:TemplateField HeaderText="S.No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                    Text='<%# Container.DataItemIndex +1  %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Stateid" HeaderText="StateId " SortExpression="Stateid" />
                        <asp:BoundField DataField="statecode" HeaderText="State Code" SortExpression="statecode" />
                        <asp:BoundField DataField="statename" HeaderText="State Name" SortExpression="statename">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="districtid" HeaderText="Dis Id" SortExpression="districtid" />
                        <asp:BoundField DataField="Districtcode" HeaderText="District code" SortExpression="Districtcode" />
                        <asp:BoundField DataField="districtname" HeaderText="District Name" SortExpression="districtname">
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                    <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                        ForeColor="White" />
                    <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                        HorizontalAlign="Right"  />
                    <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                        ForeColor="White" />
                    <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                    <PagerSettings FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    </td> </tr> </table>

    <script language="javascript" type="text/javascript">
    function valid()
    {     
    if(document.getElementById("<%=ddStateCode.ClientID%>").selectedIndex==0)
    {
        alert("Select State Name");
        document.getElementById("<%=ddStateCode.ClientID %>").focus();        
        return false;
    }    
    if(document.getElementById("<%=txtDistrictCode.ClientID%>").value == "")
    {
        alert("Enter District Code");
        document.getElementById("<%=txtDistrictCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtDistrictName.ClientID%>").value == "")
    {
        alert("Enter District Name");
        document.getElementById("<%=txtDistrictName.ClientID%>").focus();
        return false;
    }  
    return true;
    }
    </script>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
