﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" SmartNavigation="true"  AutoEventWireup="true"  CodeFile="ReportVSMatchedStatus.aspx.vb" Inherits="ReportVSMatchedStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
        <div class="contentmain">
            <table class="tablerowcolor" align="center" width="80%">
                <tr class="heading">
                    
                    <td colspan="3" >
                        <asp:Label ID="Label3" runat="server" Text="Match Status Report" Font-Bold="True"></asp:Label></td>
                   
                </tr>
                <tr>
                    <td >
                       <b> <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label> </b></td>
                    <td >
                       
                        <asp:TextBox ID="ListDatefrom" runat="server"></asp:TextBox></td>
                    <td >
                    <ajaxtoolkit:calendarextender runat="server" ID="datefrom" 
                            TargetControlID="Listdatefrom" Format="dd/MM/yyyy"  PopupPosition="Right"></ajaxtoolkit:calendarextender>
                    </td>
                </tr>
                <tr>
                    <td >
                       <b> <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></b></td>
                    <td >
                        <asp:TextBox ID="listDateto" runat="server"></asp:TextBox></td>
                    <td >
                    <ajaxtoolkit:calendarextender ID="dateto" runat="server" 
                            TargetControlID="ListDateTo" Format="dd/MM/yyyy"  PopupPosition="Right"></ajaxtoolkit:calendarextender>
                    </td>
                </tr>
                <tr>
                    <td style="height: 24px" >
                    </td>
                    <td style="height: 24px" >
                        <asp:CheckBox ID="OnlyMatchedCases" runat="server" Text="Only Matched Cases" />
                    </td>
                    <td style="height: 24px" >
                        </td>
                </tr>
                <tr>
                    <td >
                        &nbsp;</td>
                    <td >
                    <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td >
                        &nbsp;</td>
                    <td >
                        <asp:Button ID="Button1" runat="server" Text="View" /></td>
                    <td >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Msglabel" runat="server"></asp:Label>
                        <br />
                    <asp:Button ID="Converttoexecl" runat="server" visible="false" 
                            Text="Generate to excel (.csv) format file" />
                    </td>
                </tr>
            </table>
        </div>
        &nbsp;<br />
        &nbsp;
        <br />

</html>
</asp:Content>
