<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="proles.aspx.cs" Inherits="proles" Title="Vahan Samanvay Define Roles" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">   
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>
    <center>
        <table class="contentmain"><tr> <td> 
<TABLE class="tablerowcolor" width="80%" align=center><TBODY><TR><TD colspan="2" class="heading">Roles</TD></TR><TR><TD style="HEIGHT: 17px; TEXT-ALIGN: left" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=2><asp:Label id="lblmsg" runat="server" __designer:wfdid="w102"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=2>
    <asp:GridView id="gvRoles" runat="server" Width="97%" 
        OnRowCreated="gvRoles_RowCreated" ShowFooter="True" 
        OnRowUpdating="gvRoles_RowUpdating" OnRowEditing="gvRoles_RowEditing" 
        OnRowCommand="gvRoles_RowCommand" OnRowCancelingEdit="gvRoles_RowCancelingEdit" 
        GridLines="Vertical" ForeColor="Black" CellPadding="4" 
        AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" 
        BorderStyle="None" BorderWidth="1px" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField HeaderText="S.No." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="snum" runat="server" CommandName='<%# Container.DataItemIndex %>'
                                     Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                     
                                      <asp:Label ID="inLoginId" runat="server" Text='<%# Eval("inLoginId") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="50px" />
                            <FooterStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ROLE&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblRoleId" runat="server" Text='<%# Eval("inRoleId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="SHORT ROLE&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtShortRoleName" runat="server" Text='<%# Eval("vcRoleDesc") %>'></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtShortRoleName"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblShortRoleName" runat="server" Text='<%# Eval("vcRoleDesc") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtShortRoleName1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1a" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtShortRoleName1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ROLE&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRoleName" runat="server" Text='<%# Eval("vcFullDesc") %>'></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1b" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtRoleName"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRoleName" runat="server" Text='<%# Eval("vcFullDesc") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRolesName1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1c" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtRolesName1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="SHORT ROLE&#160;NAME">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtUserType" runat="server" Text='<%# Eval("vcUserType") %>'></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1d" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtUserType"></cc1:FilteredTextBoxExtender>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("vcUserType") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtUserType1" runat="server" Visible="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="fetxtRegistration1e" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtUserType1"></cc1:FilteredTextBoxExtender>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACTIVE">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" Enabled="true"  Text='<%# Eval("boEnabled") %>'></asp:CheckBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" runat="server" Enabled="false" Text='<%# Eval("boEnabled") %>'></asp:CheckBox>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:CheckBox ID="chkActive1" runat="server" Visible="false" ></asp:CheckBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="Edit / Delete" ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="lbInsert" runat="server" CommandName="Insert" ImageUrl="~/img/updt_icon.gif"
                                    ToolTip="Insert" Visible="false" />
                                <asp:Button ID="lbAdd" runat="server" CausesValidation="False" CommandName="Add"
                                    CssClass="button" Text="Add" ToolTip="Add New" />
                                <asp:ImageButton ID="lbCancel" runat="server" CausesValidation="False" CommandName="CancelInsert"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" Visible="false" />
                            </FooterTemplate>  
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />                                
                            </ItemTemplate>                          
                            <ItemStyle Width="100px" />
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>                        
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" />                    
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> &nbsp; </TD></TR><TR><TD style="TEXT-ALIGN: left" align=center></TD></TR><TR><TD style="TEXT-ALIGN: left" align=center><IMG alt="Edit" src="img/edit_icon.gif" />&nbsp; Edit Data&nbsp; <IMG alt="Update" src="img/updt_icon.gif" />&nbsp; Submit Data&nbsp; <IMG alt="Cancel" src="img/cancel_icon.gif" />&nbsp; Cancel &nbsp; </TD></TR></TBODY></TABLE>
        </td></tr></table>


</contenttemplate>
    </asp:UpdatePanel>&nbsp;</center>
</asp:Content>

