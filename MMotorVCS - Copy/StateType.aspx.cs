using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class StateType : System.Web.UI.Page
{
    static int stype_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();

    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {


        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");
        _userid = Session["vcUserName"].ToString();
        //_userid = "1";

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }


        if ((Session.SessionID) == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
        }

        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }

            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindgrid();

        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    public void getmaxid()
    {
        try
        {
            stype_id = mm.getStateMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateType();
            gvStateType.DataSource = ds;
            gvStateType.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtStateCode.Text = "";
        txtStateName.Text = "";
        bindgrid();
    }
    private Boolean checkillegal()
    {
        bool illegal;
        illegal = false;
        if (txtStateCode.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtStateName.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtStateCodealpha.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    ////private Boolean checkillegalstatecodealpha()
    ////{
    ////    bool illegal;
    ////    illegal = false;
    ////    if (txtStateCode.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
    ////    {
    ////        illegal = true; 
    ////    }
    ////    if (txtStateCodealpha.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
    ////    {
    ////        illegal = true;
    ////    }
    ////    if (illegal)
    ////    {
    ////        return true;
    ////    }
    ////    {
    ////        return false;
    ////    }
    ////}
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getStateType();
            bool illegal = checkillegal();
            if (illegal == false)
            {
                if (txtStateCode.Text != "" && txtStateName.Text != "")
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["statename"].ToString().Trim().ToUpper()) == txtStateName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This State Name already exists.</font>";
                            break;
                        }
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["statecode"].ToString().Trim().ToUpper()) == txtStateCode.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This State Code already exists.</font>";
                            break;
                        }
                    }
                    if (flag != 1)
                    {

                        if (mm.AddState((stype_id), (GenericMethods.check(txtStateCode.Text.Trim().ToUpper())), GenericMethods.check(txtStateName.Text.Trim().ToUpper()), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            //refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not inserted</font>";

                        }


                        if (mm.AddStatealpha((GenericMethods.check(txtStateCode.Text.Trim().ToUpper())), GenericMethods.check(txtStateCodealpha.Text.Trim().ToUpper())) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            refresh();
                        }
                        else
                        {

                        }


                    }
                }
                else
                {
                    lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getStateType();
            if (txtStateCode.Text != "" && txtStateName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["stateid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["statename"].ToString().Trim().ToUpper()) == txtStateName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This State Name already exists.</font>";
                            return;
                        }
                        string aa = GenericMethods.check(ds.Tables[0].Rows[i]["statecode"].ToString().ToUpper());
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["statecode"].ToString().Trim().ToUpper()) == txtStateCode.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This State Code already exists.</font>";
                            return;
                        }
                    }

                }
                if (flag != 1)
                {
                    if (mm.updateStateType(Convert.ToInt32(lblid.Text), (GenericMethods.check(txtStateCode.Text.Trim().ToUpper())), GenericMethods.check(txtStateName.Text.Trim().ToUpper()), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values Updated</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not Updated</font>";
                    }
                }

            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }

    }
    protected void gvStateType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvStateType.Rows[Convert.ToInt32(e.CommandName) - (gvStateType.PageSize * pagecount)].Cells[1].Text;
                txtStateCode.Text = gvStateType.Rows[Convert.ToInt32(e.CommandName) - (gvStateType.PageSize * pagecount)].Cells[2].Text;
                txtStateName.Text = gvStateType.Rows[Convert.ToInt32(e.CommandName) - (gvStateType.PageSize * pagecount)].Cells[3].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvStateType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
    protected void gvStateType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvStateType.PageIndex = e.NewPageIndex;
        refresh();
    }
}
