using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

public partial class frmChangePassword : System.Web.UI.Page
{
    static int user_id;
    static string _userid;
    DataSet ds = new DataSet();
    ListItemCollection liColl;
    static int pagecount = 0;
    int PostCount = 0;
    private string vcStateCode;
    private string vcStateDesc;
    private int _roleId;
    static int randomNumber;

    CommonFunctions eform = new CommonFunctions();
    Class1 myclass = new Class1();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }


        if (Session["vcStateCode"] == null)
        {


        }
        else
        {
            vcStateCode = Session["vcStateCode"].ToString();
            _roleId = Convert.ToInt32(Session["inRoleId"]);
            if (_roleId == 3)
            {
                tblState.Visible = true;
                txtUsername.Text = Session["vcUserName"].ToString();
                txtUsername.Enabled = false;

            }
            else
            {
                txtUsername.Text = Session["vcUserName"].ToString();
                txtUsername.Enabled = false;
            }

        }

        if (!IsPostBack)
        {
            bindStateName();
            btnReset.Visible = true;
            gvUserDetails.Visible = false;

            if (_roleId == 3)
            {
                tblState.Visible = true;
                txtUsername.Text = Session["vcUserName"].ToString();
                txtUsername.Enabled = false;

            }
            else
            {
                txtUsername.Text = Session["vcUserName"].ToString();
                txtUsername.Enabled = false;
            }

            Random autoRand = new Random();
            randomNumber = Convert.ToInt32(autoRand.Next(0, 36));
            String rno = randomNumber.ToString();
            //btnUpdate.Attributes.Add("OnClick", "return EncryptChangePassword(" + "'" + rno + "'" + ");");
        }
        lblmsg.Text = "";
        //string value = Session["inRoleid"].ToString();
        //switch (value)
        //{
        //    case "1":
        //        break;
        //    case "4": //authority
        //        break;
        //    case "10": //crimerecords
        //        break; 
        //    case "7": //district
        //        break;

        //    default:
        //        Response.Redirect("LogoutModule.aspx");
        //        break;
        //}

    }

    public void bindStateName()
    {
        try
        {
            MasterMethods mm = new MasterMethods();
            DataSet dTblState = mm.getStateType();
            ddStateName.DataSource = dTblState;
            ddStateName.DataValueField = GenericMethods.check(dTblState.Tables[0].Columns[1].ColumnName);
            ddStateName.DataTextField = GenericMethods.check(dTblState.Tables[0].Columns[2].ColumnName);
            ddStateName.DataBind();

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        {

            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }

    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = eform.getUser_Details_for_pass_updation((ddStateName.SelectedValue.ToString().Trim()));
            gvUserDetails.DataSource = ds;
            gvUserDetails.DataBind();
            if (gvUserDetails.Rows.Count > 0)
            {
                gvUserDetails.Visible = true;

                lblmsg.Text = "";
            }
            else
            {
                lblmsg.Text = "<font color=red>Not any user is present Within this State.</font>";

            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        {

            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindgrid();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void refresh()
    {
        //txtOldPassword.Text = hdnFieldOldPassword.Value;
        txtPassword.Text = "";
        txtConPass.Text = "";
        txtUsername.Text = Session["vcUserName"].ToString();
    }
    protected void gvUserDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvUserDetails.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void gvUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                txtUsername.Text = gvUserDetails.Rows[Convert.ToInt32(e.CommandName) - (gvUserDetails.PageSize * pagecount)].Cells[2].Text;

            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        {

            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void gvUserDetails_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
    private Boolean checkillegal(string oldp, string newp, string newconf)
    {

        bool illegal;
        illegal = false;
        if (oldp.IndexOfAny(new char[] { '\'', '(', ')', ';' }) >= 0)
        {
            illegal = true;
        }
        if (newp.IndexOfAny(new char[] { '\'', '(', ')', ';' }) >= 0)
        {
            illegal = true;
        }
        if (newconf.IndexOfAny(new char[] { '\'', '(', ')', ';' }) >= 0)
        {
            illegal = true;
        }
        Regex re = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}");

        if (!(re.IsMatch(txtOldPassword.Text) && re.IsMatch(txtConPass.Text) && re.IsMatch(txtPassword.Text)))
        {
            illegal = true;
        }

        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        bool illegal = checkillegal(txtOldPassword.Text.ToString().Trim(), txtPassword.Text.ToString().Trim(), txtConPass.Text.ToString().Trim());
        if (illegal == false)
        {
            if (this.IsValid)
            {

                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5;
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)    
                md5 = new MD5CryptoServiceProvider();
                originalBytes = ASCIIEncoding.Default.GetBytes(txtOldPassword.Text);
                encodedBytes = md5.ComputeHash(originalBytes);
                string pwd = BitConverter.ToString(encodedBytes).Replace("-", "");

                //password
                Byte[] originalBytespwd;
                Byte[] encodedBytespwd;
                MD5 md5pwd;
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)    
                md5pwd = new MD5CryptoServiceProvider();
                originalBytespwd = ASCIIEncoding.Default.GetBytes(txtPassword.Text.ToString().Trim());
                encodedBytespwd = md5pwd.ComputeHash(originalBytespwd);
                string Newpwd = BitConverter.ToString(encodedBytespwd).Replace("-", "");

                //confirm password
                Byte[] originalBytesConfm;
                Byte[] encodedBytesConfm;
                MD5 md5Confm;
                //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)    
                md5Confm = new MD5CryptoServiceProvider();
                originalBytesConfm = ASCIIEncoding.Default.GetBytes(txtConPass.Text.ToString().Trim());
                encodedBytesConfm = md5Confm.ComputeHash(originalBytesConfm);
                string Confmpwd = BitConverter.ToString(encodedBytesConfm).Replace("-", "");

                try
                {
                    int flag = 0;

                    if (txtPassword.Text.Length >= 12 && txtConPass.Text.Length >= 12)
                    {
                        if (txtUsername.Text != "" && txtConPass.Text != "" && txtPassword.Text != "" && txtOldPassword.Text != "")
                        {
                            string old_database_pp = eform.getOlddatabasepwd(txtUsername.Text);
                            if (old_database_pp == GenericMethods.check(pwd))
                            {
                                if (old_database_pp != GenericMethods.check(Newpwd))
                                {
                                    if (GenericMethods.check(Newpwd) == GenericMethods.check(Confmpwd))
                                    {

                                        if (eform.UpdateUser_Password(_userid, GenericMethods.check(txtUsername.Text.ToString().Trim()), GenericMethods.check(Newpwd), vcStateCode, Session["vcUserName"].ToString().Trim()) > 0)
                                        {
                                            lblmsg.Text = "<font color=white>Updated Successfully !!!</font>";

                                            refresh();
                                            txtUsername.Text = Session["vcUserName"].ToString();
                                        }
                                        else
                                        {
                                            lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again</font>";
                                            txtUsername.Text = Session["vcUserName"].ToString();
                                        }
                                    }


                                    else
                                    {
                                        lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                                        txtUsername.Text = Session["vcUserName"].ToString();

                                    }
                                }
                                else
                                {
                                    lblmsg.Text = lblmsg.Text = "<font color=red>Old Password and New Password Can't be same</font>";
                                    txtUsername.Text = Session["vcUserName"].ToString();
                                }

                            }
                            else
                            {
                                lblmsg.Text = lblmsg.Text = "<font color=red>The old Password entered is not correct.</font>";
                                refresh();
                            }
                        }
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
                    }

                }
                catch (SqlException)
                { lblmsg.Text = "Occuring Problem in connectivity to database."; }
                catch (HttpCompileException)
                { lblmsg.Text = "System Occuring Problem."; }
                catch (HttpParseException)
                { lblmsg.Text = "Internet Browser Occuring Problem."; }
                catch (Exception ex)
                {
                    lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
                }
            }
            else
            {
                refresh();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
        }
    }
    protected bool checkusername(string name)
    {
        try
        {
            ds = eform.Login_User(name);
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    protected void gvUserDetails_RowdataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {



        }
    }
}
