<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="frmUserdetails.aspx.cs" Inherits="frmUserdetails" Theme="emsTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.password-strength.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
   
        var myPlugin = $("[id$='txtPassword']").password_strength();

        $("[id$='btnSubmit']").click(function() {
            return myPlugin.metReq(); //return true or false
        });

        $("[id$='passwordPolicy']").click(function(event) {
            var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
            window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
            event.preventDefault();
            return false;
        });

    });
</script>
<script language="javascript" type="text/javascript">
    function valid()
    {     
    
    if(document.getElementById("<%=txtUsername.ClientID%>").value == "")
    {
        alert("Enter User Name");
        document.getElementById("<%=txtUsername.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtPassword.ClientID%>").value == "")
    {
        alert("Enter Password");
        document.getElementById("<%=txtPassword.ClientID%>").focus();
        return false;
    }  
    if(document.getElementById("<%=txtConPass.ClientID%>").value == "")
    {
        alert("Enter Password for Confirmation");
        document.getElementById("<%=txtConPass.ClientID%>").focus();
        return false;
    }   
     if(document.getElementById("<%=txtPassword.ClientID%>").value != "" || document.getElementById("<%=txtConPass.ClientID%>").value != "")
    {
       if(document.getElementById("<%=txtPassword.ClientID%>").value!=document.getElementById("<%=txtConPass.ClientID%>").value)
       {
        alert("Password and Confirm Password Doesn't match");
        document.getElementById("<%=txtPassword.ClientID%>").focus();
        return false;
        }
    }  
    if(document.getElementById("<%=ddUserRole.ClientID%>").selectedIndex==0)
    {
        alert("Select User Role");
        document.getElementById("<%=ddUserRole.ClientID %>").focus();        
        return false;
    }    
    if(document.getElementById("<%=txtLogonName.ClientID%>").value == "")
    {
        alert("Enter Logon Name");
        document.getElementById("<%=txtLogonName.ClientID%>").focus();
        return false;
    }  

    if(document.getElementById("<%=ddStateName.ClientID%>").selectedIndex==0)
    {
        alert("Select State Name");
        document.getElementById("<%=ddStateName.ClientID %>").focus();        
        return false;
    }     
    return true;
    }    
    </script>
    <script language="javascript" type="text/javascript">

function SelectDropDown()
{

    if (document.getElementById("<%=ddUserRole.ClientID%>").value == 4 || document.getElementById("<%=ddUserRole.ClientID%>").value == 5 || document.getElementById("<%=ddUserRole.ClientID%>").value == 6 || document.getElementById("<%=ddUserRole.ClientID%>").value == 6 || document.getElementById("<%=ddUserRole.ClientID%>").value == 7 || document.getElementById("<%=ddUserRole.ClientID%>").value == 3)
    {
  
        document.getElementById("<%=tblRow6.ClientID%>").style.display = '';
        document.getElementById("<%=tblRow7.ClientID%>").style.display = '';
        document.getElementById("<%=tblRow8.ClientID%>").style.display = '';
        
        return false
	
	}
	else
	{
	
        document.getElementById("<%=tblRow6.ClientID%>").style.display = 'none';
        document.getElementById("<%=tblRow7.ClientID%>").style.display = 'none';
        document.getElementById("<%=tblRow8.ClientID%>").style.display = 'none';
	    
	    return true
	}
    
 }
 
    function validation()
    {     
    
    if(document.getElementById("<%=txtUsername.ClientID%>").value == "")
    {
        alert("Enter User Name");
        document.getElementById("<%=txtUsername.ClientID%>").focus();
        return false;
    }    
   if(document.getElementById("<%=txtPassword.ClientID%>").value!=document.getElementById("<%=txtConPass.ClientID%>").value)
   {
    alert("Password and Confirm Password Doesn't match");
    document.getElementById("<%=txtPassword.ClientID%>").focus();
    return false;
    }
    if(document.getElementById("<%=ddUserRole.ClientID%>").selectedIndex==0)
    {
        alert("Select User Role");
        document.getElementById("<%=ddUserRole.ClientID %>").focus();        
        return false;
    }    
    if(document.getElementById("<%=txtLogonName.ClientID%>").value == "")
    {
        alert("Enter Logon Name");
        document.getElementById("<%=txtLogonName.ClientID%>").focus();
        return false;
    }  

    if(document.getElementById("<%=ddStateName.ClientID%>").selectedIndex==0)
    {
        alert("Select State Name");
        document.getElementById("<%=ddStateName.ClientID %>").focus();        
        return false;
    }  
    return true;
    }    
    </script>
    <table class="contentmain">
    <tr> <td> 
    <asp:Label Id="lblResult" runat="server" Visible="false" ></asp:Label>
<asp:Table id="tblIncidents" runat="server" class="tablerowcolor" HorizontalAlign="Center"  style="width:80%" CellPadding="0" CellSpacing="1">
<asp:TableRow class="heading">
<asp:TableCell ColumnSpan="6" ID="ErrorDisplayCell">
<asp:Label ID="lblHeader"  runat="server">User Details Entry Form</asp:Label>
<asp:Label ID="lblmsg" ForeColor="red"  runat="server"></asp:Label>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow   runat="server" Width="70%">
<asp:TableCell Width="24%" ID="tblcell1" runat="server" style="color: #800000"><strong>User Name</strong>
</asp:TableCell>
<asp:TableCell ID="tblCell1a" runat="server" >&nbsp;
<asp:TextBox id="txtUsername" runat="server" Width="250px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow5" runat="server" Width="70%" visible="false">
<asp:TableCell Width="24%" ID="TableCell5a" runat="server" style="color: #800000">Logon Name
</asp:TableCell>
<asp:TableCell ID="TableCell5c" runat="server" >&nbsp;
<asp:TextBox id="txtLogonName" runat="server"  Width="250px"></asp:TextBox>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow4" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell4a" runat="server" style="color: #800000">Activate/Deactivate
</asp:TableCell>
<asp:TableCell ID="TableCell4c" runat="server" >&nbsp;
<asp:DropDownList id="ddAcDeac" runat="server" Width="255px">
<asp:ListItem Value="1">Active</asp:ListItem>
<asp:ListItem Value="0">Deactive</asp:ListItem>
</asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow51" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell51a" runat="server" style="color: #800000">User Role
</asp:TableCell>
<asp:TableCell ID="TableCell51c" runat="server" >&nbsp;
<asp:DropDownList id="ddUserRole" runat="server" Width="255px" ></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow6" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell6a" runat="server" >State
</asp:TableCell>
<asp:TableCell ID="TableCell6c" runat="server" >&nbsp;
<asp:DropDownList id="ddStateName" runat="server" Width="255px" AutoPostBack="true" OnSelectedIndexChanged="ddStateName_SelectedIndexChanged"></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow7" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell7a" runat="server" >District
</asp:TableCell>
<asp:TableCell ID="TableCell7c" runat="server" >&nbsp;
<asp:DropDownList id="ddDistrictName" runat="server" AutoPostBack="true" Width="255px" OnSelectedIndexChanged="ddDistrictName_SelectedIndexChanged"></asp:DropDownList>
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow8" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell8a" runat="server" >Police Station
</asp:TableCell>
<asp:TableCell ID="TableCell8c" runat="server" >&nbsp;
<asp:DropDownList id="ddPsName" runat="server" AutoPostBack="true" Width="255px"></asp:DropDownList></asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow9" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell9a" runat="server" >Mail
</asp:TableCell>
<asp:TableCell ID="TableCell9c" runat="server" >&nbsp;
<asp:TextBox id="txtemail" runat="server" Width="250px"></asp:TextBox> 
<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Width="194px" ErrorMessage="Email is not in proper format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail"></asp:RegularExpressionValidator></asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow1" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell1" runat="server" style="color: #800000" >File Number
</asp:TableCell>
<asp:TableCell ID="TableCell2" runat="server" >&nbsp;
<asp:TextBox id="txtFileNo" runat="server" Width="250px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtFileNo" runat="server" ErrorMessage="Enter File No." ValidationGroup="valid"></asp:RequiredFieldValidator>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow2" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell3" runat="server" style="color: #800000">Address
</asp:TableCell>
<asp:TableCell ID="TableCell4" runat="server" >&nbsp;
<asp:TextBox id="txtAddress" runat="server" Width="250px"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtAddress" runat="server" ErrorMessage="Enter Address"  ValidationGroup="valid"></asp:RequiredFieldValidator>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow3" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell5" runat="server" style="color: #800000">Authority
</asp:TableCell>
<asp:TableCell ID="TableCell6" runat="server" >&nbsp;
<asp:TextBox id="txtAuthority" runat="server" Width="250px"  Text="Duty Officer"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtAuthority" runat="server" ErrorMessage="Enter Authority "  ValidationGroup="valid"></asp:RequiredFieldValidator>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow4" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell7" runat="server" style="color: #800000">Counter Charges
</asp:TableCell>
<asp:TableCell ID="TableCell8" runat="server" >&nbsp;
<asp:TextBox id="txtAmount" runat="server" Width="250px" Text="0"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtAmount" runat="server" ErrorMessage="Enter Counter Charges"  ValidationGroup="valid"></asp:RequiredFieldValidator>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="TableRow5" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell9" runat="server" style="color: #800000">Signature
</asp:TableCell>
<asp:TableCell ID="TableCell10" runat="server" >&nbsp;
<asp:TextBox id="txtSignature" runat="server" Width="250px" Text="Duty Officer"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtSignature" runat="server" ErrorMessage="Enter Signature"  ValidationGroup="valid"></asp:RequiredFieldValidator>
</asp:TableCell>
</asp:TableRow>
<asp:TableRow  ID="tblRow11" runat="server" Width="70%" >
<asp:TableCell Width="24%" ID="TableCell1b" runat="server" style="color: #800000">Password
</asp:TableCell>
<asp:TableCell ID="TableCell2b" runat="server" AssociatedControlId="txtPassword">&nbsp;
<asp:TextBox id="txtPassword"  runat="server" TextMode="Password" Width="250px" autocomplete="off"  ValidationGroup="valid"></asp:TextBox>
<asp:RegularExpressionValidator ID="Regex4" runat="server" ControlToValidate="txtPassword"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red" ValidationGroup="valid" />
</asp:TableCell>
</asp:TableRow>

<asp:TableRow  ID="tblRow3" runat="server" Width="70%">
<asp:TableCell Width="24%" ID="TableCell4b" runat="server" style="color: #800000">Confirm Password
</asp:TableCell>
<asp:TableCell ID="TableCell3b" runat="server" >&nbsp;
<asp:TextBox id="txtConPass" runat="server"  TextMode="Password" Width="250px"  ValidationGroup="valid"></asp:TextBox>
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtConPass"
    ValidationExpression="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{12,}"
ErrorMessage="Password must contain: Minimum 12 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character" ForeColor="Red" ValidationGroup="valid" />
</asp:TableCell>
</asp:TableRow>

<asp:TableRow HorizontalAlign="center">
<asp:tableCell HorizontalAlign="center" runat="server" id="tblrow22" colspan="3">
<asp:Button id="btnAdd" onclick="btnAdd_Click" runat="server" CssClass="button" Width="76px" Text="Add" ValidationGroup="valid"></asp:Button>
<asp:Button id="btnUpdate" onclick="btnUpdate_Click" runat="server" CssClass="button" Width="71px" Text="Update" ValidationGroup="valid"></asp:Button><asp:Button id="btnReset" onclick="btnReset_Click" runat="server" CssClass="button" Width="76px" Text="Reset" CausesValidation="False"></asp:Button> <asp:Button id="btndelete" runat="server" CssClass="button" Width="71px" Text="Delete" OnClick="btndelete_Click" Visible="False"></asp:Button></asp:tableCell></asp:TableRow>
</asp:Table>

<asp:GridView id="gvUserDetails" runat="server" CssClass="grid" Width="80%" align="center"
        AutoGenerateColumns="False" OnRowCommand="gvUserDetails_RowCommand" 
        AllowPaging="True" PageSize="15" 
        OnPageIndexChanging="gvUserDetails_PageIndexChanging" 
        OnRowCreated="gvUserDetails_RowCreated" BackColor="White" BorderColor="#DEDFDE" 
        BorderStyle="None" BorderWidth="1px" CellPadding="4" 
        EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
<PagerSettings PreviousPageText="Previous" LastPageText="Last" 
        FirstPageText="First" NextPageText="Next"></PagerSettings>

<FooterStyle CssClass="gridfooter" BackColor="#CCCC99"></FooterStyle>
<Columns>
<asp:TemplateField HeaderText="S No."><ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                    Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                            
</ItemTemplate>
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:TemplateField>
<asp:BoundField DataField="Userid" SortExpression="Userid" HeaderText="User Id ">
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="username" SortExpression="username" HeaderText="User Name">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="Password" SortExpression="Password" HeaderText="Password">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="role_id" SortExpression="role_id" HeaderText="Role id">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="Role_name" SortExpression="Role_name" HeaderText="Role Name">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="isactive" SortExpression="isactive" HeaderText="IsActive">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="logon_name" SortExpression="logon_name" HeaderText="Logon Name">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="watermark" SortExpression="watermark" HeaderText="Water Mark">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="stateid" SortExpression="stateid" HeaderText="state Id">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="statename" SortExpression="statename" HeaderText="State Name">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
<asp:BoundField DataField="email" SortExpression="email" HeaderText="E-Mail">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
</asp:BoundField>
</Columns>

<RowStyle CssClass="gridrow" BackColor="#F7F7DE"></RowStyle>

<SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
        ForeColor="White"></SelectedRowStyle>

<PagerStyle ForeColor="Black" CssClass="gridpager" BackColor="#F7F7DE" 
        HorizontalAlign="Right"></PagerStyle>

<HeaderStyle CssClass="gridheader" BackColor="#6B696B" Font-Bold="True" 
        ForeColor="White"></HeaderStyle>

<AlternatingRowStyle CssClass="gridalterrow" BackColor="White"></AlternatingRowStyle>
</asp:GridView>
</td></tr></table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
