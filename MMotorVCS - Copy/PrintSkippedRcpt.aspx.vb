Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS

Partial Class PrintSkippedRcpt
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    
    Protected Sub prntrcpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles prntrcpt.Click
        Dim sql, sql1, sqlSumTotal As String
        Dim cmd As New SqlCommand
        Dim cmd1 As New SqlCommand
        sql = "SELECT  * from CounterQueryView"
        sqlSumTotal = ""
        Dim whr As String
        Dim ORD As String
        If Session("inRoleid") = 1 Then
            whr = " Where inReceiptId='" & Me.Rcptno.Text & "'"
        Else
            whr = " Where vcUsername='" & Session("vcUserName") & "' and inReceiptId='" & Me.Rcptno.Text & "'"
        End If

        ORD = " ORDER BY "
        sql = sql & whr
        sql1 = sql & " and matsource is not null"
        sqlSumTotal = sqlSumTotal & whr
        Session("SQLSTRING") = sql
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        With cmd1
            .Connection = consupac
            .CommandText = sql1
        End With
        Dim da1 As New SqlDataAdapter(cmd1)
        Dim dt As New DataTable()
        Dim dt1 As New DataTable()
        da.Fill(dt1)
        da1.Fill(dt)
        consupac.Close()
        Session("Data") = dt1
        Session("Data1") = dt
        If dt1.Rows.Count > 0 Then
            Response.Write("<script>window.open('Printskippedcounterrcpt.aspx','_blank')</script>")
        Else
            MsgBox("No Receipt to Print")
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then
            Response.Redirect("LogoutModule.aspx")
        End If
        Response.Redirect("LogoutModule.aspx")
    End Sub

    Protected Sub btnreset_click1()
        Response.Redirect(Request.RawUrl)
    End Sub
End Class
