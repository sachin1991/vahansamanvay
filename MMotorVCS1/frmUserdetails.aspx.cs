using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using MMotorVCS;
using System.Text.RegularExpressions;


public partial class frmUserdetails : System.Web.UI.Page
{
    static int user_id;
    static string _userid;
    DataSet ds = new DataSet();
    ListItemCollection liColl;
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    Class1 myclass = new Class1();
    static int _add, _edit, _delete, _view;
    static int pagecount = 0;
    int PostCount = 0;
    private string vcDistrictCode;
    private string vcTypeCode;
    private string vcStateCode;
    private int _roleId;
    string uniqueCode;
    string strUserType;


    protected void Page_Load(object sender, EventArgs e)
   
   

    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return validation();");
        ddUserRole.Attributes.Add("onchange", "return SelectDropDown()");
        
        

        if (Session["vcStateCode"] == null)
        {


        }
        else
        {
            vcStateCode = Session["vcStateCode"].ToString();
            _roleId = Convert.ToInt32(Session["inRoleId"]);
            vcTypeCode = Session["vcType"].ToString();
        }

      

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        
        if (!IsPostBack)
        {
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
           
            bindRoleName();
            bindStateName();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0")); 

        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "10":
                break;
            case "12":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }

    public void bindStateName()
    {
        try
        {

            DataSet dTblState = mm.getStateTypeNew();
            ddStateName.DataSource = dTblState;
            ddStateName.DataTextField = dTblState.Tables[0].Columns["statename"].ColumnName;
            ddStateName.DataValueField = dTblState.Tables[0].Columns["statecode"].ColumnName;
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }

    }

    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {
          
            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();
            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
          
            ddDistrictName.Items.Clear();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));


        }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            string[] temp = ddDistrictName.SelectedValue.Split('-');
            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]));
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = ds.Tables[0].Columns["Psname"].ColumnName;
            ddPsName.DataValueField = ds.Tables[0].Columns["psidpscode"].ColumnName;
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = dt.Columns["districtname"].ColumnName;
            ddDistrictName.DataValueField = dt.Columns["districtcode"].ColumnName;
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }



    public void bindRoleName()
    {
        try
        {
            DataTable dTblRole = clsPageBase.GetRole();
            ddUserRole.DataSource = dTblRole;
            ddUserRole.DataValueField = GenericMethods.check(dTblRole.Columns[0].ColumnName);
            ddUserRole.DataTextField = GenericMethods.check(dTblRole.Columns[1].ColumnName);
            ddUserRole.DataBind();
            ddUserRole.Items.Insert(0, "--Select--");
           
            
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void bindgrid()
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected bool checkusername(string UserName)
    {
        try
        {
            if (Convert.ToBoolean((clsPageBase.GetVerifyUserName(UserName))))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected string decrypt(string EncryptValue)
    {
        char[] EncAry = new char[EncryptValue.Length];
        char[] OrgAry = new char[EncryptValue.Length];
        int i;
        EncAry = EncryptValue.ToCharArray();
        for (i = 0; i < EncryptValue.Length; i++)
        {
            OrgAry[i] = (char)((int)EncAry[i] - 63);
        }
        string OriginalString = new string(OrgAry);
        return OriginalString;
    }
    protected string encrypt(string OriginalValue)
    {
        char[] OrgAry = new char[OriginalValue.Length];
        char[] EncAry = new char[OriginalValue.Length];
        int i;
        OrgAry = OriginalValue.ToCharArray();
        for (i = 0; i < OriginalValue.Length; i++)
        {
            EncAry[i] = (char)((int)OrgAry[i] + 63);
        }
        string encryptString = new string(EncAry);
        return encryptString;
    }

    public byte[] EncryptPassword(string username, string password, byte[] salt1, byte[] salt2)
    {
        string tmpPassword = null;
        string rsPwd;
        tmpPassword = Convert.ToBase64String(salt1) + Convert.ToBase64String(salt2) + username.ToLower() + password;

        UTF8Encoding textConvertor = new UTF8Encoding();
        byte[] passBytes = textConvertor.GetBytes(tmpPassword);
        return new SHA384Managed().ComputeHash(passBytes);


    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            try
            {

                byte[] encPassword;

                if (ddStateName.SelectedIndex != 0)
                {
                    if (txtUsername.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0 &&
                    ddStateName.SelectedIndex != 0)
                    {
                        if (checkusername(txtUsername.Text.Trim().ToUpper()) == false)
                        {
                            if (txtPassword.Text == txtConPass.Text)
                            {

                                string pp = myclass.md5(txtPassword.Text.ToString().Trim());

                                uniqueCode = ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                                if (ddDistrictName.SelectedIndex != 0)
                                {
                                    uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                                }

                                if (ddPsName.SelectedIndex != 0)
                                {
                                    uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                                }

                                uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));
                                if (txtAmount.Text == "")
                                {
                                    txtAmount.Text = "0";
                                }

                                if (clsPageBase.AddUser_Detail(txtUsername.Text.Trim().ToUpper(), pp,
                                    (ddUserRole.SelectedValue.Trim().ToString()),
                                    (ddAcDeac.SelectedValue.Trim().ToString()), uniqueCode, ddStateName.SelectedValue.Split('-')[0].Trim().ToString(), ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString(),
                                    ddPsName.SelectedValue.Split('-')[0].Trim().ToString(), txtFileNo.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtAuthority.Text.ToString().Trim(),
                                    Convert.ToInt32(txtAmount.Text.ToString().Trim()), txtSignature.Text.ToString().Trim(), txtemail.Text.ToString().Trim()) > 0)
                                {
                                    lblmsg.Text = "<font color=green>New User Created Successfully !!!</font>";
                                    refresh();
                                }
                                else
                                {
                                    lblmsg.Text = lblmsg.Text = "<font color=red>User Not Created ?? Try Again</font>";
                                }
                            }
                            else
                            {
                                lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                            }
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                        }
                    }
                }
                else
                {
                    if (txtUsername.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0)
                    {
                        if (checkusername(txtUsername.Text.Trim().ToUpper()) == false)
                        {
                            if (txtPassword.Text == txtConPass.Text)
                            {

                                string pp = myclass.md5(txtPassword.Text.ToString().Trim());

                                if (ddStateName.SelectedIndex != 0)
                                {
                                    uniqueCode = uniqueCode + ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                                }
                                if (ddDistrictName.SelectedIndex != 0)
                                {
                                    uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                                }

                                if (ddPsName.SelectedIndex != 0)
                                {
                                    uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                                }

                                uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));
                                if (txtAmount.Text.ToString().Trim() == "")
                                {
                                    txtAmount.Text = "0";
                                }
                                string mstatnam = "";
                                if (ddStateName.SelectedIndex == 0)
                                {
                                    mstatnam = "0";
                                }

                                if (clsPageBase.AddUser_Detail(txtUsername.Text.Trim().ToUpper(), pp,
                                   (ddUserRole.SelectedValue.Trim().ToString()),
                                   (ddAcDeac.SelectedValue.Trim().ToString()), uniqueCode, mstatnam, ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString(),
                                   ddPsName.SelectedValue.Split('-')[0].Trim().ToString(), txtFileNo.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtAuthority.Text.ToString().Trim(),
                                   Convert.ToInt32(txtAmount.Text.ToString().Trim()), txtSignature.Text.ToString().Trim(), txtemail.Text.ToString().Trim()) > 0)
                                {
                                    lblmsg.Text = "<font color=green>New User Created Successfully !!!</font>";
                                    refresh();
                                }
                                else
                                {
                                    lblmsg.Text = lblmsg.Text = "<font color=red>User Not Created ?? Try Again</font>";
                                }
                            }
                            else
                            {
                                lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                            }
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                        }
                    }

                }

            }
            catch (SqlException)
            { lblmsg.Text = "Occuring Problem in connectivity to database."; }
            catch (HttpCompileException)
            { lblmsg.Text = "System Occuring Problem."; }
            catch (HttpParseException)
            { lblmsg.Text = "Internet Browser Occuring Problem."; }
            catch (Exception ex)
            {
                lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
            }
        }
    
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { 
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    public void refresh()
    {
        ddUserRole.SelectedIndex = 0;
        ddAcDeac.SelectedIndex = 0;
        txtUsername.Text = "";
        txtPassword.Text = "";
        txtConPass.Text = "";
     
        txtLogonName.Text = "";
        txtUsername.Enabled = true;
        ddDistrictName.SelectedIndex = 0;
        ddPsName.SelectedIndex = 0;
        txtemail.Text = "";
        ddStateName.SelectedIndex = 0;
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        
    }
    protected void gvUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

            }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
            { //lblmsg.Text = GenericMethods.check(ex.Message); 
                lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
            }
    }
    protected void gvUserDetails_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[11].Visible = false;
        }
    }
    protected void gvUserDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvUserDetails.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {

    }
   
}

