﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmForgetPassword.aspx.cs" SmartNavigation="true" Inherits="frmForgetPassword" Title="Vahan Samanvay Forget Password Page" EnableViewState="true" EnableViewStateMac="true" ViewStateEncryptionMode="Always" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Vahan Samanvay:- Forget Password</title>
     <link href="style/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            height: 22px;
        }
        .combo
        {}
        .style2
        {
            text-align: left;
        }
        .style3
        {
            height: 22px;
            text-align: left;
        }
    </style>
</head>
<body ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
    <form id="form1" runat="server" autocomplete="off">
<script language="javascript">
    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(e) {
        if (event.button == 2) {
            alert("Right Click Disabled");
            return false;
        }
    }
</script>
<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.password-strength.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var myPlugin = $("[id$='txtPassword']").password_strength();

        $("[id$='btnSubmit']").click(function () {
            return myPlugin.metReq(); //return true or false
        });

        $("[id$='passwordPolicy']").click(function (event) {
            var width = 350, height = 300, left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);
            window.open("PasswordPolicy.xml", 'Password_poplicy', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
            event.preventDefault();
            return false;
        });

    });
</script>
<SCRIPT language="javascript" type="text/javascript">
    function ValidateTPACode() {
        if (document.getElementById("<%=ddQues1.ClientID%>").selectedIndex == 0) {
            alert("Select Question 1");
            document.getElementById("<%=ddQues1.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=ddQues2.ClientID%>").selectedIndex == 0) {
            alert("Select Question 2");
            document.getElementById("<%=ddQues2.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtAns1.ClientID%>").value == "") {
            alert("Please enter Ans 1");
            document.getElementById("<%=txtAns1.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtAns2.ClientID%>").value == "") {
            alert("Please enter Ans 2");
            document.getElementById("<%=txtAns2.ClientID %>").focus();
            return false;
        }
        if (document.getElementById("<%=txtLoginCode.ClientID%>").value == "") {
            alert("Enter User Name");
            document.getElementById("<%=txtLoginCode.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
            alert("Enter Password");
            document.getElementById("<%=txtPassword.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txtConPass.ClientID%>").value == "") {
            alert("Enter Password for Confirmation");
            document.getElementById("<%=txtConPass.ClientID%>").focus();
            return false;
        }

        return true;
    }    
    </SCRIPT>
   
       <table class="contentmain">
            <tr> <td>
             <table class="tablerowcolor" align="center" width="100%">
             <tr>
                <td colspan="4" > <asp:Image ID="Image1" runat="server" 
                        AlternateText="NCRB CCTNS Image" ImageUrl="~/img/BlueVSHead3.jpg" Width="100%"/>
               </td> </tr>

                    <tr>
                        <td style="height: 100px" >
                        </td>
                    </tr>
                    <tr>
                        <td align="center" >
                            <table cellspacing="1"  >
                                <tr>
                                    <td class="style2" style="color: #800000"><strong>
                                        User ID</strong>
                                    </td>
                                    <td class="rB" >
                                   
                                        <asp:TextBox ID="txtLoginCode" runat="server"  Width="250px" MaxLength="75"></asp:TextBox>
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" style="color: #800000"><strong>Question1</strong></td>
                                    <td class="rB" >
                                   
                                        <asp:DropDownList ID="ddQues1" CssClass="combo" runat="server" Height="17px" 
                                            style="margin-left: 0px" Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3" style="color: #800000" ><strong>Ans1</strong></td>
                                    <td class="style1" >
                                   
                                        <asp:TextBox ID="txtAns1" CssClass="combo" runat="server"  Width="250px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" style="color: #800000"><strong>Question2</strong></td>
                                    <td class="rB" >
                                   
                                        <asp:DropDownList ID="ddQues2" CssClass="combo" runat="server" Height="16px" 
                                            Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" style="color: #800000"><strong>Ans2</strong></td>
                                    <td class="rB" >
                                   
                                        <asp:TextBox ID="txtAns2" CssClass="combo" runat="server"  Width="250px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2" style="color: #800000"><strong>
                                        Password</strong>
                                    </td>
                                    <td class="rB" >
                                        <asp:TextBox ID="txtPassword" CssClass="combo" runat="server" 
                                            TextMode="Password" MaxLength="36"  Width="250px" autocomplete="off" 
                                            CausesValidation="True"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                <td class="style2" style="color: #800000"><strong>
                                        Confirm Password</strong>
                                    </td>
                                    <td class="rB" >
                                        <asp:TextBox ID="txtConPass" CssClass="combo" runat="server" 
                                            TextMode="Password"  MaxLength="36"  Width="250px" CausesValidation="True"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="center" colspan="2" class="rB" >
                                        <asp:Button ID="btnLogin" CssClass="btn" Text="Update" 
                                            runat="server" onclick="btnLogin_Click1" OnClientClick="javascript:return ValidateTPACode();" />
                                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" 
                                            onclick="btnRefresh_Click" />
                                    </td>
                                </tr>
                                <tr id="trRrrMsg"   class="rB" style="height: 25px;" runat="server">
                                    <td align="center" colspan="2" class="errortext">
                                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                                        <br />
<asp:RegularExpressionValidator ID="RegExp1" runat="server"    
ErrorMessage="Password length must be between 12 to 36 characters"
ControlToValidate="txtPassword"    
ValidationExpression="^[a-zA-Z0-9'@&#.\s]{12,36}$" SetFocusOnError="True" EnableViewState="true"  
                                            EnableViewStateMac="true"  />
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"    
ErrorMessage="Confirmation Password length must be between 12 to 36 characters"
ControlToValidate="txtConPass"    
ValidationExpression="^[a-zA-Z0-9'@&#.\s]{12,36}$" SetFocusOnError="True" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        </form>
</body>
</html>