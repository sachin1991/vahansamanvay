Imports System.IO
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS
Partial Class ImportExportVahanSamanvay
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Public myCon As New SqlClient.SqlConnection
    Public Cmd1 As New SqlClient.SqlCommand
    Public Dr As SqlClient.SqlDataReader
    Public strSql As String
    Public ConStr As String
  
    Protected Sub Import_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImportMVCS.Click
        Dim FILENAM
        Dim path1
        Dim msg
        Dim filenam1
        If Me.FileImportMVCS.HasFile = Nothing Then
            msg = "<script language='javascript'>"
            msg = msg + "alert('" + "Select file for import" & "');"
            msg = msg + "</script>"
            Response.Write(msg)
            'MsgBox("Select file for import", MsgBoxStyle.OkOnly, "Select file for import")
            Exit Sub
        Else
            path1 = dirpath1.Text
            FILENAM = path1 + "\" + Path.GetFileName(FileImportMVCS.FileName)
            filenam1 = "D:\VSImportExport\" + Path.GetFileName(FileImportMVCS.FileName)
        End If

        If FileImportMVCS.HasFile Then
            Try
                FileImportMVCS.SaveAs("d:\VSImportExport\" & _
                    FileImportMVCS.FileName)
                'Label1.Text = "File name: " & _
                '    FileImportMVCS.PostedFile.FileName & "<br>" & _
                '    "File Size: " & _
                '    FileImportMVCS.PostedFile.ContentLength & "<br>" & _
                '    "Content type: " & _
                '    FileImportMVCS.PostedFile.ContentType & "<br>" & _
                '    "Location Saved: D:\VSImportExport\" & _
                '    FileImportMVCS.FileName
                Label1.Text = "File name: " & _
                    FileImportMVCS.FileName
            Catch ex As Exception
                Label1.Text = "ERROR: " & ex.Message.ToString()
            End Try
        Else
            Label1.Text = "You have not specified a file."
        End If
        Dim I As Integer
        Dim Sr As Integer = 0
        'Dim Cont As String

        Dim cmdINSERT As New SqlCommand(strSql, consupac)
        strSql = "usp_InsertDailyTransaction"
        cmdINSERT.CommandText = GenericMethods.check(strSql)
        cmdINSERT.CommandType = CommandType.StoredProcedure
        Dim ln As String
        Dim objStreamReader As StreamReader
        objStreamReader = File.OpenText(filenam1)
        Dim pos As String
        Dim fldname As String
        Dim fldtxt As String

        consupac.Open()

        Dim nor As Int32
        nor = 0
        Dim flerr As String
        flerr = ""
        Dim correctfileformat As Boolean
        correctfileformat = False
        Do While Not objStreamReader.EndOfStream
            Try
                ln = objStreamReader.ReadLine()
                
                pos = InStr(ln, ".", CompareMethod.Text)
                fldname = Left(ln, pos - 1)
                fldtxt = Mid(ln, pos + 2, Len(ln) - pos - 2)
                If Left(ln, 2) <> "st" And fldname <> "st" And correctfileformat = "false" Then
                    flerr = "The file contents are not correct"
                    correctfileformat = False
                    Exit Do
                Else
                    flerr = ""
                    correctfileformat = True
                End If
                If fldname <> "eof" Then
                    If (fldname = "firdate" And fldtxt.Trim = "") Then
                        fldtxt = Date.Today
                    End If
                    If (fldname = "OCCDATE" And fldtxt.Trim = "") Then
                        fldtxt = Date.Today
                    End If
                    If (fldname = "regdate" And fldtxt.Trim = "") Then
                        fldtxt = Date.Today
                    End If
                    If (fldname = "entrydate" And fldtxt.Trim = "") Then
                        fldtxt = Date.Today
                    End If
                    If (fldname = "entrytime" And fldtxt.Trim = "") Then
                        fldtxt = Date.Now.TimeOfDay.ToString()
                    End If
                    cmdINSERT.Parameters.AddWithValue(convertfldname(fldname), fldtxt.Trim)



                Else
                    Try
                        nor = nor + 1
                        cmdINSERT.ExecuteNonQuery()
                        cmdINSERT.Parameters.Clear()
                    Catch ex As Exception
                        msg = "<script language='javascript'>"
                        msg = msg + "alert('" + ex.Message & "');"
                        msg = msg + "</script>"
                        Response.Write(msg)


                    End Try

                End If
            Catch
            End Try

        Loop
        'Set the text of the file to a Web control


        'We may wish to replace carraige returns with <br>s

        If flerr <> "" Then
            msg = "<script language='javascript'>"
            msg = msg + "alert('" + flerr + "');"
            msg = msg + "</script>"
            Response.Write(msg)
        Else
            msg = "<script language='javascript'>"
            msg = msg + "alert('" + "No. of Records Imported " & nor & "');"
            msg = msg + "</script>"
            Response.Write(msg)
        End If
        objStreamReader.Close()

        consupac.Close()

    End Sub
    Function ASSIGNVALU(ByRef FLDN, ByVal FLDT)
        Return FLDN.value = FLDT
    End Function
    Function convertfldname(ByVal fldname)
        Return fldname.ToString
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If


        If Not IsPostBack Then


            Dim sqlcmd As String
            Dim cmdselect As New SqlCommand
            Dim dr As SqlDataReader
            sqlcmd = "Select * from sysobjects"
            With cmdselect
                .Connection = consupac
                .CommandText = sqlcmd
            End With
            consupac.Open()
            dr = cmdselect.ExecuteReader
            Do While dr.Read
                If Left(dr.Item(0), 3) = "COR" Then
                    Me.ListBox1.Items.Add(dr.Item(0))
                End If
            Loop
            dr.Close() ' added new
            consupac.Close()
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "8" 'ncrb
                Exit Select
            Case "10" 'crime records
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub ExportVahanSamanvay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ExportVahanSamanvay.Click
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sqlcmd As String
        Dim cmdselect As New SqlCommand
        Dim dr As SqlDataReader
        sqlcmd = "Select * from " & Me.ListBox1.Text
        With cmdselect
            .Connection = consupac
            .CommandText = sqlcmd
        End With
        consupac.Open()
        dr = cmdselect.ExecuteReader
        Dim i
        Dim objstreamwriter As StreamWriter
        objstreamwriter = File.CreateText("d:\vs\vs" & Year(Now()) & Month(Now()) & Day(Now()) & ".csv")
        Dim st
        Do While dr.Read
            st = ""
            For i = 0 To dr.FieldCount - 2
                st = st & Chr(34) & dr.Item(i) & Chr(34) & ","
            Next
            st = st & Chr(34) & dr.Item(dr.FieldCount - 1) & Chr(34) & Chr(13)
            '   MsgBox(st)
            objstreamwriter.Write(st)
        Loop
        objstreamwriter.Close()
        consupac.Close()
    End Sub

    Protected Sub ImportVahanSamanvay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ImportVahanSamanvay.Click
        If Me.FileImportfromVahanSamanvay.Value = "" Then
            MsgBox("Please select VahanSamanvay file")
            Exit Sub
        End If
        Dim table As New DataTable()
        table.Columns.Add("st")
        table.Columns.Add("stdesc")
        table.Columns.Add("dist")
        table.Columns.Add("distname")
        table.Columns.Add("pscode")
        table.Columns.Add("psname")
        table.Columns.Add("firyear")
        table.Columns.Add("firno")
        table.Columns.Add("firtype")
        table.Columns.Add("firdate")
        table.Columns.Add("crimeno")
        table.Columns.Add("autotype")
        table.Columns.Add("autotypedesc")
        table.Columns.Add("estimatedvalue")
        table.Columns.Add("automake")
        table.Columns.Add("automakedesc")
        table.Columns.Add("autocolor")
        table.Columns.Add("autocolordesc")
        table.Columns.Add("pecu")
        table.Columns.Add("model")
        table.Columns.Add("regno")
        table.Columns.Add("chasisno")
        table.Columns.Add("engineno")
        table.Columns.Add("regnokey")
        table.Columns.Add("chasisnokey")
        table.Columns.Add("enginenokey")
        table.Columns.Add("insno")
        table.Columns.Add("insname")
        table.Columns.Add("belong")
        table.Columns.Add("stolen")
        table.Columns.Add("recovered")
        table.Columns.Add("involved")
        table.Columns.Add("seized")
        table.Columns.Add("coordinationrefno")
        table.Columns.Add("OCCDATE")
        table.Columns.Add("INSNAMECODE")
        table.Columns.Add("regdate")
        table.Columns.Add("FIRNO1")
        table.Columns.Add("ACT1")
        table.Columns.Add("ACT2")
        table.Columns.Add("ACT3")
        table.Columns.Add("ACT4")
        table.Columns.Add("ACTDESC1")
        table.Columns.Add("ACTDESC2")
        table.Columns.Add("ACTDESC3")
        table.Columns.Add("ACTDESC4")
        table.Columns.Add("SECTION1")
        table.Columns.Add("SECTION2")
        table.Columns.Add("section3")
        table.Columns.Add("section4")
        table.Columns.Add("section5")
        table.Columns.Add("section6")
        table.Columns.Add("section7")
        table.Columns.Add("section8")
        table.Columns.Add("section9")
        table.Columns.Add("section10")
        table.Columns.Add("section11")
        table.Columns.Add("section12")
        table.Columns.Add("section13")
        table.Columns.Add("section14")
        table.Columns.Add("section15")
        table.Columns.Add("section16")
        table.Columns.Add("section17")
        table.Columns.Add("section18")
        table.Columns.Add("section19")
        table.Columns.Add("section20")
        table.Columns.Add("section21")
        table.Columns.Add("section22")
        table.Columns.Add("section23")
        table.Columns.Add("section24")
        table.Columns.Add("CHASISKEY2")
        table.Columns.Add("ENGINEKEY2")
        table.Columns.Add("ownerName")
        table.Columns.Add("fatherName")
        table.Columns.Add("address")
        table.Columns.Add("DELETED")
        table.Columns.Add("usr")
        table.Columns.Add("entrydate")
        table.Columns.Add("entrytime")

        '--TextField Parser is used to read the files 
        Dim parser As New FileIO.TextFieldParser(Me.FileImportfromVahanSamanvay.Value)

        parser.Delimiters = New String() {","} ' fields are separated by comma
        parser.HasFieldsEnclosedInQuotes = True ' each of the values is enclosed with double quotes
        parser.TrimWhiteSpace = True

        '--First line is skipped , its the header
        'parser.ReadLine()
        'MsgBox(parser.ToString)
        'Exit Sub
        'table.Rows.Add(parser.ReadLine)
        '-- Add all the rows to datatable
        Do Until parser.EndOfData = True
            table.Rows.Add(parser.ReadFields())
        Loop
        Me.GridView1.DataSource = table
        Me.GridView1.DataBind()

        Dim strSql As String = " INSERT INTO CORAPPEND " _
& "(st , " _
        & "stdesc, " _
        & "dist," _
        & "distname," _
        & "pscode," _
        & "psname," _
        & "firyear," _
        & "firno," _
        & "firtype, " _
        & "firdate, " _
        & "crimeno," _
        & "autotype," _
        & "autotypedesc," _
        & "estimatedvalue," _
        & "automake," _
        & "automakedesc," _
        & "autocolor," _
        & "autocolordesc," _
        & "pecu," _
        & "model," _
        & "regno," _
        & "chasisno," _
        & "engineno," _
        & "regnokey," _
        & "chasisnokey," _
        & "enginenokey," _
        & "insno," _
        & "insname," _
        & "belong," _
        & "stolen," _
        & "recovered," _
        & "involved," _
        & "seized," _
        & "coordinationrefno," _
        & "OCCDATE," _
        & "INSNAMECODE," _
        & "regdate," _
        & "FIRNO1," _
        & "ACT1," _
        & "ACT2," _
        & "ACT3," _
        & "ACT4," _
        & "ACTDESC1," _
        & "ACTDESC2," _
        & "ACTDESC3," _
        & "ACTDESC4," _
        & "SECTION1," _
        & "SECTION2," _
        & "section3," _
        & "section4," _
        & "section5," _
        & "section6," _
        & "section7," _
        & "section8," _
        & "section9," _
        & "section10," _
        & "section11," _
        & "section12," _
        & "section13," _
        & "section14," _
        & "section15," _
        & "section16," _
        & "section17," _
        & "section18," _
        & "section19," _
        & "section20," _
        & "section21," _
        & "section22," _
        & "section23," _
        & "section24," _
        & "CHASISKEY2," _
        & "ENGINEKEY2," _
        & "ownerName," _
        & "fatherName," _
        & "address," _
        & "DELETED," _
        & "usr," _
        & "entrydate," _
        & "entrytime)" _
        & "VALUES " _
        & "(@st," _
        & "@stdesc," _
        & "@dist," _
        & "@distname," _
        & "@pscode," _
        & "@psname," _
        & "@firyear," _
        & "@firno," _
        & "@firtype," _
        & "@firdate," _
        & "@crimeno," _
        & "@autotype," _
        & "@autotypedesc," _
        & "@estimatedvalue," _
        & "@automake," _
        & "@automakedesc," _
        & "@autocolor," _
        & "@autocolordesc," _
        & "@pecu," _
        & "@model," _
        & "@regno," _
        & "@chasisno," _
        & "@engineno," _
        & "@regnokey," _
        & "@chasisnokey," _
        & "@enginenokey," _
        & "@insno," _
        & "@insname," _
        & "@belong," _
        & "@stolen," _
        & "@recovered," _
        & "@involved," _
        & "@seized," _
        & "@coordinationrefno," _
        & "@OCCDATE," _
        & "@INSNAMECODE," _
        & "@regdate," _
        & "@FIRNO1," _
        & "@ACT1," _
        & "@ACT2," _
        & "@ACT3," _
        & "@ACT4," _
        & "@ACTDESC1," _
        & "@ACTDESC2," _
        & "@ACTDESC3," _
        & "@ACTDESC4," _
        & "@SECTION1," _
        & "@SECTION2," _
        & "@section3," _
        & "@section4," _
        & "@section5," _
        & "@section6," _
        & "@section7," _
        & "@section8," _
        & "@section9," _
        & "@section10," _
        & "@section11," _
        & "@section12," _
        & "@section13," _
        & "@section14," _
        & "@section15," _
        & "@section16," _
        & "@section17," _
        & "@section18," _
        & "@section19," _
        & "@section20," _
        & "@section21," _
        & "@section22," _
        & "@section23," _
        & "@section24," _
        & "@CHASISKEY2," _
        & "@ENGINEKEY2," _
        & "@ownerName," _
        & "@fatherName," _
        & "@address," _
        & "@DELETED," _
        & "@usr," _
        & "@entrydate," _
        & "@entrytime)"
        consupac.Open()
        Dim cmd As New SqlClient.SqlCommand(strSql, consupac) ' create command objects and add parameters
        cmd.CommandType = CommandType.Text
        With cmd.Parameters
            .Add("@st", SqlDbType.VarChar, 50, "st")
            .Add("@stdesc", SqlDbType.VarChar, 50, "stdesc")
            .Add("@dist", SqlDbType.VarChar, 50, "dist")
            .Add("@distname", SqlDbType.VarChar, 50, "distname")
            .Add("@pscode", SqlDbType.VarChar, 50, "pscode")
            .Add("@psname", SqlDbType.VarChar, 50, "psname")
            .Add("@firyear", SqlDbType.VarChar, 50, "firyear")
            .Add("@firno", SqlDbType.VarChar, 50, "firno")
            .Add("@firtype", SqlDbType.VarChar, 50, "firtype")
            .Add("@firdate", SqlDbType.VarChar, 50, "firdate")
            .Add("@crimeno", SqlDbType.VarChar, 50, "crimeno")
            .Add("@autotype", SqlDbType.VarChar, 50, "autotype")
            .Add("@autotypedesc", SqlDbType.VarChar, 50, "autotypedesc")
            .Add("@estimatedvalue", SqlDbType.VarChar, 50, "estimatedvalue")
            .Add("@automake", SqlDbType.VarChar, 50, "automake")
            .Add("@automakedesc", SqlDbType.VarChar, 50, "automakedesc")
            .Add("@autocolor", SqlDbType.VarChar, 50, "autocolor")
            .Add("@autocolordesc", SqlDbType.VarChar, 50, "autocolordesc")
            .Add("@pecu", SqlDbType.VarChar, 50, "pecu")
            .Add("@model", SqlDbType.VarChar, 50, "model")
            .Add("@regno", SqlDbType.VarChar, 50, "regno")
            .Add("@chasisno", SqlDbType.VarChar, 50, "chasisno")
            .Add("@engineno", SqlDbType.VarChar, 50, "engineno")
            .Add("@regnokey", SqlDbType.VarChar, 50, "regnokey")
            .Add("@chasisnokey", SqlDbType.VarChar, 50, "chasisnokey")
            .Add("@enginenokey", SqlDbType.VarChar, 50, "enginenokey")
            .Add("@insno", SqlDbType.VarChar, 50, "insno")
            .Add("@insname", SqlDbType.VarChar, 50, "insname")
            .Add("@belong", SqlDbType.VarChar, 50, "belong")
            .Add("@stolen", SqlDbType.VarChar, 50, "stolen")
            .Add("@recovered", SqlDbType.VarChar, 50, "recovered")
            .Add("@involved", SqlDbType.VarChar, 50, "involved")
            .Add("@seized", SqlDbType.VarChar, 50, "seized")
            .Add("@coordinationrefno", SqlDbType.VarChar, 50, "coordinationrefno")
            .Add("@OCCDATE", SqlDbType.VarChar, 50, "OCCDATE")
            .Add("@INSNAMECODE", SqlDbType.VarChar, 50, "INSNAMECODE")
            .Add("@regdate", SqlDbType.VarChar, 50, "regdate")
            .Add("@FIRNO1", SqlDbType.VarChar, 50, "FIRNO1")
            .Add("@ACT1", SqlDbType.VarChar, 50, "ACT1")
            .Add("@ACT2", SqlDbType.VarChar, 50, "ACT2")
            .Add("@ACT3", SqlDbType.VarChar, 50, "ACT3")
            .Add("@ACT4", SqlDbType.VarChar, 50, "ACT4")
            .Add("@ACTDESC1", SqlDbType.VarChar, 50, "ACTDESC1")
            .Add("@ACTDESC2", SqlDbType.VarChar, 50, "ACTDESC2")
            .Add("@ACTDESC3", SqlDbType.VarChar, 50, "ACTDESC3")
            .Add("@ACTDESC4", SqlDbType.VarChar, 50, "ACTDESC4")
            .Add("@SECTION1", SqlDbType.VarChar, 50, "SECTION1")
            .Add("@SECTION2", SqlDbType.VarChar, 50, "SECTION2")
            .Add("@section3", SqlDbType.VarChar, 50, "section3")
            .Add("@section4", SqlDbType.VarChar, 50, "section4")
            .Add("@section5", SqlDbType.VarChar, 50, "section5")
            .Add("@section6", SqlDbType.VarChar, 50, "section6")
            .Add("@section7", SqlDbType.VarChar, 50, "section7")
            .Add("@section8", SqlDbType.VarChar, 50, "section8")
            .Add("@section9", SqlDbType.VarChar, 50, "section9")
            .Add("@section10", SqlDbType.VarChar, 50, "section10")
            .Add("@section11", SqlDbType.VarChar, 50, "section11")
            .Add("@section12", SqlDbType.VarChar, 50, "section12")
            .Add("@section13", SqlDbType.VarChar, 50, "section13")
            .Add("@section14", SqlDbType.VarChar, 50, "section14")
            .Add("@section15", SqlDbType.VarChar, 50, "section15")
            .Add("@section16", SqlDbType.VarChar, 50, "section16")
            .Add("@section17", SqlDbType.VarChar, 50, "section17")
            .Add("@section18", SqlDbType.VarChar, 50, "section18")
            .Add("@section19", SqlDbType.VarChar, 50, "section19")
            .Add("@section20", SqlDbType.VarChar, 50, "section20")
            .Add("@section21", SqlDbType.VarChar, 50, "section21")
            .Add("@section22", SqlDbType.VarChar, 50, "section22")
            .Add("@section23", SqlDbType.VarChar, 50, "section23")
            .Add("@section24", SqlDbType.VarChar, 50, "section24")
            .Add("@CHASISKEY2", SqlDbType.VarChar, 50, "CHASISKEY2")
            .Add("@ENGINEKEY2", SqlDbType.VarChar, 50, "ENGINEKEY2")
            .Add("@ownerName", SqlDbType.VarChar, 50, "ownerName")
            .Add("@fatherName", SqlDbType.VarChar, 50, "fatherName")
            .Add("@address", SqlDbType.VarChar, 50, "address")
            .Add("@DELETED", SqlDbType.VarChar, 50, "DELETED")
            .Add("@usr", SqlDbType.VarChar, 50, "usr")
            .Add("@entrydate", SqlDbType.VarChar, 50, "entrydate")
            .Add("@entrytime", SqlDbType.VarChar, 50, "entrytime")
        End With
        Dim adapter As New SqlClient.SqlDataAdapter()
        adapter.InsertCommand = cmd

        '--Update the original SQL table from the datatable
        Try
            'adapter.InsertCommand.ExecuteNonQuery()
            Dim iRowsInserted As Int32 = adapter.Update(table)
        Catch ex As Exception
            MsgBox("error" & ex.ToString)
        End Try

        consupac.Close()
    End Sub
End Class