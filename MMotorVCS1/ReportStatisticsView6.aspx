<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportStatisticsView6.aspx.vb" Inherits="ReportStatisticsView6" title="Vahan Samanvay Statistics Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Vahan Samanvay Report</title>
    <link href="style/main.css" rel="stylesheet" type="text/css" />
    <style type="text/css">

        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 97px;
        }
        .style4
        {
            width: 75px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="contentmain">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="GOI" runat="server" Font-Bold="True" Text="Government of India" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="MHA" runat="server" Font-Bold="True" Text="Mnistry of Home Affairs"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="NCRB" runat="server" Font-Bold="True" Text="National Crime Records Bureau"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="VS" runat="server" Font-Bold="True" Text="Vahan Samanvay" Visible="False"></asp:Label>
                    <strong>&nbsp;Statistics</strong></td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="LISTOFSTOLEN" runat="server" Text="From"
                        Visible="False"></asp:Label>
                    <asp:TextBox ID="DTFROM" runat="server" Visible="False" BorderStyle="None"></asp:TextBox>
                    <asp:Label ID="LISTTO" runat="server" Text="to" Visible="False"></asp:Label>
                    <asp:TextBox ID="DTTO" runat="server" Visible="False" BorderStyle="None"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <div align="center">
            <asp:GridView ID="GridView1" runat="server">
            </asp:GridView>
                    <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table border='1' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td>
                        State
                        </td>
                        <td>
                         Type
                        </td>
                        <td>
                         Stolen
                        </td>
                        <td>
                        Recovered
                        </td>
                        <td>
                         Deleted
                         </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                     <td>
                     <%# DataBinder.Eval(Container, "DataItem.state")%>
                    </td>
                    <td>
                     <%# DataBinder.Eval(Container, "DataItem.Typeofvehicle")%>
                    </td>
                    <td align="right">
                     <%# DataBinder.Eval(Container, "DataItem.stolen")%>
                    </td>
                    <td align="right">
                     <%# DataBinder.Eval(Container, "DataItem.recovered")%>
                    </td>
                    <td align="right">
                     <%# DataBinder.Eval(Container, "DataItem.deleted")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
            <tr>
                        <td>
                        Total
                        </td>
                        <td>
                        </td>
                        <td align="right">
                        <%Response.Write(Stolens)%>
                        </td>
                        <td align="right">
                        <%Response.Write(Recovereds)%>
                        </td>
                        <td align="right">
                         <%Response.Write(Deleteds)%>
                         </td>
                    </tr>
                </table>
                </table>
            </FooterTemplate>
        </asp:Repeater>
            <br />
        <table class="style1">
            <tr>
                <td class="style2">
        <asp:LinkButton ID="FirstPage" runat="server">First Page</asp:LinkButton>
                </td>
                <td class="style3">
        
        <asp:LinkButton ID="Lnkbtnprev" runat="server" Visible="False">Previous Page</asp:LinkButton>
                </td>
                <td class="style4">
        <asp:LinkButton ID="LnkbtnNext" runat="server" Visible="False">Next Page</asp:LinkButton>
                </td>
                <td>
        <asp:LinkButton ID="LastPage" runat="server">Last Page</asp:LinkButton>
                </td>
            </tr>
        </table>
        </div>
        
        </div>
    </form>
</body>
</html>
