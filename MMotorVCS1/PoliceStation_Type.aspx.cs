using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class PoliceStation_Type : System.Web.UI.Page
{
    static int pstype_id;
    static string _userid;

    MasterMethods mm = new MasterMethods();
    static int pagecount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");
        //_userid = "1";

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }

        if ((Session.SessionID) == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {

        }

        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }

            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindStateName();

        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    protected void ddStateCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();

        if (ddStateCode.SelectedIndex != 0)
        {
            ds = mm.getStatecode((ddStateCode.SelectedValue));
            txtStateCode.Text = ds.Tables[0].Rows[0]["statecode"].ToString();
            ds = mm.getDistrictType(txtStateCode.Text);
            ddDisCode.DataSource = ds;
            ddDisCode.DataTextField = GenericMethods.check(ds.Tables[0].Columns["districtname"].ColumnName);
            ddDisCode.DataValueField = GenericMethods.check(ds.Tables[0].Columns["districtid"].ColumnName);
            ddDisCode.DataBind();
            ddDisCode.Items.Insert(0, "--Select--");

        }
        else
        {
            txtStateCode.Text = "";
            ddStateCode.Focus();
        }

    }
    protected void txtStateCode_TextChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        if (txtStateCode.Text.Trim() != "")
        {
            ds = mm.getStateNameByCode(txtStateCode.Text.Trim());
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddStateCode.Text = GenericMethods.check(ds.Tables[0].Rows[0]["stateid"].ToString());

            }
            else
            {
                ddStateCode.SelectedIndex = 0;
                lblmsg.Text = "<font color=red>This state code does not exists.</font>";
                txtStateCode.Focus();
            }
        }
        else
        {
            ddStateCode.SelectedIndex = 0;
            txtStateCode.Focus();
        }
    }

    public void getmaxid()
    {
        try
        {
            pstype_id = mm.getPsMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindStateName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateType();
            ddStateCode.DataSource = ds;
            ddStateCode.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateCode.DataValueField = GenericMethods.check(ds.Tables[0].Columns["stateid"].ColumnName);
            ddStateCode.DataBind();
            ddStateCode.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindDistrictName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getDistrictTypeNamewise();
            ddDisCode.DataSource = ds;
            ddDisCode.DataTextField = GenericMethods.check(ds.Tables[0].Columns["districtname"].ColumnName);
            ddDisCode.DataValueField = GenericMethods.check(ds.Tables[0].Columns["districtid"].ColumnName);
            ddDisCode.DataBind();
            ddDisCode.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getPsType();
            gvPSType.DataSource = ds;
            gvPSType.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtdisCode.Text = "";
        txtPSCode.Text = "";
        txtPSName.Text = "";
        txtStateCode.Text = "";
        ddDisCode.SelectedIndex = 0;
        ddStateCode.SelectedIndex = 0;
        gvPSType.DataSource = null;
        gvPSType.DataBind();
        // bindgrid();
    }
    private Boolean checkillegal(string code,string description,string district,string statecode)
    {
        bool illegal;
        illegal = false;

        if (code.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (description.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (district.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (statecode.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }

        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        try
        {
            DataSet ds = new DataSet();
            int flag = 0;
            ds = mm.getPsTypenew(txtStateCode.Text.ToString(), txtdisCode.Text.ToString());
            if (ddDisCode.SelectedIndex != 0 && txtPSCode.Text != "" && txtPSName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["Psname"].ToString().Trim().ToUpper()) == txtPSName.Text.Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Police Station Name is already exists.</font>";
                        break;
                    }
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["Pscode"].ToString().Trim().ToUpper()) == txtPSCode.Text.Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Police station Code is already exists.</font>";
                        break;
                    }
                }
                if (flag != 1)
                {
                    string locationkey, code, description, locationtype, parentkey, updatedby;
                    locationkey = txtStateCode.Text.ToString().Trim() + txtdisCode.Text.ToString().Trim() + txtPSCode.Text.ToString().Trim();
                    code = txtPSCode.Text.ToString();
                    description = txtPSName.Text.ToString();
                    locationtype = "3";
                    parentkey = txtStateCode.Text.ToString().Trim() + txtdisCode.Text.ToString().Trim();
                    updatedby = "Admin";
                    bool illegal = checkillegal(code, description, txtdisCode.Text.ToString().Trim(),txtStateCode.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.AddPsType(GenericMethods.check(locationkey), GenericMethods.check(code), GenericMethods.check(description), GenericMethods.check(locationtype), GenericMethods.check(parentkey), updatedby) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not inserted</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException)
        {
            lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>";
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        try
        {

            DataSet ds = new DataSet();
            int flag = 0;
            ds = mm.getPsType();
            lblid.Text = ddStateCode.SelectedValue.ToString().Trim() + ddDisCode.SelectedValue.ToString().Trim() + txtPSCode.Text.ToString().Trim();

            if (ddDisCode.SelectedIndex != 0 && txtPSCode.Text != "" && txtPSName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["Psid"].ToString() == lblid.Text)
                    {
                        if (ds.Tables[0].Rows[i]["Psname"].ToString().Trim().ToUpper() == txtPSName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Police Station Name is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    string totid = ddStateCode.SelectedValue.ToString().Trim() + ddDisCode.SelectedValue.ToString().Trim() + txtPSCode.Text.ToString().Trim();
                    bool illegal = checkillegal(txtPSCode.Text.ToString().Trim(), txtPSName.Text.ToString().Trim(), txtdisCode.Text.ToString().Trim(),txtStateCode.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.updatePsType(GenericMethods.check(totid), Convert.ToInt32(GenericMethods.check(ddDisCode.SelectedValue)), (GenericMethods.check(txtPSCode.Text.Trim().ToUpper())), GenericMethods.check(txtPSName.Text.Trim().ToUpper()), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values Updated</font>";

                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not Updated</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException)
        {
            lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>";
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }

    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void gvPSType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                ddDisCode.Text = gvPSType.Rows[Convert.ToInt32(e.CommandName) - (gvPSType.PageSize * pagecount)].Cells[1].Text;
                txtdisCode.Text = gvPSType.Rows[Convert.ToInt32(e.CommandName) - (gvPSType.PageSize * pagecount)].Cells[2].Text;
                lblid.Text = gvPSType.Rows[Convert.ToInt32(e.CommandName) - (gvPSType.PageSize * pagecount)].Cells[4].Text;
                txtPSCode.Text = gvPSType.Rows[Convert.ToInt32(e.CommandName) - (gvPSType.PageSize * pagecount)].Cells[5].Text;
                txtPSName.Text = gvPSType.Rows[Convert.ToInt32(e.CommandName) - (gvPSType.PageSize * pagecount)].Cells[6].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void ddDisCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        if (ddDisCode.SelectedIndex != 0)
        {
            ds = mm.getDistrictcodeList(Convert.ToInt32(ddStateCode.SelectedValue.ToString()));
            txtdisCode.Text = ddDisCode.SelectedValue.ToString();
            //txtdisCode.Text = ds.Tables[0].Rows[0]["districtcode"].ToString();
            //string stateid = ds.Tables[0].Rows[0]["statecode"].ToString();
            string stateid = ddStateCode.SelectedValue.ToString();
            try
            {
                if (psbyname.Checked == true)
                {
                    ds = mm.getPsType1(txtdisCode.Text, stateid);
                }
                else
                {
                    ds = mm.getPsType(txtdisCode.Text, stateid);
                }
                gvPSType.DataSource = ds;
                gvPSType.DataBind();
            }
            catch (SqlException)
            { lblmsg.Text = "Occuring Problem in connectivity to database."; }
            catch (HttpCompileException)
            { lblmsg.Text = "System Occuring Problem."; }
            catch (HttpParseException)
            { lblmsg.Text = "Internet Browser Occuring Problem."; }
            catch (Exception ex)
            { lblmsg.Text = ex.Message; }

        }
        else
        {
            txtdisCode.Text = "";
        }
    }
    protected void gvPSType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[4].Visible = false;
        }
    }
    protected void gvPSType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvPSType.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void psbyname_CheckedChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        ds = mm.getDistrictcode(Convert.ToInt32(ddDisCode.SelectedValue.ToString()));
        txtdisCode.Text = ds.Tables[0].Rows[0]["districtcode"].ToString();
        string stateid = ds.Tables[0].Rows[0]["statecode"].ToString();
        try
        {
 
            if (psbyname.Checked == true)
            {
                ds = mm.getPsType1(txtdisCode.Text, stateid);
            }
            else
            {
                ds = mm.getPsType(txtdisCode.Text, stateid);
            }
            gvPSType.DataSource = ds;
            gvPSType.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
}


