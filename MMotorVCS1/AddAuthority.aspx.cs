using System;
using System.Net;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.Util;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;
public partial class AddAuthority : System.Web.UI.Page
{
    int isdeleted = 0;
    static string _userid;
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    DataSet ds = new DataSet();
    int PostCount = 0;


    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }
        btnAdd.Attributes.Add("onclick", "return valid();");

        //ddVehTypeName.Attributes.Add("OnChange", "GetVehTypeCode();");
        //txtVehTypeCode.Attributes.Add("OnBlur", "return SelectVehTypeddl();");

        //ddMakeName.Attributes.Add("OnChange", "GetMakeCode();");
        //txtMakeCode.Attributes.Add("OnBlur", "return SelectMakeddl();");

        //ddColName.Attributes.Add("OnChange", "GetColCode();");
        //txtColCode.Attributes.Add("OnBlur", "return SelectColddl();");
        ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            _userid = Session["vcUserName"].ToString();

        }

        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }


            bindStateName();
            bindddyear();
            bindVehicleType();
            bindColName();

            ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));

            if (Session["vcStateCode1"].ToString() != "0" && Session["vcStateCode1"].ToString() != "000" && Session["vcStateCode1"].ToString() != "" && Session["vcStateCode1"].ToString() != null)
            {

                bindStateName(Session["vcStateCode1"].ToString());
            }
            if (Session["vcDistrict"].ToString() != "0" && Session["vcDistrict"].ToString() != "" && Session["vcDistrict"].ToString() != null)
            {
                bindDistrictName(Session["vcDistrict"].ToString());
            }
            else
            {
                bindDistrictName();
            }
            if (Session["vcPS"].ToString() != "0" && Session["vcPS"].ToString() != "" && Session["vcPS"].ToString() != null)
            {

            }
            else
            {

            }

        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "4": //authority
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }

    }
    public void bindddyear()
    {
        ListItem li;
        int val = 1950;
        for (int i = System.DateTime.Now.Year; i >= val; i--)
        {
            li = new ListItem(Convert.ToString(i), Convert.ToString(i));
            txtYear.Items.Add(li);
        }
        txtYear.Items.Insert(0, "--Select--");
    }
    protected void refresh1()
    {

        btnAdd.Visible = true;
        btnReset.Visible = true;
        txtChasisno.Text = "";
        txtColCode.Text = "";

        txtEngineno.Text = "";

        txtMakeCode.Text = "";

        txtRegistrationno.Text = "";

        txtVehTypeCode.Text = "";

        ddColName.SelectedIndex = 0;

        ddMakeName.Items.Clear();
        ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddMakeName.SelectedIndex = 0;
        ddModelName.Items.Clear();
        ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddModelName.SelectedIndex = 0;

        ddVehTypeName.SelectedIndex = 0;
    }
    protected void refresh()
    {
        btnAdd.Visible = true;
        btnReset.Visible = true;
        txtChasisno.Text = "";
        txtColCode.Text = "";
        txtEngineno.Text = "";
        txtMakeCode.Text = "";
        txtRegistrationno.Text = "";
        txtVehTypeCode.Text = "";
        ddColName.SelectedIndex = 0;
        ddMakeName.Items.Clear();
        ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddMakeName.SelectedIndex = 0;
        ddModelName.Items.Clear();
        ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddModelName.SelectedIndex = 0;
        ddVehTypeName.SelectedIndex = 0;
    }
    public void getmaxid()
    {
        try
        {
            hfLostId.Value = eform.getEntry_LostMaxId().ToString();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleTypeNew();
            ddVehTypeName.DataSource = ds;
            ddVehTypeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehTypeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehidvehcode"].ColumnName);
            ddVehTypeName.DataBind();
            ddVehTypeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindMakeName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake_accVehTypeNew(Convert.ToInt32(ddVehTypeName.SelectedValue.Split('-')[1]));
            ddMakeName.DataSource = ds;
            ddMakeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["makename"].ColumnName);
            ddMakeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["makeidmakecode"].ColumnName);
            ddMakeName.DataBind();
            ddMakeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindColName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourNameNew();
            ddColName.DataSource = ds;
            ddColName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["colourname"].ColumnName);
            ddColName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["colidcolcode"].ColumnName);
            ddColName.DataBind();
            ddColName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindModelName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehModel_accMake(Convert.ToInt32(ddMakeName.SelectedValue));
            ddModelName.DataSource = ds;
            ddModelName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["modelname"].ColumnName);
            ddModelName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["modelid"].ColumnName);
            ddModelName.DataBind();
            ddModelName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindDistrict_StateWise()
    {
        try
        {
            DataTable ds = new DataTable();
            ds = mm.getDistrictType(Session["vcStateCode1"].ToString()).Tables[0];
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    private bool SaveForm(bool blSave)
    {
        try
        {

            if (txtRegistrationno.Text == "")
            {
                Alert("Please enter the Registration No.");
                txtRegistrationno.Focus();
                return false;
            }

            if (txtChasisno.Text == "")
            {
                Alert("Please enter the Chasis No.");
                txtChasisno.Focus();
                return false;
            }

            if (txtEngineno.Text == "")
            {
                Alert("Please enter the Engine No.");
                txtEngineno.Focus();
                return false;
            }

            if (ddVehTypeName.SelectedIndex == 0)
            {
                Alert("Please select the Vehicle Type to be entered");
                ddVehTypeName.Focus();
                return false;
            }

            if (ddMakeName.SelectedIndex == 0)
            {
                Alert("Please select the Make Type to be entered.");
                ddMakeName.Focus();
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
        }
    }

    public string GetIPAddress()
    {
        string strHostName = System.Net.Dns.GetHostName();
        IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        HttpRequest currentRequest = HttpContext.Current.Request;
        String clientIP1 = currentRequest.ServerVariables["REMOTE_HOST"];
        return clientIP1;
        //return ipAddress.ToString();

    }
    private Boolean checkillegal(string regno, string enginno, string chasisno)
    {
        bool illegal;
        illegal = false;
        if (regno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (enginno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (chasisno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {

        try
        {


            string StolenDate = "";
            bool illegal = checkillegal(txtRegistrationno.Text.ToString().Trim(), txtEngineno.Text.ToString().Trim(), txtChasisno.Text.ToString().Trim());
            if (illegal == false)

            {
                bool blnIsSaved = SaveForm(false);
                if (blnIsSaved == true)
                {
                    DataSet ds = new DataSet();
                    string fir = "F";

                    string strHostName = System.Net.Dns.GetHostName();
                    string clientName = GetIPAddress().ToString();
                    string crimeno;
                    int loop = eform.getEntry_AuthorityDetailsfast(GenericMethods.check(txtRegistrationno.Text.ToString().Trim()), GenericMethods.check(txtChasisno.Text.ToString().Trim()), GenericMethods.check(txtEngineno.Text.ToString().Trim()), int.Parse(ddVehTypeName.SelectedValue.Split('-')[0]));
                    if (loop >= 1)
                    {
                        lblmsg.Text = "<font color=red>Registration No.,Engine No.,Chassis No. or Fir No one of these 4 parameter is matching with insert Data...</font>";
                        return;

                    }
                    string Act1 = "";
                    string Act2 = "";
                    string Act3 = "";
                    string Color = "";
                    if (ddColName.SelectedIndex == 0)
                    {
                        Color = "0";
                    }
                    else
                    {
                        Color = ddColName.SelectedValue.ToString().Split('-')[1];
                    }
                    string[] MakeCode = ddMakeName.SelectedValue.ToString().Split('-');
                    string ModelNo = ddModelName.SelectedValue.ToString();

                    string color;
                    if (ddColName.SelectedIndex == 0)
                    {
                        color = "N.K";
                    }
                    else
                    {
                        color = ddColName.SelectedItem.Text.ToString().Trim();
                    }
                    DateTime dtime;
                    dtime = System.DateTime.Now;
                    
                    ds = eform.AddAuthorities(((ddVehTypeName.SelectedValue.Split('-')[0])), (MakeCode[1]), ModelNo, (Color), GenericMethods.check(txtRegistrationno.Text.Trim().ToUpper()), GenericMethods.check(txtChasisno.Text.Trim().ToUpper()), GenericMethods.check(txtEngineno.Text.Trim().ToUpper()), (txtYear.Text), _userid, Session["strRandom"].ToString(), strHostName, clientName, dtime);

                    if (ds.Tables[0].Rows.Count > 0)
                    {


                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = ddVehTypeName.SelectedItem.Text.ToString().Trim();
                        if (ddMakeName.SelectedItem.Text.ToString().Trim() != "--Select--")
                        { Session["MakeDesc"] = ddMakeName.SelectedItem.Text.ToString().Trim(); }
                        else
                        { Session["MakeDesc"] = ""; }

                        Session["Registration"] = HttpUtility.HtmlDecode(txtRegistrationno.Text.ToString().Trim());
                        //Session["Registration"] = txtRegistrationno.Text.ToString().Trim();
                        Session["Chasis"] = txtChasisno.Text.ToString().Trim();
                        Session["Engine"] = txtEngineno.Text.ToString().Trim();
                        Session["Color"] = color;
                        if (txtYear.Text != "--Select--")
                        {
                            Session["Model"] = txtYear.Text.ToString();
                        }
                        else
                        {
                            Session["Model"] = "";
                        }
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                        Session["MatchStatus"] = "1";


                        Response.Write("<script>window.open('AuthorityNCRBReport.aspx','_blank')</script>");
                    }
                    else
                    {
                        Session["VehicleTypeDesc"] = ddVehTypeName.SelectedItem.Text.ToString().Trim();
                        if (ddMakeName.SelectedItem.Text.ToString().Trim() != "--Select--")
                        { Session["MakeDesc"] = ddMakeName.SelectedItem.Text.ToString().Trim(); }
                        else
                        { Session["MakeDesc"] = ""; }
                        Session["Registration"] = txtRegistrationno.Text.ToString().Trim();
                        Session["Chasis"] = txtChasisno.Text.ToString().Trim();
                        Session["Engine"] = txtEngineno.Text.ToString().Trim();
                        Session["Color"] = color;
                        if (txtYear.Text != "--Select--")
                        {
                            Session["Model"] = txtYear.Text.ToString();
                        }
                        else
                        {
                            Session["Model"] = "";
                        }
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                        Session["MatchStatus"] = "3";
                        Response.Write("<script>window.open('AuthorityNCRBReport.aspx','_blank')</script>");
                    }

                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }

    }

    private void Alert(string strErrMsg)
    {

        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";

        if (!ClientScript.IsClientScriptBlockRegistered("ScriptAlert"))
        {

            ClientScript.RegisterStartupScript(typeof(AddAuthority), "ScriptAlert", strScript);

        }

    }

    protected string convertToDateTime(string old)
    {
        string date1 = "";
        string month1 = "";
        string year1 = "";
        char[] bCharArray = old.ToCharArray();
        for (int i = 0; i < bCharArray.Length; i++)
        {
            if (i < 2)
            {
                date1 += bCharArray[i].ToString();
            }
            else if (i > 2 && i < 5)
            {
                month1 += bCharArray[i].ToString();
            }
            else if (i > 5 && i < bCharArray.Length)
            {
                year1 += bCharArray[i].ToString();
            }

        }
        string newdate = month1 + "/" + date1 + "/" + year1;
        return newdate;
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }

    protected void ddVehTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectVehicleType();
            PostCount++;
        }


    }

    protected void RbtnNewEntry_CheckedChanged(object sender, EventArgs e)
    {
        btnAdd.Visible = true;
        btnReset.Visible = true;
    }
    protected void ddModelName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtVehTypeCode_TextChanged(object sender, EventArgs e)
    {

    }
    private void afterSelectVehicleType()
    {
        if (ddVehTypeName.SelectedIndex != 0)
        {
            bindMakeName();
        }
        else
        {
            txtVehTypeCode.Text = "";

            ddMakeName.Items.Clear();
            txtMakeCode.Text = "";
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));

            ddModelName.Items.Clear();

            ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }
    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            DataSet ds = new DataSet();
            afterSelectDistrict();
            PostCount++;
        }
    }
    protected void txtDistrictCode_TextChanged(object sender, EventArgs e)
    {

    }

    private void afterSelectDistrict()
    {
    }
    public void bindDistrictName()
    {
        try
        {
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindDistrictName(string districtCode)
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    private void afterSelectState()
    {
    }
    public void bindStateName(string stateCode)
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindStateName()
    {
        try
        {
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;

    }
}
