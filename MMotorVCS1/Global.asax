﻿<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup
        Application["noofUserloggedin"] = 0;
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        Exception ex = Server.GetLastError();
        if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 500)
        {
            Response.Redirect("Error500.htm");
        }
        //if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 404)
        //{
        //    Response.Redirect("Error404.htm",false);
        //}
        //if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 403)
        //{
        //    Response.Redirect("Error403.htm");
        //}
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
        Application.Lock();
        Application["noofUserloggedin"] = Convert.ToInt32(Application["noofUserloggedin"]) + 1;
        Application.UnLock();

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        Application.Lock();
        Application["noofUserloggedin"] = Convert.ToInt32(Application["noofUserloggedin"]) - 1;
        Application.UnLock();
    }

    //void Application_BeginRequest(object sender, EventArgs e)
    //{
    //    HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN");
    //    HttpContext.Current.Response.AddHeader("x-content-type-options", "nosniff");
    //}
    
    
</script>
