﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="false" AutoEventWireup="true" CodeFile="ReportLoginlog.aspx.vb" Inherits="ReportLoginlog" Theme="emsTheme"  EnableViewState="true"  EnableViewStateMac="true"  title = "Vahan Samanvay Login Log" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--     <asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
     <Triggers> 
<asp:PostBackTrigger ControlID="Button1" /> 
<asp:PostBackTrigger ControlID="Converttoexecl" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 
    <ContentTemplate>
            <table class="contentmain">
                <tr>
                <td><table class="tablerowcolor" align="center" width="80%">
                <tr class="heading">
                    
                    <td colspan="3" >
                        <asp:Label ID="Label3" runat="server" Text="Login Log Report" Font-Bold="True"></asp:Label></td>
                   
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 92px">
                        <asp:TextBox ID="usernam" runat="server" Visible="False"></asp:TextBox>
                    </td>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label></td>
                    <td style="width: 92px">
                        <asp:TextBox ID="ListDatefrom" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender runat="server" ID="datefrom" 
                            TargetControlID="Listdatefrom" Format="dd/MM/yyyy" 
                     PopupPosition="Right"   ></ajaxtoolkit:calendarextender>
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></td>
                    <td style="width: 92px">
                        <asp:TextBox ID="listDateto" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender ID="dateto" runat="server" 
                            TargetControlID="ListDateTo" Format="dd/MM/yyyy" 
                     PopupPosition="Right"  ></ajaxtoolkit:calendarextender>
                        <asp:RadioButton ID="loginlist" runat="server" GroupName="Logging" 
                            Text="Complete Login list" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 92px">
                        &nbsp;</td>
                    <td style="width: 100px" id="UnusedLogins">
                        <asp:RadioButton ID="UnusedLogins" runat="server" GroupName="Logging" 
                            Text="Unused Logins" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 92px">
                        &nbsp;</td>
                    <td style="width: 100px">
                        <asp:RadioButton ID="currentlylogged" runat="server" GroupName="Logging" 
                            Text="Currently Logged in" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 92px">
                    </td>
                    <td style="width: 100px">
                        <asp:RadioButton ID="RadioButton1" runat="server" Checked="True" 
                            GroupName="Logging" Text="Logged Between Dates" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 92px">
                        <asp:Button ID="Button1" runat="server" Text="View" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" />
                    </td>
                    <td style="width: 100px">
                        <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 92px">
                     <asp:Label ID="Msglabel" runat="server"></asp:Label>
                    <asp:Button ID="Converttoexecl" runat="server"
                            Text="Generate to excel (.csv) format file" />
                    </td>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
            </table>
            </td> </tr> </table>
 </ContentTemplate>
           </asp:UpdatePanel>
</asp:Content>
