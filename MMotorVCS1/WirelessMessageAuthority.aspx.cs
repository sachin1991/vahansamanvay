﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;


public partial class WirelessMessageAuthority : System.Web.UI.Page
{
    public DataTable dv;
    DataSet dt;

    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {

                Response.Redirect("LogoutModule.aspx");
            }

            lblUpdated.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["vcUserName"].ToString() + " " + Session["ClientAddr"].ToString().Trim();
            lblPrinted.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            DateText.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");

            string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
            string _MakeDesc = Session["MakeDesc"].ToString();
            string _Registration = Session["Registration"].ToString();
            string _Chasis = Session["Chasis"].ToString();
            string _Engine = Session["Engine"].ToString();
            string _color = Session["Color"].ToString();
            string _model = Session["Model"].ToString();
            string _stateCode = Session["vcStateCode"].ToString();
            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;
            lblModel.Text = _model;
            lblColor.Text = _color;
            captcha.Text = Session["strRandom"].ToString() + " " + Session["visitor"].ToString();
            string _username = Session["vcusername"].ToString();

            dt = eform.getCounterHeader1(_username);

            if (dt.Tables[0].Rows.Count > 0)
            {
                lblFile.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcFileNumber"].ToString().Trim());
                AuthorityOffice.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcHeader"].ToString().Trim()) + " " + dt.Tables[0].Rows[0]["vcAddress"].ToString().Trim();
                lblFooter.Text = GenericMethods.check(dt.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
            }
            if (Session["MatchStatus"].ToString() == "1")
            {
                lblMsg.Text = " The vehicle has been linked with";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();

                    rptAddress.DataSource = dv;
                    rptAddress.DataBind();
                    toaddress.Visible = true;
                }
                else
                {
                    toaddress.Visible = false;
                    lblMsg.Text = " has not been reported as stolen by police.";
                }

            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                toaddress.Visible = false;
                lblMsg.Text = " is not recovered yet.";
            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                toaddress.Visible = false;
                lblMsg.Text = " is not linked, Clear Case.";
            }

            else
            {
                toaddress.Visible = false;
                lblMsg.Text = " is not found in the database.";
            }
        }
        catch
        {
        }
    }
    public string printsho()
    {
        string strsho ="SHO";
        return strsho;
    }

}