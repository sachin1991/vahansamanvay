<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Colour_Name.aspx.cs" Inherits="Colour_Name" Title="Vahan Samanvay Master Colour Name" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script language="javascript" type="text/javascript">
    function valid()
    {        
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtColCode').value == "")
    {
        alert("Enter Colour Code");
        document.getElementById('ctl00_ContentPlaceHolder1_txtColCode').focus();
        return false;
    }    
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtColName').value == "")
    {
        alert("Enter Colour Name");
        document.getElementById('ctl00_ContentPlaceHolder1_txtColName').focus();
        return false;
    }  
    return true;
    }
    </script>


    <table class="contentmain"> 
<tr> <td > 
    <table align="center" width="80%" class="tablerowcolor">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" class="heading">
                Colour of Vehicle</td>
        </tr>
        <tr>
            <td >
                Colour Code</td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txtColCode" runat="server" Width="148px"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Colour Name</td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txtColName" runat="server" Width="148px"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
            </td>
            <td >
            </td>
            <td >
            </td>
        </tr>
        <tr>
        
            <td colspan="3" align="center">
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" Width="76px" CssClass="button" /><asp:Button
                    ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="71px" CssClass="button" /><asp:Button
                        ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" Width="76px" CssClass="button" /></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 15px">
                <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#000000"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <h4>Vehicle Colour Details</h4></td>
        </tr>
        <tr>
            <td colspan="3">             
                <asp:GridView ID="gvColName" runat="server" AutoGenerateColumns="False" 
                    Width="97%" OnRowCommand="gvColName_RowCommand" AllowPaging="True" 
                    PageSize="15" OnPageIndexChanging="gvColName_PageIndexChanging" 
                    OnRowCreated="gvColName_RowCreated" CssClass="grid" BackColor="White" 
                    BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                    EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
                    <Columns>
                        <asp:TemplateField HeaderText="S No.">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                    Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="colourid" HeaderText="Colour Id" SortExpression="Colourid" />
                        <asp:BoundField DataField="colourcode" HeaderText="Colour code" SortExpression="colour code" />
                        <asp:BoundField DataField="colourname" HeaderText="Colour Name" SortExpression="Colour name" >
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="gridfooter" BackColor="#CCCC99" />                    
                    <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />                    
                    <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                        ForeColor="White" />
                    <PagerStyle CssClass="gridpager" ForeColor="Black" BackColor="#F7F7DE" 
                        HorizontalAlign="Right" />
                    <HeaderStyle CssClass="gridheader" BackColor="#6B696B" Font-Bold="True" 
                        ForeColor="White" />
                    <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                        PreviousPageText="Previous" />
                </asp:GridView>                
            </td>
        </tr>
        <tr>
            <td colspan="3">
                </td>
        </tr>
    </table>
    </td> </tr> </table>
</asp:Content>

