<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="PoliceStation_Type.aspx.cs" Theme="emsTheme" Inherits="PoliceStation_Type"
Title="Vahan Samanvay Master Police Station"  EnableViewState="true"  EnableViewStateMac="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
    <table class="contentmain">
    <tr> <td> 
    <table class="tablerowcolor" align="center" width="80%">
                 <tr>
                <td colspan="4">
                    <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="4" class="heading">
                   
                        Police Station Type
                </td>
            </tr>
            <tr>
                <td colspan="4">
                </td>
            </tr>
            <tr>
                <td style="width: 100px; height: 26px;">
                    Select State
                </td>
                <td style="width: 77px; height: 26px;">
                    <asp:TextBox ID="txtStateCode" runat="server" Width="64px" AutoPostBack="True" MaxLength="6"
                        OnTextChanged="txtStateCode_TextChanged"></asp:TextBox></td>
                <td colspan="2" style="height: 26px">
                    <asp:DropDownList ID="ddStateCode" runat="server" AutoPostBack="True" Width="300px"
                        OnSelectedIndexChanged="ddStateCode_SelectedIndexChanged">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 168px; height: 26px">
                    District Code</td>
                <td style="width: 69px; height: 26px">
                    <asp:TextBox ID="txtdisCode" runat="server" Width="64px" Enabled="False"></asp:TextBox></td>
                <td style="height: 26px" colspan="2">
                    <asp:DropDownList ID="ddDisCode" runat="server" Width="300px" OnSelectedIndexChanged="ddDisCode_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="width: 168px">
                    Police St. Code</td>
                <td style="width: 69px">
                </td>
                <td style="width: 167px">
                    <asp:TextBox ID="txtPSCode" runat="server" Width="148px"></asp:TextBox></td>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="txtPSCode"></cc1:FilteredTextBoxExtender>
                <td style="width: 383px">
                    <asp:CheckBox ID="psbyname" runat="server" 
                        oncheckedchanged="psbyname_CheckedChanged" Text="PS by name" 
                        AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td style="width: 168px">
                    Police St. Name</td>
                <td style="width: 69px">
                </td>
                <td style="width: 167px">
                    <asp:TextBox ID="txtPSName" runat="server" Width="148px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtPSName"></cc1:FilteredTextBoxExtender>
                    
                    </td>
                <td style="width: 383px">
                </td>
            </tr>
            <tr>
                <td style="width: 168px">
                </td>
                <td colspan="3">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="button" Width="76px"
                        Text="Add"></asp:Button><asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server"
                            CssClass="button" Width="71px" Text="Update"></asp:Button><asp:Button ID="btnReset"
                                OnClick="btnReset_Click" runat="server" CssClass="button" Width="76px" Text="Reset">
                            </asp:Button></td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="lblmsg" runat="server" ForeColor="#000000" Font-Size="Larger" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="4">
                    <h4>
                        Police Station Details</h4>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:GridView ID="gvPSType" runat="server" CssClass="gridText" Width="97%"
                        OnPageIndexChanging="gvPSType_PageIndexChanging" AllowPaging="False" OnRowCreated="gvPSType_RowCreated"
                        OnRowCommand="gvPSType_RowCommand" AutoGenerateColumns="False" 
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="4" EnableModelValidation="True" ForeColor="Black" 
                        GridLines="Vertical">
                        <Columns>
                            <asp:TemplateField HeaderText="S No.">
                                <ItemTemplate>
                                    <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                        Text='<%# Container.DataItemIndex +1  %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Districtid" HeaderText="DistrictId " SortExpression="Districtid" />
                            <asp:BoundField DataField="districtcode" HeaderText="District&#160;Code" SortExpression="districtcode" />
                            <asp:BoundField DataField="Districtname" HeaderText="District&#160;Name" SortExpression="Districtname">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="psid" HeaderText="PSId" SortExpression="psid" />
                            <asp:BoundField DataField="PScode" HeaderText="Station code" SortExpression="PScode" />
                            <asp:BoundField DataField="PSname" HeaderText="Station Name" SortExpression="PSname">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />
                        <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                        <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                            ForeColor="White" />
                        <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                            HorizontalAlign="Right" />
                        <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                            ForeColor="White" />
                        <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                        <PagerSettings PreviousPageText="Previous" />
                    </asp:GridView>
                </td>
            </tr>
             </table>
        </td></tr>

       
    </table>

    <script language="javascript" type="text/javascript">
    function valid()
    {     
    if(document.getElementById("<%=ddDisCode.ClientID%>").selectedIndex==0)
    {
        alert("Select District Name");  
        document.getElementById("<%=ddDisCode.ClientID %>").focus();       
        return false;
    }    
    if(document.getElementById("<%=txtPSCode.ClientID%>").value == "")
    {
        alert("Enter Police Station Code");
        document.getElementById("<%=txtPSCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtPSName.ClientID%>").value == "")
    {
        alert("Enter Police Station Name");
        document.getElementById("<%=txtPSName.ClientID%>").focus();
        return false;
    }  
    return true;
    }
    </script>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
