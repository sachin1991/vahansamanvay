using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class proles : System.Web.UI.Page
{
    MenuClass mObj = new MenuClass();
    static string _userid;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
           
        }
        if (!IsPostBack)
        {
            try
            {
          
            }
            catch
            {
                Response.Redirect("error.aspx");
            }
            bindGrid();
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    private void extractActive()
    {
        foreach (GridViewRow gRow in gvRoles.Rows)
        {
            CheckBox chkActive = (CheckBox)gRow.FindControl("chkActive");
            if (Convert.ToBoolean(chkActive.Text) == true)
            {
                chkActive.Checked = true;
            }
            else
            {
                chkActive.Checked = false;
            }
            chkActive.Text = "";
        }
    }
    private void bindGrid()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mObj.getPRoles();
            gvRoles.DataSource = ds;
            gvRoles.DataBind();
            extractActive();
            if (ds.Tables[0].Rows.Count == 0)
            {
                DataRow drow = ds.Tables[0].NewRow();
                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    drow[i] = DBNull.Value;
                }
                ds.Tables[0].Rows.Add(drow);
                gvRoles.DataSource = ds;
                gvRoles.DataBind();
                gvRoles.Rows[0].Visible = false;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvRoles_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvRoles.EditIndex = -1;
        bindGrid();
    }
    private Boolean checkillegal(string rolename,string shortrolename, string usertype)
    {
        bool illegal;
        illegal = false;
        if (rolename.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (shortrolename.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (usertype.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void gvRoles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.Equals("Add"))
            {
                ((ImageButton)gvRoles.FooterRow.FindControl("lbInsert")).Visible = true;
                ((ImageButton)gvRoles.FooterRow.FindControl("lbCancel")).Visible = true;
                ((Button)gvRoles.FooterRow.FindControl("lbAdd")).Visible = false;
                ((TextBox)gvRoles.FooterRow.FindControl("txtRolesName1")).Visible = true;
                ((CheckBox)gvRoles.FooterRow.FindControl("chkActive1")).Visible = true;
                ((TextBox)gvRoles.FooterRow.FindControl("txtShortRoleName1")).Visible = true;
                ((TextBox)gvRoles.FooterRow.FindControl("txtUserType1")).Visible = true;    
                return;
            }
            else
            {
                ((ImageButton)gvRoles.FooterRow.FindControl("lbInsert")).Visible = false;
                ((ImageButton)gvRoles.FooterRow.FindControl("lbCancel")).Visible = false;
                ((Button)gvRoles.FooterRow.FindControl("lbAdd")).Visible = true;
                ((TextBox)gvRoles.FooterRow.FindControl("txtRolesName1")).Visible = false;
                ((CheckBox)gvRoles.FooterRow.FindControl("chkActive1")).Visible = false;
                ((TextBox)gvRoles.FooterRow.FindControl("txtShortRoleName1")).Visible = false;
                ((TextBox)gvRoles.FooterRow.FindControl("txtUserType1")).Visible = false;    
            }
            if (e.CommandName.Equals("CancelInsert"))
            {
                gvRoles.EditIndex = -1;
                bindGrid();
                return;
            }
            if (e.CommandName.Equals("Insert"))
            {
                int _lblRoleId = mObj.getPRoleMaxId();
                TextBox _txtRoleName = (TextBox)gvRoles.FooterRow.FindControl("txtRolesName1");
                CheckBox _chkActive = (CheckBox)gvRoles.FooterRow.FindControl("chkActive1");
                TextBox _ShorttxtRoleName = (TextBox)gvRoles.FooterRow.FindControl("txtShortRoleName1");
                TextBox _UserType =(TextBox)gvRoles.FooterRow.FindControl("txtUserType1");
                bool illegal = checkillegal(_txtRoleName.Text, _ShorttxtRoleName.Text,_UserType.Text );
                if (illegal == false)
                {
                    if (_txtRoleName.Text.Trim() == "")
                    {
                        lblmsg.Text = "<font color=red>Please write role name.</font>";
                        return;
                    }
                    int _active = 0;
                    if (Convert.ToBoolean(_chkActive.Checked) == true)
                    {
                        _active = 1;
                    }
                    if (mObj.addPRoles(_lblRoleId, GenericMethods.check(_ShorttxtRoleName.Text.Trim().ToUpper()), GenericMethods.check(_txtRoleName.Text.Trim().ToUpper()), _active, GenericMethods.check(_UserType.Text.Trim().ToUpper())) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values Inserted</font>";
                        bindGrid();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not Inserted</font>";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                }
                return;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvRoles_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[1].Visible = false;
    }
    protected void gvRoles_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvRoles.EditIndex = e.NewEditIndex;
        bindGrid();
    }
    protected void gvRoles_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label _lblRoleId = (Label)gvRoles.Rows[e.RowIndex].FindControl("lblRoleId");
            Label _lblLoginId = (Label)gvRoles.Rows[e.RowIndex].FindControl("inLoginId");
            TextBox _txtRoleName = (TextBox)gvRoles.Rows[e.RowIndex].FindControl("txtRoleName");
            TextBox _ShorttxtRoleName = (TextBox)gvRoles.Rows[e.RowIndex].FindControl("txtShortRoleName");
            CheckBox _chkActive = (CheckBox)gvRoles.Rows[e.RowIndex].FindControl("chkActive");
            TextBox _UserType = (TextBox)gvRoles.Rows[e.RowIndex].FindControl("txtUserType");
             bool illegal = checkillegal(_txtRoleName.Text, _ShorttxtRoleName.Text,_UserType.Text);
             if (illegal == false)
             {
                 if (_txtRoleName.Text.Trim() == "")
                 {
                     lblmsg.Text = "<font color=red>Please write role name.</font>";
                     return;
                 }
                 int _active = 0;
                 if (_chkActive.Checked == true)
                 {
                     _active = 1;
                 }
                 if (mObj.updatePRoles(int.Parse(_lblRoleId.Text), GenericMethods.check(_ShorttxtRoleName.Text.Trim().ToUpper()), GenericMethods.check(_txtRoleName.Text.Trim().ToUpper()), _active, GenericMethods.check(_UserType.Text.Trim().ToUpper()), Convert.ToInt32(_lblLoginId.Text.ToString())) > 0)
                 {
                     lblmsg.Text = "<font color=green>Values Updated</font>";
                     gvRoles.EditIndex = -1;
                     bindGrid();
                 }
                 else
                 {
                     lblmsg.Text = "<font color=red>Values not Updated</font>";
                 }
             }
             else
             {
                 ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
             }

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
}
