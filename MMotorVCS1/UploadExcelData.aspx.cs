﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using System.Windows.Forms;
using System.Data;

public partial class UploadExcelData : System.Web.UI.Page
{
    private string saveLocation;
    public string strSql;
    int i;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1": //admin
                break;
            case "8": //ncrb
                break;
            case "10": //Crime Records
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }

   

    protected void Button1_Click1(object sender, EventArgs e)
    
     {
        if (FileUpload1.HasFile)
        {
            string tempFileName = FileUpload1.FileName;
            string tempFileLocation = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FileLocation"]);
            saveLocation = Path.Combine(tempFileLocation, tempFileName);
 
            FileUpload1.SaveAs(saveLocation);
            Boolean wf;
            wf = false;
            if (File.Exists(saveLocation))
            {
                string strExcelConnection = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0;", saveLocation);
                try
                {
                    using (OleDbConnection conncetion= new OleDbConnection(strExcelConnection))
                    {
                        OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", conncetion);
                        conncetion.Open();

                        using (OleDbDataReader dr = command.ExecuteReader())
                        {
                            string sqlConncetionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                            
                           

                           
                           string RegistrationNumber = "";
                           string ChassisNumber = "";
                           string EngineNumber = "";
                           string InsurerCode = "";
                           string Stolen_Date = "";
                           string StolenPlace = "";
                           string Intimation_Date = "";
                           string RTALocation = "";
                           string VehicleAge = "";
                           string ClassCode = "";
                           string MakeCode = "";
                           string ZoneCode = "";
                           string Policy_Year = "";
                           string PolicyNumber = "";
                           
                            while (dr.Read())
                            {
                                RegistrationNumber = valid(dr, 0);
                                ChassisNumber = valid(dr, 1);
                                EngineNumber = valid(dr, 2);
                                InsurerCode = valid(dr, 3);
                                Stolen_Date = valid(dr, 4);
                                StolenPlace = valid(dr, 5);
                                Intimation_Date = valid(dr, 6);
                                RTALocation = valid(dr, 7);
                                VehicleAge = valid(dr, 8);
                                ClassCode = valid(dr, 9);
                                
                                MakeCode = valid(dr, 10);
                               
                                ZoneCode = valid(dr, 11);
                                Policy_Year = valid(dr, 12);
                                PolicyNumber = valid(dr, 13);
                                i = i + 1;
                                //Here using this method we are inserting the data into the database
                                try
                                {
                                    insertdataintosql(RegistrationNumber.Trim(), ChassisNumber.Trim(), EngineNumber.Trim(), InsurerCode.Trim(), Stolen_Date.Trim(), StolenPlace.Trim(), Intimation_Date.Trim(), RTALocation.Trim(), VehicleAge.Trim(), ClassCode.Trim(), MakeCode.Trim(), ZoneCode.Trim(), Policy_Year.Trim(), PolicyNumber.Trim());
                                    wf = true;
                                }
                                catch
                                {
                                    MessageBox.Show("wrong file format picked!hence not imported");
                                    wf = false;
                                    break;
                                }
                                
                            }

                        }
                        conncetion.Close();
                        if (wf)
                        {
                            Label2.Style.Add("Color", "00ff00");
                            Label2.Text = "Records Imported " + i.ToString();
                            //MessageBox.Show("Records Imported " + i.ToString());
                            //Response.Redirect(Request.RawUrl);
                        }

                    }
                }
                catch (Exception ex)
                {
                    
                    {
                        if (!wf)
                        {
                            Label2.Style.Add("Color", "ff0000");
                            Label2.Text = "Wrong file format picked! Not imported. " ;
                            //MessageBox.Show("Wrong file format picked! Not imported. ");
                        }
                        else
                        {

                            //MessageBox.Show("Records Imported " + i.ToString());
                        }
                        //Response.Redirect(Request.RawUrl);
                    }
                }
            }
        }
    }

    //This valid method is mainly used to check where the null values are 
    //contained in the Excel Sheet and replacing them with zero
    protected string valid(OleDbDataReader myreader, int stval)//if any columns are 
    //found null then they are replaced by zero
    {
        object val = myreader[stval];
        if (val != DBNull.Value)
            return val.ToString();
        else
            return Convert.ToString(0);
    }

    public void insertdataintosql( string RegistrationNumber,string ChassisNumber , string EngineNumber ,  string InsurerCode ,string Stolen_Date ,string StolenPlace,string Intimation_Date ,string RTALocation ,string VehicleAge ,string ClassCode, string MakeCode,string ZoneCode ,string Policy_Year,string PolicyNumber )
     {
         try
         {
             //inserting data into the Sql Server
             string sqlConncetionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
             SqlConnection conn = new SqlConnection(sqlConncetionString);
             SqlCommand cmd = new SqlCommand("usp_ImportIRDAData", conn);
             cmd.Connection = conn;
             cmd.CommandType = CommandType.StoredProcedure;
             cmd.Parameters.Add("@RegistrationNumber", SqlDbType.NVarChar).Value = RegistrationNumber;
             cmd.Parameters.Add("@ChassisNumber", SqlDbType.NVarChar).Value = ChassisNumber;
             cmd.Parameters.Add("@EngineNumber", SqlDbType.NVarChar).Value = EngineNumber;
             cmd.Parameters.Add("@InsurerCode", SqlDbType.NVarChar).Value = (InsurerCode);
             cmd.Parameters.Add("@Stolen_Date", SqlDbType.NVarChar).Value = Stolen_Date;
             cmd.Parameters.Add("@StolenPlace", SqlDbType.NVarChar).Value = StolenPlace;
             cmd.Parameters.Add("@Intimation_Date", SqlDbType.NVarChar).Value = Intimation_Date;
             cmd.Parameters.Add("@RTALocation", SqlDbType.NVarChar).Value = RTALocation;
             cmd.Parameters.Add("@VehicleAge", SqlDbType.NVarChar).Value = VehicleAge;
             cmd.Parameters.Add("@ClassDesc", SqlDbType.NVarChar).Value = (ClassCode);
             cmd.Parameters.Add("@MakeDesc", SqlDbType.NVarChar).Value = MakeCode;
             cmd.Parameters.Add("@ZoneCode", SqlDbType.NVarChar).Value = ZoneCode;
             cmd.Parameters.Add("@Policy_Year", SqlDbType.NVarChar).Value = Policy_Year;
             cmd.Parameters.Add("@PolicyNumber", SqlDbType.NVarChar).Value = (PolicyNumber);
             conn.Open();
             cmd.ExecuteNonQuery();
             conn.Close();
         }
         catch
         { 

         }
        
            
     }
}