﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Theme="emsTheme" CodeFile="ClearCase.aspx.cs" Inherits="ClearCase" Title="Vahan Samanvay Police Enquiry"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <Triggers> 
<asp:PostBackTrigger ControlID="btnSearch" /> 
<asp:PostBackTrigger ControlID="btnrefresh"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="btnrefresh"></asp:PostBackTrigger>
</Triggers> 
     <Triggers> 
<asp:PostBackTrigger ControlID="btnrefresh" /> 
</Triggers> 
   <ContentTemplate>
 
  <table class="contentmain">
 <tr> <td>
 
    <table id="Table1" width="80%" align="center" border="0" cellpadding="0" cellspacing="0" class="tablerowcolor" runat="server">
            <tr runat="server" visible ="false"> <td colspan="3" > <asp:Image ID="Image1" runat="server" AlternateText="NCRB CCTNS Image" ImageUrl="img/h1.png" Width="100%"/>
               </td> </tr>
            
            
            <tr class="heading"><td colspan="3">Clear Case Vehicle Enquiry</td></tr>
             <tr>
                 <td >
                     <asp:Label ID="lblType" runat="server" Text="Type" 
                         style="color: #800000; font-weight: 700" ></asp:Label>
                 </td>
                 <td >
                <asp:DropDownList id="ddVehTypeName" runat="server" Width="300px" TabIndex="9" AutoPostBack="True" OnSelectedIndexChanged="ddVehTypeName_SelectedIndexChanged"></asp:DropDownList>
                 </td>
                     <td>
                 <asp:RequiredFieldValidator ID="rfvVehicleType" runat="server" 
               ErrorMessage="Please select your Vehicle Type"
                   ForeColor="Red" ControlToValidate="ddVehTypeName" 
                   InitialValue="--Select--" Display="Dynamic"></asp:RequiredFieldValidator>
                 </td>
             </tr>
       
             <tr>
                 <td >
                     <asp:Label ID="lblMake" runat="server" Text="Make"  ></asp:Label>
                 </td>
                 <td >
                    <asp:DropDownList ID="ddMakeName" runat="server"  Width="300px" TabIndex="10">
                    </asp:DropDownList>
                 </td>
                     <td ></td>
             </tr>
       
             <tr>
                 <td>
                     <asp:Label ID="lblColor" runat="server" Text="Color"  ></asp:Label>
                 </td>
                 <td>
                   <asp:DropDownList id="ddColName" TabIndex="11" runat="server" Width="300px" ></asp:DropDownList></td>
                     <td></td>
             </tr>
       
             <tr>
                 <td  >
                     <asp:Label ID="lblRegistration" runat="server" Text="Registration" class="mandatory" ></asp:Label>
                 </td>
                 <td >
                    <asp:TextBox ID="txtRegistration" runat="server" MaxLength="40" Width="300px" TabIndex="12"></asp:TextBox>
                   </td>
                   <td>
                     
                 </td>
             </tr>
       
             <tr>
                 <td>
                     <asp:Label ID="lblChasis" runat="server" Text="Chasis" class="mandatory" ></asp:Label>
                 </td>
                 <td>
                    <asp:TextBox ID="txtChasis" runat="server" MaxLength="40" Width="300px" TabIndex="13"></asp:TextBox>
                    </td>
                   <td>
                

                 </td>
                     
             </tr>
       
             <tr>
                 <td >
                     <asp:Label ID="lblEngine" runat="server" Text="Engine" class="mandatory" ></asp:Label>
                 </td>
                 <td>
                    <asp:TextBox ID="txtEngine" runat="server" MaxLength="40" Width="300px" TabIndex="14"></asp:TextBox>
                    </td>
                   <td>

                    <asp:CustomValidator id="CustomValidator1" runat="server" 
      OnServerValidate="TextValidate" 
      Display="Dynamic"
      ErrorMessage="One of the Registration, Chasis or Engine must have valid input.">
    </asp:CustomValidator>
                     
                 </td>
                     
             </tr>

       
             <tr>
                 <td>
                 <asp:Label ID="lblYear" runat="server" Text="Year of Manufacture"  ></asp:Label>
                     </td>
                 <td>
                     <asp:TextBox ID="txtYear" runat="server" Width="300px" TabIndex="15" ></asp:TextBox>
                 </td>
                     <td></td>
                
             </tr>
       
             <tr>
                 <td >
                     <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
                 </td>
                 <td class="style15" >
                     <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click2" 
                         TabIndex="17" Text="Search" />
                     <asp:Button ID="btnrefresh" runat="server" onclick="btnrefresh_Click" 
                         TabIndex="18" Text="Refresh" />
                 </td>
                     <td >
                         &nbsp;</td>
                
             </tr>
             <tr>
             <td>
             
              <cc1:FilteredTextBoxExtender ID="fetxtRegistrationno" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtRegistration">
                    </cc1:FilteredTextBoxExtender>
                   <cc1:FilteredTextBoxExtender ID="fetxtChasisno" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtChasis">
                    </cc1:FilteredTextBoxExtender>
                    <cc1:FilteredTextBoxExtender ID="fetxtEngineno" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtEngine">
                    </cc1:FilteredTextBoxExtender>
              
             </td>
             </tr>
       
         </table>

    </td> </tr></table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

