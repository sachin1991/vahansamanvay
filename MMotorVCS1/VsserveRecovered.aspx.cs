﻿using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Text;

    public partial class VSserveRecovered : System.Web.UI.Page
    {
        public DataTable dv;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                searchrecovered();
            }
            catch (Exception ex)
            {
            }
        }
        public void searchrecovered()
        {
            try
            {
                string nameofapplicant;
                string vehicletype;
                string RegistrationNo;
                string Chasisno;
                string Engineno;
                string apiKey;
                string API_KEY = "C28J68z3A9IKCP4KGJb1M9QvYBgVuHy6";
                nameofapplicant = Request.QueryString.Get("nameofapplicant");
                vehicletype = Request.QueryString.Get("vehicletype");
                RegistrationNo = Request.QueryString.Get("RegistrationNo");
                Chasisno = Request.QueryString.Get("Chasisno");
                Engineno = Request.QueryString.Get("Engineno");
                apiKey = Request.QueryString.Get("apikey");
                apiKey = "C28J68z3A9IKCP4KGJb1M9QvYBgVuHy6";
                if (apiKey != null)
                {
                    if (apiKey.ToString() == API_KEY.ToString())
                    {
                        nameofapplicant = "ramu";
                        vehicletype = "300";
                        //RegistrationNo = "";
                        RegistrationNo = "6157";
                        //Chasisno = "MBLHA10ADBHD05954";
                       Chasisno = "";
                        Engineno = "";
                        if (RegistrationNo == "")
                        {
                            RegistrationNo = "@";
                        }
                        if (Chasisno == "")
                        {
                            Chasisno = "@";
                        }
                        if (Engineno == "")
                        {
                            Engineno = "@";
                        }

                        DataSet ds = new DataSet();
                        DataTable dt1 = new DataTable();
                        //Create a connection string.
                        using (SqlConnection conn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString()))
                        {
                            SqlCommand cmd = new SqlCommand("usp_Ins_Mobile_Details_Recover", conn);
                            //Write Command Type as here it is in form of stored procedure 
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@vcStateCode", "");
                            cmd.Parameters.AddWithValue("@vcDistCode", "");
                            cmd.Parameters.AddWithValue("@vcPSCode", "");
                            cmd.Parameters.AddWithValue("@vcApplicantName", nameofapplicant);
                            cmd.Parameters.AddWithValue("@vcMobile", "");
                            cmd.Parameters.AddWithValue("@vcEmail", "");
                            cmd.Parameters.AddWithValue("@vcAddr", "");
                            cmd.Parameters.AddWithValue("@vcPin", "");
                            cmd.Parameters.AddWithValue("@vcVehicleType", vehicletype);
                            cmd.Parameters.AddWithValue("@vcVehicleMake", "@");
                            cmd.Parameters.AddWithValue("@vcYear", "");
                            cmd.Parameters.AddWithValue("@vcColor", "");
                            cmd.Parameters.AddWithValue("@vcRegistration", RegistrationNo);
                            cmd.Parameters.AddWithValue("@vcChasis", Chasisno);
                            cmd.Parameters.AddWithValue("@vcEngine", Engineno);
                            cmd.Parameters.AddWithValue("@vcSource", "");
                            cmd.Parameters.AddWithValue("@vcHostAddr", "");
                            cmd.Parameters.AddWithValue("@vcClientAddr", "");
                            cmd.Parameters.AddWithValue("@vcTypeOfQuery", "");
                            //Create SQL data adapter
                            SqlDataAdapter da = new SqlDataAdapter();
                            da.SelectCommand = cmd;
                            //Fill dataset
                            DataTable rr;
                            da.Fill(dt1);
                            Session["Data"] = dt1;
                            dv = (DataTable)Session["Data"];
                            //rr = findowner();
                            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row;
                            foreach (DataRow dr in dt1.Rows)
                            {
                                row = new Dictionary<string, object>();
                                foreach (DataColumn col in dt1.Columns)
                                {
                                    row.Add(col.ColumnName, dr[col]);
                                }
                                //foreach (DataRow dr1 in rr.Rows)
                                //{
                                //    //row = new Dictionary<string, object>();
                                //    foreach (DataColumn col in rr.Columns)
                                //    {
                                //        row.Add(col.ColumnName, dr1[col]);
                                //    }
                                //}
                                rows.Add(row);
                            }
                            Response.ContentType = "application/json";
                            Response.Write(serializer.Serialize(rows));
                            Response.End();
                        }
                    }
                }
            }
            catch
            {
            }
        }

        public DataTable findowner()
        {
            try
            {
                VahanWSRef.VahanInfo serv = new VahanWSRef.VahanInfo();
                String resp;
                String regis, chas, engi;
                regis = dv.Rows[0]["registration"].ToString();
                chas = dv.Rows[0]["chasis"].ToString();
                engi = dv.Rows[0]["engine"].ToString();
                String response = serv.getDetails("DLNCRB", regis);
                response = accesswebservice.decrypt(response, "D#WsLD@nCRb");
                resp = response;
                DataTable ds = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter();
                ds = ConvertXMLToDataSet(resp);
                return ds;
            }
            catch
            {
                return null;
            }
        }

        public DataTable ConvertXMLToDataSet(string xmlData)
        {

            StringReader theReader = new StringReader(xmlData);
            try
            {
                DataSet theDataSet = new DataSet();
                theDataSet.ReadXml(theReader);
                return theDataSet.Tables[0];
            }
            catch
            {
                return null;
            }
            finally
            {
                if (theReader != null) theReader.Close();
            }
        }
    }