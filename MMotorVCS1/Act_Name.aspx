<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Act_Name.aspx.cs" Inherits="Act_Name" Title="Vahan Samanvay Master Act Name" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<script language="javascript" type="text/javascript">
    function valid()
    {        
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtActCode').value == "")
    {
        alert("Enter Act Code");
        document.getElementById('ctl00_ContentPlaceHolder1_txtActCode').focus();
        return false;
    }    
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtActName').value == "")
    {
        alert("Enter Act Name");
        document.getElementById('ctl00_ContentPlaceHolder1_txtActName').focus();
        return false;
    }  
    return true;
    }
    </script>

    <asp:UpdatePanel id="UpdatePanel1" runat="server">
    </asp:UpdatePanel>


    <table class="contentmain"> 
<tr > <td > 
    <table align="center" width="80%" class="tablerowcolor">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" class="heading" >
               
                    Act Name List 
            </td>
        </tr>
        <tr> <td colspan="3"></td></tr>
        <tr style="color: #cc0000">
            <td >
                Act Code</td>
            <td>
                <asp:TextBox ID="txtActCode" runat="server" Width="148px"></asp:TextBox></td>
            <td >
            </td>
        </tr>
        <tr>
            <td >
                Act Name</td>
            <td >
                <asp:TextBox ID="txtActName" runat="server" Width="148px"></asp:TextBox></td>
            <td >
            </td>
        </tr>
        <tr>
            <td >
            </td>
            <td >
            </td>
            <td >
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add"
                    Width="76px" /><asp:Button ID="btnUpdate" runat="server" CssClass="button" OnClick="btnUpdate_Click"
                        Text="Update" Width="71px" /><asp:Button ID="btnReset" runat="server" CssClass="button"
                            OnClick="btnReset_Click" Text="Reset" Width="76px" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#000000"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <h4>
                    Act Details</h4>
            </td>
        </tr>
        <tr>
            <td colspan="3">                
                    <asp:GridView ID="gvActName" runat="server" AutoGenerateColumns="False" 
                        CssClass="grid" OnRowCommand="gvActName_RowCommand"
                        Width="97%" AllowPaging="True" PageSize="15" 
                        OnPageIndexChanging="gvActName_PageIndexChanging" 
                        OnRowCreated="gvActName_RowCreated" BackColor="White" BorderColor="#DEDFDE" 
                        BorderStyle="None" BorderWidth="1px" CellPadding="4" 
                        EnableModelValidation="True" ForeColor="Black" GridLines="Vertical">
                        <Columns>
                            <asp:TemplateField HeaderText="S No.">
                                <ItemTemplate>
                                    <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                        Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Actid" HeaderText="Act Id " SortExpression="Actid" />
                            <asp:BoundField DataField="Actcode" HeaderText="Act Code" SortExpression="Actcode" />
                            <asp:BoundField DataField="Actname" HeaderText="Act Name" SortExpression="Actname" />
                        </Columns>
                        <FooterStyle CssClass="gridfooter" BackColor="#CCCC99" />
                        <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                        <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                            ForeColor="White" />
                        <PagerStyle CssClass="gridpager" ForeColor="Black" BackColor="#F7F7DE" 
                            HorizontalAlign="Right" />
                        <HeaderStyle CssClass="gridheader" BackColor="#6B696B" Font-Bold="True" 
                            ForeColor="White" />
                        <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                            PreviousPageText="Previous" />
                    </asp:GridView>                
            </td>
        </tr>
        <tr>
            <td colspan="3">
                </td>
        </tr>
    </table>
    </td>
    </tr>
    </table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

