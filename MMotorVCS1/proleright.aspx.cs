using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class proleright : System.Web.UI.Page
{
    MenuClass mObj = new MenuClass();
    static string _userid;
   
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }
        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
           
        }
        if (!IsPostBack)
        {
            try
            {
         
            }
            catch
            {
                Response.Redirect("error.aspx");
            }
            bindRole();// 
            bindRootFormGrid(); 
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    private void bindRole()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mObj.getPActiveRoles();
            ddlRole.DataSource = ds;
            ddlRole.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vcFullDesc"].ColumnName);
            ddlRole.DataValueField = GenericMethods.check(ds.Tables[0].Columns["inRoleId"].ColumnName);
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    private void extractActive(GridView gv, DataSet ds)
    {
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            string _frmId = GenericMethods.check(ds.Tables[0].Rows[i]["inFormId"].ToString());
            string _rightId = GenericMethods.check(ds.Tables[0].Rows[i]["inRoleId"].ToString());
            string _view = GenericMethods.check(ds.Tables[0].Rows[i]["boEnabled"].ToString());
            string _add = GenericMethods.check(ds.Tables[0].Rows[i]["boEnabled"].ToString());
            string _edit = GenericMethods.check(ds.Tables[0].Rows[i]["boEnabled"].ToString());
            string _delete = GenericMethods.check(ds.Tables[0].Rows[i]["boEnabled"].ToString());
            string _attach = GenericMethods.check(ds.Tables[0].Rows[i]["boEnabled"].ToString());
            foreach (GridViewRow gRow in gv.Rows)
            {
                Label _lblFrmId = (Label)gRow.FindControl("lblFormId");
                if (_lblFrmId.Text == _frmId)
                {
                    ((Label)gRow.FindControl("lblRightsId")).Text = _rightId;

                    if (Convert.ToBoolean(_attach) == true)
                    {
                        ((CheckBox)gRow.FindControl("chkFrmAtch")).Checked = true;
                        ((CheckBox)gRow.FindControl("chkFrmDelete")).Checked = true;
                        ((CheckBox)gRow.FindControl("chkFrmEdit")).Checked = true;
                        ((CheckBox)gRow.FindControl("chkFrmAdd")).Checked = true;
                        ((CheckBox)gRow.FindControl("chkFrmView")).Checked = true;
                    }
                    else
                    {
                        ((CheckBox)gRow.FindControl("chkFrmAtch")).Checked = false;
                        ((CheckBox)gRow.FindControl("chkFrmDelete")).Checked = false;
                        ((CheckBox)gRow.FindControl("chkFrmEdit")).Checked = false;
                        ((CheckBox)gRow.FindControl("chkFrmAdd")).Checked = false;
                        ((CheckBox)gRow.FindControl("chkFrmView")).Checked = false;
                        break;
                    }
                }
            }
        }            
    }
    private void bindRootFormGrid()
    {
        if (ddlRole.SelectedIndex != 0)
        {
            DataSet ds = new DataSet();
            ds = mObj.getPForms();            
            gvForms.DataSource = ds;
            gvForms.DataBind();
            DataSet ds1 = new DataSet();
            ds1 = mObj.getPRoleRights(int.Parse(ddlRole.SelectedValue.ToString()));
            extractActive(gvForms, ds1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvForms.Visible = true;
            }
            else
            {
                gvForms.Visible = false;
                lblmsg.Text = "<font color=red>No Rows exist at 0 level.</font>";
            }
        }
    }
    private void bindChildGrid(int parentId)
    {
        if (ddlRole.SelectedIndex != 0)
        {
            DataSet ds = new DataSet();
            ds = mObj.getPForms(parentId, 1);
            gvChild.DataSource = ds;
            gvChild.DataBind();
            DataSet ds1 = new DataSet();
            ds1 = mObj.getPRoleRights(parentId, 1, int.Parse(ddlRole.SelectedValue.ToString()));
            extractActive(gvChild, ds1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvChild.Visible = true;
            }
            else
            {
                gvChild.Visible = false;
                lblmsg.Text = "<font color=red>No Rows exist at 1 level for this parent.</font>";
            }
        }
    }
    private void bindSubChildGrid(int parentId)
    {
        if (ddlRole.SelectedIndex != 0)
        {
            DataSet ds = new DataSet();
            ds = mObj.getPForms(parentId, 2);
            gvSubChild.DataSource = ds;
            gvSubChild.DataBind();
            DataSet ds1 = new DataSet();
            ds1 = mObj.getPRoleRights(parentId, 2, int.Parse(ddlRole.SelectedValue.ToString()));
            extractActive(gvSubChild, ds1);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvSubChild.Visible = true;
            }
            else
            {
                gvSubChild.Visible = false;
                lblmsg.Text = "<font color=red>No Rows exist at 2 level for this parent.</font>";
            }
        }
    }
    private bool checkInt(string value)
    {
        int blank = 0;
        bool isInt = false;
        isInt = int.TryParse(value, out blank);
        return isInt;
    }

    protected void gvForms_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvForms.EditIndex = -1;
        bindRootFormGrid();
    }
    protected void gvForms_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {           
            if (checkInt(e.CommandName))
            {
                int _prntFrmId = Convert.ToInt32(((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text);
                lblchldPrntId.Text = _prntFrmId.ToString();
                lblChild.Text = "";
                lblChild.Text += "Parent Form Id: " + ((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text + "&nbsp;&nbsp;&nbsp;&nbsp;Parent Form Name: " + ((Label)gvForms.Rows[int.Parse(e.CommandName)].FindControl("lblFormName")).Text;
                bindChildGrid(_prntFrmId);
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }    
    protected void gvForms_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvForms.EditIndex = e.NewEditIndex;
        bindRootFormGrid();
    }
    protected void gvForms_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[2].Visible = false;
    }
    protected void gvForms_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {            
            Label _lblFrmId = (Label)gvForms.Rows[e.RowIndex].FindControl("lblFormId");            
            Label _lblRightId = (Label)gvForms.Rows[e.RowIndex].FindControl("lblRightsId");
            CheckBox _chkView = (CheckBox)gvForms.Rows[e.RowIndex].FindControl("chkFrmView");
            CheckBox _chkAdd = (CheckBox)gvForms.Rows[e.RowIndex].FindControl("chkFrmAdd");
            CheckBox _chkEdit = (CheckBox)gvForms.Rows[e.RowIndex].FindControl("chkFrmEdit");
            CheckBox _chkDelete = (CheckBox)gvForms.Rows[e.RowIndex].FindControl("chkFrmDelete");
            CheckBox _chkAttach = (CheckBox)gvForms.Rows[e.RowIndex].FindControl("chkFrmAtch");

            if (_lblRightId.Text.Trim() == "")
            {
                int _maxRightId = mObj.getPRoleRightMaxId();
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked) == true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                if (mObj.addPRoleRights(_maxRightId, int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Inserted.</font>";
                    gvForms.EditIndex = -1;
                    bindRootFormGrid();                    
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Inserted.</font>";
                }
            }
            else
            {                
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked) == true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                 
                if (mObj.updatePRoleRights(int.Parse(_lblRightId.Text), int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Updated.</font>";
                    gvForms.EditIndex = -1;
                    bindRootFormGrid();                    
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Updated.</font>";
                }
            }             
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    // Child 1
    
    protected void gvChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvChild.EditIndex = -1;
        bindChildGrid(int.Parse(lblchldPrntId.Text));
    }
    protected void gvChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {            
            if (checkInt(e.CommandName))
            {
                int _prntFrmId = Convert.ToInt32(((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text);
                lblSubchldPrntId.Text = _prntFrmId.ToString();
                lblSubChild.Text = "";
                lblSubChild.Text += "Parent Form Id: " + ((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormId")).Text + "&nbsp;&nbsp;&nbsp;&nbsp;Parent Form Name: " + ((Label)gvChild.Rows[int.Parse(e.CommandName)].FindControl("lblFormName")).Text;
                bindSubChildGrid(_prntFrmId);
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvChild.EditIndex = e.NewEditIndex;
        bindChildGrid(int.Parse(lblchldPrntId.Text));
    }
    protected void gvChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label _lblFrmId = (Label)gvChild.Rows[e.RowIndex].FindControl("lblFormId");
            Label _lblRightId = (Label)gvChild.Rows[e.RowIndex].FindControl("lblRightsId");
            CheckBox _chkView = (CheckBox)gvChild.Rows[e.RowIndex].FindControl("chkFrmView");
            CheckBox _chkAdd = (CheckBox)gvChild.Rows[e.RowIndex].FindControl("chkFrmAdd");
            CheckBox _chkEdit = (CheckBox)gvChild.Rows[e.RowIndex].FindControl("chkFrmEdit");
            CheckBox _chkDelete = (CheckBox)gvChild.Rows[e.RowIndex].FindControl("chkFrmDelete");
            CheckBox _chkAttach = (CheckBox)gvChild.Rows[e.RowIndex].FindControl("chkFrmAtch");

            if (_lblRightId.Text.Trim() == "")
            {
                int _maxRightId = mObj.getPRoleRightMaxId();
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked )== true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                if (mObj.addPRoleRights(_maxRightId, int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Inserted.</font>";
                    gvChild.EditIndex = -1;
                    bindChildGrid(int.Parse(lblchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Inserted.</font>";
                }
            }
            else
            {
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked) == true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                if (mObj.updatePRoleRights(int.Parse(_lblRightId.Text), int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Updated.</font>";
                    gvChild.EditIndex = -1;
                    bindChildGrid(int.Parse(lblchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Updated.</font>";
                }
            }
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvChild_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[2].Visible = false;        
    }


    // Sub Child
    protected void gvSubChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvSubChild.EditIndex = -1;
        bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
    }
    protected void gvSubChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
                
    }
    protected void gvSubChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvSubChild.EditIndex = e.NewEditIndex;
        bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
    }
    protected void gvSubChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            Label _lblFrmId = (Label)gvSubChild.Rows[e.RowIndex].FindControl("lblFormId");
            Label _lblRightId = (Label)gvSubChild.Rows[e.RowIndex].FindControl("lblRightsId");
            CheckBox _chkView = (CheckBox)gvSubChild.Rows[e.RowIndex].FindControl("chkFrmView");
            CheckBox _chkAdd = (CheckBox)gvSubChild.Rows[e.RowIndex].FindControl("chkFrmAdd");
            CheckBox _chkEdit = (CheckBox)gvSubChild.Rows[e.RowIndex].FindControl("chkFrmEdit");
            CheckBox _chkDelete = (CheckBox)gvSubChild.Rows[e.RowIndex].FindControl("chkFrmDelete");
            CheckBox _chkAttach = (CheckBox)gvSubChild.Rows[e.RowIndex].FindControl("chkFrmAtch");

            if (_lblRightId.Text.Trim() == "")
            {
                int _maxRightId = mObj.getPRoleRightMaxId();
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked) == true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                if (mObj.addPRoleRights(_maxRightId, int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Inserted.</font>";
                    gvSubChild.EditIndex = -1;
                    bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Inserted.</font>";
                }
            }
            else
            {
                int _view = 0;
                int _add = 0;
                int _edit = 0;
                int _delete = 0;
                int _attach = 0;
                if (Convert.ToBoolean(_chkView.Checked) == true)
                {
                    _view = 1;
                }
                if (Convert.ToBoolean(_chkAdd.Checked) == true)
                {
                    _add = 1;
                }
                if (Convert.ToBoolean(_chkEdit.Checked) == true)
                {
                    _edit = 3;
                }
                if (Convert.ToBoolean(_chkDelete.Checked) == true)
                {
                    _delete = 5;
                }
                if (Convert.ToBoolean(_chkAttach.Checked) == true)
                {
                    _attach = 1;
                }
                if (mObj.updatePRoleRights(int.Parse(_lblRightId.Text), int.Parse(ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _add, _edit, _delete, _attach, _userid) > 0)
                {
                    lblmsg.Text = "<font color=green>Values Updated.</font>";
                    gvSubChild.EditIndex = -1;
                    bindSubChildGrid(int.Parse(lblSubchldPrntId.Text));
                }
                else
                {
                    lblmsg.Text = "<font color=red>Values Not Updated.</font>";
                }
            }

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (ArgumentException)
        { lblmsg.Text = "<font color=red>Parameter Value MissMatch Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvSubChild_RowCreated(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[2].Visible = false;        
    }
    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvForms.DataSource = null;
        gvForms.DataBind();

        gvChild.DataSource = null;
        gvChild.DataBind();

        if (ddlRole.SelectedIndex != 0)
        {
            bindRootFormGrid();
        }
        else
        {
            gvForms.Visible = false;
            gvChild.Visible = false;
            gvSubChild.Visible = false;
        }
    }
    
}
