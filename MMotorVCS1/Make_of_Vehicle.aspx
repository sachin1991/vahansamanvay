<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" Theme="emsTheme" AutoEventWireup="true" CodeFile="Make_of_Vehicle.aspx.cs" Inherits="Make_of_Vehicle" Title="Vahan Samanvay Master Make of Vehicle" EnableViewState="true"  EnableViewStateMac="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>

<ContentTemplate>
<table class="contentmain"> 
<tr > <td > 
    <table align="center" width="80%" class="tablerowcolor">
        <tr>
            <td colspan="4" style="height: 20px">
                <asp:Label ID="lblid" runat="server" Visible="False"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4" class="heading">
                
                    Make of Vehicle
            </td>
        </tr>
        <tr>
            <td colspan="4" >
            </td>
        </tr>
        <tr>
            <td style="color: #800000">
                Vehicle Type</td>
            <td >
                <asp:TextBox ID="txtVehCode" runat="server" Enabled="False" Width="51px"></asp:TextBox></td>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtVehCode"></cc1:FilteredTextBoxExtender>
            <td colspan="2">
                &nbsp;<asp:DropDownList ID="ddVehCode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddVehCode_SelectedIndexChanged"
                    Width="250px">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="color: #800000">
                Make Code</td>
            <td >
            </td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txtMakeCode" runat="server" Width="148px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtMakeCode"></cc1:FilteredTextBoxExtender>
                <asp:CheckBox ID="makenamewise" runat="server" AutoPostBack="True" 
                    Text="Make Name wise" oncheckedchanged="makenamewise_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td style="color: #800000">
                Make Name</td>
            <td >
            </td>
            <td colspan="2">
                &nbsp;<asp:TextBox ID="txtMakeName" runat="server" Width="148px"></asp:TextBox></td>
                <cc1:FilteredTextBoxExtender ID="fetxtRegistration" runat="server" FilterMode="InvalidChars" InvalidChars="!+=-/\@#$%^&amp;*(){}[].,:'<>" TargetControlID="txtMakeName"></cc1:FilteredTextBoxExtender>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td colspan="2">
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" Width="76px" CssClass="button" /><asp:Button
                    ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="71px" CssClass="button" /><asp:Button
                        ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" Width="76px" CssClass="button" /></td>
        </tr>
        <tr>
            <td colspan="4" style="height: 15px">
                <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#000000"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="4">
                <h4>
                    Vehicle's Make Details</h4>
            </td>
        </tr>
        <tr>
            <td colspan="4">               
                    <asp:GridView ID="gvVehMake" runat="server" AutoGenerateColumns="False" 
                        OnRowCommand="gvVehMake_RowCommand" PageSize="15" OnPageIndexChanging="gvVehMake_PageIndexChanging"
                        Width="97%" OnRowCreated="gvVehMake_RowCreated" CssClass="gridText" 
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
                        CellPadding="4" EnableModelValidation="True" ForeColor="Black" 
                        GridLines="Vertical">
                        <Columns>
                            <asp:TemplateField HeaderText="S No.">
                                <ItemTemplate>
                                    <asp:LinkButton ID="snum" runat="server" CausesValidation="false" CommandName='<%# Container.DataItemIndex %>'
                                        Text='<%# Container.DataItemIndex +1  %>'>
                        </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Vehicletypeid" HeaderText="VehicleId " SortExpression="Vehicleid" />
                            <asp:BoundField DataField="VehicleTypecode" HeaderText="VehicleTypeCode" SortExpression="VehicleTypecode" />
                            <asp:BoundField DataField="VehicleTypename" HeaderText="VehicleTypeName" SortExpression="VehicleTypename" >
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Makeid" HeaderText="Make Id" SortExpression="Makeid" />
                            <asp:BoundField DataField="Makecode" HeaderText="Make code" SortExpression="Makecode" />
                            <asp:BoundField DataField="Makename" HeaderText="Make Name" SortExpression="Makename" >
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle CssClass="GridHeader" BackColor="#CCCC99" />
                        <RowStyle CssClass="gridrow" BackColor="#F7F7DE" />
                        <SelectedRowStyle CssClass="gridselect" BackColor="#CE5D5A" Font-Bold="True" 
                            ForeColor="White" />
                        <PagerStyle CssClass="GridHeader" BackColor="#F7F7DE" ForeColor="Black" 
                            HorizontalAlign="Right" />
                        <HeaderStyle CssClass="GridHeader" BackColor="#6B696B" Font-Bold="True" 
                            ForeColor="White" />
                        <AlternatingRowStyle CssClass="gridalterrow" BackColor="White" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="Next" 
                            PreviousPageText="Previous" />
                    </asp:GridView>
               
            </td>
        </tr>
        <tr>
            <td colspan="4">
                </td>
        </tr>
    </table>

    </td></tr></table>
<script language="javascript" type="text/javascript">
    function valid()
    {
    if(document.getElementById("<%=ddVehCode.ClientID%>").selectedIndex == 0)
    {
        alert("Select Vehicle Name");
        document.getElementById("<%=ddVehCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtMakeCode.ClientID%>").value == "")
    {
        alert("Enter Make Code");
        document.getElementById("<%=txtMakeCode.ClientID%>").focus();
        return false;
    }    
    if(document.getElementById("<%=txtMakeName.ClientID%>").value == "")
    {
        alert("Enter Make Name");
        document.getElementById("<%=txtMakeName.ClientID%>").focus();
        return false;
    }  
    return true;
    }
    </script>
</ContentTemplate>
<%--</asp:UpdatePanel>--%>
</asp:Content>

