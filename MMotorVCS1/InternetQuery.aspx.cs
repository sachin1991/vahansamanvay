﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.NetworkInformation;
public partial class InternetQuery : System.Web.UI.Page
{
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    int PostCount = 0;
    protected void Page_Load(object sender, EventArgs e)
        
    {
        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }
            bindVehicleType();
            bindStateName();
            bindColName();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
            MasterMethods mm = new MasterMethods();
            DataSet dtbinternetvisitor = mm.internetgetvisitor();
            InternetVisitor.Text = dtbinternetvisitor.Tables[0].Rows[0][0].ToString();
            Session["InternetVisitor"] = InternetVisitor.Text;
            Entry_FormMethods eform = new Entry_FormMethods();
            DataSet ds1 = new DataSet();
            ds1 = eform.AddInternetVisitor(InternetVisitor.Text.ToString());
        }
        lblmsg.Text = "";
    }

    public void bindColName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourNameNew();
            ddColName.DataSource = ds;
            ddColName.DataTextField = ds.Tables[0].Columns["colourname"].ColumnName;
            ddColName.DataValueField = ds.Tables[0].Columns["colidcolcode"].ColumnName;
            ddColName.DataBind();
            ddColName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    protected void ddVehTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectVehicleType();
            PostCount++;
        }
    }

    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();

            ddPsName.Items.Clear();
          
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            
            ddDistrictName.Items.Clear();
            
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));



        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }

    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {
            
            ddPsName.Items.Clear();
          
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }
    }

    private void afterSelectVehicleType()
    {
        if (ddVehTypeName.SelectedIndex != 0)
        {
            bindMakeName();
        }
        else
        {
           
            ddMakeName.Items.Clear();
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
          }
    }

    public void bindStateName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateTypeNew();
            ddStateName.DataSource = ds;
            ddStateName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["statecode"].ColumnName);
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = GenericMethods.check(dt.Columns["districtname"].ColumnName);
            ddDistrictName.DataValueField = GenericMethods.check(dt.Columns["districtcode"].ColumnName);
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            string[] temp = ddDistrictName.SelectedValue.Split('-');
            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]));
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["Psname"].ColumnName);
            ddPsName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["psidpscode"].ColumnName);
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleTypeNew();
            ddVehTypeName.DataSource = ds;
            ddVehTypeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehTypeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehidvehcode"].ColumnName);
            ddVehTypeName.DataBind();
            ddVehTypeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindMakeName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake_accVehTypeNew(Convert.ToInt32(ddVehTypeName.SelectedValue.Split('-')[1]));
            ddMakeName.DataSource = ds;
            ddMakeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["makename"].ColumnName);
            ddMakeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["makeidmakecode"].ColumnName);
            ddMakeName.DataBind();
            ddMakeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    private bool SaveForm(bool blSave)
    {
        try
        {

            if (txtAppName.Text == "")
            {
                Alert("Please enter the Registration No.");
                txtAppName.Focus();
                return false;
            }

            if (ddVehTypeName.SelectedIndex == 0)
            {
                Alert("Please select the Vehicle Type to be entered");
                ddVehTypeName.Focus();
                return false;
            }


            return true;

        }
        catch (Exception ex)
        {
            return false;
        }

        finally
        {
        }
    }

    private void Alert(string strErrMsg)
    {

        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";

        if (!ClientScript.IsClientScriptBlockRegistered("ScriptAlert"))
        {

            ClientScript.RegisterStartupScript(typeof(InternetQuery), "ScriptAlert", strScript);

        }

    }

    protected void btnSearch_Click2(object sender, EventArgs e)
    {
        try
        {
           
            bool blnIsSaved = SaveForm(false);
            if (blnIsSaved == true)
            {
                Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
                string strHostName = System.Net.Dns.GetHostName();

               
                string clientName;

                clientName = Request.ServerVariables["LOCAL_ADDR"];

              
                string typeOfQuery;
                string strsource = "P";
                string[] MakeCode = ddMakeName.SelectedValue.ToString().Split('-');
                string Color;
                string VehicleNameDesc;
                string MakeDecs;
                string Makecode1;
                Makecode1 = ddMakeName.SelectedValue.ToString().Split('-')[0].Trim();
                if (Makecode1 == "0")
                {
                    Makecode1 = "@";
                }

                if (ddVehTypeName.SelectedIndex == 0)
                {
                    VehicleNameDesc = "";
                }
                else
                {
                    VehicleNameDesc = re.Replace(ddVehTypeName.SelectedItem.Text.ToString().Trim()," ");
                }

                if (ddMakeName.SelectedIndex == 0)
                {
                    MakeDecs = "";
                }
                else
                {
                    MakeDecs = ddMakeName.SelectedItem.Text.ToString().Trim();
                }

                if (ddColName.SelectedIndex == 0)
                {
                    Color = "";
                }
                else
                {
                    Color = ddColName.SelectedValue.ToString().Split('-')[1];
                }

              
                string opRegistration = re.Replace(txtRegistration.Text.ToString().Trim(), "");
                string opChasis = re.Replace(txtChasis.Text.ToString().Trim(), "");
                string opEngine = re.Replace(txtEngine.Text.ToString().Trim(), "");

                string stoleninfoavailable = "false";
                stoleninfoavailable = "true";
                DateTime dtime;
                dtime = System.DateTime.Now.Date;
                DataSet ds = new DataSet();
                string MSTATV = (ddStateName.SelectedValue.Split('-')[0]);
                string MMAK1V = (ddMakeName.SelectedValue.ToString().Split('-')[0]);
                string MDISTV = (ddDistrictName.SelectedValue.Split('-')[0]);
                string MPSV = (ddPsName.SelectedValue.Split('-')[0]);
                //ds = eform.AddEntry_InternetStolen(MSTATV, MDISTV , MPSV, re.Replace(txtAppName.Text.ToString().Trim(),""), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtPinCode.Text.ToString().Trim(), (ddVehTypeName.SelectedValue.Split('-')[0]), ((ddMakeName.SelectedValue.ToString().Split('-')[0])), txtYear.Text.ToString(), Color, opRegistration, opChasis, opEngine, strsource, strHostName, clientName, "SH");
                
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    stoleninfoavailable = "true";
                    
                //}
                //else
                //{
                //    stoleninfoavailable = "false";
                //}
                if (rbSecondHand.Checked == true)
                {
                    typeOfQuery = "SH";

      
                    ds = eform.AddEntry_InternetSecondHand(MSTATV, MDISTV , MPSV, re.Replace(txtAppName.Text.ToString().Trim(),""), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtPinCode.Text.ToString().Trim(), (ddVehTypeName.SelectedValue.Split('-')[0]), ((ddMakeName.SelectedValue.ToString().Split('-')[0])), txtYear.Text.ToString(), Color, opRegistration, opChasis, opEngine, strsource, strHostName, clientName, typeOfQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        Session["Stolenavailable"] = stoleninfoavailable;
                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "1";
                        Session["Nameofquery"] = txtAppName.Text.ToString();
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: openWin('InternetQueryOutput.aspx'); ", true);
                    }
                    else
                    {
                        Session["Stolenavailable"] = "true";
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "3";
                        Session["Nameofquery"] = txtAppName.Text.ToString();
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";


                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: openWin('InternetQueryOutput.aspx'); ", true);
                    }
                }
                else if (rbRecoverStatus.Checked == true)
                {
                    typeOfQuery = "R";

                    string MSTAT = (ddStateName.SelectedValue.Split('-')[0]);
                    string MMAK1 = (ddMakeName.SelectedValue.ToString().Split('-')[0]);
                    string MDIST =(ddDistrictName.SelectedValue.Split('-')[0]);
                    string MPS = (ddPsName.SelectedValue.Split('-')[0]);
                   
                    
                    ds = eform.AddEntry_InternetRecover(MSTAT,MDIST ,MPS , txtAppName.Text.ToString().Trim(), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtPinCode.Text.ToString().Trim(), (ddVehTypeName.SelectedValue.Split('-')[0]), MMAK1, txtYear.Text.ToString(), Color, txtRegistration.Text.ToString().Trim(), txtChasis.Text.ToString().Trim(), txtEngine.Text.ToString().Trim(), strsource, strHostName, clientName, typeOfQuery);


                    if (ds.Tables[0].Rows.Count > 0)
                    {

              
                        Session["Stolenavailable"] = stoleninfoavailable;
                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Cache["Data"] = (DataTable)(ds.Tables[0]);
                        Session["MatchStatus"] = "1";
                        Session["Nameofquery"] = txtAppName.Text.ToString();
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                        //+GetMAC();
                        ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript: openWin('InternetQueryOutput.aspx'); ", true);
                    }
                    else
                    {
                        Session["Stolenavailable"] = "true";
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "2";
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                        Session["Nameofquery"] = txtAppName.Text.ToString();
                    
                        ClientScript.RegisterStartupScript (GetType(), "Javascript", "javascript: openWin('InternetQueryOutput.aspx'); ", true);
                     
                    }



                }

            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message );
        }
    }

    protected void btnrefresh_Click(object sender, EventArgs e)
    {
 
        Response.Redirect(Request.RawUrl);
    }

    protected void TextValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (txtRegistration.Text.Length > 0 || txtChasis.Text.Length > 0 || txtEngine.Text.Length > 0);
    }
    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }

    protected void btnrefresh_Click1(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
}