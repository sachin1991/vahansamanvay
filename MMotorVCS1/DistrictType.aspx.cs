using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class DistrictType : System.Web.UI.Page
{
    static int dtype_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();
   
    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");
        string ss = Request.QueryString["stateid"];
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            
        }
        

        if (!IsPostBack)
        {
            try
            {
                
            }
            catch
            {
                Response.Redirect("error.aspx");
            }

            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindStateName();
            txtStateCode.Focus();
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    public void getmaxid()
    {
        try
        {
            dtype_id = mm.getDistrictMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindStateName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateType();
            ddStateCode.DataSource = ds;
            ddStateCode.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateCode.DataValueField = GenericMethods.check(ds.Tables[0].Columns["stateid"].ColumnName);
            ddStateCode.DataBind();
            ddStateCode.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtDistrictCode.Text = "";
        txtDistrictName.Text = "";
        txtStateCode.Text = "";
        ddStateCode.SelectedIndex = 0;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
                
        try
        {            
            DataSet ds = new DataSet();

            ds = mm.getDistrictType(ddStateCode.SelectedValue.ToString().Trim());
            int flag = 0;
            if (ddStateCode.SelectedIndex!=0 && txtDistrictCode.Text != "" && txtDistrictName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    if (GenericMethods.check(ds.Tables[0].Rows[i]["districtname"].ToString().Trim().ToUpper()) == txtDistrictName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This District Name is already exists.</font>";
                            break;
                        }
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["districtcode"].ToString().Trim().ToUpper()) == txtDistrictCode.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This District Code is already exists.</font>";
                            break;
                        }
                    
                }
                if (flag != 1)
                {
                    //dtype_id = Convert.ToInt32(txtStateCode.Text.ToString().Trim() + txtDistrictCode.Text.Trim().ToUpper().Trim());
                    string dtype;
                    dtype = "" + txtStateCode.Text.ToString().Trim() + ""+ txtDistrictCode.Text.Trim().ToUpper().Trim() + "";
                    //if (mm.AddDistrict((dtype_id), Convert.ToInt32(ddStateCode.SelectedValue), (txtDistrictCode.Text.Trim().ToUpper()), txtDistrictName.Text.Trim().ToUpper(), _userid) > 0)
                    if (mm.AddDistrict(dtype, (txtDistrictCode.Text.Trim().ToUpper()), txtDistrictName.Text.Trim().ToUpper(), "2", txtStateCode.Text.ToString().Trim()) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values inserted</font>";
                        refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not inserted</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getDistrictType();
            int flag = 0;
            if (ddStateCode.SelectedIndex != 0 && txtDistrictCode.Text != "" && txtDistrictName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["districtid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["districtname"].ToString().Trim().ToUpper()) == txtDistrictName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This District Name is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    if (mm.updateDistrictType(Convert.ToInt32(lblid.Text), Convert.ToInt32(ddStateCode.SelectedValue), (txtDistrictCode.Text.Trim().ToUpper()), txtDistrictName.Text.Trim().ToUpper(), _userid) > 0)
                    {
                        lblmsg.Text = "<font color=green>Values updated</font>";
                        ds = mm.getDistrictType1(ddStateCode.SelectedValue.ToString().Trim());
                        gvDisType.DataSource = ds;
                        gvDisType.DataBind();
                       refresh();
                    }
                    else
                    {
                        lblmsg.Text = "<font color=red>Values not updated</font>";
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }

    }
    protected void gvDisType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {               
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvDisType.Rows[Convert.ToInt32(e.CommandName) ].Cells[4].Text;
                txtDistrictCode.Text = gvDisType.Rows[Convert.ToInt32(e.CommandName) ].Cells[5].Text;
                txtDistrictName.Text = gvDisType.Rows[Convert.ToInt32(e.CommandName)].Cells[6].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void ddStateCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();

        if (ddStateCode.SelectedIndex != 0)
        {
            ds = mm.getStatecode((ddStateCode.SelectedValue));
            txtStateCode.Text =GenericMethods.check(ds.Tables[0].Rows[0]["statecode"].ToString());
            txtDistrictCode.Focus();
            string str = txtStateCode.Text.Trim().ToString();
            if (districtbyname.Checked == true)
            {
                ds = mm.getDistrictType(str);
            }
            else
            {
                ds = mm.getDistrictType1(str);
            }
                gvDisType.DataSource = ds;
                gvDisType.DataBind();
           
        }
        else
        {
            txtStateCode.Text = "";
            ddStateCode.Focus();
        }
        
    }
    protected void gvDisType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[4].Visible = false;
        }
    }

    protected void gvDisType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvDisType.PageIndex = e.NewPageIndex;
    }
    protected void txtStateCode_TextChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        if (txtStateCode.Text.Trim() != "")
        {
            ds = mm.getStateNameByCode(txtStateCode.Text.Trim());
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddStateCode.Text =GenericMethods.check(ds.Tables[0].Rows[0]["stateid"].ToString());
                txtDistrictCode.Focus();
               
            }
            else
            {
                ddStateCode.SelectedIndex = 0;
                lblmsg.Text = "<font color=red>This state code does not exists.</font>";
                txtStateCode.Focus();
            }
        }
        else
        {
            ddStateCode.SelectedIndex = 0;
            txtStateCode.Focus();
        }
    }
    protected void districtbyname_CheckedChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        string str = txtStateCode.Text.Trim().ToString();
        if (districtbyname.Checked == true)
        {
            ds = mm.getDistrictType(str);
        }
        else
        {
            ds = mm.getDistrictType1(str);
        }
        gvDisType.DataSource = ds;
        gvDisType.DataBind();
    }
}
