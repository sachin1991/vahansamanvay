<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportInternetDailyhitstat.aspx.vb" Inherits="ReportInternetDailyhitstat" Title="Vahan Samanvay Daily Hit Statistics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="style/main.css" rel="stylesheet" type="text/css" />
    <title>Untitled Page</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 97px;
        }
        .style4
        {
            width: 75px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="contentmain">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="GOI" runat="server" Font-Bold="True" Text="Government of India" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="MHA" runat="server" Font-Bold="True" Text="Mnistry of Home Affairs"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="NCRB" runat="server" Font-Bold="True" Text="National Crime Records Bureau"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3"><asp:Label ID="lblMoney" runat="server" Font-Bold="True" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="VS" runat="server" Font-Bold="True" Text="Vahan Samanvay" Visible="False"></asp:Label>
                    <br />

                    <asp:Label ID="lblHeading" runat="server" style="font-weight: 700"></asp:Label>

                    <strong>&nbsp;Daily Hit Statistics</strong><br />
                    <br />
                </td>
            </tr>
           

            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="LISTOFSTOLEN" runat="server" Text="Query Fired Report"
                        Visible="False"></asp:Label>
                    <asp:TextBox ID="DTFROM" runat="server" Visible="False" BorderStyle="None"></asp:TextBox>
                    <asp:Label ID="LISTTO" runat="server" Text="to" Visible="False"></asp:Label>
                    <asp:TextBox ID="DTTO" runat="server" Visible="False" BorderStyle="None"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <div align="center">
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table border='1' cellpadding='0' cellspacing='0'>
   
                    <tr>
                        <td>
                        Dated
                        </td >
                        <td align="right">
                        No. of Hits
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                    <%# DataBinder.Eval(Container.DataItem, "dt")%> 
                    
                    </td>
                    <td align="right">
                    <%# DataBinder.Eval(Container, "DataItem.cnt")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
            <tr>
                    <td>
                    Total
                    </td>
                    <td align="right">
                    <%Response.Write(cnt)%>
                    </td>
                </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        
        Report Generated on
        <asp:Label ID="toda" runat="server" Text="Label"></asp:Label>
        <br />
        
        <table class="style1">
            <tr>
                <td class="style2">
        <asp:LinkButton ID="FirstPage" runat="server">First Page</asp:LinkButton>
                </td>
                <td class="style3">
        
        <asp:LinkButton ID="Lnkbtnprev" runat="server" Visible="False">Previous Page</asp:LinkButton>
                </td>
                <td class="style4">
        <asp:LinkButton ID="LnkbtnNext" runat="server" Visible="False">Next Page</asp:LinkButton>
                </td>
                <td>
        <asp:LinkButton ID="LastPage" runat="server">Last Page</asp:LinkButton>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </div>
    </form>
</body>
</html>
