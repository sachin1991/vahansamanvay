<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportqueryfiredPolice.aspx.vb" Inherits="ReportqueryfiredPolice" title="Vahan Samanvay query fired Police" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<link href="style/main.css" rel="stylesheet" type="text/css" />
    <title>Untitled Page</title>
    <style type="text/css">

        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 97px;
        }
        .style4
        {
            width: 75px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="contentmain">
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="GOI" runat="server" Font-Bold="True" Text="Government of India" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="MHA" runat="server" Font-Bold="True" Text="Mnistry of Home Affairs"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="NCRB" runat="server" Font-Bold="True" Text="National Crime Records Bureau"
                        Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3"><asp:Label ID="lblMoney" runat="server" Font-Bold="True" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="VS" runat="server" Font-Bold="True" Text="Vahan Samanvay" Visible="False"></asp:Label>
                    <br />
                    <strong>Data Entered and Query Fired by Police Report</strong><br />
                    <br />
                </td>
            </tr>
           
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="LISTOFSTOLEN" runat="server" Text="Query Fired Report"
                        Visible="False"></asp:Label>
                    <asp:TextBox ID="DTFROM" runat="server" Visible="False" BorderStyle="None"></asp:TextBox>
                    <asp:Label ID="LISTTO" runat="server" Text="to" Visible="False"></asp:Label>
                    <asp:TextBox ID="DTTO" runat="server" Visible="False" BorderStyle="None"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <div align="center">
        <asp:Repeater ID="Repeater1" runat="server">
            <HeaderTemplate>
                <table border='1' cellpadding='0' cellspacing='0'>
   
                    <tr>
                        <td>
                            S.NO.
                        </td>
                        <td>
                        Date of Enquiry
                        </td>
                        <td>
                        User
                        </td>

                        <td>
                        STATE
                        </td>
                        <td>
                        FIR No<br />
                        FIR Date
                        </td>
                        <td>
                            VEHICLE TYPE<br />
                            MAKE
                        </td>
                        <td>
                            REG.NO.<br />
                            CHA.NO.<br />
                            ENG. NO.
                        </td>
                        <td>
                        Status
                        </td>

                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                    <%Response.Write(serialnumberprint())%>                       
                    </td>
                     <td>
                     <%# DataBinder.Eval(Container, "DataItem.dtCreated_Date")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.vcUserName")%>
                    <%# DataBinder.Eval(Container, "DataItem.vcHeader")%>
                    <%# DataBinder.Eval(Container, "DataItem.vcAddress")%>
                    </td>

                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.statename")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.vcFIRNO")%>
                    <%# DataBinder.Eval(Container, "DataItem.FIRdt")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.vcType")%>
                    <%# DataBinder.Eval(Container, "DataItem.vehicleTypeName")%>
                    <%# DataBinder.Eval(Container, "DataItem.vcMake")%>
                    <%# DataBinder.Eval(Container, "DataItem.MakeName")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.vcRegistrationNo")%>
                    <%# DataBinder.Eval(Container, "DataItem.vcChasisNo")%>
                    <%# DataBinder.Eval(Container, "DataItem.vcEngineNo")%>
                    </td>
                    <td>
                    <%# DataBinder.Eval(Container, "DataItem.vcStatus")%>
                    </td>

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        
        Report Generated on
        <asp:Label ID="toda" runat="server" Text="Label"></asp:Label>
        <br />
        
        <br />
        <table class="style1">
            <tr>
                <td class="style2">
        <asp:LinkButton ID="FirstPage" runat="server">First Page</asp:LinkButton>
                </td>
                <td class="style3">
        
        <asp:LinkButton ID="Lnkbtnprev" runat="server" Visible="False">Previous Page</asp:LinkButton>
                </td>
                <td class="style4">
        <asp:LinkButton ID="LnkbtnNext" runat="server" Visible="False">Next Page</asp:LinkButton>
                </td>
                <td>
        <asp:LinkButton ID="LastPage" runat="server">Last Page</asp:LinkButton>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
