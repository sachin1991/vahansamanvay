 <%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Theme="emsTheme" CodeFile="AddInsurance.aspx.cs"
    Inherits="AddInsurance" Title="Vahan Samanvay Enquiry for Recovered vehicles" ValidateRequest="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
     <Triggers> 
<asp:PostBackTrigger ControlID="btnAdd" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 
<ContentTemplate>
    <script language="javascript" type="text/javascript">
        function valid() {     
    document.getElementById('ct100_ContentPlaceHolder1_ddStateName').selectedIndex
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').selectedIndex==0)
    {
        alert("Select State Name");
        document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').focus();        
        return false;
    }
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').selectedIndex==0)
    {
        alert("Select District Name");
        document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').focus();        
        return false;
    } 
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').selectedIndex==0)
    {
        alert("Select Police Station Name");
        document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').focus();        
        return false;
    }        
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').value == "")
    {
        alert("Enter FIR No.");
        document.getElementById('ctl00_ContentPlaceHolder1_txtFIRNo').focus();
        return false;
    }    
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').value == "")
    {
        alert("Select FIR Date");
        document.getElementById('ctl00_ContentPlaceHolder1_txtFIRDt').focus();
        return false;
    }  

    if(document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name').selectedIndex==0)
    {
        if(document.getElementById('ctl00_ContentPlaceHolder1_txta1sec1').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta1sec2').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta1sec3').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta1sec4').value != "" )
        {
            alert("Select Corresponding Act Name");
            document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name').focus();
            return false;
        }        
    }
    if(document.getElementById("ctl00_ContentPlaceHolder1_ddAct2Name").selectedIndex==0)
    {
        if(document.getElementById('ctl00_ContentPlaceHolder1_txta2sec1').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta2sec2').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta2sec3').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta2sec4').value != "" )
        {
            alert("Select Corresponding Act Name");
            document.getElementById('ctl00_ContentPlaceHolder1_ddAct2Name').focus();
            return false;
        }        
    }
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name').selectedIndex==0)
    {
        if(document.getElementById('ctl00_ContentPlaceHolder1_txta3sec1').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta3sec2').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta3sec3').value != "" || document.getElementById('ctl00_ContentPlaceHolder1_txta3sec4').value != "" )
        {
            alert("Select Corresponding Act Name");
            document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name').focus();
            return false;
        }        
    }
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').selectedIndex==0)
    {
        alert("Select Vehicle Type");
        document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').focus();        
        return false;
    } 
    if(document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').selectedIndex==0)
    {
        alert("Select Make of Vehicle");
        document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').focus();        
        return false;
    }  
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value == "")
    {
        alert("Enter Registration No.");
        document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').focus();
        return false;
    } 
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').value.length<5)
        {
            alert("Registration No. must be alteast 5 digit.");
            document.getElementById('ctl00_ContentPlaceHolder1_txtRegistrationno').focus();        
            return false;
        }
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value == "")
    {
        alert("Enter Chassis No.");
        document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').focus();
        return false;
    } 
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').value.length<5)
        {
            alert("Chassis No. must be alteast 5 digit.");
            document.getElementById('ctl00_ContentPlaceHolder1_txtChasisno').focus();        
            return false;
        }
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value == "")
    {
        alert("Enter Engine no.");
        document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').focus();
        return false;
    } 
    if(document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').value.length<5)
        {
            alert("Engine No. must be alteast 5 digit.");
            document.getElementById('ctl00_ContentPlaceHolder1_txtEngineno').focus();        
            return false;
        }

            
    
    return true;
}
function openNewWindowshelp() {
    window.open("VahanSamanvayInsurance.pdf");
}
  
    </script>    
    <script type = "text/javascript">
        function checkField(fieldname) {
            if (/[^0-9a-bA-B]/gi.test(fieldname.value)) {
                alert("Only alphanumeric characters  are valid in this field");
                fieldname.value = ""; 
                fieldname.focus();
                return false;
            }
        }
</script>
    <script language="javascript" type="text/javascript">
    
    function GetStateCode()
        {
            var Statearray=document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value=Statearray[0];
            document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value="";
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value="";
        }
        function GetInsurancecode() {


        }    
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectStateddl()
    {
        document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value="";
        document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value="";
    
        var StateCode=document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value;
        var Stateddl=document.getElementById('ctl00_ContentPlaceHolder1_ddStateName');
        var inc=0;
        for(var i=0;i<Stateddl.length;i++)
        {
            var Statearray=Stateddl.options[i].value.split("-");
            try
            {
                if(trim(Statearray[0])==trim(StateCode))
                {             
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }

        if(document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddStateName').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtStateCode').value='';
        alert('This State Code is not Exist');
        return false;
        }
        
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetDistrictCode()
        {
            var Districtarray=document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value=Districtarray[0];
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value="";
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectDistrictddl()
    {
        document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value="";
    
        var DistrictCode=document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value;
        var Districtddl=document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName');
        var inc=0;
        for(var i=0;i<Districtddl.length;i++)
        {
            var Districtarray=Districtddl.options[i].value.split("-");
            try
            {
                if(trim(Districtarray[0])==trim(DistrictCode))
                {
                    document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').options[i].selected=true
                    inc=1;
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddDistrictName').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtDistrictCode').value='';
        alert('This District Code is not Exist');
        return false;
        }
        
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetPSCode()
        {
            var mytool_array=document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value=mytool_array[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectPSddl()
    {
        var PsCode=document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value;
        var Psddl=document.getElementById('ctl00_ContentPlaceHolder1_ddPsName');
        var inc=0;
        for(var i=0;i<Psddl.length;i++)
        {
            var mytool_array=Psddl.options[i].value.split("-");
            try
            {
                if(trim(mytool_array[0])==trim(PsCode))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddPsName').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtPsCode').value='';
        alert('This Polic Station Code is not Exist');
        return false;
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetAct1Code()
        {
            
            var Act1Array=document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtAct1Code').value=Act1Array[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectAct1ddl()
    {
    
        var Act1Code=document.getElementById('ctl00_ContentPlaceHolder1_txtAct1Code').value;
        var Act1ddl=document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name');
        var inc=0;
        for(var i=0;i<Act1ddl.length;i++)
        {
            var Act1Array=Act1ddl.options[i].value.split("-");
            try
            {
                if(trim(Act1Array[0])==trim(Act1Code))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtAct1Code').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddAct1Name').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtAct1Code').value='';
        alert('This Act 1 Code is not Exist');
        return false;
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetAct2Code()
        {
            var Act2Array=document.getElementById('ctl00_ContentPlaceHolder1_ddAct2Name').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtAct2Code').value=Act2Array[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectAct2ddl()
    {
        var Act2Code=document.getElementById('ctl00_ContentPlaceHolder1_txtAct2Code').value;
        var Act2ddl=document.getElementById('ctl00_ContentPlaceHolder1_ddAct2Name');
        var inc=0;
        for(var i=0;i<Act2ddl.length;i++)
        {
            var Act2Array=Act2ddl.options[i].value.split("-");
            try
            {
                if(trim(Act2Array[0])==trim(Act2Code))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddAct2Name').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtAct2Code').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddAct2Name').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtAct2Code').value='';
        alert('This Act 2 Code is not Exist');
        return false;
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetAct3Code()
        {
            var Act3Array=document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtAct3Code').value=Act3Array[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectAct3ddl()
    {
        var Act3Code=document.getElementById('ctl00_ContentPlaceHolder1_txtAct3Code').value;
        var Act3ddl=document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name');
        var inc=0;
        for(var i=0;i<Act3ddl.length;i++)
        {
            var Act3Array=Act3ddl.options[i].value.split("-");
            try
            {
                if(trim(Act3Array[0])==trim(Act3Code))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
       if(document.getElementById('ctl00_ContentPlaceHolder1_txtAct3Code').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddAct3Name').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtAct3Code').value='';
        alert('This Act 3 Code is not Exist');
        return false;
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetVehTypeCode()
        {
            var VehTypeArray=document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtVehTypeCode').value=VehTypeArray[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectVehTypeddl()
    {
        document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value='';
        var VehTypeCode=document.getElementById('ctl00_ContentPlaceHolder1_txtVehTypeCode').value;
        var VehTypeddl=document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName');
        var inc=0;
        for(var i=0;i<VehTypeddl.length;i++)
        {
            var VehTypeArray=VehTypeddl.options[i].value.split("-");
            try
            {
                if(trim(VehTypeArray[0])==trim(VehTypeCode))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtVehTypeCode').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddVehTypeName').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtVehTypeCode').value='';
        alert('This vehcile type Code is not Exist');
        return false;
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function GetMakeCode()
        {
            var MakeArray=document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value=MakeArray[0];
        }
        
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectMakeddl()
    {
        var MakeCode=document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value;
        var Makeddl=document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName');
        var inc=0;
        for(var i=0;i<Makeddl.length;i++)
        {
            var MakeArray=Makeddl.options[i].value.split("-");
            try
            {
                if(trim(MakeArray[0])==trim(MakeCode))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value!='' && inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddMakeName').options[0].selected=true
        document.getElementById('ctl00_ContentPlaceHolder1_txtMakeCode').value='';
        alert('This vehcile make Code is not Exist');
        return false;
        }
    }
    </script>

<script language="javascript" type="text/javascript">
    
    function GetColCode()
        {
            var ColArray=document.getElementById('ctl00_ContentPlaceHolder1_ddColName').value.split("-");
            document.getElementById('ctl00_ContentPlaceHolder1_txtColCode').value=ColArray[0];
        }
        

    </script>
    
    <script language="javascript" type="text/javascript">
    
    function SelectColddl()
    {
        var ColCode=document.getElementById('ctl00_ContentPlaceHolder1_txtColCode').value;
        var Colddl=document.getElementById('ctl00_ContentPlaceHolder1_ddColName');
        var inc=0;
        for(var i=0;i<Colddl.length;i++)
        {
            var ColArray=Colddl.options[i].value.split("-");
            try
            {
                if(trim(ColArray[0])==trim(ColCode))
                {
                    inc=1;
                    document.getElementById('ctl00_ContentPlaceHolder1_ddColName').options[i].selected=true
                    break;
                }
            }
            catch(e)
            {}
        }
        if(inc!=1)
        {
        document.getElementById('ctl00_ContentPlaceHolder1_ddColName').options[0].selected=true
        alert('Enter Code is not Exist');
        }
    }
    </script>
    
    <script language="javascript" type="text/javascript">
    
    function trim(str)
    {    
        if(!str || typeof str != 'string')   
        return null;    
        return str.replace(/^[\s]+/,'').replace(/[\s]+$/,'').replace(/[\s]{2,}/,' ');
    }

    </script> 
    

    
    

<script language="JavaScript" type="text/javascript" src="js/calendar_us.js"></script>
  
  <table class="contentmain">
   <tr > <td >
        <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0" runat="server" class="tablerowcolor" >
                    <tr><td colspan="4"> <asp:Label id="lblid" runat="server"></asp:Label></TD> <td></td></tr>
                    <tr><td colSpan="5" class="heading">Entries of Stolen Vehicle</td></tr>
                    <tr><td> Status</td> <td ></td>
                        <td> <asp:DropDownList ID="drpStatus" runat="server" CssClass="dropdownlistCss" 
                                TabIndex="1" Width="300px" 
                                onselectedindexchanged="drpStatus_SelectedIndexChanged">
                            </asp:DropDownList> </td>
                        <td colSpan=2> <asp:RadioButton id="RbtnFir" runat="server" Text="FIR" GroupName="FirGdsde" Checked="True" Visible="False"></asp:RadioButton>
                        <asp:RadioButton id="RbtnGdsde" runat="server" Text="GD/SDE" GroupName="FirGdsde" Visible="False"></asp:RadioButton></td>
                     </tr>
                    <tr><td> Insurance Co. Code</td> <td >&nbsp;</td>
                        <td> 
                            <asp:DropDownList ID="InsuranceCode" AutoPostBack="true" runat="server" onselectedindexchanged="InsuranceCode_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                        <td colSpan=2> &nbsp;</td>
                     </tr>
                    <tr><td> Insurance Co. Name</td> <td >&nbsp;</td>
                        <td> 
                            <asp:TextBox ID="InsuranceCompanyName" AutoPostBack="true" runat="server" Width="387px"></asp:TextBox>
                        </td>
                        <td colSpan=2> &nbsp;</td>
                     </tr>
                    <tr><td> Insurance Co. Address</td> <td >&nbsp;</td>
                        <td> 
                            <asp:TextBox ID="InsuranceCompanyAddress"  AutoPostBack="true" runat="server" TextMode="MultiLine" 
                                Width="386px"></asp:TextBox>
                        </td>
                        <td colSpan=2> 
                            <asp:TextBox ID="datetoday" runat="server" Visible="False"></asp:TextBox>
                        </td>
                     </tr>
                    <tr>
                        <td style="color: #800000"><strong>State Name</strong></td>
                        <td><asp:TextBox id="txtStateCode" runat="server" TabIndex="2"  AutoPostBack="true"   OnTextChanged="ddStateName_SelectedIndexChanged" Width="51px" Font-Bold="True"  MaxLength="5"  Visible="False" ></asp:TextBox></td>
                        
                        <td><asp:DropDownList CssClass="dropdownlistCss"  id="ddStateName"  AutoPostBack="true"  OnSelectedIndexChanged="ddStateName_SelectedIndexChanged" runat="server" Width="300px"></asp:DropDownList></td>
                        <td style="color: #800000"><strong>FIR No.</strong></td>
                        <td><asp:TextBox id="txtFIRNo" runat="server" MaxLength="5" TabIndex="5" 
                                ValidationGroup="ctr"></asp:TextBox>
                                <cc1:filteredtextboxextender ID="Filteredtextboxextender1" runat="server" 
                                  FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                                  TargetControlID="txtFIRNo"></cc1:filteredtextboxextender> 
                                </td>
                     </tr>
                    <tr>
                        <td style="color: #800000"><strong>District Name</strong></td>
                        <td><asp:TextBox id="txtDistrictCode" runat="server" Width="51px" OnTextChanged="txtDistrictCode_TextChanged" AutoPostBack="True" Font-Bold="True"  MaxLength="5" TabIndex="3" Visible="False"></asp:TextBox></td>
                        
                        <td><asp:DropDownList id="ddDistrictName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddDistrictName_SelectedIndexChanged" Width="300px" ></asp:DropDownList></td>
                        <td style="color: #800000"><strong>FIR Date</strong></td>
                        <td><asp:TextBox id="txtFIRDt" runat="server" TabIndex="6" 
                                ValidationGroup="ctr" AutoPostBack="True" CausesValidation="True" 
                                ></asp:TextBox>
                       
                                                     <cc1:CalendarExtender 
                    ID="firdtcalendar" 
                    runat="server" 
                    TargetControlID="txtFIRdt" 
                    Format="dd/MM/yyyy" 
                     PopupPosition="Right"  />
               
                            </td>
                    </tr>
                    <tr>
                        <td style="color: #800000"><strong>Police Station Name</strong></td>
                        <td><asp:TextBox id="txtPsCode" runat="server" Width="51px" Font-Bold="True"  MaxLength="10" TabIndex="4"  Visible="False"></asp:TextBox></td>
                        <td><asp:DropDownList id="ddPsName" runat="server" Width="300px" ></asp:DropDownList></td>
                        <td><SPAN style="COLOR: #000000">Occurance Date</SPAN></td>
                        <td><asp:TextBox id="txtStolenDt" runat="server" TabIndex="7" 
                                ValidationGroup="ctr" AutoPostBack="True" CausesValidation="True"></asp:TextBox>
                                <asp:Button id="btnStolenDt" runat="server" CssClass="button" Width="26px" Text="..." Visible="False"></asp:Button><br />
                                (dd/MM/yyyy)<br />
                                <cc1:CalendarExtender 
                    ID="stolendtextend" 
                    runat="server" 
                    TargetControlID="txtStolendt" 
                    Format="dd/MM/yyyy" 
                     PopupPosition="Right"  />
                     
                                </td>
                     </tr>
                    <tr>
                        <td style="color: #800000" ><strong>Vehicle Type</strong></td>
                        <td><asp:TextBox id="txtVehTypeCode" TabIndex="10" runat="server" OnTextChanged="ddVehTypeName_SelectedIndexChanged" AutoPostBack="True" Width="51px" Font-Bold="True"  MaxLength="5" Visible="False" ></asp:TextBox>&nbsp;</td>
                        
                        <td><asp:DropDownList id="ddVehTypeName" runat="server" Width="300px"  AutoPostBack="True" OnSelectedIndexChanged="ddVehTypeName_SelectedIndexChanged" ></asp:DropDownList></td>
                        <td style="color: #800000"><b>Registration &nbsp;No.</b></td>
                        <td>
                        
                        <asp:TextBox id="txtRegistrationno" runat="server" CssClass="textareacss" MaxLength="20" TabIndex="12"></asp:TextBox>
                        </td>
                     </tr>
                    <tr>
                        <td style="color: #800000"><strong>Make Of Vehicle</strong></td>
                        <td><asp:TextBox id="txtMakeCode" runat="server" Width="51px" Font-Bold="True"  MaxLength="5" TabIndex="11" Visible="False"></asp:TextBox></td>
                        
                        <td><asp:DropDownList id="ddMakeName" runat="server" Width="300px" ></asp:DropDownList></td>
                        <td style="color: #800000"><b>Chassis No.</b></td>
                         <td><asp:TextBox id="txtChasisno" TabIndex="13" runat="server" CssClass="textareacss"  MaxLength="20"></asp:TextBox></td>
                      </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td><asp:DropDownList id="ddModelName" runat="server" Width="300px" OnSelectedIndexChanged="ddModelName_SelectedIndexChanged" Visible="False" ></asp:DropDownList></td>
                            <td style="color: #800000"><b>Engine No.</b></td>
                            <td><asp:TextBox id="txtEngineno" runat="server" CssClass="textareacss" MaxLength="20" TabIndex="14"></asp:TextBox></td>
                         </tr>
                         <tr>
                            <td><span style="COLOR: #000000">Colour Of Vehicle</SPAN></td>
                            <td><asp:TextBox id="txtColCode" TabIndex="15" runat="server" Width="51px" Font-Bold="True"  MaxLength="5"  Visible="False"></asp:TextBox></td>
                            
                            <td><asp:DropDownList id="ddColName" runat="server" Width="300px" ></asp:DropDownList></td>
                            <td><span style="COLOR: #000000">Year of&nbsp; Manufacture</SPAN></td>
                            <td><asp:DropDownList id="ddYearofManu" runat="server" Width="73px" Visible="False" ></asp:DropDownList>
                            <asp:DropDownList id="txtYear" runat="server" Width="73px"  ></asp:DropDownList>
                                </td>
                          </tr>
                        <tr>
                            <td colspan="5"><asp:RadioButton id="RbtnStolen" runat="server" CssClass="blk" Text="Stolen" GroupName="Status" Checked="True" Visible="False"></asp:RadioButton>&nbsp; </TD>
                         </tr>
                         <tr class="tablerowcolor">
                            <td colspan="5"> <asp:Label ID="lblmsg" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Black"></asp:Label> </td>
                        </tr>
                        <tr>
                            <td colspan="5"> <asp:Button ID="btnAdd" runat="server" CssClass="button" onclick="btnAdd_Click" Text="Enquire" Width="76px" TabIndex="17" ValidationGroup="ctr" CausesValidation="true" />
                                <asp:Button ID="btnReset" runat="server" CssClass="button" 
                                    onclick="btnReset_Click" Text="Reset" Width="76px" TabIndex="18" 
                                    CausesValidation="False"/>
                            </td>
                         </tr>
                        <tr>
                            <td colspan="5"> <asp:HiddenField ID="hfLostId" runat="server" Visible="False" />
                                <asp:HiddenField ID="hfRecId" runat="server" Visible="False" />
                            </td>
                         </tr>
                         <tr>
                          <td colspan="5">
                    <cc1:filteredtextboxextender ID="fetxtRegistrationno" runat="server" 
                                  FilterMode="InvalidChars" InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " 
                                  TargetControlID="txtRegistrationno">
                    </cc1:filteredtextboxextender>
                   <cc1:filteredtextboxextender ID="fetxtChasisno" runat="server" FilterMode="InvalidChars" 
                                  InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtChasisno">
                    </cc1:filteredtextboxextender>
                     <cc1:filteredtextboxextender ID="fetxtEngineno" runat="server" FilterMode="InvalidChars" 
                                  InvalidChars="!+=-/\ @#$%^&amp;*(){}[].,:'<> " TargetControlID="txtEngineno">
                    </cc1:filteredtextboxextender>                     

        <asp:RequiredFieldValidator ID="rqddrpStatus"  runat="server" InitialValue="--Select--" ControlToValidate="drpStatus" ErrorMessage="Please select the Status.">  
        </asp:RequiredFieldValidator>
         <asp:RequiredFieldValidator ID="rqdddStateName"  runat="server" InitialValue="--Select--" ControlToValidate="ddStateName" ErrorMessage="Please select the State.">  
        </asp:RequiredFieldValidator>
         <asp:RequiredFieldValidator ID="rqdddDistrictName"  runat="server" InitialValue="--Select--" ControlToValidate="ddDistrictName" ErrorMessage="Please select the District.">  
        </asp:RequiredFieldValidator>
         <asp:RequiredFieldValidator ID="rqdddPsName"  runat="server" InitialValue="--Select--" ControlToValidate="ddPsName" ErrorMessage="Please select the PS.">  
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rqdddVehTypeName"  runat="server" InitialValue="--Select--" ControlToValidate="ddVehTypeName" ErrorMessage="Please select the Vehicle Type.">  
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rqdddMakeName"  runat="server" InitialValue="--Select--" ControlToValidate="ddMakeName" ErrorMessage="Please select the Make/Model of Vehicle.">  
        </asp:RequiredFieldValidator>
       <asp:RequiredFieldValidator ID="rqdtxtFIRNo"  runat="server"  ControlToValidate="txtFIRNo" ErrorMessage="Please enter the FIR No.">  
        </asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="rqdtxtFIRDt"  runat="server"  ControlToValidate="txtFIRDt" ErrorMessage="Please enter the FIR Date">  
        </asp:RequiredFieldValidator>
           <asp:RequiredFieldValidator ID="rqdtxtRegistrationno"  runat="server"  ControlToValidate="txtRegistrationno" ErrorMessage="Please enter the Registration No.">  
        </asp:RequiredFieldValidator> 
            <asp:RequiredFieldValidator ID="rqdtxtChasisno"  runat="server"  ControlToValidate="txtChasisno" ErrorMessage="Please enter the Chasis No.">  
        </asp:RequiredFieldValidator> 
        <asp:RequiredFieldValidator ID="rqdtxtEngineno"  runat="server"  ControlToValidate="txtEngineno" ErrorMessage="Please enter the Engine No.">  
        </asp:RequiredFieldValidator>
                    <cc1:textboxwatermarkextender ID="tbwm1" runat = "server" 
                                  TargetControlID="txtRegistrationno" WatermarkText="Enter Registration No">
                              </cc1:textboxwatermarkextender>
                    <cc1:textboxwatermarkextender ID="TextBoxWatermarkExtender1" runat = "server" 
                                  TargetControlID="txtChasisno" WatermarkText="Enter Chasis No">
                              </cc1:textboxwatermarkextender>
                    <cc1:textboxwatermarkextender ID="TextBoxWatermarkExtender2" runat = "server" 
                                  TargetControlID="txtEngineno" WatermarkText="Enter Engine No">
                              </cc1:textboxwatermarkextender>
                    <cc1:textboxwatermarkextender ID="TextBoxWatermarkExtender3" runat = "server" 
                                  TargetControlID="txtFIRno" WatermarkText="Enter FIR No">
                              </cc1:textboxwatermarkextender>
                    <cc1:textboxwatermarkextender ID="TextBoxWatermarkExtender4" runat = "server" 
                                  TargetControlID="txtFIRdt" WatermarkText="Enter FIR date">
                              </cc1:textboxwatermarkextender>
                    <cc1:textboxwatermarkextender ID="TextBoxWatermarkExtender5" runat = "server" 
                                  TargetControlID="txtStolendt" WatermarkText="Enter date of occurance">
                              </cc1:textboxwatermarkextender>
              
                </td>
                         </tr>
                         <tr><td colspan="5">
        <asp:Comparevalidator id="comptoday" runat="server" Operator="LessThanEqual" 
                                 ControlToValidate="txtFIRDt"  ControlToCompare="datetoday" 
                                 ErrorMessage="Fir date should be less than or equal to current date" 
                                 ValidationGroup="ctr" SetFocusOnError="true" Type="Date" ></asp:Comparevalidator>
        <asp:CompareValidator id="compstolen" runat="server" Operator="LessThanEqual"  
                                 ControlToValidate ="txtStolenDt" ControlToCompare="txtFIRDt" 
                                 ErrorMessage="Occurance Date Should be less than or equal to FIR date" 
                                 ValidationGroup="ctr" SetFocusOnError="true" Type="Date"  ></asp:CompareValidator>
                         
                         </td></tr>
                         <tr><td colspan="5" style="text-align: center">
                             <asp:HyperLink ID="HelpInsurance" runat="server" 
                                 onclick="openNewWindowshelp()" style="cursor:pointer; text-decoration:underline; color:Blue">Help</asp:HyperLink>
                         
                         </td></tr>
                     </table>
        </td> </tr> </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>