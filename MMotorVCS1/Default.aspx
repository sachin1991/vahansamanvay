﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"  MasterPageFile="~/MasterPage.master" Theme="emsTheme" MaintainScrollPositionOnPostback="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
<ContentTemplate >
    <div>
       
      <table>
      <tr>
      <td>Auto Type</td><td><asp:DropDownList ID="ddAutoType" runat="server">
          </asp:DropDownList></td>
          
      </tr>
          <tr>
      <td>Make Type</td><td><asp:DropDownList ID="ddMake" runat="server">
          </asp:DropDownList></td>
          
      </tr>
           <tr>
      <td>Registration No</td><td><asp:TextBox ID="txtReg" runat="server"></asp:TextBox></td>
         
      </tr>
           <tr>
      <td>Chassis No</td><td><asp:TextBox ID="txtChasis" runat="server"></asp:TextBox></td>
         
      </tr>
         <tr>
      <td>Engine No</td><td><asp:TextBox ID="txtEngine" runat="server"></asp:TextBox></td>
         
      </tr>
          <tr>
              <td>
                  &nbsp;</td>
              <td>
                  <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" />
              </td>
          </tr>
<TR>
<TD colspan="2">
<asp:GridView id="gvForms" runat="server" Width="97%" 
        GridLines="Vertical" ForeColor="Black"  OnRowDataBound="gvForms_RowDataBound"
        CellPadding="4" ShowFooter="True"  AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
         
                    <Columns>                        
                        <asp:TemplateField HeaderText="FIRNO">
                            <ItemTemplate>
                                <asp:Label ID="lblFirId" runat="server" Text='<%# Eval("FIR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="VEHICLE&#160;STATUS">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtStatus" runat="server" Text='<%# Eval("Status") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtStatus1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderText="AUTO&#160;TYPE">
                            <EditItemTemplate>
                                <asp:DropDownList Width="50" runat="server" id="ddgAutoType" AutoPostBack="true" OnSelectedIndexChanged="ddgAutoType_SelectedIndexChanged"></asp:DropDownList> 
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAutoId" runat="server" Text='<%# Eval("autotypedesc") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtAutoId1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MAKE&#160;TYPE">
                            <EditItemTemplate>
                                  <asp:DropDownList Width="50" runat="server" id="ddgMake"></asp:DropDownList> 
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblMakeLink" runat="server" Text='<%# Eval("automakedesc") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtMakeLink1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="REGISTRATION&#160;">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRegistration" runat="server" Text='<%# Eval("regnokey") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRegistration" runat="server" Text='<%# Eval("regnokey") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRegistration1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CHASIS&#160;NO">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRegistration" runat="server" Text='<%# Eval("chasisnokey") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRegistration" runat="server" Text='<%# Eval("chasisnokey") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRegistration1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ENGINE&#160;NO">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRegistration" runat="server" Text='<%# Eval("enginenokey") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblRegistration" runat="server" Text='<%# Eval("enginenokey") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtRegistration1" runat="server" Visible="false"></asp:TextBox>
                            </FooterTemplate>
                            <ItemStyle Width="160px" />
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Edit / Delete"
                            ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:ImageButton ID="lbInsert" runat="server" CommandName="Insert" ImageUrl="~/img/updt_icon.gif"
                                    ToolTip="Insert" Visible="false" />
                                <asp:Button ID="lbAdd" runat="server" CausesValidation="False" CommandName="Add"
                                    CssClass="button" Text="Add" ToolTip="Add New" />
                                <asp:ImageButton ID="lbCancel" runat="server" CausesValidation="False" CommandName="CancelInsert"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" Visible="false" />
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />&nbsp;&nbsp;
                                <asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False" CommandName="Delete"
                                    ImageUrl="~/img/delete_icon.jpg" ToolTip="Delete" />    
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>                        
                                           
                    </Columns>                    
                    <FooterStyle BackColor="#CCCC99" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> &nbsp; </TD></TR>

</table>
    </div>

    


</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

