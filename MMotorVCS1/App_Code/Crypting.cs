﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Crypting
/// </summary>
public class Crypting
{
public static string Encrypt(string raw)
{
    using (RijndaelManaged csp = new RijndaelManaged())
    {
        ICryptoTransform e = GetCryptoTransform(csp, true);
        byte[] inputBuffer = Encoding.UTF8.GetBytes(raw);
        byte[] output = e.TransformFinalBlock(inputBuffer, 0, inputBuffer.Length);
        string encrypted = Convert.ToBase64String(output);
        return encrypted;
    }
}
public static string Decrypt(string encrypted)
{
    using (RijndaelManaged csp = new RijndaelManaged())
    {
        ICryptoTransform  d = GetCryptoTransform(csp, false);
        byte[] output = Convert.FromBase64String(encrypted);
        byte[] decryptedOutput = d.TransformFinalBlock(output, 0, output.Length);
        string decypted = Encoding.UTF8.GetString(decryptedOutput);
        return decypted;
    }
}
public static ICryptoTransform GetCryptoTransform(RijndaelManaged csp, bool encrypting)
{
    csp.Mode = CipherMode.CBC;
    csp.Padding = PaddingMode.PKCS7;
    String passWord = "Pass@word1";
    String salt = "S@1tS@lt";
    //a random Init. Vector. just for testing
    //String iv = "e675f725e675f725";
    String iv = "e675f725e675f726";
    Rfc2898DeriveBytes spec = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(passWord), Encoding.UTF8.GetBytes(salt), 65536);
    byte[] key = spec.GetBytes(16);
    csp.IV = Encoding.UTF8.GetBytes(iv);
    csp.Key = key;
    if (encrypting)
    {
        return csp.CreateEncryptor();
    }
    return csp.CreateDecryptor();
}
}