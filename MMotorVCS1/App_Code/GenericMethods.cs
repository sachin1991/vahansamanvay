using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Security;

/// <summary>
/// Summary description for GenericMethods
/// </summary>
public class GenericMethods
{
	public GenericMethods()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string check(String s)
    {
        String encode;
        encode = Microsoft.Security.Application.AntiXss.HtmlEncode(s);
        return (encode);
    }
    public static string decodecheck(String s)
    {
        String encode;
        encode = HttpUtility.HtmlDecode(s);
        return (encode);
    }
}
