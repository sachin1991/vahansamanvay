using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Entry_FormMethods
/// </summary>
public class Entry_FormMethods
{
    private string conStr;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    DataSet ds;
    Connection Mycon = new Connection();
    string strfilter;

    public DataSet getStateReport()
    {
        DataSet dsStateReport = new DataSet();
        dsStateReport = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_state");
        return dsStateReport;
    }

    public DataSet getRecordEdit_Del(string autoType, string make, string Reg, string Chasis, string Engine)
    {
        DataSet dsStateReport = new DataSet();
        dsStateReport = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_Edit_Delte");
        return dsStateReport;
    }

    public DataSet getCounterHeader1(string strusername)
    {
        DataSet dsGetHeader = new DataSet();
        dsGetHeader = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_getCounterHeaderFooter", strusername);
        return dsGetHeader;
    }
    public DataSet getemail(string strusername)
    {
        DataSet dsGetHeader = new DataSet();
        dsGetHeader = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_getemail", strusername);
        return dsGetHeader;
    }

    public DataSet getEditDelete(string strUserName)
    {
        DataSet dsStateReport = new DataSet();
        dsStateReport = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_Edit_Delte", strUserName);
        return dsStateReport;
    }
    public DataSet getEditDeleteadmin(string stregno, string strchasisno, string strengino)
    {
        DataSet dsStateReport = new DataSet();
        dsStateReport = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_Delte", stregno, strchasisno, strengino);
        return dsStateReport;
    }
    public DataSet getEditDeleteuser(string stregno, string strchasisno, string strengino, string username)
    {
        DataSet dsStateReport = new DataSet();
        dsStateReport = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_Delteuser", stregno, strchasisno, strengino, username);
        return dsStateReport;
    }
    public Entry_FormMethods()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    # region Methods for Lost Entry Form

    public DataSet AddEntry_LostRecovered(string stateid, string disid, string PsId, string strStatus, string firgd, string firno, DateTime firDt, string act1id, string act2id, string act3id, string a1sec1, string a1sec2, string a1sec3, string a1sec4, string a2sec1, string a2sec2, string a2sec3, string a2sec4, string a3sec1, string a3sec2, string a3sec3, string a3sec4, DateTime stlDt, string vehtypeId, string makeId, string modId, string colId, string regno, string chano, string engno, string yrofmanu, string crimeno, string _userid, string capcha, string hostadd, string clientadd)
    {
        try
        {
            if (regno == "")
            {
                regno = "@";
            }
            if (chano == "")
            {
                chano = "@";
            }

            if (engno == "")
            {
                engno = "@";
            }

            if (makeId == "")
            {
                makeId = "@";
            }


            if (strStatus == "L")
            {
                strfilter = "R";
            }
            else if (strStatus == "R")
            {
                strfilter = "L";
            }
            else if (strStatus == "C")
            {
                strfilter = "L";
            }
            else if (strStatus == "S")
            {
                strfilter = "L";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            string msq;
            msq = stateid.Trim() + "," +
                                       disid.Trim() + "," +
                                       PsId.Trim() + "," +
                                       strStatus.Trim() + "," +
                                       firgd.Trim() + "," +
                                       firno.Trim() + "," +
                                      firDt.ToString().Trim() + "," +
                                       act1id.Trim() + "," +
                                       act2id.Trim() + "," +
                                       act3id.Trim() + "," +
                                       a1sec1.Trim() + "," +
                                       a1sec2.Trim() + "," +
                                       a1sec3.Trim() + "," +
                                       a1sec4.Trim() + "," +
                                       a2sec1.Trim() + "," +
                                       a2sec2.Trim() + "," +
                                       a2sec3.Trim() + "," +
                                       a2sec4.Trim() + "," +
                                       a3sec1.Trim() + "," +
                                       a3sec2.Trim() + "," +
                                       a3sec3.Trim() + "," +
                                       a3sec4.Trim() + "," +
                                       stlDt.ToString().Trim() + "," +
                                       vehtypeId.Trim() + "," +
                                       makeId.Trim() + "," +
                                       modId.Trim() + "," +
                                       colId.Trim() + "," +
                                       regno.Trim() + "," +
                                       chano.Trim() + "," +
                                       engno.Trim() + "," +
                                       yrofmanu.Trim() + "," +
                                       crimeno.Trim() + "," +
                                       strfilter.Trim() + "," +
                                       _userid.Trim() + "," +
                                       capcha + "," +
                                       hostadd + "," +
                                       clientadd;
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_L_R_Details",
                                        stateid.Trim(),
                                        disid.Trim(),
                                        PsId.Trim(),
                                        strStatus.Trim(),
                                        firgd.Trim(),
                                        firno.Trim(),
                                       firDt.ToString().Trim(),
                                        act1id.Trim(),
                                        act2id.Trim(),
                                        act3id.Trim(),
                                        a1sec1.Trim(),
                                        a1sec2.Trim(),
                                        a1sec3.Trim(),
                                        a1sec4.Trim(),
                                        a2sec1.Trim(),
                                        a2sec2.Trim(),
                                        a2sec3.Trim(),
                                        a2sec4.Trim(),
                                        a3sec1.Trim(),
                                        a3sec2.Trim(),
                                        a3sec3.Trim(),
                                        a3sec4.Trim(),
                                        stlDt.ToString().Trim(),
                                        vehtypeId.Trim(),
                                        makeId.Trim(),
                                        modId.Trim(),
                                        colId.Trim(),
                                        regno.Trim(),
                                        chano.Trim(),
                                        engno.Trim(),
                                        yrofmanu.Trim(),
                                        crimeno.Trim(),
                                        strfilter.Trim(),
                                        _userid.Trim(),
                                        capcha,
                                        hostadd,
                                        clientadd

                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                regno.Trim(),
            //                            chano.Trim(),
            //                            engno.Trim(),
            //                                "CCTNS",
            //        //strsource.Trim(),
            //                                hostadd,
            //                                clientadd,
            //                                ""
            //        //typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}


            return ds;


        }
        catch (Exception ex)
        {
            return null;
        }

    }
    public DataSet AddVisitor(string visitor)
    {
        try
        {

            DataSet ds = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "AddVisitor", visitor);
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }
    public DataSet AddInternetVisitor(string visitor)
    {
        try
        {

            DataSet ds = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "AddInternetVisitor", visitor);
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }
    public DataSet Addcountnoc(string cnt, string usr)
    {
        try
        {

            DataSet ds = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "Addcounteruserwise", cnt, usr);
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }


    public DataSet AddAuthorities(string vehtypeId, string makeId, string modId, string colId, string regno, string chano, string engno, string yrofmanu, string _userid, string capcha, string hostnam, string clientnam, DateTime createddate)
    {
        try
        {
            if (regno == "")
            {
                regno = "@";
            }
            if (chano == "")
            {
                chano = "@";
            }

            if (engno == "")
            {
                engno = "@";
            }

            if (makeId == "")
            {
                makeId = "@";
            }
            if (yrofmanu == "")
            {
                yrofmanu = "@";
            }

            capcha = "'" + capcha + "'";
            clientnam = "'" + clientnam + "'";
            hostnam = "'" + hostnam + "'";
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            String em;
            em = "usp_Ins_AuthorityDetailsfast " + vehtypeId.Trim() + "," + makeId.Trim() + "," + modId.Trim() + "," + colId.Trim() + "," + regno.Trim() + "," + chano.Trim();
            em = em + "," + engno.Trim() + ",'" + yrofmanu.Trim() + "'," + _userid.Trim() + "," + capcha + "," + hostnam + ",'" + clientnam + "'";




            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_AuthorityDetailsfast",
                                        vehtypeId.Trim(),
                                        makeId.Trim(),
                                        modId.Trim(),
                                        colId.Trim(),
                                        regno.Trim(),
                                        chano.Trim(),
                                        engno.Trim(),
                                        yrofmanu.Trim(),
                                        _userid.Trim(),
                                        capcha,
                                        hostnam,
                                        clientnam,
                                        createddate

                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                regno.Trim(),
            //                            chano.Trim(),
            //                            engno.Trim(),
            //                                "CCTNS",
            //    //strsource.Trim(),
            //                                hostnam,
            //                                clientnam ,
            //                                ""
            //    //typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return ds;


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddInsurancedetails(string stateid, string disid, string PsId, string strStatus, string firgd, string firno, DateTime firDt, DateTime stlDt, string vehtypeId, string makeId, string modId, string colId, string regno, string chano, string engno, string yrofmanu, string crimeno, string _userid, string capcha, string hostnam, string clientnam, DateTime dtime)
    {
        try
        {
            if (regno == "")
            {
                regno = "@";
            }
            if (chano == "")
            {
                chano = "@";
            }

            if (engno == "")
            {
                engno = "@";
            }

            if (makeId == "")
            {
                makeId = "@";
            }


            if (strStatus == "L")
            {
                strfilter = "R";
            }
            else if (strStatus == "R")
            {
                strfilter = "L";
            }
            else if (strStatus == "C")
            {
                strfilter = "L";
            }
            else if (strStatus == "S")
            {
                strfilter = "L";
            }
            if (yrofmanu == "--Select--")
            {
                yrofmanu = "@";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            string em;
            em = "usp_Ins_InsuranceDetails" + " " +
                                        stateid.Trim() + "," +
                                        disid.Trim() + "," +
                                        PsId.Trim() + "," +
                                        strStatus.Trim() + "," +
                                        firgd.Trim() + "," +
                                        firno.Trim() + ",'" +
                                       firDt.ToString().Trim() + "','" +
                                        stlDt.ToString().Trim() + "'," +
                                        vehtypeId.Trim() + "," +
                                        makeId.Trim() + "," +
                                        modId.Trim() + "," +
                                        colId.Trim() + "," +
                                        regno.Trim() + "," +
                                        chano.Trim() + "," +
                                        engno.Trim() + ",'" +
                                        yrofmanu.Trim() + "'," +
                                        crimeno.Trim() + "," +
                                        strfilter.Trim() + "," +
                                        _userid.Trim() + "," +
                                        capcha + "," +
                                        hostnam + ",'" +
                                        clientnam + "'";

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_InsuranceDetails",
                                        stateid.Trim(),
                                        disid.Trim(),
                                        PsId.Trim(),
                                        strStatus.Trim(),
                                        firgd.Trim(),
                                        firno.Trim(),
                                       firDt.ToString().Trim(),
                                        stlDt.ToString().Trim(),
                                        vehtypeId.Trim(),
                                        makeId.Trim(),
                                        modId.Trim(),
                                        colId.Trim(),
                                        regno.Trim(),
                                        chano.Trim(),
                                        engno.Trim(),
                                        yrofmanu.Trim(),
                                        crimeno.Trim(),
                                        strfilter.Trim(),
                                        _userid.Trim(),
                                        capcha,
                                        hostnam,
                                        clientnam,
                                        dtime
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                regno.Trim(),
            //                                chano.Trim(),
            //                            engno.Trim(),
            //                                "CCTNS",
            //                                hostnam,
            //                                clientnam,
            //                                ""
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return ds;


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    // txtAppName.Text.ToString().Trim(), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(),txtPinCode.Text.ToString().Trim(), ddVehTypeName.SelectedValue.ToString(), ddMakeName.ToString().Trim(), txtYear.Text.ToString(), ddColName.SelectedValue.ToString().Trim(), txtRegistration.Text.ToString().Trim(), txtChasis.Text.ToString().Trim(), txtEngine.Text.ToString().Trim(), strsource, strHostName, clientName, typeOfQuery
    public DataSet AddEntry_InternetSecondHand(string stateid, string disid, string PsId, string ApplicantName, string MobileNo, string Email, string Address, string PinCode, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strHostName, string clientName, string typeOfQuery)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Internet_Details",
                                        stateid.Trim(),
                                        disid.Trim(),
                                        PsId.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        Email.Trim(),
                                        Address.Trim(),
                                        PinCode.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strHostName.Trim(),
                                        clientName.Trim(),
                                        typeOfQuery.Trim()
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                strHostName.Trim(),
            //                                clientName.Trim(),
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return (ds);


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddEntry_InternetStolen(string stateid, string disid, string PsId, string ApplicantName, string MobileNo, string Email, string Address, string PinCode, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strHostName, string clientName, string typeOfQuery)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Internet_DetailsStolen",
                                        stateid.Trim(),
                                        disid.Trim(),
                                        PsId.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        Email.Trim(),
                                        Address.Trim(),
                                        PinCode.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strHostName.Trim(),
                                        clientName.Trim(),
                                        typeOfQuery.Trim()
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                strHostName.Trim(),
            //                                clientName.Trim(),
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return (ds);


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddEntry_PoliceEnquiry(string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strHostName, string clientName, string typeOfQuery)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_PoliceEnquiry",
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strHostName.Trim(),
                                        clientName.Trim(),
                                        typeOfQuery.Trim()
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                strHostName.Trim(),
            //                                clientName.Trim(),
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}
            return (ds);
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet checkcount(Int64 recieptid, string usernam)
    {
        try
        {
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "countReceipt",
                                        recieptid,
                                        usernam
                                     );

            return (ds);
        }
        catch (Exception ex)
        {
            return null;
        }


    }
    //strStateCode, txtAppName.Text.ToString().Trim(), txtmobno.Text.ToString().Trim(), txtresino.Text.ToString().Trim(), txtAppadd.Text.ToString().Trim(),  (ddVehTypeName.SelectedValue.Split('-')[0]), (MakeCode[1]), ddYearofManu.SelectedValue.ToString().Trim(), Color, opRegistration, opChasis, opEngine, strsource,  strUserName.ToString().Trim(),typeOfQuery,txtcashamt.Text.ToString().Trim(),txtIopNo.Text.ToString().Trim(),txtIPOamt.Text.ToString().Trim()  ,"","",txtFIRNo.Text.ToString().Trim(),txtFIRDt.Text.ToString().Trim()
    public DataSet AddEntry_CounterSecondHand(Int64 Recieptid, string stcode, string distcode, string pscode, string strStateCode, string ApplicantName, string MobileNo, string ResiPhNo, string Address, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strUserName, string typeOfQuery, string strcash, string IopNo, string IPOamt, string lostid, string recoveryid, string FIRNo, string FIRDt, string ipodate, DateTime dtime)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            if (FIRDt == "")
            {
                FIRDt = "1/1/1900";
            }
            if (FIRNo == "")
            {
                FIRNo = "0";
            }
            if (ipodate == "")
            {
                ipodate = "1/1/1900";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Counter_Details",
                                        Recieptid,
                                        stcode.Trim(),
                                        distcode,
                                        pscode,
                                        strStateCode.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        ResiPhNo.Trim(),
                                        Address.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strUserName.Trim(),
                                        typeOfQuery.Trim(),
                                        strcash,
                                        IopNo,
                                        IPOamt,
                                        lostid,
                                        recoveryid,
                                        FIRNo,
                                        FIRDt,
                                        ipodate,
                                        dtime
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                "",
            //                                "",
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return (ds);


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddEntry_CounterStolen(Int64 Recieptid, string stcode, string distcode, string pscode, string strStateCode, string ApplicantName, string MobileNo, string ResiPhNo, string Address, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strUserName, string typeOfQuery, string strcash, string IopNo, string IPOamt, string lostid, string recoveryid, string FIRNo, string FIRDt, string ipodate, DateTime dtime)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            if (FIRDt == "")
            {
                FIRDt = "1/1/1900";
            }
            if (FIRNo == "")
            {
                FIRNo = "0";
            }
            if (ipodate == "")
            {
                ipodate = "1/1/1900";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Counter_DetailsStolen",
                                        Recieptid,
                                        stcode.Trim(),
                                        distcode,
                                        pscode,
                                        strStateCode.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        ResiPhNo.Trim(),
                                        Address.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strUserName.Trim(),
                                        typeOfQuery.Trim(),
                                        strcash,
                                        IopNo,
                                        IPOamt,
                                        lostid,
                                        recoveryid,
                                        FIRNo,
                                        FIRDt,
                                        ipodate,
                                        dtime
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                "",
            //                                "",
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return (ds);


        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddEntry_InternetRecover(string stateid, string disid, string PsId, string ApplicantName, string MobileNo, string Email, string Address, string PinCode, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strHostName, string clientName, string typeOfQuery)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }
            if (MakeName == "")
            {
                MakeName = "@";
            }
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Internet_Details_Recover",
                                        stateid.Trim(),
                                        disid.Trim(),
                                        PsId.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        Email.Trim(),
                                        Address.Trim(),
                                        PinCode.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strHostName.Trim(),
                                        clientName.Trim(),
                                        typeOfQuery.Trim()
                                     );
            return ds;


        }
        catch (Exception ex)
        {
            return null;
        }

    }


    public DataSet AddEntry_CounterRecover(Int64 RecieptId, string stcode, string distcode, string pscode, string strStateCode, string ApplicantName, string MobileNo, string ResiPhNo, string Address, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strUserName, string typeOfQuery, string strcash, string IopNo, string IPOamt, string lostid, string recoveryid, string FIRNo, string FIRDt, string ipodate, DateTime dtime)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }
            if (FIRDt == "")
            {
                FIRDt = "1/1/1900";
            }
            if (FIRNo == "")
            {
                FIRNo = "0";
            }
            if (ipodate == "")
            {
                ipodate = "1/1/1900";
            }
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Counter_Details_Recover",
                                        RecieptId,
                                        stcode.Trim(),
                                        distcode.Trim(),
                                        pscode.Trim(),
                                         strStateCode.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        ResiPhNo.Trim(),
                                        Address.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strUserName.Trim(),
                                        typeOfQuery.Trim(),
                                        strcash,
                                        IopNo,
                                        IPOamt,
                                        lostid,
                                        recoveryid,
                                        FIRNo,
                                        FIRDt,
                                        ipodate,
                                        dtime
                                     );
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet AddEntry_Insurance_RTO_Recover(Int64 RecieptId, string stcode, string distcode, string pscode, string strStateCode, string ApplicantName, string MobileNo, string ResiPhNo, string Address, string VehTypeName, string MakeName, string Year, string Color, string Registration, string Chasis, string Engine, string strsource, string strUserName, string typeOfQuery, string strcash, string IopNo, string IPOamt, string lostid, string recoveryid, string FIRNo, string FIRDt)
    {
        try
        {
            if (Registration == "")
            {
                Registration = "@";
            }
            if (Chasis == "")
            {
                Chasis = "@";
            }

            if (Engine == "")
            {
                Engine = "@";
            }

            if (MakeName == "")
            {
                MakeName = "@";
            }

            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();


            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
              "usp_Ins_Counter_Details_Recover",
                                        RecieptId,
                                        stcode.Trim(),
                                        distcode.Trim(),
                                        pscode.Trim(),
                                         strStateCode.Trim(),
                                        ApplicantName.Trim(),
                                        MobileNo.Trim(),
                                        ResiPhNo.Trim(),
                                        Address.Trim(),
                                        VehTypeName.Trim(),
                                        MakeName.Trim(),
                                        Year.Trim(),
                                        Color.Trim(),
                                        Registration.Trim(),
                                        Chasis.Trim(),
                                        Engine.Trim(),
                                        strsource.Trim(),
                                        strUserName.Trim(),
                                        typeOfQuery.Trim(),
                                        strcash,
                                        IopNo,
                                        IPOamt,
                                        lostid,
                                        recoveryid,
                                        FIRNo,
                                        FIRDt
                                     );
            //try
            //{
            //    ds1 = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnCCTNS"].ToString(),
            //      "usp_cctns_query_recover",
            //                                Registration.Trim(),
            //                                Chasis.Trim(),
            //                                Engine.Trim(),
            //                                strsource.Trim(),
            //                                "",
            //                                "",
            //                                typeOfQuery.Trim()
            //                             );
            //    ds.AcceptChanges();
            //    ds.Merge(ds1);
            //}
            //catch
            //{
            //}

            return ds;


        }
        catch (Exception ex)
        {
            return null;
        }

    }


    public int UpdateEntry_Lost(int lostid, int stateid, int disid, int PsId, string firgd, string firno, DateTime firDt, DateTime stlDt, int vehtypeId, int makeId, int modId, string regno, string chano, string engno, string stolensta, string _userid)
    {
        string _query = string.Empty;
        if (stlDt != DateTime.MinValue)
        {
            _query = "update Entry_Lost set stateid=" + stateid + ",districtid=" + disid + ",psid=" + PsId + ",firgdsde='" + firgd + "',firno='" + firno + "',firdt='" + firDt + "',stolenDt='" + stlDt + "',vehicletypeId=" + vehtypeId + ",makeid=" + makeId + ",modelId=" + modId + ",registrationno='" + regno + "',chasisno='" + chano + "',engineno='" + engno + "',stolenstatus='" + stolensta + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + " where lostid=" + lostid + "";
        }
        else
        {
            _query = "update Entry_Lost set stateid=" + stateid + ",districtid=" + disid + ",psid=" + PsId + ",firgdsde='" + firgd + "',firno='" + firno + "',firdt='" + firDt + "',vehicletypeId=" + vehtypeId + ",makeid=" + makeId + ",modelId=" + modId + ",registrationno='" + regno + "',chasisno='" + chano + "',engineno='" + engno + "',stolenstatus='" + stolensta + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + " where lostid=" + lostid + "";

        }
        con = Mycon.MakeConnection();
        cmd = new SqlCommand(_query, con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int UpdateEntry_Lost_withoutModel(int lostid, int stateid, int disid, int PsId, string firgd, string firno, string firDt, string stlDt, int vehtypeId, int makeId, string regno, string chano, string engno, string crimeno, string _userid)
    {
        string _query = string.Empty;
        if (stlDt.Trim().Length != 0)
        {
            _query = "update Entry_Lost set stateid=" + stateid + ",districtid=" + disid + ",psid=" + PsId + ",firsde='" + firgd + "',firno='" + firno + "',firdt='" + Convert.ToDateTime(firDt) + "',stolenDt='" + Convert.ToDateTime(stlDt) + "',vehicletypeId=" + vehtypeId + ",makeid=" + makeId + ",registrationno='" + regno + "',chasisno='" + chano + "',engineno='" + engno + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + ",crime_no='" + crimeno + "' where lostid=" + lostid + "";
        }
        else
        {
            _query = "update Entry_Lost set stateid=" + stateid + ",districtid=" + disid + ",psid=" + PsId + ",firsde='" + firgd + "',firno='" + firno + "',firdt='" + Convert.ToDateTime(firDt) + "',vehicletypeId=" + vehtypeId + ",makeid=" + makeId + ",registrationno='" + regno + "',chasisno='" + chano + "',engineno='" + engno + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + ",crime_no='" + crimeno + "' where lostid=" + lostid + "";
        }
        con = Mycon.MakeConnection();
        cmd = new SqlCommand(_query, con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int getEntry_LostMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(LostId) MAX from Entry_lost", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    public DataSet getLostEntry_total()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from entry_lost ", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getEntry_Lost()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from entry_lost where (matchedstatus='N' ) and Isdeleted!='1'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getEntry_LostDetailsfast(string regno, string chno, string engno, int typeid, string CrimeNo)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("select count(lostid) from entry_lost where (matchedstatus='N' ) and Isdeleted='0' and vehicletypeid=" + typeid + "and Crime_no='" + CrimeNo + "'and (registrationno='" + regno + "' or chasisno='" + chno + "' or engineno='" + engno + "')", con);
        con.Open();
        int i = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        //da = new SqlDataAdapter("select * from entry_lost where (matchedstatus='N' ) and Isdeleted!='1' and vehicletypeid="+ typeid +" and (registrationno='" + regno + "' or chasisno='" + chno + "' or engineno='" + engno + "')", con);
        //DataSet ds = new DataSet();
        //da.Fill(ds);
        con.Close();
        return i;
    }



    public int getEntry_LostDetailsfast(string regno, string chno, string engno, int typeid, string CrimeNo, string status)
    {
        try
        {
            int inResult;
            inResult = Convert.ToInt32(MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_getLostCount", typeid, CrimeNo, regno, chno, engno, status));

            return inResult;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet getEntry_LostDetailsfastinsurance(string regno, string chno, string engno, int typeid, string CrimeNo, string status)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_getLostCountinsurance", typeid, CrimeNo, regno, chno, engno, "L");
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataSet sendmail(string profilename, string receipients, string bodytext, string subject)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.sendmailmvcs", profilename, receipients, bodytext, subject);
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public int getEntry_AuthorityDetailsfast(string regno, string chno, string engno, int typeid)
    {
        try
        {
            int inResult;
            inResult = Convert.ToInt32(MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_getLostCountAuthority", typeid, regno, chno, engno));

            return inResult;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet getEntry_LostDetails(string regno, string chno, string engno, int typeid)
    {
        con = Mycon.MakeConnection();
        //cmd = new SqlCommand("select count(lostid) from entry_lost where (matchedstatus='N' ) and Isdeleted!='1' and vehicletypeid=" + typeid + " and (registrationno='" + regno + "' or chasisno='" + chno + "' or engineno='" + engno + "')", con);
        //int i = Convert.ToInt32(cmd.ExecuteScalar().ToString());
        da = new SqlDataAdapter("select * from entry_lost where (matchedstatus='N' ) and Isdeleted!='1' and vehicletypeid=" + typeid + " and (registrationno='" + regno + "' or chasisno='" + chno + "' or engineno='" + engno + "')", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getEntry_Lost(string regno, string chno, string engno, int typeid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from entry_lost where vehicletypeid=" + typeid + " and (registrationno='" + regno + "'  or  chasisno='" + chno + "' or engineno='" + engno + "') and Isdeleted!='1'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getEntry_Lost_acc_date(string startDate, string endDate, string _userid)
    {
        con = Mycon.MakeConnection();
        string sqlQry = "select vehicle_type.vehicletypeid,vehicle_type.vehicletypename,make_of_vehicle.makeid,make_of_vehicle.makename,model_name.modelid,model_name.modelname,State_type.stateid,State_type.statename,district_type.districtid,district_type.districtname,policest_type.psid,policest_type.psname,entry_lost.firno,entry_lost.firdt,entry_lost.stolendt,entry_lost.registrationno,entry_lost.chasisno,entry_lost.engineno,entry_lost.lostid from Entry_Lost inner join state_type on state_type.stateid=entry_lost.stateid inner join district_type on district_type.districtid=entry_lost.districtid inner join policest_type on Policest_type.psid=entry_lost.psid inner join vehicle_type on vehicle_type.vehicletypeid=entry_lost.vehicletypeid inner join make_of_vehicle on make_of_vehicle.makeid=entry_lost.makeid left join model_name on model_name.modelid=entry_lost.modelid where entry_lost.matchedstatus='N' and entry_lost.Isdeleted!='1' and entry_lost.recent_user=" + int.Parse(_userid) + "";
        if (startDate.Trim().Length != 0)
        {
            sqlQry = sqlQry + " and entry_lost.created_date >='" + Convert.ToDateTime(startDate) + "'";
        }
        if (endDate.Trim().Length != 0)
        {
            sqlQry = sqlQry + " and entry_lost.created_date <= '" + Convert.ToDateTime(endDate).AddDays(1) + "'";
        }

        da = new SqlDataAdapter(sqlQry, con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet DeleteEntry_Lost_bind(string regno, string chno, string engno, int typeid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select vehicle_type.vehicletypeid,vehicle_type.vehicletypename,make_of_vehicle.makeid,make_of_vehicle.makename,model_name.modelid,model_name.modelname,State_type.stateid,State_type.statename,district_type.districtid,district_type.districtname,policest_type.psid,policest_type.psname,entry_lost.firno,entry_lost.firdt,entry_lost.stolendt,entry_lost.registrationno,entry_lost.chasisno,entry_lost.engineno,entry_lost.lostid from Entry_Lost inner join state_type on state_type.stateid=entry_lost.stateid inner join district_type on district_type.districtid=entry_lost.districtid inner join policest_type on Policest_type.psid=entry_lost.psid inner join vehicle_type on vehicle_type.vehicletypeid=entry_lost.vehicletypeid inner join make_of_vehicle on make_of_vehicle.makeid=entry_lost.makeid left join model_name on model_name.modelid=entry_lost.modelid where ((entry_lost.registrationno Like '%" + regno + "%') and (entry_lost.chasisno Like '%" + chno + "%')  and (entry_lost.engineno Like '%" + engno + "%')) and entry_lost.vehicletypeid=" + typeid + " and Isdeleted!='1'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int DeleteEntry_Lost(int lostid, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update entry_lost set isdeleted='1',deleted_date=getdate(),deleted_user=" + int.Parse(_userid) + "  where lostid=" + lostid + "", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }

    public DataSet getLostEntry_accdeletion()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from entry_lost where Isdeleted='1'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getLostEntry_acclostid(int lostid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from entry_lost where lostId=" + lostid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    #endregion

    #region Methods for Counter Enquiry Form

    public int getCounter_EnqMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(CounterEnqId) MAX from Counter_Enquiry", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }

    public DataSet getRecovery_Detail(string regno, string chno, string engno, int typeid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from Recovery_Details where (registrationno='" + regno + "' or chasisno='" + chno + "' or engineno='" + engno + "') and vehicletypeid=" + typeid + " and Isdeleted!='1'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;

    }
    public int AddCounter_Detail_Inv(int countid, int receid, string appname, string mobno, string resino, string appadd, DateTime EnqDt, string cashamt, string ipono, string ipoamt, int lostid, string _userid)
    {

        try
        {
            //if (Registration == "")
            //{
            //    Registration = "@";
            //}
            //if (Chasis == "")
            //{
            //    Chasis = "@";
            //}

            //if (Engine == "")
            //{
            //    Engine = "@";
            //}
            //DataSet ds = new DataSet();

            //ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
            //  "usp_Ins_Internet_Details_RecoveryStatus",
            //                            stateid.Trim(),
            //                            disid.Trim(),
            //                            PsId.Trim(),
            //                            ApplicantName.Trim(),
            //                            MobileNo.Trim(),
            //                            Email.Trim(),
            //                            Address.Trim(),
            //                            PinCode.Trim(),
            //                            VehTypeName.Trim(),
            //                            MakeName.Trim(),
            //                            Year.Trim(),
            //                            Color.Trim(),
            //                            Registration.Trim(),
            //                            Chasis.Trim(),
            //                            Engine.Trim(),
            //                            strsource.Trim(),
            //                            strHostName.Trim(),
            //                            clientName.Trim(),
            //                            typeOfQuery.Trim()
            //                         );

            return 1;


        }
        catch (Exception ex)
        {
            return 1;
        }
        //con = Mycon.MakeConnection();
        //cmd = new SqlCommand("insert into Counter_Enquiry(counterEnqid,receiptid,appname,mobno,resino,appadd,enqdt,CashAmt,ipono,ipoamt,lostid,created_date,recent_user) values("+countid+","+receid+",'"+appname+"','"+mobno+"','"+resino+"','"+appadd+"','"+EnqDt+"','"+cashamt+"','"+ipono+"','"+ipoamt+"',"+lostid+",getdate()," + int.Parse(_userid) + ")", con);
        //con.Open();
        //int i = cmd.ExecuteNonQuery();
        //con.Close();
        //return i;
    }
    public int AddCounter_Detail_Rec(int countid, int receid, string appname, string mobno, string resino, string appadd, DateTime EnqDt, string cashamt, string ipono, string ipoamt, int recid, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into Counter_Enquiry(counterEnqid,receiptid,appname,mobno,resino,appadd,enqdt,cashamt,ipono,ipoamt,recoveryid,created_date,recent_user) values(" + countid + "," + receid + ",'" + appname + "','" + mobno + "','" + resino + "','" + appadd + "','" + EnqDt + "','" + cashamt + "','" + ipono + "','" + ipoamt + "'," + recid + ",getdate()," + int.Parse(_userid) + ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int UpdateCounter_Detail(int countid, int recid, string appname, string mobno, string resino, string appadd, int inscompid, DateTime EnqDt, int lostid, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update Counter_Enquiry set receiptid=" + recid + ",appname='" + appname + "',mobno='" + mobno + "',resino='" + resino + "',appadd='" + appadd + "',inscompid=" + inscompid + ",enqdt='" + EnqDt + "',lostid=" + lostid + ",modified_date=getdate(),recent_user=" + int.Parse(_userid) + " where CounterEnqId=" + countid + " ", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public DataSet getCounterEnquiry()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from TblTran_Counter_Enquiry", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getReceiptMaxNo(string vcUsercode)
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(inReceiptId) MAX from TblTran_Counter_Enquiry where vcCounterCode= '" + vcUsercode + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    #endregion

    #region Login Details

    public DataSet Login_User(string UserName)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from User_Details where Username='" + UserName + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet UserStateDetails(string UserName)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from TblMst_LoginDetails where vcUsername='" + UserName + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    #endregion

    #region User Details

    public int Change_Password(int userid, string password, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update User_Details set password='" + password + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + " where userid=" + userid + " ", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }

    public DataSet User_details_acc_Id(string userid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select * from User_Details where UserId='" + int.Parse(userid) + "'", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }

    public DataSet getUser_Details()
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select P_roles.role_id,p_roles.role_name,state_type.stateid,state_type.statename,User_Details.userid,User_Details.username,User_Details.password,User_Details.isactive,User_Details.Logon_Name,User_Details.watermark,User_Details.email from User_Details inner join P_roles on p_roles.role_id=User_Details.role_id inner join state_type on state_type.stateid=user_details.stateid", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public DataSet getUser_Details_for_pass_updation(int stateid)
    {
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select User_Details.userid,User_Details.username,User_Details.password from User_Details where stateid=" + stateid + "", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
    }
    public int getUserDetailMaxId()
    {
        int max = 0;
        con = Mycon.MakeConnection();
        da = new SqlDataAdapter("select max(inLoginId) MAX from TblMst_LoginDetails", con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0]["MAX"].ToString() != "")
        {
            max = Convert.ToInt32(ds.Tables[0].Rows[0]["MAX"].ToString());
            max = max + 1;
        }
        else
        {
            max = max + 1;
        }
        return max;
    }
    public int AddUser_Detail(int userid, string name, string password, int urole, int ac, string logname, string watermark, int sid, string email, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("insert into TblMst_LoginDetails(vcStateCode,inRoleId,vcUserName,vcPassword,vcType,vcFileNumber,vcAddress,vcAuthority,boEnabled) values(" + userid + ",'" + name + "','" + password + "'," + urole + "," + ac + ",'" + logname + "','" + watermark + "'," + sid + ",'" + email + "',getdate()," + int.Parse(_userid) + ")", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int UpdateUser_Detail(string name, string fileno, string Address1, string authsignature1, string email, string cashamount, string vcheader)
    {
        Regex re = new Regex("[;\\/:*?\"<>|&'%^]'");
        con = Mycon.MakeConnection();


        //string sql = "update TblMst_LoginDetails set vCusername='" + name + "',vCfilenumber='" + fileno + "',vcAddress='" + Address1 + "',vcAuthority='" + authsignature1 +"',vCmail='" + email + "',cashamount = '" + cashamount + "',vcHeader = '" + vcheader + "' where vcUsername = '" + name + "'"; //+ int.Parse(_userid) + " ";
        string sql = "update TblMst_LoginDetails set vCusername=@vcname,vCfilenumber=@filenumber,vcAddress=@address,vcAuthority=@authority,vCmail=@email,cashamount = @cashamount,vcHeader = @vcheader where vcUsername = @vcname"; //+ int.Parse(_userid) + " ";
        cmd = new SqlCommand(sql, con);
        cmd.Parameters.AddWithValue("vcname", name);
        cmd.Parameters.AddWithValue("filenumber", fileno);
        cmd.Parameters.AddWithValue("address", Address1);
        cmd.Parameters.AddWithValue("authority", authsignature1);
        cmd.Parameters.AddWithValue("email", email);
        if (cashamount.ToString().Contains("[;\\/:*?\"<>|&'%^]'") || cashamount == "")
        {
            cmd.Parameters.AddWithValue("cashamount", "0");
        }
        else
        {
            cmd.Parameters.AddWithValue("cashamount", cashamount);
        }
        cmd.Parameters.AddWithValue("vcheader", vcheader);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    public int UpdateUser_Password(int userid, string username, string password, string _userid)
    {
        con = Mycon.MakeConnection();
        cmd = new SqlCommand("update TblMst_LoginDetails set username='" + username + "',password='" + password + "',modified_date=getdate(),modified_user=" + int.Parse(_userid) + " where userid=" + userid + " ", con);
        con.Open();
        int i = cmd.ExecuteNonQuery();
        con.Close();
        return i;
    }
    #endregion

    #region Recovery



    #endregion
}
