﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Text.RegularExpressions;



namespace MMotorVCS
{

    public class clsPageBase
    {
        #region Private Data members

        private string strLoginCode;
        private string strPassword;
        private string strStateCode;
        private string strStateCode1;
        private string strDistrictCode;
        private string strPSCode;
        private int inRoleId;
        private string strLoginType;

        #endregion

        public clsPageBase()
        {

            if (HttpContext.Current.Session["Pagebase"] != null)
            {
                clsPageBase obj = (clsPageBase)HttpContext.Current.Session["Pagebase"];

                strLoginCode = obj.UserName;
                strPassword = obj.Password;
                strStateCode = obj.strStateCode;
                strLoginType = obj.strLoginType;
            }

        }
        #region Property Functions


        public string UserName
        {
            set { strLoginCode = value; }
            get { return strLoginCode; }
        }


        public string Password
        {
            set { strPassword = value; }
            get { return strPassword; }
        }

        public int RoleId
        {
            set { inRoleId = value; }
            get { return inRoleId; }
        }

        public string StateCode
        {
            set { strStateCode = value; }
            get { return strStateCode; }
        }

        public string LoginType
        {
            set { strLoginType = value; }
            get { return strLoginType; }
        }

        public string DistrictCode
        {
            set { strDistrictCode = value; }
            get { return strDistrictCode; }
        }

        public string StateCode1
        {
            set { strStateCode1 = value; }
            get { return strStateCode1; }
        }

        public string PSCode
        {
            set { strPSCode = value; }
            get { return strPSCode; }
        }

        #endregion


        #region " Global Procedures "

        public DataTable IsValidLoginCode(string strlogincode, string strPasswd)
        {
            DataTable dtLoginConfigData = new DataTable();

            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@LoginCode", SqlDbType.VarChar, 20);
                param[0].Value = strlogincode;
                param[1] = new SqlParameter("@Password", SqlDbType.VarChar, 2000);
                param[1].Value = strPasswd;

                //BusinessLogic BL = new BusinessLogic();
                dtLoginConfigData = MMotorVCS.DTask.ExecuteDataTable(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_CheckLoginDetails", strlogincode, strPasswd);
            }
            catch
            {
            }
            return dtLoginConfigData;

        }

        public DataTable ValidateUser(string strlogincode)
        {
            DataTable dtLoginConfigData = new DataTable();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@LoginCode", SqlDbType.VarChar, 20);
                param[0].Value = strlogincode;

                //BusinessLogic BL = new BusinessLogic();
                dtLoginConfigData = MMotorVCS.DTask.ExecuteDataTable(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_CheckLoginName", strlogincode);
            }
            catch
            {
            }
            return dtLoginConfigData;
        }

        public DataTable DashBoardCountersr(String strlogincode)
        {
            DataTable dtLoginConfigData = new DataTable();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@LoginCode", SqlDbType.VarChar, 20);
                param[0].Value = strlogincode;

                //BusinessLogic BL = new BusinessLogic();
                dtLoginConfigData = MMotorVCS.DTask.ExecuteDataTable(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_CheckLoginName", strlogincode);
            }
            catch
            {
            }
            return dtLoginConfigData;
        }
        public static string GetUserName(int inRoleId)
        {
            try
            {
                string strUsername = Convert.ToString(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_getUserName", inRoleId));

                return strUsername;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static string GetUserType(int inRoleId)
        {
            try
            {
                string strUsername = Convert.ToString(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_getUserType", inRoleId));

                return strUsername;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static int GetVerifyUserName(string vcUserName)
        {
            try
            {
                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_CheckUserName", vcUserName));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }

        public static DataTable GetRole()
        {
            try
            {
                DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_getRole");

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }
        public static DataTable getInusranceCod()
        {
            try
            {
                DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_getInsuranceCode");

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static DataTable GetFormname()
        {
            try
            {
                DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_getFormname");

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static DataTable GetUsername()
        {
            try
            {
                DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_getUserNames");

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static DataTable GetComboboxoption()
        {
            try
            {
                DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_getComboBox");

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static int InsertQuestion(string vcUserName, string Q1, string Q2, string A1, string A2, string pp)
        {
            try
            {
                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_InsertQuestion", vcUserName, Q1, Q2, A1, A2, pp));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }
        public static int removealreadyloggedin(string vcUserName, string clientadr)
        {
            try
            {
                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "removealreadylogged", vcUserName, clientadr));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }
        public static int deletealreadyloggedin(string vcUserName)
        {
            try
            {
                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_deletealreadyloggedin", vcUserName));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }


        public static int insertloggedin(string vcUserName)
        {
            try
            {


                string strHostName = System.Net.Dns.GetHostName();
                IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                HttpRequest currentRequest = HttpContext.Current.Request;
                String clientIP1 = currentRequest.ServerVariables["REMOTE_HOST"];
                string clientName = clientIP1;


                //clientName = Session["ClientAddr"].ToString().Trim();



                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_loggedinNEW", vcUserName, DateTime.Now.ToLongTimeString(), clientName));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }

        public static int ChkQuestion(string vcUserName)
        {
            try
            {
                int inUsername = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_chkQuestion", vcUserName));

                return inUsername;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }
        public static int Chkuser(string vcUserName)
        {
            try
            {
                int logAvailable = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_CheckLoginAvailable", vcUserName));

                return logAvailable;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }
        public static int Chkalreadylogedin(string vcUserName)
        {
            try
            {
                int alreadylogAvailable = Convert.ToInt32(MMotorVCS.DTask.ExecuteScalar(
                   System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "usp_CheckAlreadyLoggedin", vcUserName));

                return alreadylogAvailable;

            }
            catch (Exception ex)
            {
                return 1;

            }
        }
        public static DataSet getUserLog_Details(string username, string formid)
        {
            try
            {
                DataSet dtAct = MMotorVCS.DTask.ExecuteDataset(
                    System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                    "usp_GetUserAuditLog", username, formid);

                return dtAct;

            }
            catch (Exception ex)
            {
                return null;

            }
        }


        //public static int AddUser_Detail(string UserName, string password, string urole, string ac, string vcStateCode, string vcStateCode1, string vcDistrictCode, string vcPSCode, string vcFile, string vcAddress, string vcAuth, int Amt, string vcSignature,  string email)
        public static int AddUser_Detail(string UserName, string password, string urole, string ac, string vcStateCode, string vcStateCode1, string vcDistrictCode, string vcPSCode, string vcFile, string vcAddress, string vcAuth, int Amt, string vcSignature, string email)
        {
            Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
            if (vcFile != "" && vcAddress != "" && vcAuth != "" && vcSignature != "")
            {
                string sqlstr;
                sqlstr = "dbo.usp_Ins_UserName  '" + UserName.Trim() + "','" + password.Trim() + "','" + urole.Trim() + "','" + ac.Trim() + "','" + vcStateCode.Trim() + "','" + vcStateCode1.Trim() + "','" + vcDistrictCode.Trim() + "','" + vcPSCode.Trim() + "','" + vcFile.Trim() + "','" + vcAddress.Trim() + "','" + vcAuth.Trim() + "','" + Amt + "','" + vcSignature.Trim() + "','" + email.Trim() + "'";
                try
                {
                    int amount=0;
                    if (Amt.ToString().Contains("[;\\/:*?\"<>|&'%^]'") || Amt.ToString()  == "")
                    {
                        amount = 0;
                    }
                    else
                    {
                        amount = Amt;
                    }
                    MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                       "dbo.usp_Ins_UserName",
                                                UserName.Trim().ToString(),
                                                password.Trim().ToString(),
                                                urole.Trim().ToString(),
                                                ac.Trim().ToString(),
                                                vcStateCode.Trim(),
                                                vcStateCode1.Trim(),
                                                vcDistrictCode.Trim(),
                                                vcPSCode.Trim(),
                                                re.Replace(vcFile.Trim().ToString(), ""),
                                                re.Replace(vcAddress.Trim().ToString(), ""),
                                                re.Replace(vcAuth.Trim().ToString(), ""),
                                                amount,
                                                re.Replace(vcSignature.Trim().ToString(), ""),
                                                email.Trim().ToString());

                    return 1;

                }

                catch (Exception ex)
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        public static int AddUser_Detail1(string UserName, string urole, string ac, string vcStateCode, string vcStateCode1, string vcDistrictCode, string vcPSCode, string insurancecode)
        {

            try
            {
                MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "dbo.usp_Ins_UserName1",
                                            UserName.Trim(),
                                            urole.Trim(),
                                            ac.Trim(),
                                            vcStateCode.Trim(),
                                            vcStateCode1.Trim(),
                                            vcDistrictCode.Trim(),
                                            vcPSCode.Trim(),
                                            insurancecode.Trim());

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int UpdateUser_Detail(string UserName, string password, string urole, string ac, string vcStateCode, string vcStateCode1, string vcDistrictCode, string vcPSCode, string vcFile, string vcAddress, string vcAuth, int Amt, string vcSignature, string email)
        {

            try
            {
                MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "dbo.usp_Upd_UserName1",
                                            UserName.Trim(),
                    //password.Trim(),
                                            urole.Trim(),
                                            ac.Trim(),
                                            vcStateCode.Trim(),
                                            vcStateCode1.Trim(),
                                            vcDistrictCode.Trim(),
                                            vcPSCode.Trim(),
                                            vcFile.Trim(),
                                            vcAddress.Trim(),
                                            vcAuth.Trim(),
                                            Amt,
                                            vcSignature.Trim(),
                                            email.Trim());

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public DataSet getTopMenu(int inRoleid)
        {

            DataSet dsMenu = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@inRoleId", SqlDbType.Int, 4);
                param[0].Value = inRoleid;

                //BusinessLogic BL = new BusinessLogic();
                dsMenu = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_getMenu", inRoleid);
            }
            catch
            {
            }
            return dsMenu;

        }

        public DataSet getSubMenu(int inRoleId, int inLevelId, int inParentId)
        {
            DataSet dsSubMenu = new DataSet();

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@inRoleId", SqlDbType.Int, 4);
            param[0].Value = inRoleId;
            param[1] = new SqlParameter("@inLevelId", SqlDbType.Int, 4);
            param[1].Value = inLevelId;
            param[2] = new SqlParameter("@inParentId", SqlDbType.Int, 4);
            param[2].Value = inParentId;


            dsSubMenu = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "usp_getSubMenu", inRoleId, inLevelId, inParentId);


            return dsSubMenu;
        }


        #endregion


        public static DataSet getPForms(int prParentId, int prLevelId)
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "dbo.usp_getFormNameFilter", prParentId, prLevelId);
            return ds;

        }



        public static DataSet getPRoleRights(int prParentId, int prLevelId, int roleid)
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "dbo.usp_getchildForm", prParentId, prLevelId, roleid);
            return ds;

        }

        public static DataSet getPRoleRights(int UserRoleId)
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "dbo.usp_getFormCriteria", UserRoleId);
            return ds;

        }
        /// <summary>
        /// Root Level Forms
        /// </summary>
        /// <param name="prParentId"></param>
        /// <returns></returns>
        public static DataSet getPForms()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "dbo.usp_getFormNames");
                return ds;

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        //ddlRole.SelectedValue.ToString()), int.Parse(_lblFrmId.Text), _view, _userid
        public static bool EditRole(int inRole, int inFormId, int _view, string _userid)
        {

            try
            {
                MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                   "dbo.usp_AssignUserRights",
                                             inRole,
                                             inFormId,
                                             _view,
                                             3);
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}

