using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;

/// <summary>
/// Summary description for clsIncident
/// </summary>
public class clsIncident
{
	public clsIncident()
	{
		//
		// TODO: Add constructor logic here
		//
	}

   
    public static DataTable GetQues()
    {
        try
        {
            DataTable dtState = MMotorVCS.DTask.ExecuteDataTable(
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                "usp_getQuestion");

            return dtState;

        }
        catch (Exception ex)
        {
            return null;

        }
    }
    public static DataTable GetUserName()
    {
        try
        {
            DataTable dtUserName = MMotorVCS.DTask.ExecuteDataTable(
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                "usp_getUserNamedropdown");

            return dtUserName;

        }
        catch (Exception ex)
        {
            return null;

        }
    }
    public static DataTable GetAct()
    {
        try
        {
            DataTable dtAct = MMotorVCS.DTask.ExecuteDataTable(
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                "usp_getAct");

            return dtAct;

        }
        catch (Exception ex)
        {
            return null;

        }
    }

    

    public static DataTable GetSection(string vcAct)
    {
        try
        {
            DataTable dtSection = MMotorVCS.DTask.ExecuteDataTable(
                System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
                "dbo.usp_getSection",vcAct.Trim().ToString());

            return dtSection;

        }
        catch (Exception ex)
        {
            return null;

        }
    }

    
    public static void ActSection(string FirNo, string Section, string Act)
    {
        try
        {
            MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "usp_Save_Incidents",
                                         FirNo,
                                         Section,
                                         Act
                                      );


        }
        catch (Exception ex)
        {
            // ex.ToString();
        }
       

    }

    

}
