//***********************************************************************************
//	Copyright			:	Copyright(C) NCRB. All Rights Reserved.		*
//                          This software is the confidential and proprietary       *
//                          information of HCL Technologies ("Confidential          *
//                          Information"). You shall not disclose such Confidential *
//                          Information and shall use it only in accordance with the*
//                          terms of the license agreement you entered into with    *
//                          NCRB.		                                *
// File Name			:	ExceptionManager.cs		                                *
// Project Name         :	MVCS						            *
// Description          :	This is class will act as Common Exceation Handler for  *
//                          all the modules for                                     *
// Version              :	1.0                                                     *
// Author               :	MOnika Thawani                                        *
// Created On           :	17-01-2013                                              *
//
// Modification History                                                             *
//*==================================================================================
// Modified By			:                                                           *
// Date					:                                                           *
// Reason				:                                                           *
//*==================================================================================
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Text;
using System.Web.Util;
using System.IO;
using System.Net.Mail;


/// <summary>
/// Summary description for EMSException
/// </summary>
namespace MMotorVCS
{
    [Serializable]
    public enum ErrorCodes
    {
        DTask = 1,
        FillListBox = 2,
        UnKnownError,
        MailManger = 4,
        UnauthorisedAccess,
        ResignationSave = 6,
        ResignationClearanceSave,
        SapSeparationSave = 8,
        LegalNoticeSave,
        ResiganationApprove = 10,
        ResignationInitiation,
        ReminderDetails = 12,
        IntimationDetails,
        SendPersonalMail = 14,
        SendMails,
        ResignationClose = 16,
        ResignationRetrieve,
        Revoke = 18,
        RetrieveRevoke,
        ResignationCategories = 20,
        ResignationReasons,
        EHSRetrieve = 22,
        EmailFormat = 23,
        Roles = 24,
        ConfigurationDetails = 25,
        URLDetails = 26,
        EmpUnInitiatedResignationList = 27,
        Managers = 28,
        DocumentUpload = 29,
        DocumentDownLoad = 30,
        IssueOfLetters = 31,
        GetMailID = 32,
        RetrieveForEmpDetails = 33,
        ClearningAgentCodes = 34,
        ClearanceDetails = 35,
        ReleaseExitInterview = 36,
        SubmitExitInterview = 37,
        SubmitExitDiscussion = 38,
        ExitInterviewQuestions = 39,
        ExitDiscussionQuestions = 40,
        OpinionPoll = 41,
        UI = 42,
        SelfStatus = 43,
        EmailArgument = 44,
        MailDetails = 45,
        ClearanceQuestions = 46,
        ClearanceSave = 47,
        RetrieveDetails = 48,
        RevokeForm = 49,
        LegalNoticeDetails = 50,
        SAPSynchronization = 51,
        RetrieveLetterDetails = 52,
        Encryption=53,
        Editing = 54,
        CancelError=55,
        UpdateError=56,
        DeleteError=57,
        InsertError=58,
        SortError=59,
        PageError=60


    }
    [Serializable]
    public class ExceptionManager
    {
        public const string EMSEXCEPTIONDATAKEY = "ErrMsg";
        public const string EMSEXCEPTIONCODEKEY = "ErrCode";

        public ExceptionManager()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        /// <summary>
        /// Overloaded function to assign message to Exception Data
        /// </summary>
        /// <param name="exException"></param>
        /// <param name="ecErrorCode"></param>
        /// <returns></returns>
        public static Exception GetError(Exception exException, ErrorCodes ecErrorCode)
        {
            if (exException.Data[ExceptionManager.EMSEXCEPTIONDATAKEY] != null)
            {
                return exException;
            }

            int inErroCode = (int)ecErrorCode;
            exException.Data.Add(ExceptionManager.EMSEXCEPTIONDATAKEY, System.Web.Configuration.WebConfigurationManager.AppSettings["E" + inErroCode.ToString()].ToString());
            exException.Data.Add(ExceptionManager.EMSEXCEPTIONCODEKEY, inErroCode.ToString());
            WriteLog(exException.ToString());
            return exException;


        }

        /// <summary>
        /// Over Loaded Function to Assign error messages to exception data
        /// </summary>
        /// <param name="exException"></param>
        /// <param name="strErrorMsg"></param>
        /// <returns></returns>
        public static Exception GetError(Exception exException, string strErrorMsg)
        {
            if (exException.Data[ExceptionManager.EMSEXCEPTIONDATAKEY] == null)
            {
                exException.Data.Add(ExceptionManager.EMSEXCEPTIONDATAKEY, strErrorMsg);
                return exException;
            }
            else
            {
                return exException;
            }

        }

        /// <summary>
        /// Function to Write Exception to System Event Log
        /// </summary>
        /// <param name="exException"></param>
        //public static void WriteToLog(Exception exException)
        //{
        //    EventLog evtLogger = new EventLog();
        //    try
        //    {
        //        if (!EventLog.SourceExists("EMS"))
        //        {
        //            EventLog.CreateEventSource("EMS", "EMS");
        //        }
        //        StringBuilder sbError = new StringBuilder();
        //        if (exException.Data[ExceptionManager.EMSEXCEPTIONDATAKEY] != null)
        //        {
        //            sbError.Append("Error Message : " + exException.Data[ExceptionManager.EMSEXCEPTIONDATAKEY].ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //        }

        //        if (exException.GetBaseException() != null)evt
        //        {
        //            sbError.Append("Message : " + exException.GetBaseException().Message.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append("Base Exception:" + exException.GetBaseException().StackTrace.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append("Source : " + exException.GetBaseException().Source.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //        }
        //        else
        //        {

        //            sbError.Append("Message: " + exException.Message.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append("Source : " + exException.Source.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //        }
        //        sbError.Append("Stack Trace :" + exException.StackTrace.ToString());
        //        //WriteLog(sbError);

        //        if (exException.InnerException != null)
        //        {
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append("Inner Exception Message : " + exException.InnerException.Message.ToString());
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append(System.Environment.NewLine);
        //            sbError.Append("Inner Exception Source : " + exException.InnerException.Source.ToString());
        //        }

        //        WriteLog(sbError.ToString());

        //        evtLogger.Source = "EMS";
        //        evtLogger.WriteEntry(sbError.ToString(0, sbError.Length), EventLogEntryType.Error);

        //    }
        //    catch (Exception exExceptionManager)
        //    {
        //        exExceptionManager = null;
        //    }

        //}
        private static void WriteLog(string text)
        {
            try
            {
                string filename = DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Today;
                filename = "TISErrorLogFile_" + filename + ".log";
                string temp = "#********************";
                temp += DateTime.Now.ToString();
                temp += "********************#";
                temp += System.Environment.NewLine;
                temp += text;
                temp += System.Environment.NewLine;
                temp += "#********************End";
                temp += "********************#";

                UpdateErrorLog(text);
                //StreamWriter sw = new StreamWriter((@System.Configuration.ConfigurationSettings.AppSettings["fileuploadpath"].ToString() + "\\" + filename), true);
                //StreamWriter sw = new StreamWriter((@"D:\\EMS_Ver2\\ems" + "\\" + filename), true);

                //sw.WriteLine("");
                //sw.WriteLine(temp);
                //sw.Close();

                
            }
            catch(Exception ex)
            {
            }
                
        }
        private static void UpdateErrorLog(string strError)
        {
            try
            {
                //EMS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["EmsConnectionString"].ToString()
                //                       , "exitsystem.USP_InsertToErrorLog", strError);

            }
            catch (Exception exGen)
            {
                
            }
        }

    }
}