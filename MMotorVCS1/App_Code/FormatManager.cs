using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

namespace MMotorVCS
{
    [Serializable]
   public class FormatManager
    {
            public DataTable GetURLConfigData(int mon)
       {
           DataTable dtLoginConfigData = new DataTable();

           try
           {
               SqlParameter[] param = new SqlParameter[1];
               param[0] = new SqlParameter("@inRoleId", SqlDbType.Int, 2);
               param[0].Value = mon;


               dtLoginConfigData = MMotorVCS.DTask.ExecuteDataTable(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(), "USP_GET_URLConfigData", mon);
           }
           catch
           {
           }
           return dtLoginConfigData;
       }
        
    }

}
