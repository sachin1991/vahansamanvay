using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;

/// <summary>
/// Summary description for CommonFunctions
/// </summary>
public class CommonFunctions
{
	public CommonFunctions()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int AddUser_Detail(int userid, string name, string password, int urole,
                              int ac, string logname, string sid, string DictID,
                              string PsID, string email)
    {
        try
        {
            MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_CreateUser",
                                       userid,
                                       name,
                                       password,
                                       urole,
                                       ac,
                                       logname,
                                       sid,
                                       DictID,
                                       PsID,
                                       email);
            return 1;

        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public DataSet Login_User(string UserName)
    {
        DataSet ds = new DataSet();
        ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
           "dbo.usp_fetchUserName", UserName);
        return ds;

    }

    public int UpdateUser_Detail(int userid, string name, string password, int urole,
                              int ac, string logname, string sid, string DictID,
                              string PsID, string email)
    {
        try
        {
            MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_UpdateCreatedUser",
                                       userid,
                                       name,
                                       password,
                                       urole,
                                       ac,
                                       logname,
                                       sid,
                                       DictID,
                                       PsID,
                                       email);
            return 1;

        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public DataSet getAllUser_Details()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_getUseddetails");
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }
    public int UpdateUser_Password(string userid, string username, string password, string statecode, string modifyingUser)
    {
        try
        {
            MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_UpdatePassword",
                                    
                                     username,
                                     password,
                                     statecode,
                                     modifyingUser);
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public string getOlddatabasepwd(string username)
    {
        try
        {
            string strOldpwd = MMotorVCS.DTask.ExecuteScalar(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_OldMd5Password", username).ToString();
           return strOldpwd;
        }
        catch (Exception ex)
        {
            return "0";
        }
    }
    

    public int ResetUser_Password( string username, string password, bool chkEnable,string modifyingUser )
    {
        try
        {
            MMotorVCS.DTask.ExecuteNonQuery(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_ResetPassword",

                                     username,
                                     password,
                                     chkEnable,
                                     modifyingUser);
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public bool ChkOld_Password(string username, string password)
    {
        try
        {
            bool inResult;
            inResult = Convert.ToBoolean(MMotorVCS.DTask.ExecuteScalar(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
             "dbo.usp_chkOldPassword",

                                     username,
                                     password));
            return inResult;
            
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public DataSet getUser_Details_for_pass_updation(string vcStateCode)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_getStateWiseUsers", vcStateCode);
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public DataSet getUser_Details(string vcStateCode)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = MMotorVCS.DTask.ExecuteDataset(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString(),
               "dbo.usp_getStateWiseUsers", vcStateCode);
            return ds;

        }
        catch (Exception ex)
        {
            return null;
        }

    }

    
    
}
