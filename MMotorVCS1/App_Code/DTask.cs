//***********************************************************************************
//	Copyright			:	Copyright(C) NCRB. All Rights Reserved.		*
//                          This software is the confidential and proprietary       *
//                          information of NCRB ("Confidential          *
//                          Information"). You shall not disclose such Confidential *
//                          Information and shall use it only in accordance with the*
//                          terms of the license agreement you entered into with    *
//                          HCL Technologies.		                                *
// File Name			:	CommonMethods.cs		                                *
// Project Name         :	Terrorist Information System						            *
// Description          :	This is class will act as Data Acees Layer class for    *
//                          wrapping all the database transations                    *
// Version              :	1.0                                                     *
// Author               :	Monika Thawani                                       *
// Created On           :	23-11-2010                                             *
//
// Modification History                                                             *
//*==================================================================================
// Modified By			:  Monika Thawani                                        *
// Date					:  11-12-20010                                               *
// Reason				:  Incorporating methods for handling transaction and       *
//                         Output parameters     
//*==================================================================================

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Security;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Collections.Generic;

namespace MMotorVCS
{
    public sealed class DTask
    {
        private DTask() { }
        /// <summary>
        /// This function will accept connection string as parameter and 
        /// retrun open connections
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <returns>SqlConnection</returns>
        public static SqlConnection GetConnection(string strConnectionString)
        {
            try
            {
                SqlConnection conDAL = new SqlConnection(strConnectionString);
                conDAL.ConnectionString = BuildConnectionString(strConnectionString);

                conDAL.Open();
                return conDAL;
            }

            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static string BuildConnectionString(string strConnectionString)
        {
            ConnectionStringSettings settings = new ConnectionStringSettings();
            settings.ConnectionString = strConnectionString;
            string connectionString = settings.ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
            return (builder.ConnectionString);
        }
        public static string BuildConnectionString()
        {

            ConnectionStringSettings settings = new ConnectionStringSettings();
            settings.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString();


            string connectString = settings.ConnectionString;


            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectString);

            return (builder.ConnectionString);


        }

        /// <summary>
        /// Function to attatch parameters to command object
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandParameters"></param>
        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            try
            {
                foreach (SqlParameter prmDAL in commandParameters)
                {

                    if ((prmDAL.Direction == ParameterDirection.InputOutput) && (prmDAL.Value == null))
                    {
                        prmDAL.Value = DBNull.Value;
                    }

                    command.Parameters.Add(prmDAL);
                }
            }
            catch (Exception exDTask)
            {

                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        // added by sunil

        public static DataSet ExecuteDataset_bandpayroll(string strConnectionString, string strSPName, params object[] parameterValues)
        {

            try
            {
                return ExecuteDataset(strConnectionString, strSPName, parameterValues);
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }



        // sunil

        /// <summary>
        /// Function to Assign values to parameters 
        /// </summary>
        /// <param name="commandParameters"></param>
        /// <param name="parameterValues"></param>

        private static void AssignParameterValues(SqlParameter[] commandParameters, object[] parameterValues)
        {
            try
            {
                if ((commandParameters == null) || (parameterValues == null))
                {
                    return;
                }

                if (commandParameters.Length != parameterValues.Length)
                {
                    throw new ArgumentException("Parameter count does not match Parameter Value count.");
                }

                for (int i = 0, j = commandParameters.Length; i < j; i++)
                {
                    commandParameters[i].Value = parameterValues[i];
                }
            }

            catch (Exception exDTask)
            {

                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);

            }
        }

        /// <summary>
        /// function to prepare comman for execution
        /// </summary>
        /// <param name="command"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction,
            CommandType commandType, string strCommandText, SqlParameter[] commandParameters)
        {

            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                command.Connection = connection;
                command.CommandText = strCommandText;
                command.CommandTimeout = 900;
                if (transaction != null)
                {
                    command.Transaction = transaction;
                }
                command.CommandType = commandType;
                if (commandParameters != null)
                {
                    AttachParameters(command, commandParameters);
                }
                return;
            }
            catch (Exception exDTask)
            {

                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string strConnectionString, CommandType commandType, string strCommandText,
            params SqlParameter[] commandParameters)
        {

            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {
                    conDAL.Open();
                    return ExecuteNonQuery(conDAL, commandType, strCommandText, commandParameters);
                }
            }
            catch (Exception exDTask)
            {

                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteNonQuery(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteNonQuery(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {

                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {

            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, (SqlTransaction)null, commandType, strCommandText, commandParameters);
                int retval = cmdDALCommand.ExecuteNonQuery();
                cmdDALCommand.Parameters.Clear();
                return retval;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(string strConnectionString, CommandType commandType, string strCommandText,
            params SqlParameter[] commandParameters)
        {

            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {
                    conDAL.Open();
                    return ExecuteOutParameterQuery(conDAL, commandType, strCommandText, commandParameters);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteOutParameterQuery(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteOutParameterQuery(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(SqlConnection connection, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {

            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, (SqlTransaction)null, commandType, strCommandText, commandParameters);
                int retval = cmdDALCommand.ExecuteNonQuery();
                return cmdDALCommand.Parameters;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns></returns>

        public static int ExecuteNonQuery(SqlConnection connection, SqlTransaction trans, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(connection.ConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteNonQuery(connection, trans, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteNonQuery(connection, trans, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(SqlConnection connection, SqlTransaction transaction, string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteNonQuery(connection, transaction, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteNonQuery(connection, transaction, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing insert,update,delete statements/procudures which does not return 
        /// any result sets
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {

            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, transaction, commandType, strCommandText, commandParameters);
                int retval = cmdDALCommand.ExecuteNonQuery();
                cmdDALCommand.Parameters.Clear();
                return retval;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(SqlConnection connection, SqlTransaction transaction, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(connection.ConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteOutParameterQuery(connection, transaction, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteOutParameterQuery(connection, transaction, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(SqlConnection connection, SqlTransaction transaction, string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString,
                        strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteOutParameterQuery(connection, transaction, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteOutParameterQuery(connection, transaction, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return
        /// results as out parameters
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>SqlParameterCollection</returns>
        public static SqlParameterCollection ExecuteOutParameterQuery(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {

            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, transaction, commandType, strCommandText, commandParameters);
                int retval = cmdDALCommand.ExecuteNonQuery();
                return cmdDALCommand.Parameters;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>DataSet</returns>
        public static DataSet ExecuteDataset(string strConnectionString, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {
                    conDAL.Open();
                    return ExecuteDataset(conDAL, commandType, strCommandText, commandParameters);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static SqlDataAdapter GetDataAdapter(string strConnectionString, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {

                    conDAL.Open();
                    SqlCommand cmdDALCommand = new SqlCommand();
                    PrepareCommand(cmdDALCommand, conDAL, (SqlTransaction)null, commandType, strCommandText, commandParameters);
                    SqlDataAdapter adaDAL = new SqlDataAdapter();
                    adaDAL.SelectCommand = cmdDALCommand;

                    conDAL.Close();
                    return adaDAL;
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        public static SqlCommandBuilder GetCommandBuilder(SqlDataAdapter adaDAL)
        {
            SqlCommandBuilder cmdBld = new SqlCommandBuilder(adaDAL);
            cmdBld.SetAllValues = false;
            return cmdBld;
        }
        public static SqlDataAdapter GetDataAdapter(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return GetDataAdapter(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return GetDataAdapter(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static DataSet FillDataSet(string strConnectionString, SqlDataAdapter adaDAL)
        {
            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {
                    conDAL.Open();
                    DataSet dSetDAL = new DataSet();
                    adaDAL.SelectCommand.Connection = conDAL;
                    adaDAL.Fill(dSetDAL);
                    conDAL.Close();
                    return dSetDAL;
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static void UpdateDataSet(SqlConnection conDAL, SqlDataAdapter adaDAL, DataSet dSetDAL)
        {
            try
            {
                //using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                //{
                //if (conDAL.State != ConnectionState.Open)
                //{
                // conDAL.Open();
                //}
                //SqlCommandBuilder cmdBld = new SqlCommandBuilder(adaDAL);
                adaDAL.SelectCommand.Connection = conDAL;

                adaDAL.Update(dSetDAL);
                //adaDAL.UpdateCommand.Connection = conDAL;                          
                //conDAL.Close();
                //}
                //}
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>DataSet</returns>
        public static DataSet ExecuteDataset(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteDataset(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteDataset(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }

        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, (SqlTransaction)null, commandType, strCommandText, commandParameters);
                SqlDataAdapter adaDAL = new SqlDataAdapter(cmdDALCommand);
                DataSet dSetDAL = new DataSet();
                adaDAL.Fill(dSetDAL);
                cmdDALCommand.Parameters.Clear();
                return dSetDAL;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <returns>Dataset</returns>

        public static DataSet ExecuteDataset(SqlConnection connection, SqlTransaction trans, CommandType commandType, string strCommandText)
        {

            try
            {
                return ExecuteDataset(connection, trans, commandType, strCommandText);
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteDataset(SqlConnection connection, SqlTransaction trans, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(connection.ConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteDataset(connection, trans, CommandType.StoredProcedure, strSPName, commandParameters);
                }

                else
                {
                    return ExecuteDataset(connection, trans, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Dataset
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>Dataset</returns>

        public static DataSet ExecuteDataset(SqlConnection connection, SqlTransaction trans, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlCommand cmdDALCommand = new SqlCommand();
                PrepareCommand(cmdDALCommand, connection, trans, commandType, strCommandText, commandParameters);
                SqlDataAdapter adaDAL = new SqlDataAdapter(cmdDALCommand);
                DataSet dSetDAL = new DataSet();
                adaDAL.Fill(dSetDAL);
                cmdDALCommand.Parameters.Clear();
                return dSetDAL;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Datareader
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteDataReader(string strConnectionString, string strSPName, params object[] parameterValues)
        {

            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {

                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteDataReader(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }
                else
                {
                    return ExecuteDataReader(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Datareader
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteDataReader(string strConnectionString, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlConnection conDAL = new SqlConnection(strConnectionString);
                conDAL.Open();
                return ExecuteDataReader(conDAL, commandType, strCommandText, commandParameters);

            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Datareader
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>SqlDataReader</returns>
        public static SqlDataReader ExecuteDataReader(SqlConnection connection, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlCommand cmdDAL = new SqlCommand();
                PrepareCommand(cmdDAL, connection, (SqlTransaction)null, commandType, strCommandText, commandParameters);

                SqlDataReader drDAL;
                drDAL = cmdDAL.ExecuteReader();
                cmdDAL.Parameters.Clear();
                return drDAL;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as DataTable
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>DataTable</returns>

        public static DataTable ExecuteDataTable(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                return ExecuteDataset(strConnectionString, strSPName, parameterValues).Tables[0];
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as DataView
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>DataView</returns>
        public static DataView ExecuteDataView(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                return ExecuteDataTable(strConnectionString, strSPName, parameterValues).DefaultView;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will return results as Row
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>DataRow</returns>
        public static DataRow ExecuteDataRow(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                return ExecuteDataTable(strConnectionString, strSPName, parameterValues).Rows[0];
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggreagate Values 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <returns>Object</returns>

        public static object ExecuteScalar(SqlConnection connection, SqlTransaction trans, CommandType commandType, string strCommandText)
        {

            try
            {
                return ExecuteScalar(connection, commandType, strCommandText);
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }

        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggregate Values 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>Object</returns>

        public static object ExecuteScalar(SqlConnection connection, SqlTransaction trans, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    SqlParameter[] commandParameters = GetSpParameterSet(connection.ConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteScalar(connection, trans, CommandType.StoredProcedure, strSPName, commandParameters);
                }
                else
                {
                    return ExecuteScalar(connection, trans, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggregate Values 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="trans"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>Object</returns>
        public static object ExecuteScalar(SqlConnection connection, SqlTransaction trans, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlCommand cmdDAL = new SqlCommand();
                PrepareCommand(cmdDAL, connection, trans, commandType, strCommandText, commandParameters);

                object retval = cmdDAL.ExecuteScalar();

                cmdDAL.Parameters.Clear();
                return retval;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggregate Values 
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>Object</returns>
        public static object ExecuteScalar(string strConnectionString, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                {
                    conDAL.Open();

                    return ExecuteScalar(conDAL, commandType, strCommandText, commandParameters);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggregate Values 
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="parameterValues"></param>
        /// <returns>Object</returns>
        public static object ExecuteScalar(string strConnectionString, string strSPName, params object[] parameterValues)
        {
            try
            {
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    SqlParameter[] commandParameters = GetSpParameterSet(strConnectionString, strSPName);
                    AssignParameterValues(commandParameters, parameterValues);
                    return ExecuteScalar(strConnectionString, CommandType.StoredProcedure, strSPName, commandParameters);
                }
                else
                {
                    return ExecuteScalar(strConnectionString, CommandType.StoredProcedure, strSPName);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Overloaded Function to Excecute the sql statements against open connections
        /// Useful for executing sql statements/procudures which will 
        /// return Aggreagate Values 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandType"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        /// <returns>Object</returns>
        public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                SqlCommand cmdDAL = new SqlCommand();
                PrepareCommand(cmdDAL, connection, (SqlTransaction)null, commandType, strCommandText, commandParameters);

                object retval = cmdDAL.ExecuteScalar();

                cmdDAL.Parameters.Clear();
                return retval;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        private static Hashtable hshTblParamCache = Hashtable.Synchronized(new Hashtable());
        /// <summary>
        /// Function to fetch parameters from a given stored procedure
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="blnIsIncludeReturnValueParameter"></param>
        /// <returns>SqlParameter</returns>
        private static SqlParameter[] DiscoverSpParameterSet(string strConnectionString, string strSPName, bool blnIsIncludeReturnValueParameter)
        {
            try
            {
                using (SqlConnection conDAL = new SqlConnection(strConnectionString))
                using (SqlCommand cmdDAL = new SqlCommand(strSPName, conDAL))
                {
                    conDAL.Open();
                    cmdDAL.CommandType = CommandType.StoredProcedure;

                    SqlCommandBuilder.DeriveParameters(cmdDAL);

                    if (!blnIsIncludeReturnValueParameter)
                    {
                        cmdDAL.Parameters.RemoveAt(0);
                    }

                    SqlParameter[] discoveredParameters = new SqlParameter[cmdDAL.Parameters.Count];
                    cmdDAL.Parameters.CopyTo(discoveredParameters, 0);

                    return discoveredParameters;
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Function used for parameter cloning
        /// </summary>
        /// <param name="originalParameters"></param>
        /// <returns>SqlParameter</returns>
        private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
        {

            try
            {
                SqlParameter[] clonedParameters = new SqlParameter[originalParameters.Length];
                for (int i = 0, j = originalParameters.Length; i < j; i++)
                {
                    clonedParameters[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
                }

                return clonedParameters;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// function to cache the fetched parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strCommandText"></param>
        /// <param name="commandParameters"></param>
        public static void CacheParameterSet(string strConnectionString, string strCommandText, params SqlParameter[] commandParameters)
        {
            try
            {
                string hashKey = strConnectionString + ":" + strCommandText;
                hshTblParamCache[hashKey] = commandParameters;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// function to retrived cached parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strCommandText"></param>
        /// <returns></returns>
        public static SqlParameter[] GetCachedParameterSet(string strConnectionString, string strCommandText)
        {
            try
            {
                string hashKey = strConnectionString + ":" + strCommandText;
                SqlParameter[] cachedParameters = (SqlParameter[])hshTblParamCache[hashKey];
                if (cachedParameters == null)
                {
                    return null;
                }
                else
                {
                    return CloneParameters(cachedParameters);
                }
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Over loaded function to get feteched  parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <returns></returns>
        public static SqlParameter[] GetSpParameterSet(string strConnectionString, string strSPName)
        {
            try
            {
                return GetSpParameterSet(strConnectionString, strSPName, false);
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }
        /// <summary>
        /// Over loaded function to get feteched  parameters
        /// </summary>
        /// <param name="strConnectionString"></param>
        /// <param name="strSPName"></param>
        /// <param name="blnIsIncludeReturnValueParameter"></param>
        /// <returns></returns>
        public static SqlParameter[] GetSpParameterSet(string strConnectionString, string strSPName, bool blnIsIncludeReturnValueParameter)
        {
            try
            {
                string hashKey = strConnectionString + ":" + strSPName + (blnIsIncludeReturnValueParameter ? ":include ReturnValue Parameter" : "");
                SqlParameter[] cachedParameters;
                cachedParameters = (SqlParameter[])hshTblParamCache[hashKey];
                if (cachedParameters == null)
                {
                    cachedParameters = (SqlParameter[])(hshTblParamCache[hashKey] = DiscoverSpParameterSet(strConnectionString, strSPName, blnIsIncludeReturnValueParameter));
                }
                return CloneParameters(cachedParameters);
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static DataSet ExecuteQueryDataset(string connection, string query)//ABC
        {
            try
            {
                SqlConnection cn = new SqlConnection(connection);
                SqlDataAdapter adaDAL = new SqlDataAdapter(query, cn);
                DataSet dSetDAL = new DataSet();
                adaDAL.Fill(dSetDAL);
                return dSetDAL;
            }
            catch (Exception exDTask)
            {
                throw ExceptionManager.GetError(exDTask, ErrorCodes.DTask);
            }
        }

        public static DataSet convertListToDataSet(NDCSearchServiceReference.ResultWrapper[] list)
        {
            NDCSearchServiceReference.ResultWrapper[] dr = list;

            DataTable dt = new DataTable();

            //for each of your properties            
            dt.Columns.Add("autotype", typeof(string));
            dt.Columns.Add("chasis", typeof(string));
            dt.Columns.Add("color", typeof(string));
            dt.Columns.Add("engine", typeof(string));
            dt.Columns.Add("FIR", typeof(string));
            dt.Columns.Add("make", typeof(string));
            dt.Columns.Add("matchingParam", typeof(string));
            dt.Columns.Add("registration", typeof(string));
            dt.Columns.Add("source", typeof(string));
            dt.Columns.Add("stateName", typeof(string));
            dt.Columns.Add("status", typeof(string));
            dt.Columns.Add("stcode", typeof(string));
            dt.Columns.Add("vehicle", typeof(string));
            dt.Columns.Add("year", typeof(string));

            foreach (NDCSearchServiceReference.ResultWrapper entity in list)
            {
                if (entity.registration != "" && entity.chasis != "" && entity.engine != "")
                {
                    DataRow row = dt.NewRow();
                    //foreach of your properties
                    row["autotype"] = entity.autotype;
                    row["chasis"] = entity.chasis;
                    row["color"] = entity.color;
                    row["engine"] = entity.engine;
                    row["FIR"] = entity.FIR;
                    row["make"] = entity.make;
                    row["matchingParam"] = entity.matchingParam;
                    row["registration"] = entity.registration;
                    row["source"] = entity.source;
                    row["stateName"] = entity.stateName;
                    row["status"] = entity.status;
                    row["stcode"] = entity.stcode;
                    row["vehicle"] = entity.vehicle;
                    row["year"] = entity.year;
                    dt.Rows.Add(row);
                }
            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

    }
}

