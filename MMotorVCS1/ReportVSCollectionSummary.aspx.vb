Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS

Partial Class ReportVSCollectionSummary
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Private Sub preparesql()
        Dim sql, sqlSumTotal As String
        Dim cmd As New SqlCommand
        If Me.Cash.Checked Then
            sql = "SELECT   * from cashamount_summary"
        Else
            sql = "SELECT   * from ipoamount_summary"
        End If

        If Me.Cash.Checked Then
            sqlSumTotal = "SELECT sum(cash) cash from cashamount_summary"
        Else
            sqlSumTotal = "SELECT sum(cash) cash from ipoamount_summary"
        End If
        Dim whr As String
        Dim ORD As String
        whr = ""
        ORD = ""
        Dim mtf As String
        mtf = Month(Me.ListDatefrom.Text.ToString())
        If Len(mtf) = 1 Then
            mtf = "0" & mtf
        End If
        Dim dtf As String
        dtf = Day(Me.ListDatefrom.Text.ToString())
        If Len(dtf) = 1 Then
            dtf = "0" & dtf
        End If
        Dim Mto As String
        Mto = Month(Me.listDateto.Text.ToString())
        If Len(Mto) = 1 Then
            Mto = "0" & Mto
        End If

        Dim dto As String
        dto = Day(Me.listDateto.Text.ToString())
        If Len(dto) = 1 Then
            dto = "0" & dto
        End If
        If Me.Cash.Checked Then
            If Session("inRoleid") = 1 Then
                If Me.Usernam.Text = "" Then
                    whr = whr & " CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                Else
                    whr = whr & " vcusername = '" & Me.Usernam.Text & "' and CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                End If
            Else
                whr = whr & " vcusername ='" & Session("vcUserName").ToString().Trim() & "' and CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
            End If
        Else
            If Session("inRoleid") = 1 Then
                If Me.Usernam.Text = "" Then
                    whr = whr & " CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                Else
                    whr = whr & " vcusername = '" & Me.Usernam.Text & "' and  CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
                End If
            Else
                whr = whr & " vcusername ='" & Session("vcUserName").ToString().Trim() & "' and CONVERT(varchar(10),dtENQdt,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtENQdt,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
            End If

        End If
        If whr <> "" Then
            whr = " WHERE " & whr
        End If
        sql = sql & whr & ORD
        sqlSumTotal = sqlSumTotal & whr
        Session("SQLSTRING") = sql
        Session("SQLSTRINGSUM") = sqlSumTotal
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If Me.ListDatefrom.Text > Me.listDateto.Text Then
        '    Msglabel.Text = "Date to is less than date from"
        '    Exit Sub
        'End If
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then

            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""
        preparesql()
        If Me.Cash.Checked = True Then
            Response.Write("<script>window.open('ReportVSCollectionSummaryview.aspx','_blank')</script>")
        Else
            Response.Write("<script>window.open('ReportVSCollectionSummaryIPOview.aspx','_blank')</script>")
        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If
        If Session("inRoleid") <> 1 And Session("inRoleid") <> 10 Then
            Me.Usernam.Text = Session("vcUserName")
            Me.Usernam.Enabled = False
        Else
            Me.Usernam.Enabled = True
        End If
        If Not IsPostBack Then
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "2" 'National counter
                Exit Select
            Case "8" 'NCRB
                Exit Select
            Case "12" 'ncrbexecutive
                Exit Select
            Case "5" 'state counter
                Exit Select
            Case "11" 'CR_Executive1
                Exit Select
            Case "22" 'Intern
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Response.Redirect(Request.RawUrl)
    End Sub
    Protected Sub btnreset_click1()
        Response.Redirect(Request.RawUrl)
    End Sub

    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
