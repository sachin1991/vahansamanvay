﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;

public partial class ClearCasePrinting : System.Web.UI.Page
{
    public DataTable dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblUpdationDate.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["ClientAddr"].ToString().Trim();
        }
        try
        {
            string usrn;
            usrn = "";
            lblUpdationDate.Text = DateTime.Today.ToShortDateString().Trim() + " " + Session["ClientAddr"].ToString().Trim();
            lblenqdatetime.Text = System.DateTime.Now.Date.ToString("dd/MM/yyyy");
            string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
            string _MakeDesc = Session["MakeDesc"].ToString();
            string _Registration = Session["Registration"].ToString();
            string _Chasis = Session["Chasis"].ToString();
            string _Engine = Session["Engine"].ToString();
            lblvectype.Text = _VehicleTypeDesc;
            lblmake.Text = _MakeDesc;
            lblregistrationno.Text = _Registration;
            lblchasisno.Text = _Chasis;
            lblengineno.Text = _Engine;

            if (Session["MatchStatus"].ToString() == "1")
            {
                lblmsg.Text = " is matched with";
                Label1.Text = " Status of";
                dv = (DataTable)Session["Data"];

                int _count = Convert.ToInt32(dv.Rows.Count);

                if (_count > 0)
                {
                    Repeater1.DataSource = dv;
                    Repeater1.DataBind();


                }
                else
                {
                    lblmsg.Text = " is not recovered yet.";
                    Label1.Text = " Recovery status of";
                }
            }
            else if (Session["MatchStatus"].ToString() == "2")
            {
                lblmsg.Text = " is not recovered yet.";
                Label1.Text = " Recovery status of";
            }
            else if (Session["MatchStatus"].ToString() == "3")
            {
                lblmsg.Text = " has not been reported as stolen by police.";
                Label1.Text = " Status of";
            }

            else
            {
                lblmsg.Text = " is not found in the database.";
            }
        }
    
    catch
    {
    }
  }
}