Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS

Partial Class Reportqueryfired
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Private Sub preparesql()
        Dim sql, sqlSumTotal As String
        Dim tofq As String
        Dim cmd As New SqlCommand
        If RdbAuthority.Checked = True Then
            sql = "SELECT dtCreatedOn,vcUserName,vcHeader,vcAddress,vcType,vehicleTypeName,vcMake,MakeName,vcRegistrationNo,vcChasisNo,vcEngineNo,matvehicle,matmake,matregistration,matchasis,matengine,matstatename,matfir,matstatus,matmatchingparam FROM AuthorityQueryView"
        End If
        If RdbInsurance.Checked = True Then
            sql = "SELECT dtCreatedOn,vcUserName,vcHeader,vcAddress,vcType,vehicleTypeName,vcMake,MakeName,vcRegistrationNo,vcChasisNo,vcEngineNo,matvehicle,matmake,matregistration,matchasis,matengine,matstatename,matfir,matstatus,matmatchingparam FROM InsuranceQueryView"
        End If

        If RdbInternet.Checked = True Then
            sql = "SELECT dtInsertedOn,vcTypecode,vehicleTypeName,vcMakeCode,MakeName,vcRegistrationNo,vcChasisNo,vcEngineNo,matvehicle,matmake,matregistration,matchasis,matengine,matstatename,matfir,matstatus,matmatchingparam FROM InternetQueryView"
        End If
        If RdbPolice.Checked = True Then
            If Me.Alllist.Checked Then
                sql = "SELECT dtCreated_Date,vcUserName,vcHeader,vcAddress,statename,vcFIRNO,FIRdt,vcType,vehicleTypeName,vcMake,MakeName,vcRegistrationNo,vcChasisNo,vcEngineNo,vcStatus FROM PSEntryView"
            Else
                If Me.rdbdaily.Checked = True Then
                    sql = "SELECT   CONVERT(varchar(10),dtCreated_Date,111) as datecreated,vcusername,vcheader,vcaddress,count(vcUsername) as recordsentered,statename,inroleid  FROM [PSEntryView] group by statename,vcUserName,vcheader,vcaddress,CONVERT(varchar(10),dtCreated_Date,111),inRoleid"
                End If
                If Me.rdbmonthly.Checked = True Then
                    sql = "SELECT   month(dtCreated_Date) as datecreated,year(dtCreated_Date),vcusername,vcheader,vcaddress,count(vcUsername) as recordsentered,statename,inroleid  FROM [PSEntryView] group by statename,vcUserName,vcheader,vcaddress,year(dtCreated_Date),month(dtCreated_Date),inRoleid"
                End If
                If Me.rdbyearly.Checked = True Then
                    sql = "SELECT   year(dtCreated_Date) as datecreated,vcusername,vcheader,vcaddress,count(vcUsername) as recordsentered,statename,inRoleid  FROM [PSEntryView] group by statename,vcUserName,vcheader,vcaddress,year(dtCreated_Date),inRoleid"
                End If

            End If
        End If
        If Me.DailyhitStats.Checked = True Then
            sql = "SELECT dt,cnt FROM [internetdailyhitcount]"
            tofq = "Internet"
        End If
        If Me.AuthorityDailyhitStats.Checked = True Then
            sql = "SELECT dt,cnt FROM [Authoritydailyhitcount]"
            tofq = "Authority"
        End If
        If Me.InsuranceDailyhitStats.Checked = True Then
            sql = "SELECT dt,cnt FROM [Insurancedailyhitcount]"
            tofq = "Insurance"
        End If
        If Me.PoliceDailyhitStats.Checked = True Then
            sql = "SELECT dt,cnt FROM [Policedailyhitcount]"
            tofq = "Police"
        End If
        If Me.Alllist.Checked = True Then
            sqlSumTotal = "select SUM(CAST(CashAmt AS INT)) as netamount FROM TblTrn_Counter_Enquiry "
        Else
            If Me.RdbPolice.Checked Then
                sqlSumTotal = "select count(vcUsername) as recordsentered FROM PSENTRYVIEW "
            Else
                sqlSumTotal = sql
            End If
        End If
            Dim whr As String
            Dim whr1 As String
            Dim ORD As String
            whr = ""
            whr1 = ""
            ORD = " ORDER BY "
            If RdbInternet.Checked Then
                ORD = ORD & "dtInsertedOn"
            End If

            If RdbAuthority.Checked Or RdbInsurance.Checked Then
                ORD = ORD & "dtCreatedOn"
            End If
            If RdbPolice.Checked Then
                If Me.Alllist.Checked Then
                    ORD = ORD & "dtCreated_Date"
                Else
                    If Me.rdbdaily.Checked = True Then
                        ORD = ORD & "CONVERT(varchar(10),dtCreated_Date,111)"
                    End If
                    If Me.rdbmonthly.Checked = True Then
                        ORD = ORD & "year(dtCreated_Date),month(dtCreated_Date)"
                    End If
                    If Me.rdbyearly.Checked = True Then
                        ORD = ORD & "year(dtCreated_Date)"
                    End If
                End If
            End If
        If (DailyhitStats.Checked = True Or AuthorityDailyhitStats.Checked = True Or InsuranceDailyhitStats.Checked = True Or PoliceDailyhitStats.Checked = True) Then
            ORD = " ORDER BY dt"
        End If
            Dim mtf As String
            mtf = Month(Me.ListDatefrom.Text.ToString())
            If Len(mtf) = 1 Then
                mtf = "0" & mtf
            End If
            Dim dtf As String
            dtf = Day(Me.ListDatefrom.Text.ToString())
            If Len(dtf) = 1 Then
                dtf = "0" & dtf
            End If
            Dim Mto As String
            Mto = Month(Me.listDateto.Text.ToString())
            If Len(Mto) = 1 Then
                Mto = "0" & Mto
            End If

            Dim dto As String
            dto = Day(Me.listDateto.Text.ToString())
            If Len(dto) = 1 Then
                dto = "0" & dto
            End If
            Dim uid As String
        Dim sta As String
        uid = ""
        sta = ""
            If Me.Userid.Text <> "" Then
            uid = " and vcUsername = '" & Trim(Userid.Text) & "'"
            End If
        If Me.ddlState.Text <> "ALL STATES" Then
            sta = " and statename = '" & Trim(ddlState.Text.Trim()) & "'"
        End If
        If RdbAuthority.Checked Or RdbInsurance.Checked Then
            whr = whr & " CONVERT(varchar(10),dtCreatedOn,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreatedOn,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'" & uid & sta
            If Me.OnlyMatchedcases.Checked = True Then
                whr = whr & " and (matstatus<>'' or matstatus is not null)"
            End If
        End If
        If RdbPolice.Checked = True Then
            If Me.Alllist.Checked = True Then
                whr = whr & " CONVERT(varchar(10),dtCreated_Date,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreated_Date,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'" & uid & sta
            Else
                If Me.rdbdaily.Checked = True Then
                    whr = whr & " CONVERT(varchar(10),dtCreated_Date,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtCreated_Date,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'" & uid & sta
                End If
                If Me.rdbmonthly.Checked = True Then
                    whr = whr & " datepart(month,dtCreated_Date) >='" & mtf & "' AND datepart(month,dtCreated_Date) <='" & Mto & "' and  datepart(year,dtCreated_Date) >='" & Year(Me.ListDatefrom.Text) & "' AND datepart(year,dtCreated_Date) <='" & Year(Me.listDateto.Text) & "' " & uid & sta
                End If
                If Me.rdbyearly.Checked = True Then
                    whr = whr & " datepart(year,dtCreated_Date) >='" & Year(Me.ListDatefrom.Text) & "' AND datepart(year,dtCreated_Date) <='" & Year(Me.listDateto.Text) & "' " & uid & sta
                End If
            End If
        End If
        If RdbInternet.Checked = True Then
            whr = whr & " CONVERT(varchar(10),dtInsertedon,111) >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND CONVERT(varchar(10),dtInsertedon,111) <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
            If Me.OnlyMatchedcases.Checked = True Then
                whr = whr & " and (matstatus<>'' or matstatus is not null)"
            End If
        End If
        If (DailyhitStats.Checked = True Or AuthorityDailyhitStats.Checked = True Or InsuranceDailyhitStats.Checked = True Or PoliceDailyhitStats.Checked = True) Then
            whr = " dth >='" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND dth <='" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "'"
        End If
        whr1 = ""
        If whr <> "" Then
            If (DailyhitStats.Checked = True Or AuthorityDailyhitStats.Checked = True Or InsuranceDailyhitStats.Checked = True Or PoliceDailyhitStats.Checked = True) Then
                whr = " WHERE " & whr
                whr1 = ""
            Else
                If Me.RdbPolice.Checked Then
                    If Me.Rdbenteredbystate.Checked = True Then
                        whr = whr & " and (inroleid = 3 or inroleid=6 or inroleid=7)"
                    Else
                        whr = whr & " and (inroleid <>3 and inroleid <>6 and inroleid<>7)"
                    End If
                    If Me.Alllist.Checked Then
                        whr = " WHERE " & whr
                    Else

                        whr1 = " WHERE " & whr
                        whr = " HAVING " & whr
                    End If
                Else
                    whr = " WHERE " & whr
                End If
            End If
        End If
        sql = sql & whr & ORD
        sqlSumTotal = sqlSumTotal & whr1
        Session("TypeofQuery") = tofq
        Session("SQLSTRING") = sql
        Session("SQLSTRINGSUM") = sqlSumTotal
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then
            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""
        preparesql()
        If (DailyhitStats.Checked = True Or AuthorityDailyhitStats.Checked = True Or InsuranceDailyhitStats.Checked = True Or PoliceDailyhitStats.Checked = True) Then
            Response.Write("<script>window.open('ReportInternetDailyhitstat.aspx','_blank')</script>")
            Exit Sub
        End If
        If RdbAuthority.Checked = True Or RdbInsurance.Checked = True Then
            Response.Write("<script>window.open('Reportqueryfiredview.aspx','_blank')</script>")
            Exit Sub
        End If
        If RdbInternet.Checked = True Then
            Response.Write("<script>window.open('Reportqueryfiredinternetview.aspx','_blank')</script>")
            Exit Sub
        End If
        If RdbPolice.Checked = True Then
            If Me.Alllist.Checked Then
                Response.Write("<script>window.open('ReportqueryfiredPolice.aspx','_blank')</script>")
            Else
                Response.Write("<script>window.open('Reportqueryfiredentered.aspx','_blank')</script>")
            End If
            Exit Sub
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If
        Dim dr As SqlDataReader
        Dim cn As New SqlConnection

        If Not IsPostBack Then
            Dim ConStr As ConnectionStringSettings
            ConStr = ConfigurationManager.ConnectionStrings("Conn")
            cn.ConnectionString = MMotorVCS.DTask.BuildConnectionString()
            cn.Open()
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
            Dim cmdstate1 As New SqlCommand
            cmdstate1.CommandText = "Select Distinct statename From vw_ps"
            cmdstate1.Connection = cn
            dr = cmdstate1.ExecuteReader
            ddlState.Items.Add("ALL STATES")
            If dr.HasRows Then
                Do While dr.Read
                    Me.ddlState.Items.Add(dr("Statename"))
                Loop
            End If
            dr.Close()
            cn.Close()
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "10" 'crime records
                Exit Select
            Case "12 'ncrb executive"
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click

        Response.Redirect(Request.RawUrl)
    End Sub
    Protected Sub btnreset_click1()
        Response.Redirect(Request.RawUrl)
    End Sub

    Protected Sub RdbAuthority_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdbAuthority.CheckedChanged
        If RdbAuthority.Checked = True Then
            Me.Alllist.Checked = True
        End If
    End Sub

    Protected Sub RdbInsurance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdbInsurance.CheckedChanged
        If RdbInsurance.Checked = True Then
            Me.Alllist.Checked = True
        End If
    End Sub

    

    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub

    Protected Sub Alllist_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Alllist.CheckedChanged
        If Alllist.Checked = False Then
            Me.RdbPolice.Checked = True
            Me.DailyhitStats.Checked = False
        End If
    End Sub
End Class
