﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="false" AutoEventWireup="true" CodeFile="Reportqueryfired.aspx.vb" Inherits="Reportqueryfired"  Theme="emsTheme"  EnableViewState="true"  EnableViewStateMac="true"  Title="Vahan Samanvay Queryfired" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
.top {
    border-top:thin solid;
    border-color:black;
}

.bottom {
    border-bottom:thin solid;
    border-color:black;
}

.left {
    border-left:thin solid;
    border-color:black;
}

.right {
    border-right:thin solid;
    border-color:black;
}
</style>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
<Triggers> 
<asp:PostBackTrigger ControlID="Button1" /> 
<asp:PostBackTrigger ControlID="Converttoexecl" /> 
<asp:PostBackTrigger ControlID="btnReset"></asp:PostBackTrigger>
</Triggers> 
    <ContentTemplate>
            <table class="contentmain">
                <tr>
                <td><table class="tablerowcolor" align="center" width="80%">
                <tr class="heading">
                    
                    <td colspan="3" >
                        <asp:Label ID="Label3" runat="server" Text="Query fired Report" 
                            Font-Bold="True"></asp:Label></td>
                   
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 100px">
                        <asp:RadioButton ID="RdbAuthority" runat="server" GroupName="optbtn" 
                            Text="Authority" AutoPostBack="True" />
                    </td>
                    <td style="width: 100px">
                        <asp:RadioButton ID="AuthorityDailyhitStats" runat="server" AutoPostBack="True" 
                            GroupName="optbtn" Text="Authority Daily Hit Counts" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px" class="top bottom left">
                        &nbsp;</td>
                    <td style="width: 100px" class="top bottom">
                        <asp:RadioButton ID="RdbInternet" runat="server" GroupName="optbtn" 
                            Text="Internet" AutoPostBack="True" />
                    </td>
                    <td style="width: 100px" class="top bottom right">
                        <asp:RadioButton ID="DailyhitStats" runat="server" GroupName="optbtn" 
                            Text="Internet Daily Hit Counts" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 26px;">
                        </td>
                    <td style="width: 100px; height: 26px;">
                        <asp:RadioButton ID="RdbInsurance" runat="server" GroupName="optbtn" 
                            Text="Insurance" AutoPostBack="True" />
                    </td>
                    <td style="width: 100px; height: 26px;">
                        <asp:RadioButton ID="InsuranceDailyhitStats" runat="server" AutoPostBack="True" 
                            GroupName="optbtn" Text="Insurance Daily Hit Counts" />
                        </td>
                </tr>
                <tr>
                    <td style="width: 100px" class="top left">
                        &nbsp;</td>
                    <td style="width: 100px" class="top">
                        <asp:RadioButton ID="RdbPolice" runat="server" GroupName="optbtn" 
                            Text="Police" Checked="True" AutoPostBack="True" />
                    </td>
                    <td style="width: 100px" class="top right">
                        <asp:RadioButton ID="PoliceDailyhitStats" runat="server" AutoPostBack="True" 
                            GroupName="optbtn" Text="Police Daily Hit Counts" />
                        <br />
                        <asp:CheckBox ID="Alllist" runat="server" Checked="True" 
                            Text="Select for Complete List" AutoPostBack="True" />
                        <br />
                        <asp:RadioButton ID="rdbdaily" runat="server" Checked="True" 
                            GroupName="dailyyearlymonthly" Text="Daily Summary of Entered Records" />
                        <br />
                        <asp:RadioButton ID="rdbmonthly" runat="server" GroupName="dailyyearlymonthly" 
                            Text="Monthly  Summary of Entered Records" />
                        <br />
                        <asp:RadioButton ID="rdbyearly" runat="server" GroupName="dailyyearlymonthly" 
                            Text="Yearly  Summary of Entered Records" />
                        <br />
                        <asp:RadioButton ID="Rdbenteredbystate" runat="server" Checked="True" 
                            GroupName="Enteredbystate" Text="Entered by State" />
                        <asp:RadioButton ID="RdbEnteredbyrest" runat="server" 
                            GroupName="Enteredbystate" Text="Entered by Rest" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px" class="top">
                        User Id</td>
                    <td style="width: 100px" class="top">
                        <asp:TextBox ID="Userid" runat="server"></asp:TextBox>
                    </td>
                    <td style="width: 100px" class="top">
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label1" runat="server" Text="Date from" ></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="ListDatefrom" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender runat="server" ID="datefrom" 
                            TargetControlID="Listdatefrom" Format="dd/MM/yyyy" 
                     PopupPosition="Right"   ></ajaxtoolkit:calendarextender>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <asp:Label ID="Label2" runat="server" Text="Date to"></asp:Label></td>
                    <td style="width: 100px">
                        <asp:TextBox ID="listDateto" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                    <ajaxtoolkit:calendarextender ID="dateto" runat="server" 
                            TargetControlID="ListDateTo" Format="dd/MM/yyyy" 
                     PopupPosition="Right"  ></ajaxtoolkit:calendarextender>
                        <asp:CheckBox ID="OnlyMatchedcases" runat="server" Text="Only Matched Cases" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="Button1" runat="server" Text="View" />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" />
                        </td>
                    <td style="width: 100px">
                        <asp:CheckBox ID="withpagebreak" runat="server" Checked="True" 
                            Text="With Page Break" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    <asp:Label ID="Msglabel" runat="server"></asp:Label>
                    <asp:Button ID="Converttoexecl" runat="server" 
                            Text="Generate to excel (.csv) format file" />
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
            </td> </tr> </table>
 </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
