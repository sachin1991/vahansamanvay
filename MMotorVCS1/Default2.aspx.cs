﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using MMotorVCS;
using System.Net;


public partial class Default2 : System.Web.UI.Page
{
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    int PostCount = 0;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {

        }


        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }


            bindVehicleType();
            bindStateName();
            bindColName();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "8":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }




    private bool SaveForm(bool blSave)
    {
        try
        {

            if (txtAppName.Text == "")
            {
                Alert("Please enter the Registration No.");
                txtAppName.Focus();
                return false;
            }

            if ((txtRegistration.Text == "" && txtChasis.Text == "") || (txtRegistration.Text == "" && txtEngine.Text == "") || (txtChasis.Text == "" && txtEngine.Text == ""))
            {
                Alert("Please enter the alteast two of Registration No., Chasis No. and Engine No.");
                txtRegistration.Focus();
                return false;
            }

            if (ddVehTypeName.SelectedIndex == 0)
            {
                Alert("Please select the Vehicle Type to be entered");
                ddVehTypeName.Focus();
                return false;
            }
            if (Session["strRandom"].ToString() != txtCapcha.Text.ToString())
            {
                Alert("Please Enter Captha");
                txtCapcha.Focus();
                return false;
            }
            if (txtCapcha.Text.ToString() == "")
            {
                Alert("Please Enter Captha");
                txtCapcha.Focus();
                return false;
            }

            return true;

        }
        catch (Exception ex)
        {
            return false;
        }

        finally
        {
        }
    }

    private void Alert(string strErrMsg)
    {

        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";

        if (!ClientScript.IsClientScriptBlockRegistered("ScriptAlert"))
        {

            ClientScript.RegisterStartupScript(typeof(Default2), "ScriptAlert", strScript);

        }

    }

    protected void btnSearch_Click2(object sender, EventArgs e)
    {
        try
        {


            bool blnIsSaved = SaveForm(false);
            if (blnIsSaved == true)
            {
                string strStateCode = Session["vcStateCode"].ToString().Trim();
                string strUserName = Session["vcUserName"].ToString().Trim();
                string strHostName = System.Net.Dns.GetHostName();


                string clientName;

                clientName = GetIPAddress().ToString();


                string typeOfQuery;
                string strsource = strStateCode;
                string[] MakeCode = ddMakeName.SelectedValue.ToString().Split('-');
                string Color;
                string VehicleNameDesc;
                string MakeDecs;

                if (ddVehTypeName.SelectedIndex == 0)
                {
                    VehicleNameDesc = "";
                }
                else
                {
                    VehicleNameDesc = ddVehTypeName.SelectedItem.Text.ToString().Trim();
                }

                if (ddMakeName.SelectedIndex == 0)
                {
                    MakeDecs = "";
                }
                else
                {
                    MakeDecs = ddMakeName.SelectedItem.Text.ToString().Trim();
                }



                if (ddColName.SelectedIndex == 0)
                {
                    Color = "0";
                }
                else
                {
                    Color = ddColName.SelectedValue.ToString().Split('-')[1];
                }

                Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
                string opRegistration = re.Replace(txtRegistration.Text.ToString().Trim(), "");
                string opChasis = re.Replace(txtChasis.Text.ToString().Trim(), "");
                string opEngine = re.Replace(txtEngine.Text.ToString().Trim(), "");

                if (rbSecondHand.Checked == true)
                {
                    typeOfQuery = "SH";
                    DataSet ds = new DataSet();
                    ds = eform.AddEntry_InternetSecondHand((ddStateName.SelectedValue.Split('-')[0]), (ddDistrictName.SelectedValue.Split('-')[1]), (ddPsName.SelectedValue.Split('-')[0]), txtAppName.Text.ToString().Trim(), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtPinCode.Text.ToString().Trim(), (ddVehTypeName.SelectedValue.Split('-')[0]), (MakeCode[1]), txtYear.Text.ToString(), Color, opRegistration, opChasis, opEngine, strsource, strHostName, clientName, typeOfQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {


                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "1";
                        Session["Nameofquery"] = txtAppName.Text.ToString();
                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";





                        Response.Write("<script>window.open('InternetQryOutput.aspx','_blank')</script>");



                    }
                    else
                    {
                        string _VehicleTypeDesc = Session["VehicleTypeDesc"].ToString();
                        string _MakeDesc = Session["MakeDesc"].ToString();
                        string _Registration = Session["Registration"].ToString();
                        string _Chasis = Session["Chasis"].ToString();
                        string _Engine = Session["Engine"].ToString();

                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";

                        Session["MatchStatus"] = "3";

                        Response.Write("<script>window.open('InternetQryOutput.aspx','_blank')</script>");

                    }
                }
                else if (rbRecoverStatus.Checked == true)
                {
                    typeOfQuery = "R";
                    DataSet ds = new DataSet();
                    ds = eform.AddEntry_InternetRecover((ddStateName.SelectedValue.Split('-')[0]), (ddDistrictName.SelectedValue.Split('-')[1]), (ddPsName.SelectedValue.Split('-')[0]), txtAppName.Text.ToString().Trim(), txtMobile.Text.ToString().Trim(), txtEmail.Text.ToString().Trim(), txtAddress.Text.ToString().Trim(), txtPinCode.Text.ToString().Trim(), (ddVehTypeName.SelectedValue.Split('-')[0]), (MakeCode[1]), txtYear.Text.ToString(), Color, txtRegistration.Text.ToString().Trim(), txtChasis.Text.ToString().Trim(), txtEngine.Text.ToString().Trim(), strsource, strHostName, clientName, typeOfQuery);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["Data"] = (DataTable)(ds.Tables[0]);
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Cache["Data"] = (DataTable)(ds.Tables[0]);
                        Session["MatchStatus"] = "1";

                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";


                        Response.Write("<script>window.open('InternetQryOutput.aspx','_blank')</script>");


                    }
                    else
                    {
                        Session["VehicleTypeDesc"] = VehicleNameDesc;
                        Session["MakeDesc"] = MakeDecs;
                        Session["Registration"] = opRegistration;
                        Session["Chasis"] = opChasis;
                        Session["Engine"] = opEngine;
                        Session["MatchStatus"] = "2";

                        Session["HostAddr"] = strHostName.ToString().Trim();
                        Session["ClientAddr"] = clientName.ToString().Trim() + " ";


                        Response.Write("<script>window.open('InternetQryOutput.aspx','_blank')</script>");
                    }



                }

            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public string GetIPAddress()
    {
        string strHostName = System.Net.Dns.GetHostName();
        IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        HttpRequest currentRequest = HttpContext.Current.Request;
        String clientIP1 = currentRequest.ServerVariables["REMOTE_HOST"];
        return clientIP1;
    }

    protected void btnrefresh_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }



    protected void ddVehTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectVehicleType();
            PostCount++;
        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }
    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }
    }

    public void bindColName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourNameNew();
            ddColName.DataSource = ds;
            ddColName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["colourname"].ColumnName);
            ddColName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["colidcolcode"].ColumnName);
            ddColName.DataBind();
            ddColName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();

            ddPsName.Items.Clear();

            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {

            ddDistrictName.Items.Clear();

            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));



        }
    }

    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {

            ddPsName.Items.Clear();

            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    private void afterSelectVehicleType()
    {
        if (ddVehTypeName.SelectedIndex != 0)
        {
            bindMakeName();
        }
        else
        {

            ddMakeName.Items.Clear();
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
    }

    public void bindStateName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateTypeNew();
            ddStateName.DataSource = ds;
            ddStateName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["statecode"].ColumnName);
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = GenericMethods.check(dt.Columns["districtname"].ColumnName);
            ddDistrictName.DataValueField = GenericMethods.check(dt.Columns["districtcode"].ColumnName);
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            int tempState = Convert.ToInt32(ddStateName.SelectedValue);
            string[] temp = ddDistrictName.SelectedValue.Split('-');
            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]), tempState);
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["Psname"].ColumnName);
            ddPsName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["psidpscode"].ColumnName);
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleTypeNew();
            ddVehTypeName.DataSource = ds;
            ddVehTypeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehTypeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehidvehcode"].ColumnName);
            ddVehTypeName.DataBind();
            ddVehTypeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindMakeName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake_accVehTypeNew(Convert.ToInt32(ddVehTypeName.SelectedValue.Split('-')[1]));
            ddMakeName.DataSource = ds;
            ddMakeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["makename"].ColumnName);
            ddMakeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["makeidmakecode"].ColumnName);
            ddMakeName.DataBind();
            ddMakeName.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    protected void TextValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = (txtRegistration.Text.Length > 0 || txtChasis.Text.Length > 0 || txtEngine.Text.Length > 0);
    }
    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }
}