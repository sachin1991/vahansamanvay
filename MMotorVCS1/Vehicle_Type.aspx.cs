using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Vehicle_Type : System.Web.UI.Page
{
    static int vtype_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();

    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }


        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {

        }
        try
        {

        }
        catch
        {
            Response.Redirect("error.aspx");
        }

        if (!IsPostBack)
        {
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindgrid();

        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "8":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    public void getmaxid()
    {
        try
        {
            vtype_id = mm.getVehicleMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            if (vehiclenamewise.Checked == true)
            {
                ds = mm.getVehicleType1();
            }
            {
                ds = mm.getVehicleType();
            }
            gvVehType.DataSource = ds;
            gvVehType.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtVehCode.Text = "";
        txtVehName.Text = "";
        bindgrid();
    }
    private Boolean checkillegal(string vehicalcode,string vehicalname)
    {
        bool illegal;
        illegal = false;
        if (vehicalcode.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (vehicalname.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
 
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getVehicleType();
            if (txtVehCode.Text != "" && txtVehName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["vehicletypename"].ToString().Trim().ToUpper()) == txtVehName.Text.Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Vehicle Type Name is already exists.</font>";
                        break;
                    }
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["vehicletypecode"].ToString().Trim().ToUpper()) == txtVehCode.Text.ToUpper().Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Vehicle Type Code is already exists.</font>";
                        break;
                    }
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal(txtVehCode.Text.ToString().Trim(), txtVehName.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.AddVehicleType((vtype_id), (txtVehCode.Text.Trim().ToUpper()), txtVehName.Text.Trim().ToUpper(), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not inserted</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }

            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getVehicleType();
            if (txtVehCode.Text != "" && txtVehName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["vehicletypeid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["vehicletypename"].ToString().Trim().ToUpper()) == txtVehName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Vehicle Type Name is already exists.</font>";
                            break;
                        }
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["vehicletypecode"].ToString().Trim().ToUpper()) == txtVehCode.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Vehicle Type Code is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal(txtVehCode.Text.ToString().Trim(), txtVehName.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.updateVehicleType(Convert.ToInt32(lblid.Text), (txtVehCode.Text.Trim().ToUpper()), txtVehName.Text.Trim().ToUpper(), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values Updated</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not Updated</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void gvVehType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvVehType.Rows[Convert.ToInt32(e.CommandName) - (gvVehType.PageSize * pagecount)].Cells[1].Text;
                txtVehCode.Text = gvVehType.Rows[Convert.ToInt32(e.CommandName) - (gvVehType.PageSize * pagecount)].Cells[2].Text;
                txtVehName.Text = gvVehType.Rows[Convert.ToInt32(e.CommandName) - (gvVehType.PageSize * pagecount)].Cells[3].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvVehType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvVehType.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void gvVehType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }



    protected void vehiclenamewise_CheckedChanged(object sender, EventArgs e)
    {
        bindgrid();
    }
}