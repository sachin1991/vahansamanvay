﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {


                if (Session["Error"].ToString() == "3")
                {
                    lblError.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();

                }
                else if (Session["Error"].ToString() == "2")
                {
                    lblError.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["NAUTH"].ToString();

                }
                else if (Session["Error"].ToString() == "4")
                {
                    lblError.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["Lock"].ToString();

                }
                else
                {
                    lblError.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["SessionExpiredErrorMessage"].ToString();

                }


            }
            catch (Exception ex)
            {
                    lblError.Text = "Error";
            }
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    protected void ncrb_Click(object sender, EventArgs e)
    {
        Response.Redirect("http://ncrb.gov.in");
    }

    protected void interquery_Click(object sender, EventArgs e)
    {
        Response.Redirect("Internetquery.aspx");
    }
}