using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
public partial class InsComName : System.Web.UI.Page
{
    static int inscomp_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();
  
    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
     
            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            
        }
        

        if (!IsPostBack)
        {
            try
            {
              
            }
            catch
            {
                Response.Redirect("error.aspx"); 
            }
            btnUpdate.Visible = false;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindgrid();
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "8":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }

    }
    public void getmaxid()
    {
        try
        {
            inscomp_id = mm.getInsCompDetailMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {            
            DataSet ds = new DataSet();
            ds = mm.getInsCompDetail();
            gvInsCompType.DataSource = ds;
            gvInsCompType.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = false;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtCompName.Text = "";
        txtInsCompCode.Text = "";
        txtaddress.Text = "";
        bindgrid();
    }
    private Boolean checkillegal(string inscompanycode, string inscompanyname,string inscompanyaddress)
    {
        bool illegal;
        illegal = false;
        if (inscompanycode.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (inscompanyname.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (inscompanyaddress.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }

        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getInsCompDetail();
            if (txtCompName.Text != "" && txtInsCompCode.Text!="" && txtaddress.Text!="")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["InsCompid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["inscompcode"].ToString().Trim().ToUpper()) == txtInsCompCode.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Insurance Company code is already exists.</font>";
                            break;
                        }
                        if (ds.Tables[0].Rows[i]["InsCopmName"].ToString().Trim().ToUpper() == txtCompName.Text.Trim().ToUpper())
                        {
                            flag = 1;
                            lblmsg.Text = "<font color=red>This Insurance Company name is already exists.</font>";
                            break;
                        }
                    }
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal(txtInsCompCode.Text.ToString().Trim(), txtCompName.Text.ToString().Trim(), txtaddress.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.updateInsComp(Convert.ToInt32(lblid.Text),GenericMethods.check(txtInsCompCode.Text.Trim().ToUpper()), GenericMethods.check(txtCompName.Text.Trim().ToUpper()),GenericMethods.check(txtaddress.Text.ToString().Trim()), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values Updated</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not Updated</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            int flag = 0;
            DataSet ds = new DataSet();
            ds = mm.getInsCompDetail();
            if (txtCompName.Text != "" && txtInsCompCode.Text!="" && txtaddress.Text!="")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["inscompcode"].ToString().Trim().ToUpper()) == txtInsCompCode.Text.Trim().ToUpper())
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["inscompcode"].ToString().Trim().ToUpper()) == txtInsCompCode.Text.Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Insurance Company code is already exists.</font>";
                        break;
                    }
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["InsCopmName"].ToString().Trim().ToUpper()) == txtCompName.Text.Trim().ToUpper())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Insurance Company name is already exists.</font>";
                        break;
                    }   
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal(txtInsCompCode.Text.ToString().Trim(), txtCompName.Text.ToString().Trim(), txtaddress.Text.ToString().Trim());
                    if (illegal == false)
                    {
                        if (mm.AddInsComp((inscomp_id), GenericMethods.check(txtInsCompCode.Text.Trim().ToUpper()), GenericMethods.check(txtCompName.Text.Trim().ToUpper()), GenericMethods.check(txtaddress.Text.ToString().Trim()), _userid) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not inserted</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvInsCompType_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                lblid.Text = gvInsCompType.Rows[Convert.ToInt32(e.CommandName) - (gvInsCompType.PageSize * pagecount)].Cells[1].Text;
                txtInsCompCode.Text = gvInsCompType.Rows[Convert.ToInt32(e.CommandName) - (gvInsCompType.PageSize * pagecount)].Cells[2].Text;
                txtCompName.Text = gvInsCompType.Rows[Convert.ToInt32(e.CommandName) - (gvInsCompType.PageSize * pagecount)].Cells[3].Text;
                txtaddress.Text = gvInsCompType.Rows[Convert.ToInt32(e.CommandName) - (gvInsCompType.PageSize * pagecount)].Cells[4].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvInsCompType_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvInsCompType.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void gvInsCompType_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
        }
    }
}
