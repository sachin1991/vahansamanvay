using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using MMotorVCS;
using System.Text.RegularExpressions;


public partial class frmsetup : System.Web.UI.Page
{
    static int user_id;
    static string _userid;
    DataSet ds = new DataSet();
    ListItemCollection liColl;
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    Class1 myclass = new Class1();
    static int _add, _edit, _delete, _view;
    static int pagecount = 0;
    int PostCount = 0;
    private string vcDistrictCode;
    private string vcTypeCode;
    private string vcStateCode;
    private int _roleId;
    string uniqueCode;
    string strUserType;


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            if (clsPageBase.deletealreadyloggedin(Session["vcUserName"].ToString()) > 0)
            {
            }
            Response.Redirect("LogoutModule.aspx");
        }

        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return validationsetup();");
        ddUserRole.Attributes.Add("onchange", "return SelectDropDown()");


        if (Session["vcStateCode"] == null)
        {


        }
        else
        {
            vcStateCode = Session["vcStateCode"].ToString();
            _roleId = Convert.ToInt32(Session["inRoleId"]);
            vcTypeCode = Session["vcType"].ToString();
        }



        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        btnUpdate.Visible = true;
        btnAdd.Visible = false;
        btnReset.Visible = false;

        bindRoleName();
        bindStateName();
        ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));



        if (Convert.ToInt32(Session["inRoleId"]) == 3 || Convert.ToInt32(Session["inRoleId"]) == 4 || Convert.ToInt32(Session["inRoleId"]) == 6 || Convert.ToInt32(Session["inRoleId"]) == 7 || Convert.ToInt32(Session["inRoleId"]) == 9)
        {
            TableRow4.Visible = false;
        }

        if (Convert.ToInt32(Session["inRoleId"]) != 1)
        {
            txtUsername.Enabled = false;
            ddAcDeac.Enabled = false;
            ddUserRole.Enabled = false;
            ddStateName.Enabled = false;
            ddDistrictName.Enabled = false;
            ddPsName.Enabled = false;


            if (!IsPostBack)
            {
                DataSet dsDetails = mm.getUserDetailsState(Session["vcUserName"].ToString().Trim(), _roleId);
                txtUsername.Text = dsDetails.Tables[0].Rows[0]["vcUserName"].ToString().Trim();
                ddStateName.Items.Insert(0, dsDetails.Tables[0].Rows[0]["statename"].ToString().Trim());
                ddDistrictName.Items.Insert(0, dsDetails.Tables[0].Rows[0]["districtname"].ToString().Trim());
                ddPsName.Items.Insert(0, dsDetails.Tables[0].Rows[0]["Psname"].ToString().Trim());
                ddUserRole.SelectedValue = dsDetails.Tables[0].Rows[0]["inRoleId"].ToString().Trim();
                txtemail.Text = dsDetails.Tables[0].Rows[0]["vcMail"].ToString().Trim();
                txtFileNo.Text = dsDetails.Tables[0].Rows[0]["vcFileNumber"].ToString().Trim();
                txtAmount.Text = dsDetails.Tables[0].Rows[0]["cashamount"].ToString().Trim();
                txtAuthority1.Text = dsDetails.Tables[0].Rows[0]["vcAuthority"].ToString().Trim();
                txtAddress1.Text = dsDetails.Tables[0].Rows[0]["vcAddress"].ToString().Trim();
                txtHeader.Text = dsDetails.Tables[0].Rows[0]["vcHeader"].ToString().Trim();
            }
        }

        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "2":
                break;
            case "3":
                break;
            case "4":
                break;
            case "5":
                break;
            case "6":
                break;
            case "7":
                break;
            //case "8":
            //    break;
            case "9":
                break;
            case "10":
                break;
            case "11":
                break;
            case "12":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }

    }

    public void bindStateName()
    {
        try
        {

            DataSet dTblState = mm.getStateTypeNew();
            ddStateName.DataSource = dTblState;
            ddStateName.DataTextField = dTblState.Tables[0].Columns["statename"].ColumnName;
            ddStateName.DataValueField = dTblState.Tables[0].Columns["statecode"].ColumnName;
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        {

            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }

    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }

    }

    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {

            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();
            ddPsName.Items.Clear();
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {

            ddDistrictName.Items.Clear();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));


        }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            string[] temp = ddDistrictName.SelectedValue.Split('-');
            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]));
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = ds.Tables[0].Columns["Psname"].ColumnName;
            ddPsName.DataValueField = ds.Tables[0].Columns["psidpscode"].ColumnName;
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = dt.Columns["districtname"].ColumnName;
            ddDistrictName.DataValueField = dt.Columns["districtcode"].ColumnName;
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }



    public void bindRoleName()
    {
        try
        {
            DataTable dTblRole = clsPageBase.GetRole();
            ddUserRole.DataSource = dTblRole;
            ddUserRole.DataValueField = GenericMethods.check(dTblRole.Columns[0].ColumnName);
            ddUserRole.DataTextField = GenericMethods.check(dTblRole.Columns[1].ColumnName);
            ddUserRole.DataBind();
            ddUserRole.Items.Insert(0, "--Select--");


        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void bindgrid()
    {
        try
        {

        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected bool checkusername(string UserName)
    {
        try
        {
            if (Convert.ToBoolean((clsPageBase.GetVerifyUserName(UserName))))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

        if (ds.Tables[0].Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected string decrypt(string EncryptValue)
    {
        char[] EncAry = new char[EncryptValue.Length];
        char[] OrgAry = new char[EncryptValue.Length];
        int i;
        EncAry = EncryptValue.ToCharArray();
        for (i = 0; i < EncryptValue.Length; i++)
        {
            OrgAry[i] = (char)((int)EncAry[i] - 63);
        }
        string OriginalString = new string(OrgAry);
        return OriginalString;
    }
    protected string encrypt(string OriginalValue)
    {
        char[] OrgAry = new char[OriginalValue.Length];
        char[] EncAry = new char[OriginalValue.Length];
        int i;
        OrgAry = OriginalValue.ToCharArray();
        for (i = 0; i < OriginalValue.Length; i++)
        {
            EncAry[i] = (char)((int)OrgAry[i] + 63);
        }
        string encryptString = new string(EncAry);
        return encryptString;
    }

    public byte[] EncryptPassword(string username, string password, byte[] salt1, byte[] salt2)
    {
        string tmpPassword = null;
        string rsPwd;
        tmpPassword = Convert.ToBase64String(salt1) + Convert.ToBase64String(salt2) + username.ToLower() + password;

        UTF8Encoding textConvertor = new UTF8Encoding();
        byte[] passBytes = textConvertor.GetBytes(tmpPassword);
        return new SHA384Managed().ComputeHash(passBytes);


    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            byte[] encPassword;

            if (ddStateName.SelectedIndex != 0)
            {
                if (txtUsername.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0 &&
                ddStateName.SelectedIndex != 0)
                {
                    if (checkusername(txtUsername.Text.Trim().ToUpper()) == false)
                    {
                        if (txtPassword.Text == txtConPass.Text)
                        {

                            string pp = myclass.md5(txtPassword.Text.ToString().Trim());
                            uniqueCode = ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                            if (ddDistrictName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            if (ddPsName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));

                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                        }
                    }
                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                    }
                }
            }
            else
            {
                if (txtUsername.Text != "" && txtPassword.Text != "" && txtConPass.Text != "" && ddUserRole.SelectedIndex != 0)
                {
                    if (checkusername(txtUsername.Text.Trim().ToUpper()) == false)
                    {
                        if (txtPassword.Text == txtConPass.Text)
                        {

                            string pp = myclass.md5(txtPassword.Text.ToString().Trim());
                            if (ddStateName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddStateName.SelectedValue.Split('-')[0].Trim().ToString();
                            }
                            if (ddDistrictName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddDistrictName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            if (ddPsName.SelectedIndex != 0)
                            {
                                uniqueCode = uniqueCode + ddPsName.SelectedValue.Split('-')[0].Trim().ToString();
                            }

                            uniqueCode = uniqueCode + clsPageBase.GetUserType(Convert.ToInt32(ddUserRole.SelectedValue.ToString().Trim()));
                            if (txtAmount.Text.ToString().Trim() == "")
                            {
                                txtAmount.Text = "0";
                            }
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Password and Confirm Password Doesn't match</font>";
                        }
                    }
                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>User Name Already Exist, Plz Enter Another Name</font>";
                    }
                }

            }


        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }

    }
    protected void TextValidate(object source, ServerValidateEventArgs args)
    {
        char[] testarr = args.Value.ToCharArray();
        if (args.Value.Length >= 1)
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
            return;
        }
        for (int i = 0; i < testarr.Length; i++)
        {
            if (testarr[i] == ' ')
            {
                break;
            }
            if (!char.IsLetterOrDigit(testarr[i]))
            {
                args.IsValid = false;
                break;
            }
        }
    }

    private Boolean checkillegal()
    {
        bool illegal;
        illegal = false;
        if (txtUsername.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtAddress1.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtHeader.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtAuthority1.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtFileNo.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtAmount.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        this.Validate();
        bool ill = true;
        ill = checkillegal();
        if (ill == false)
        {
            if (IsValid)
            {
                try
                {
                    string encPassword = encrypt(txtPassword.Text);
                    string a, b, c, d;
                    Regex re = new Regex("[;\\/:*?\"<>|&'%^]'");

                    txtUsername.Text = re.Replace(txtUsername.Text, "");
                    txtAddress1.Text = re.Replace(txtAddress1.Text, "");
                    txtAuthority1.Text = re.Replace(txtAuthority1.Text, "");
                    txtHeader.Text = re.Replace(txtHeader.Text, "");
                    txtFileNo.Text = re.Replace(txtFileNo.Text, "");
                    txtAmount.Text = re.Replace(txtAmount.Text, "");
                    a = txtUsername.Text;
                    a = txtAddress1.Text;
                    c = txtAuthority1.Text;

                    if (txtAddress1.Text.ToString().Trim() != "")
                    {

                        if (eform.UpdateUser_Detail(txtUsername.Text, GenericMethods.check(txtFileNo.Text.ToString().Trim()), GenericMethods.check(txtAddress1.Text.ToString().Trim()), GenericMethods.check(txtAuthority1.Text.ToString().Trim()), txtemail.Text, txtAmount.Text, GenericMethods.check(txtHeader.Text.ToString().Trim())) > 0)
                        {
                            lblmsg.Text = "<font color=Blue>Updated Successfully !!!</font>";
                        }
                        else
                        {
                            lblmsg.Text = lblmsg.Text = "<font color=red>Not Updated ?? Try Again</font>";
                        }
                    }
                    else
                    {
                        lblmsg.Text = lblmsg.Text = "<font color=red>Address Can not be blank.</font>";
                    }

                }
                catch (SqlException)
                { lblmsg.Text = "Occuring Problem in connectivity to database."; }
                catch (HttpCompileException)
                { lblmsg.Text = "System Occuring Problem."; }
                catch (HttpParseException)
                { lblmsg.Text = "Internet Browser Occuring Problem."; }
                catch (Exception ex)
                {
                    lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
                }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    public void refresh()
    {

    }
    protected void gvUserDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        {
            lblmsg.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorMsg"].ToString();
        }
    }
    protected void gvUserDetails_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[4].Visible = false;
            e.Row.Cells[7].Visible = false;
            e.Row.Cells[8].Visible = false;
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[11].Visible = false;
        }
    }
    protected void gvUserDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvUserDetails.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {

    }


}

