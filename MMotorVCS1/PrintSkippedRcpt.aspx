﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="false" AutoEventWireup="true" CodeFile="PrintSkippedRcpt.aspx.vb" Inherits="PrintSkippedRcpt" Title="Vahan Samanvay Report" Theme="emsTheme"  EnableViewState="true"  EnableViewStateMac="true"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
     <Triggers> 
<asp:PostBackTrigger ControlID="prntrcpt" /> 
</Triggers> 
    <ContentTemplate>
            <table class="contentmain">
                <tr>
                <td><table class="tablerowcolor" align="center" width="80%">
                <tr class="heading">
                    
                    <td colspan="3" >
                        <asp:Label ID="Label3" runat="server" Text="Collection Report" Font-Bold="True"></asp:Label></td>
                   
                </tr>
                <tr>
                    <td style="width: 100px">
                        Receipt No.</td>
                    <td style="width: 100px">
                        <asp:TextBox ID="Rcptno" runat="server"></asp:TextBox></td>
                    <td style="width: 100px">
                    
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 100px">
                        &nbsp;</td>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="prntrcpt" runat="server" Text="Print Receipt" />
                        </td>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
            </table>
            </td> </tr> </table>
 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
