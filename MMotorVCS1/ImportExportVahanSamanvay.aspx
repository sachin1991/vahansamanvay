﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="false" AutoEventWireup="true" CodeFile="ImportExportVahanSamanvay.aspx.vb" Inherits="ImportExportVahanSamanvay" Title="Vahan Samanvay Import Export Module" Theme="emsTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
    <ContentTemplate>
<script language="javascript" type="text/javascript">
function File1_onclick() {

}

// ]]>
</script>
         
            <table class="contentmain"> <tr><td>
             <table class="tablerowcolor" align="center" width="80%">
           <tr class="heading"> <td colspan="4"> Import</td></tr>
                <tr>
                    <td style="width: 135px">
                        <asp:Label ID="Label1" runat="server" Text="Directory"></asp:Label>
                    </td>
                    <td style="width: 118px">
                        <asp:TextBox ID="dirpath1" runat="server" style="margin-right: 51px" 
                            Width="165px" Enabled="False"></asp:TextBox>
                    </td>
                    <td style="width: 100px">
                        <asp:FileUpload ID="FileImportMVCS" runat="server" />
                    <td style="width: 100px">
                        <asp:Button ID="ImportMVCS" runat="server" Text="Import MVCS Daily Transaction files" width="400px"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr runat="server" visible="false">
                    <td colspan="2">
                        </td>
                    <td style="width: 100px">
                        <input id="FileImportfromVahanSamanvay" runat="server" type="file" /></td>
                    <td style="width: 100px">
                        <asp:Button ID="ImportVahanSamanvay" runat="server" Text="Import from Vahan Samanvay Transaction Files" width="400px"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                    </td>
                </tr>
                <tr  runat="server" visible="false">
                    <td valign="top" colspan="2" >
                        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" 
                            style="width: 78px"></asp:ListBox></td>
                    <td style="width: 100px" valign="top">
                        <input id="FileExporttovahansamanvay" runat="server" type="file" visible="false" /></td>
                    <td style="width: 100px" valign="top">
                        <asp:Button ID="ExportVahanSamanvay" runat="server" Text="Export to Vahan Samanvay Daily Transction file" width="400px" /></td>
                </tr>
            </table>
             
             
      </td></tr>
      <tr > <td > 
      <table class="tablerowcolor" width="80%" align="center">
      <tr> <td >
              <asp:GridView ID="GridView1" runat="server" BackColor="White" 
        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" 
        EnableModelValidation="True" ForeColor="Black" GridLines="Vertical" width="97%">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        </asp:GridView>
        </td></tr>

        </table>
        </td></tr>

        </table>
</ContentTemplate>
</asp:Content>
