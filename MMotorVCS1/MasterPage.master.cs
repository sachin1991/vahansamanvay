using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MMotorVCS;
using System.Collections.Generic;
using System.Text;
using System.Web.SessionState;
using System.Data.Sql;
using System.Web.Services;
using System.Web.Configuration;

public partial class CCTNSMasterPage : System.Web.UI.MasterPage
{
    private int _roleId;
    ListItemCollection liColl;



    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScriptManager cs = Page.ClientScript;
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if (!this.IsPostBack)
        {
            try
            {
                Session["Reset"] = true;
                
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~/web.config");
                SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
                int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
                cs.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
            }
            catch
            {
            }
        }



        if (Session["vcStateCode"] == null)
        {

            Response.Redirect("Error.aspx");
        }
        else
        {
            string vcStateCode = Session["vcStateCode"].ToString();
            string loginUserName = Session["vcUserName"].ToString();
            _roleId = Convert.ToInt32(Session["inRoleId"]);
            lblWelcomeMsg.Text = "Welcome: " + loginUserName + "(" + vcStateCode + ")";
            clsPageBase mObj = new clsPageBase();
            lblUserRole.Text = GenericMethods.check(clsPageBase.GetUserName(_roleId).ToString().Trim());

        }
        Response.Cache.SetCacheability(HttpCacheability.NoCache);


        if (!IsPostBack)
        {
            PopulateMenu();
        }
        else
        {
            PopulateMenu();
        }


    }

    private void PopulateMenu()
    {

              /*Start Menu Here*/
        clsPageBase mObj = new clsPageBase();

        DataSet menuData = mObj.getTopMenu(_roleId);
        AddTopMenuItems(menuData);
               
    }

    /// Filter the data to get only the rows that have a
    /// null ParentID (This will come on the top-level menu items)

    private void AddTopMenuItems(DataSet menuData1)
    {
        DataTable menuData = menuData1.Tables[0];
        DataView view = new DataView(menuData);
        view.RowFilter = "inParentID =0";
        foreach (DataRowView row in view)
        {

            tm1.Add(null, row["inMenuId"].ToString(), row["vcMenuText"].ToString(), row["vcMenuLink"].ToString(), null);



            AddChildMenuItems(Convert.ToInt32(row["inMenuId"]));
        }

    }

       //This code is used to recursively add child menu items by filtering by ParentID
 
      private void AddChildMenuItems(int parentMenuItem)
       {

           clsPageBase mObj1 = new clsPageBase();
           DataSet menuChildData = mObj1.getSubMenu(_roleId, 1, parentMenuItem);
           DataTable menuData = menuChildData.Tables[0];
           DataView view = new DataView(menuData);
           view.RowFilter = "inParentID=" + parentMenuItem;

          
           foreach (DataRowView row in view)
           {

               tm1.Add(row["inParentID"].ToString(), row["inMenuId"].ToString(), row["vcMenuText"].ToString(), row["vcMenuLink"].ToString(), null);

           }
       }
      private string SafeSqlLiteral(string inputSQL)
      {
          return inputSQL.Replace("'", "''");

      }
}
