Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports MMotorVCS

Partial Class ReportStatistics
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Dim statoption As Integer
    Private Sub preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        Dim whr As String
        Dim mtf As String
        Dim ord As String
        mtf = Month(Me.ListDatefrom.Text.ToString())
        If Len(mtf) = 1 Then
            mtf = "0" & mtf
        End If
        Dim dtf As String
        dtf = Day(Me.ListDatefrom.Text.ToString())
        If Len(dtf) = 1 Then
            dtf = "0" & dtf
        End If
        Dim Mto As String
        Mto = Month(Me.listDateto.Text.ToString())
        If Len(Mto) = 1 Then
            Mto = "0" & Mto
        End If
        Dim dto As String
        dto = Day(Me.listDateto.Text.ToString())
        If Len(dto) = 1 Then
            dto = "0" & dto
        End If
        whr = ""
        If whr <> "" Then
            whr = " WHERE " & whr & "  and FIRDATE between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "' "
        Else
            whr = "WHERE FIRDATE between'" & Year(Me.ListDatefrom.Text) & "/" & mtf & "/" & dtf & "' AND '" & Year(Me.listDateto.Text) & "/" & Mto & "/" & dto & "' "
        End If
        If Me.RdState.Checked Then
            sql = "SELECT STDESC AS State, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.StatewiseStatView " & whr & " GROUP BY STDESC"
            statoption = 1
        End If
        If Me.RdStateDistrict.Checked Then
            sql = "SELECT STDESC AS State, distname as District, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.DistrictwiseStatView " & whr & "GROUP BY STDESC, distname"
            statoption = 2
        End If
        If Me.RdStateDistrictPS.Checked Then
            sql = "SELECT STDESC AS State, distname as District, psname as PS, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.PSwiseStatView " & whr & "GROUP BY STDESC, distname, psname"
            statoption = 3
        End If
        If Me.RdType.Checked Then
            sql = "SELECT autotypedesc AS TypeofVehicle, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.TypewiseStatView " & whr & "GROUP BY autotypedesc"
            statoption = 4
        End If
        If Me.RdMake.Checked Then
            sql = "SELECT autotypedesc AS TypeofVehicle, automakedesc as Make, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.MakewiseStatView " & whr & "GROUP BY autotypedesc, automakedesc "
            statoption = 5
        End If
        If Me.RdStateType.Checked Then
            sql = "SELECT STDESC AS State,autotypedesc as TypeofVehicle, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,autotypedesc"
            statoption = 6
        End If
        If Me.RdStateDistrictType.Checked Then
            sql = "SELECT STDESC AS State,distname as District, autotypedesc as TypeofVehicle, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,distname, autotypedesc"
            statoption = 7
        End If
        If Me.RdStateDistrictPSType.Checked Then
            sql = "SELECT STDESC AS State,distname as District, psname as PS, autotypedesc as TypeofVehicle, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,distname, psname, autotypedesc"
            statoption = 8
        End If
        If Me.RdStateTypeMake.Checked Then
            sql = "SELECT STDESC AS State, autotypedesc as TypeofVehicle, automakedesc as Make, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,autotypedesc, automakedesc"
            statoption = 9
        End If
        If Me.RdStateDistrictTypeMake.Checked Then
            sql = "SELECT STDESC AS State,distname as District, autotypedesc as TypeofVehicle,automakedesc as Make, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,distname,autotypedesc,automakedesc"
            statoption = 10
        End If
        If Me.RdStateDistrictPSTypeMake.Checked Then
            sql = "SELECT STDESC AS State,distname as District, psname as PS, autotypedesc as TypeofVehicle,automakedesc as Make, SUM(STLN) AS Stolen, SUM(RECVRD) AS Recovered, SUM(DELTD) AS Deleted FROM dbo.VehicledataStatisticsView " & whr & " GROUP BY STDESC,distname,psname, autotypedesc,automakedesc"
            statoption = 11
        End If
        Dim hvng As String
        Dim annd As Boolean
        annd = False
        hvng = ""
        If Me.ddlState.Text <> "ALL STATES" Then
            If annd = True Then
                hvng = hvng & " AND STDESC = '" & ddlState.Text.Trim() & "'"
            Else
                hvng = hvng & " HAVING STDESC = '" & ddlState.Text.Trim() & "'"
                annd = True
            End If
        End If
        If Me.Districtwise.Text <> "ALL DISTRICTS" Then
            If Me.RdStateDistrictType.Checked Or RdStateDistrictTypeMake.Checked Or RdStateDistrictPSType.Checked Or RdStateDistrictPSTypeMake.Checked Then
                If annd = True Then
                    hvng = hvng & " AND DISTNAME = '" & Districtwise.Text.Trim() & "'"
                Else
                    hvng = " HAVING DISTNAME = '" & Districtwise.Text.Trim() & "'"
                    annd = True
                End If
            End If
        End If
        If Me.PSWise.Text <> "ALL POLICE STATIONS" Then
            If Me.RdStateDistrictPS.Checked Or Me.RdStateDistrictPSType.Checked Or RdStateDistrictPSTypeMake.Checked Then
                If annd = True Then
                    hvng = hvng & " AND PSNAME = '" & PSWise.Text.Trim() & "'"
                Else
                    hvng = hvng & " HAVING PSNAME = '" & PSWise.Text.Trim() & "'"
                    annd = True
                End If
            End If
        End If
        If Me.TypeWise.Text <> "-- Select --" Then
            If annd = True Then
                hvng = hvng & " AND AUTOTYPEDESC = '" & TypeWise.Text.Trim() & "'"
            Else
                hvng = hvng & " HAVING AUTOTYPEDESC = '" & TypeWise.Text.Trim() & "'"
                annd = True
            End If
        End If
        If Me.MakeWise.Text <> "" And Me.MakeWise.Text <> "-- Select --" Then
            If Me.RdStateTypeMake.Checked Or RdStateDistrictTypeMake.Checked Or RdStateDistrictPSTypeMake.Checked Then
                If annd = True Then
                    hvng = hvng & " AND AUTOMAKEDESC = '" & MakeWise.Text.Trim() & "'"
                Else
                    hvng = hvng & " HAVING AUTOMAKEDESC = '" & MakeWise.Text.Trim() & "'"
                    annd = True
                End If
            End If
        End If
        sql = sql & hvng
        ord = ""
        If Me.RdState.Checked Then
            ord = " ORDER BY STDESC"
        End If
        If Me.RdStateDistrict.Checked Then
            ord = " ORDER BY STDESC, distname"
        End If
        If Me.RdStateDistrictPS.Checked Then
            ord = " ORDER BY STDESC, distname, psname"
        End If
        If Me.RdStateType.Checked Then
            ord = " order by stdesc, autotypedesc "
        End If
        If Me.RdStateDistrictType.Checked Then
            ord = " ORDER BY STDESC, distname, autotypedesc"
        End If
        If Me.RdStateDistrictPSType.Checked Then
            ord = " ORDER BY STDESC, distname, psname, autotypedesc"
        End If
        If Me.RdStateDistrictTypeMake.Checked Then
            ord = " ORDER BY STDESC, distname, autotypedesc,automakedesc"
        End If
        If Me.RdStateDistrictPSType.Checked Then
            ord = " ORDER BY STDESC, distname, psname, autotypedesc"
        End If
        If Me.RdStateDistrictPSTypeMake.Checked Then
            ord = " ORDER BY STDESC, distname, psname, autotypedesc, automakedesc"
        End If
        If Me.RdType.Checked Then
            ord = " ORDER BY autotypedesc"
        End If
        If Me.RdMake.Checked Then
            ord = " ORDER BY autotypedesc, automakedesc "
        End If
        sql = sql & ord
        Session("SQLSTRING") = sql
        Session("DATEFROM") = Me.ListDatefrom.Text
        Session("DATETO") = Me.listDateto.Text
        Session("WPB") = Me.withpagebreak.Checked
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim result As Integer = DateTime.Compare(Me.ListDatefrom.Text, Me.listDateto.Text)
        Dim relationship As String
        If result < 0 Then
            relationship = "is earlier than"

        ElseIf result = 0 Then
            relationship = "is the same time as"
        Else
            relationship = "is later than"
            Msglabel.Text = "Date to is less than date from"
            Exit Sub
        End If
        Msglabel.Text = ""

        preparesql()
        If statoption = 1 Then
            Response.Write("<script>window.open('ReportStatisticsview1.aspx','_blank')</script>")
        End If
        If statoption = 2 Then
            Response.Write("<script>window.open('ReportStatisticsview2.aspx','_blank')</script>")
        End If
        If statoption = 3 Then
            Response.Write("<script>window.open('ReportStatisticsview3.aspx','_blank')</script>")
        End If
        If statoption = 4 Then
            Response.Write("<script>window.open('ReportStatisticsview4.aspx','_blank')</script>")
        End If
        If statoption = 5 Then
            Response.Write("<script>window.open('ReportStatisticsview5.aspx','_blank')</script>")
        End If
        If statoption = 6 Then
            Response.Write("<script>window.open('ReportStatisticsview6.aspx','_blank')</script>")
        End If
        If statoption = 7 Then
            Response.Write("<script>window.open('ReportStatisticsview7.aspx','_blank')</script>")
        End If
        If statoption = 8 Then
            Response.Write("<script>window.open('ReportStatisticsview8.aspx','_blank')</script>")
        End If
        If statoption = 9 Then
            Response.Write("<script>window.open('ReportStatisticsview9.aspx','_blank')</script>")
        End If
        If statoption = 10 Then
            Response.Write("<script>window.open('ReportStatisticsview10.aspx','_blank')</script>")
        End If
        If statoption = 11 Then
            Response.Write("<script>window.open('ReportStatisticsview11.aspx','_blank')</script>")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim cmdState, cmdMake, cmdInvStatus, cmdExp As New SqlCommand

        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If

        If Not IsPostBack Then
            Me.ListDatefrom.Text = Date.Today.ToString("dd/MM/yyyy")
            Me.listDateto.Text = Date.Today.ToString("dd/MM/yyyy")
        End If


        Dim dr As SqlDataReader
        Dim cn As New SqlConnection
        If Not Me.IsPostBack Then

            Dim ConStr As ConnectionStringSettings
            ConStr = ConfigurationManager.ConnectionStrings("Conn")
            cn.ConnectionString = MMotorVCS.DTask.BuildConnectionString()

            cn.Open()


            If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Session("inRoleId") <> 2 And Session("vcStateCode1") <> "0" Then
                Dim cmdstate1 As New SqlCommand
                cmdstate1.CommandText = "Select Distinct statename From vw_ps Where statecode ='" & Session("vcStateCode1") & "'"
                cmdstate1.Connection = cn
                dr = cmdstate1.ExecuteReader
                If dr.HasRows Then
                    Do While dr.Read
                        Me.ddlState.Items.Add(dr("Statename"))
                    Loop
                End If
                ddlStateSelected()
                dr.Close()
                Me.ddlState.Enabled = False
                If Session("vcDistrict") <> "" And Session("vcDistrict") <> "0" Then
                    Dim cmdDistrict1 As New SqlCommand
                    cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
                    cmdDistrict1.Connection = cn
                    dr = cmdDistrict1.ExecuteReader
                    If dr.HasRows Then
                        Do While dr.Read
                            Me.Districtwise.Items.Add(dr("districtname"))
                        Loop
                    End If
                    dr.Close()
                    DistrictwiseSelected()
                    Me.Districtwise.Enabled = False
                End If
                If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
                    Dim cmdps1 As New SqlCommand
                    cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
                    cmdps1.Connection = cn
                    dr = cmdps1.ExecuteReader
                    If dr.HasRows Then
                        Do While dr.Read
                            PSWise.Items.Add(dr("psname"))
                        Loop
                    End If
                    dr.Close()
                    Me.PSWise.Enabled = False
                End If
            End If
            If (Convert.ToInt32(Session("inRoleId")) = 3) Then
                cmdState.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=1 and code ='" + Session("vcStateCode") + "'  Order By Description" ' where cReportId like ('%" & repId & "')"
            Else
                If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Session("vcStateCode1") <> "0" Then

                    cmdState.CommandText = "Select Distinct statename From vw_ps Where statecode ='" & Session("vcStateCode1") & "'"
                Else
                    cmdState.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=1  Order By Description" ' where cReportId like ('%" & repId & "')"
                End If


            End If
            cmdState.Connection = cn
            dr = cmdState.ExecuteReader
            If Session("vcStateCode1") <> "" And Session("vcStateCode1") <> "000" And Session("vcStateCode1") <> "0" Then
                If dr.HasRows Then
                    Do While dr.Read
                        ddlState.Items.Add(dr("statename"))
                    Loop
                End If
            Else
                ddlState.Items.Add("ALL STATES")
                If dr.HasRows Then
                    Do While dr.Read
                        ddlState.Items.Add(dr("Description"))
                    Loop
                End If
            End If
            dr.Close() ' added new
            Dim cmdType As New SqlCommand
            Dim drType As SqlDataReader
            Dim cnType As New SqlConnection
            cnType.ConnectionString = MMotorVCS.DTask.BuildConnectionString()
            cnType.Open()
            cmdType.CommandText = "select distinct  vehicletypecode+ '-' +  convert(varchar, vehicletypeid) as vehidvehcode,vehicletypename   from vehicle_type "
            cmdType.Connection = cnType
            drType = cmdType.ExecuteReader

            TypeWise.Items.Add("-- Select --")
            If drType.HasRows Then
                Do While drType.Read
                    TypeWise.Items.Add(drType(1).ToString())
                Loop
            End If
            If Session("vcdistrict") = "" Or Session("vcdistrict") = "0" Then
                Districtwise.Items.Add("ALL DISTRICTS")
            End If
            If Session("vcPS") = "" Or Session("vcPS") = "0" Then
                PSWise.Items.Add("ALL POLICE STATIONS")
            End If
            drType.Close() 'added new
            dr.Close()
            cn.Close()
        End If
        Dim value As String = Session("inRoleid").ToString()
        Select Case value
            Case "1" 'admin
                Exit Select
            Case "10" 'crime records
                Exit Select
            Case "12" 'ncrbexecutive
                Exit Select
            Case "6" 'state
                Exit Select
            Case "7" 'District
                Exit Select
            Case "11" 'CR_Executive1
                Exit Select
            Case "22" 'Intern
                Exit Select
            Case Else
                Response.Redirect("LogoutModule.aspx")
                Exit Select
        End Select
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect(Request.RawUrl)
    End Sub

    Protected Sub TypeWise_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TypeWise.SelectedIndexChanged

        Dim cmdMake As New SqlCommand
        Dim cnMake As New SqlConnection
        Dim dr As SqlDataReader

        cnMake.ConnectionString = MMotorVCS.DTask.BuildConnectionString()

        cnMake.Open()

        cmdMake.CommandText = "select distinct  makecode+ '-' +  convert(varchar, makecode) as makeidmakecode,makename from make_of_vehicle where vehicletypeid= (Select vehicletypeid from vehicle_type Where VehicleTypeName like '%" & TypeWise.Text.Trim() & "%') order by makename"
        cmdMake.Connection = cnMake

        dr = cmdMake.ExecuteReader

        MakeWise.Items.Clear()
        MakeWise.Items.Add("-- Select --")
        If dr.HasRows Then
            Do While dr.Read
                MakeWise.Items.Add(dr("makename"))
            Loop
        End If
        dr.Close()
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlState.SelectedIndexChanged
        Dim cmdDistrict As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader


        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")

        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        cmdDistrict.CommandText = "Select Distinct Description From TblMst_Location Where LocationType=2 and ParentKey= (Select Code from TblMst_Location Where Description like '" & ddlState.Text & "' and LocationType=1) Order By Description"
        Dim cmdDistrict1 As New SqlCommand
        cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statename ='" & ddlState.Text & "'"
        cmdDistrict.Connection = cn
        dr = cmdDistrict.ExecuteReader
        Districtwise.Items.Clear()
        Districtwise.Items.Add("ALL DISTRICTS")
        If dr.HasRows Then
            Do While dr.Read
                Districtwise.Items.Add(dr("Description"))
            Loop
        End If
        dr.Close()

        If (Session("vcDistrict") <> "" And Session("vcDistrict") <> "0") Then

            cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
            cmdDistrict1.Connection = cn
            dr = cmdDistrict1.ExecuteReader
            Districtwise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    Me.Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
            DistrictwiseSelected()
            Me.Districtwise.Enabled = False
        End If
        cn.Close()

    End Sub
    Protected Sub ddlStateSelected()
        Dim cmdDistrict As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")

        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        If Session("vcDistrict") = "" Then
            cmdDistrict.CommandText = "Select distinct districtname From vw_ps Where statename like '" & ddlState.Text & "' Order By districtname"
            cmdDistrict.Connection = cn
            dr = cmdDistrict.ExecuteReader
            Districtwise.Items.Clear()
            Districtwise.Items.Add("ALL DISTRICTS")
            If dr.HasRows Then
                Do While dr.Read
                    Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
        End If

        If Session("vcDistrict") <> "" Then
            Dim cmdDistrict1 As New SqlCommand
            cmdDistrict1.CommandText = "Select Distinct districtname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "'"
            cmdDistrict1.Connection = cn
            dr = cmdDistrict1.ExecuteReader
            Me.Districtwise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    Me.Districtwise.Items.Add(dr("districtname"))
                Loop
            End If
            dr.Close()
            Me.Districtwise.Enabled = False
        End If
        cn.Close()

    End Sub


    Protected Sub Districtwise_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Districtwise.SelectedIndexChanged
        Dim cmdPS As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim StateCode, DistrictCode As String
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        cmdPS.Connection = cn

        If Me.Districtwise.Text <> "ALL DISTRICTS" Then
            cmdPS.CommandText = "Select Distinct psname From vw_ps Where statename like '" & ddlState.Text & "%' and districtname like '" & Districtwise.Text & "%'   Order By psname"
            dr = cmdPS.ExecuteReader
            PSWise.Items.Clear()
            PSWise.Items.Add("ALL POLICE STATIONS")
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
                dr.Close()

            End If
        End If
        'End If
        If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
            Dim cmdps1 As New SqlCommand
            cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode='" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
            cmdps1.Connection = cn
            dr = cmdps1.ExecuteReader
            PSWise.Items.Clear()
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
            End If
            dr.Close()
            Me.PSWise.Enabled = True
        End If
        cn.Close()
    End Sub
    Protected Sub DistrictwiseSelected()
        Dim cmdPS As New SqlCommand
        Dim cn As New SqlConnection
        Dim dr As SqlDataReader
        Dim StateCode, DistrictCode As String
        Dim ConStr As ConnectionStringSettings
        ConStr = ConfigurationManager.ConnectionStrings("Conn")
        cn.ConnectionString = ConStr.ConnectionString
        cn.ConnectionString = DTask.BuildConnectionString()
        cn.Open()

        If Session("vcPS") = "" Or Session("vcPS") = "0" Then
            cmdPS.Connection = cn
            If Me.Districtwise.Text <> "ALL DISTRICTS" Then
                cmdPS.CommandText = "Select psname From vw_ps Where  statename like '" & ddlState.Text & "' and districtname like '" & Me.Districtwise.Text & "%'   Order By statename"
                dr = cmdPS.ExecuteReader
                PSWise.Items.Clear()
                PSWise.Items.Add("ALL POLICE STATIONS")
                If dr.HasRows Then
                    Do While dr.Read
                        PSWise.Items.Add(dr("psname"))
                    Loop
                    dr.Close()

                End If
            End If
        End If
        If Session("vcPS") <> "" And Session("vcPS") <> "0" Then
            Dim cmdps1 As New SqlCommand
            cmdps1.CommandText = "Select Distinct psname From vw_ps Where statecode ='" & Session("vcStatecode1") & "' and districtcode like '" & Session("vcDistrict") & "' and pscode = '" & Session("vcPS") & "'"
            cmdps1.Connection = cn
            dr = cmdps1.ExecuteReader
            If dr.HasRows Then
                Do While dr.Read
                    PSWise.Items.Add(dr("psname"))
                Loop
            End If
            dr.Close()
            Me.PSWise.Enabled = True
        End If
        cn.Close()

    End Sub

    Protected Sub databasesize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles databasesize.Click
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim cmd1 As New SqlCommand
        Dim sql1 As String
        sql1 = "Select count(vcCrime_no) as noofrecords from [TblTrn_EnterLostRecoveredVehicle] where isDeleted=0"
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        consupac.Open()

        With cmd1
            .Connection = consupac
            .CommandText = sql1
        End With
        Dim da1 As New SqlDataAdapter(cmd1)

        Dim dt1 As New DataTable()
        da1.Fill(dt1)
        consupac.Close()
        Me.lbldatabasesize.Text = dt1.Rows(0).Item("noofrecords")

    End Sub

    Protected Sub Converttoexecl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Converttoexecl.Click
        preparesql()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
            .CommandTimeout = 0
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DataTable.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sb As New StringBuilder()
        For k As Integer = 0 To dt.Columns.Count - 1
            sb.Append(dt.Columns(k).ColumnName + ","c)
        Next
        sb.Append(vbCr & vbLf)
        For i As Integer = 0 To dt.Rows.Count - 1
            For k As Integer = 0 To dt.Columns.Count - 1
                sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
            Next
            sb.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sb.ToString())
        Response.Flush()
        Response.End()
    End Sub
End Class
