﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using MMotorVCS;

public partial class Printskippedcounterrcpt : System.Web.UI.Page
{
    public DataTable dv,dv1;
    DataSet dtCounterdata;
    DataSet dtemail;
    Entry_FormMethods eform = new Entry_FormMethods();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
            {
                Response.Redirect("LogoutModule.aspx");
            }


            if (Session["inRoleId"].ToString() == "2" || Session["inRoleId"].ToString() == "5" || Session["inRoleId"].ToString() == "1")
            {
                
                if (lblAmount.Text.ToString() == "0")
                {
                    Original.Text = "Duplicate";
                }
                else
                {
                    Original.Text = "";
                }
                lblmsg.Text = " ";
                dv1 = (DataTable)Session["Data"];

                lblDated.Text = dv1.Rows[0]["dtENQdt"].ToString();
                CashIPO.Text = "";
                if (dv1.Rows[0]["inReceiptid"].ToString().Trim() != "")
                {
                    String IPo;
                    IPo = dv1.Rows[0]["IPONo"].ToString();
                    CashIPO.Text = "IPO " + IPo;
                }
                else
                {
                    CashIPO.Text = "Cash";
                }
                
                lblReciept.Text = dv1.Rows[0]["inReceiptid"].ToString();
                lblName.Text = dv1.Rows[0]["vcAPPName"].ToString();
                lblAmount.Text = dv1.Rows[0]["CashAmt"].ToString();
                lblUpdationDate.Text = dv1.Rows[0]["dtEnqDt"].ToString();
                lblenqdatetime.Text = " " + dv1.Rows[0]["dtEnqDt"].ToString();
                if (dv1.Rows[0]["vcFIRNo"].ToString() == "0")
                {
                    lblmsg.Text = " is matched with";
                    dv = (DataTable)Session["Data1"];
                    int _count1 = Convert.ToInt32(dv.Rows.Count);
                    if (_count1 >= 1)
                    {
                        Repeater1.DataSource = dv;
                        Repeater1.DataBind();
                        String profilename;
                        String receipients;
                        String subject;
                        String bodytext;
                        profilename = "SQLProfile";
                        subject = "Vahan Samanvay Motor Vehicle Coordination";
                        receipients = "";

                        String bt;
                        bt = "";
                        foreach (DataRow dtRow in dv.Rows)
                        {
                            // on all table's columns
                            foreach (DataColumn dc in dv.Columns)
                            {
                                if (dc.ColumnName.ToString() == "STCODE")
                                {
                                    String std;
                                    std = dtRow[dc].ToString();
                                    dtemail = eform.getemail(std);
                                    receipients = receipients + dtemail.Tables[0].Rows[0]["vcMail"].ToString();
                                }
                                else
                                {
                                    bt = bt + dc.ColumnName.ToString() + ": " + dtRow[dc].ToString() + "(.) ";
                                }
                            }

                        }
                        bodytext = lblName.Text + " at " + lblAddress.Text + " " + lblReciept.Text + " enquired the particular of Vehicle Type " + Session["VehicleTypedesc"] + "(.)Make " + Session["MakeDesc"] + "(.)Registration " + Session["Registration"] + "(.)Chasis " + Session["Chasis"] + "(.)Engine " + Session["Engine"] + "(.)Color " + Session["Color"] + "(.)Year of Manufacture " + Session["Model"] + " is linked with " + bt;
                        DataSet dsmale = new DataSet();
                        dsmale = eform.sendmail(profilename, receipients, subject, bodytext);

                    }
                }
                if (dv1.Rows[0]["vcFirNo"].ToString() != "0")
                {
                    lblmsg.Text = " is not recovered yet.";
                }
                if (dv1.Rows[0]["vcFirNo"].ToString() == "0")
                {
                    lblmsg.Text = " has not been reported as stolen by police.";
                }
                string _VehicleTypeDesc = dv1.Rows[0]["Vehicle"].ToString();
                string _MakeDesc = dv1.Rows[0]["Make"].ToString();
                string _Registration = dv1.Rows[0]["Registration"].ToString();
                string _Chasis = dv1.Rows[0]["Chasis"].ToString();
                string _Engine = dv1.Rows[0]["Engine"].ToString();
                string _usrnam = dv1.Rows[0]["vcUserName"].ToString();
                lblvectype.Text = dv1.Rows[0]["Vehicle"].ToString();
                lblmake.Text = dv1.Rows[0]["Make"].ToString();
                lblregistrationno.Text = dv1.Rows[0]["Registration"].ToString();
                lblchasisno.Text = dv1.Rows[0]["Chasis"].ToString();
                lblengineno.Text = dv1.Rows[0]["Engine"].ToString();
                dtCounterdata = eform.getCounterHeader1(_usrnam);
                captcha.Text = Session["strRandom"].ToString() + " " + Session["visitor"].ToString();
                if ((dtCounterdata.Tables[0].Rows.Count) > 0)
                {
                    lblAddress.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0][1].ToString().Trim());
                    lblFooter.Text = GenericMethods.check(dtCounterdata.Tables[0].Rows[0]["vcAuthority"].ToString().Trim());
                }
                else
                {

                }

            }
        }
        catch
        {
        }
        
    }

    
}

    
 
