using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public partial class Make_of_Vehicle : System.Web.UI.Page
{
    static int mtype_id;
    static string _userid;
    MasterMethods mm = new MasterMethods();
   
    static int pagecount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        btnAdd.Attributes.Add("onclick", "return valid();");
        btnUpdate.Attributes.Add("onclick", "return valid();");

        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {

            Response.Redirect("LogoutModule.aspx");
        }

        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            
        }      
        if (!IsPostBack)
        {
            try
            {
               
            }
            catch
            { 
                Response.Redirect("error.aspx");
            }
            btnUpdate.Visible = true;
            btnAdd.Visible = true;
            btnReset.Visible = true;
            getmaxid();
            bindVehicleType();
        }
        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1":
                break;
            case "8":
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    public void getmaxid()
    {
        try
        {
            mtype_id = mm.getVehMakeMaxId();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleType();
            ddVehCode.DataSource = ds;
            ddVehCode.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehCode.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehicletypeid"].ColumnName);
            ddVehCode.DataBind();
            ddVehCode.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void bindgrid()
    {
        try
        {
            DataSet ds = new DataSet();
            if (makenamewise.Checked == true)
            {
                ds = mm.getVehMakename(txtVehCode.Text.ToString());
            }
            else
            {
                ds = mm.getVehMakecode(txtVehCode.Text.ToString());
            }
            gvVehMake.DataSource = ds;
            gvVehMake.DataBind();
        }
        catch (SqlException)
        { lblmsg.Text = "Occuring Problem in connectivity to database."; }
        catch (HttpCompileException)
        { lblmsg.Text = "System Occuring Problem."; }
        catch (HttpParseException)
        { lblmsg.Text = "Internet Browser Occuring Problem."; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void refresh()
    {
        btnUpdate.Visible = true;
        btnAdd.Visible = true;
        btnReset.Visible = true;
        getmaxid();
        txtMakeCode.Text = "";
        txtMakeName.Text = "";
        txtVehCode.Text = "";
        ddVehCode.SelectedIndex = 0;
        bindgrid();
    }
    private Boolean checkillegal()
    {
        bool illegal;
        illegal = false;
        if (txtMakeCode.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtMakeName.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (txtVehCode.Text.ToString().Trim().IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake();
            int flag = 0;
            if (ddVehCode.SelectedIndex != 0 && txtMakeCode.Text != "" && txtMakeName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["makeid"].ToString()) != lblid.Text)
                    {
                        if (GenericMethods.check(ds.Tables[0].Rows[i]["makecode"].ToString().Trim().ToUpper()) == txtMakeCode.Text.Trim().ToUpper() && ds.Tables[0].Rows[i]["vehicleTypeid"].ToString().Trim().ToUpper() == ddVehCode.SelectedValue.ToString() )
                        {
                            flag = 0;
                        }
                    }
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal();
                    if (illegal == false)
                    {
                        if (mm.updateVehMake(Convert.ToInt32(GenericMethods.check(ddVehCode.SelectedValue)), (GenericMethods.check(txtMakeCode.Text.Trim().ToUpper())), GenericMethods.check(txtMakeName.Text.Trim().ToUpper()), _userid, GenericMethods.check(txtVehCode.Text.ToString())) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values Updated</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not Updated</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        refresh();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehMake();
            int flag = 0;
            if (ddVehCode.SelectedIndex != 0 && txtMakeCode.Text != "" && txtMakeName.Text != "")
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (GenericMethods.check(ds.Tables[0].Rows[i]["makecode"].ToString().Trim().ToUpper()) == txtMakeCode.Text.Trim().ToUpper() && ds.Tables[0].Rows[i]["vehicleTypeid"].ToString().Trim().ToUpper() == ddVehCode.SelectedValue.ToString())
                    {
                        flag = 1;
                        lblmsg.Text = "<font color=red>This Make Code is already exists.</font>";
                        break;
                    }
                }
                if (flag != 1)
                {
                    bool illegal = checkillegal();
                    if (illegal == false)
                    {
                        if (mm.AddVehMake((mtype_id), Convert.ToInt32(GenericMethods.check(ddVehCode.SelectedValue)), (GenericMethods.check(txtMakeCode.Text.Trim().ToUpper())), GenericMethods.check(txtMakeName.Text.Trim().ToUpper()), _userid, GenericMethods.check(txtVehCode.Text.ToString())) > 0)
                        {
                            lblmsg.Text = "<font color=green>Values inserted</font>";
                            refresh();
                        }
                        else
                        {
                            lblmsg.Text = "<font color=red>Values not inserted</font>";
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                    }
                }
            }
            else
            {
                lblmsg.Text = "<font color=red>Fill all (*) Fields</font>";
            }

        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void gvVehMake_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName != "Page")
            {
                btnUpdate.Visible = true;
                btnAdd.Visible = false;
                btnReset.Visible = true;
                ddVehCode.Text = gvVehMake.Rows[Convert.ToInt32(e.CommandName) - (gvVehMake.PageSize * pagecount)].Cells[1].Text;
                txtVehCode.Text = gvVehMake.Rows[Convert.ToInt32(e.CommandName) - (gvVehMake.PageSize * pagecount)].Cells[2].Text;
                lblid.Text = gvVehMake.Rows[Convert.ToInt32(e.CommandName) - (gvVehMake.PageSize * pagecount)].Cells[4].Text;
                txtMakeCode.Text = gvVehMake.Rows[Convert.ToInt32(e.CommandName) - (gvVehMake.PageSize * pagecount)].Cells[5].Text;
                txtMakeName.Text = gvVehMake.Rows[Convert.ToInt32(e.CommandName) - (gvVehMake.PageSize * pagecount)].Cells[6].Text;
            }
        }
        catch (SqlException ex)
        {
            lblmsg.Text = ex.Message;
        }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    protected void ddVehCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        if (ddVehCode.SelectedIndex != 0)
        {
            ds = mm.getVehiclecode(Convert.ToInt32(ddVehCode.SelectedValue));
            txtVehCode.Text = GenericMethods.check(ds.Tables[0].Rows[0]["vehicletypecode"].ToString());
            bindgrid();
        }
        else
        {
            txtVehCode.Text = "";
        }
    }
    protected void gvVehMake_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        pagecount = e.NewPageIndex;
        gvVehMake.PageIndex = e.NewPageIndex;
        refresh();
    }
    protected void gvVehMake_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.Pager)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[4].Visible = false;
        }
    }
    protected void makenamewise_CheckedChanged(object sender, EventArgs e)
    {
        bindgrid();
    }
}
