<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="proleright.aspx.cs" Inherits="proleright" Title="Vahan Samanvay Role Rights" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center><%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
        <contenttemplate>

<table class="contentmain"><tr> <td> 
<TABLE class="tablerowcolor" width="80%" align="center">
<TBODY><TR><TD colspan="3" class="heading">Assign Rights to Role </TD></TR><TR><TD style="HEIGHT: 17px; TEXT-ALIGN: left" colSpan=3><asp:Label id="lblchldPrntId" runat="server" Visible="False"></asp:Label> <asp:Label id="lblSubchldPrntId" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 216px; HEIGHT: 17px; TEXT-ALIGN: right" colSpan=2>Choose Role &nbsp;&nbsp; &nbsp;</TD><TD style="WIDTH: 672px; HEIGHT: 17px; TEXT-ALIGN: left" colSpan=1><asp:DropDownList id="ddlRole" runat="server" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList></TD></TR><TR><TD style="WIDTH: 216px; HEIGHT: 17px; TEXT-ALIGN: left" colSpan=2></TD><TD style="WIDTH: 672px; HEIGHT: 17px; TEXT-ALIGN: left" colSpan=1></TD></TR><TR><TD style="TEXT-ALIGN: left" colspan="3"><h4>Root Menu </h4></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=3><asp:Label id="lblmsg" runat="server" __designer:wfdid="w101"></asp:Label></TD></TR><TR><TD colSpan=3>
    <asp:GridView id="gvForms" runat="server" Width="97%" Visible="False" 
        OnRowCreated="gvForms_RowCreated" OnRowUpdating="gvForms_RowUpdating" 
        OnRowEditing="gvForms_RowEditing" OnRowCommand="gvForms_RowCommand" 
        OnRowCancelingEdit="gvForms_RowCancelingEdit" GridLines="Vertical" 
        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" BackColor="White" 
        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" /><FooterStyle BackColor="#CCCC99" />
                    <Columns>
                        <asp:TemplateField HeaderText="FORM&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;NAME">                            
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RIGHT&#160;ID">
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblRightsId" runat="server" ></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;VIEW">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;ADD">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;EDIT">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;Delete">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IS&nbsp;ATTACHED">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />
                            </ItemTemplate>
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Child">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CommandName='<%# Container.DataItemIndex %>'
                                    ForeColor="Green" Text="Child&nbsp;Forms"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 15px" colSpan=3></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: left" colSpan=3><asp:Label id="lblChild" runat="server"></asp:Label></TD></TR><TR><TD style="HEIGHT: 15px; TEXT-ALIGN: left" colSpan=3>
    <asp:GridView id="gvChild" runat="server" Width="97%" Visible="False" 
        OnRowCreated="gvChild_RowCreated" OnRowUpdating="gvChild_RowUpdating" 
        OnRowEditing="gvChild_RowEditing" OnRowCommand="gvChild_RowCommand" 
        OnRowCancelingEdit="gvChild_RowCancelingEdit" GridLines="Vertical" 
        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" BackColor="White" 
        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <Columns>
                        <asp:TemplateField HeaderText="FORM&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;NAME">                            
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RIGHT&#160;ID">
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblRightsId" runat="server" ></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;VIEW">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;ADD">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;EDIT">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;Delete">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IS&nbsp;ATTACHED">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />
                            </ItemTemplate>
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Child">
                            <ItemTemplate>
                                <asp:LinkButton ID="snum" runat="server" CommandName='<%# Container.DataItemIndex %>'
                                    ForeColor="Green" Text="Child&nbsp;Forms"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 17px" colSpan=3></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=3><asp:Label id="lblSubChild" runat="server"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=3>
    <asp:GridView id="gvSubChild" runat="server" Width="97%" Visible="False" 
        OnRowCreated="gvSubChild_RowCreated" OnRowUpdating="gvSubChild_RowUpdating" 
        OnRowEditing="gvSubChild_RowEditing" OnRowCommand="gvSubChild_RowCommand" 
        OnRowCancelingEdit="gvSubChild_RowCancelingEdit" GridLines="Vertical" 
        ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" BackColor="White" 
        BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
        EnableModelValidation="True">
                    <FooterStyle BackColor="#CCCC99" />
                    <Columns>
                        <asp:TemplateField HeaderText="FORM&#160;ID">
                            <ItemTemplate>
                                <asp:Label ID="lblFormId" runat="server" Text='<%# Eval("inMenuId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;NAME">                            
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("vcMenuText") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="RIGHT&#160;ID">
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:Label ID="lblRightsId" runat="server" ></asp:Label>
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;VIEW">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmView" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;ADD">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAdd" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;EDIT">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmEdit" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORM&#160;Delete">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmDelete" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IS&nbsp;ATTACHED">
                            <EditItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="true" />
                            </EditItemTemplate>
                            <ItemStyle Width="160px" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFrmAtch" runat="server" Enabled="false" />
                            </ItemTemplate>
                            <FooterStyle Width="160px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ShowHeader="False">
                            <EditItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="True" CommandName="Update"
                                    ImageUrl="~/img/updt_icon.gif" ToolTip="Update" />
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    ImageUrl="~/img/cancel_icon.gif" ToolTip="Cancel" />
                            </EditItemTemplate>
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="False" CommandName="Edit"
                                    ImageUrl="~/img/edit_icon.gif" ToolTip="Edit" />
                            </ItemTemplate>
                            <FooterStyle Width="100px" />
                        </asp:TemplateField>                        
                    </Columns>
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView> </TD></TR><TR><TD colSpan=3></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=3></TD></TR><TR><TD style="HEIGHT: 18px; TEXT-ALIGN: left" colSpan=3></TD></TR><TR><TD style="TEXT-ALIGN: left" colSpan=3><IMG alt="Edit" src="img/edit_icon.gif" />&nbsp; Edit Data&nbsp; <IMG alt="Update" src="img/updt_icon.gif" />&nbsp; Submit Data&nbsp; <IMG alt="Cancel" src="img/cancel_icon.gif" />&nbsp; Cancel</TD></TR></TBODY></TABLE>
                </td></tr></table>
</contenttemplate>
    <%--</asp:UpdatePanel>--%>&nbsp;</center>
</asp:Content>

