using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using MMotorVCS;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LogoutModule : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (clsPageBase.deletealreadyloggedin(Session["vcUserName"].ToString()) > 0)
            {
            }
            Session[Session.SessionID] = null;
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Session["vcStateCode"] = null;
            Session["inRoleId"] = null;
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Redirect("Login.aspx");
        }
        catch
        {
        }
        
    }
}
