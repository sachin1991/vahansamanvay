Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient

Partial Class ReportStatisticsView10
    Inherits System.Web.UI.Page
    Dim consupac As New SqlConnection
    Dim totalPages As Integer
    Dim sno
    Dim lns As Integer
    Public Stolens
    Public Recovereds
    Public Deleteds

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("rndNo").ToString() <> Request.Cookies("myCookieVahan").Value Then

            Response.Redirect("LogoutModule.aspx")

        End If
        If Not IsPostBack Then
            sno = 0
        End If
        lns = 30
        Me.GOI.Visible = True
        Me.MHA.Visible = True
        Me.NCRB.Visible = True
        Me.VS.Visible = True
        Me.LISTOFSTOLEN.Text = "Database Statistics From "
        Me.LISTOFSTOLEN.Visible = True
        Me.LISTTO.Visible = True
        Me.DTFROM.Visible = True
        Me.DTTO.Visible = True
        Me.DTFROM.Text = Session("Datefrom")
        Me.DTTO.Text = Session("Dateto")
        VSPAGEDATA()
    End Sub
    Public Property CurrentPageNumber() As Integer
        Get
            If ViewState("PageNumber") IsNot Nothing Then
                Return Convert.ToInt32(ViewState("PageNumber"))
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PageNumber") = value
        End Set
    End Property

    Private Sub VSPAGEDATA()
        consupac.ConnectionString = ConfigurationManager.ConnectionStrings("Conn").ToString()
        Dim sql As String
        Dim cmd As New SqlCommand
        sql = Session("SQLSTRING")
        With cmd
            .Connection = consupac
            .CommandText = sql
        End With
        consupac.Open()
        Dim da As New SqlDataAdapter(cmd)
        Dim dt As New DataTable()
        da.Fill(dt)
        consupac.Close()
        resettotals()

        Dim pagedItems As New PagedDataSource()
        pagedItems.DataSource = dt.DefaultView
        pagedItems.AllowPaging = True
        pagedItems.CurrentPageIndex = CurrentPageNumber
        Dim rowsCount As Integer = dt.Rows.Count
        If Session("WPB") = True Then
            pagedItems.PageSize = lns
        Else
            pagedItems.PageSize = rowsCount
        End If
        If Session("WPB") = True Then
            totalPages = rowsCount / lns
        Else
            totalPages = 0
        End If

        If CurrentPageNumber > 0 Then
            Lnkbtnprev.Visible = True
            FirstPage.Visible = True
        Else
            Lnkbtnprev.Visible = False
            FirstPage.Visible = False
        End If
        If CurrentPageNumber < totalPages Then
            LnkbtnNext.Visible = True
            LastPage.Visible = True
        Else
            LnkbtnNext.Visible = False
            LastPage.Visible = False
        End If
        Repeater1.DataSource = pagedItems
        Repeater1.DataBind()
        consupac.Close()
    End Sub

    Protected Sub lnkbtnPrev_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Lnkbtnprev.Click
        CurrentPageNumber -= 1
        sno = CurrentPageNumber * lns
        VSPAGEDATA()
    End Sub
    Protected Sub lnkbtnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles LnkbtnNext.Click
        CurrentPageNumber += 1
        sno = CurrentPageNumber * lns
        VSPAGEDATA()
    End Sub

    Protected Sub FirstPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles FirstPage.Click
        CurrentPageNumber = 0
        sno = CurrentPageNumber * lns
        VSPAGEDATA()
    End Sub

    Protected Sub LastPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LastPage.Click
        CurrentPageNumber = totalPages
        sno = CurrentPageNumber * lns
        VSPAGEDATA()
    End Sub
    Sub resettotals()
        Stolens = 0
        Recovereds = 0
        Deleteds = 0
    End Sub
    Protected Sub Repeater1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles Repeater1.ItemDataBound
        Stolens = Stolens + Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "stolen"))
        Recovereds = Recovereds + Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Recovered"))
        Deleteds = Deleteds + Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Deleted"))
    End Sub
End Class