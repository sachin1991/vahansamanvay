using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using MMotorVCS;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

public partial class Entry_Lost : System.Web.UI.Page
{
    int isdeleted = 0;
    static string _userid;
    MasterMethods mm = new MasterMethods();
    Entry_FormMethods eform = new Entry_FormMethods();
    DataSet ds = new DataSet();
    DataSet dtemail = new DataSet();
    int PostCount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["rndNo"].ToString() != Request.Cookies["myCookieVahan"].Value)
        {
            Response.Redirect("LogoutModule.aspx");
        }
        if (!IsPostBack)
        {

            //datetoday.Text = DateTime.Today.ToShortDateString();
            //ddAct1Name.Attributes.Add("OnChange", "GetAct1Code();");
            //txtAct1Code.Attributes.Add("OnBlur", "return SelectAct1ddl();");

            //ddAct2Name.Attributes.Add("OnChange", "GetAct2Code();");
            //txtAct2Code.Attributes.Add("OnBlur", "return SelectAct2ddl();");

            //ddAct3Name.Attributes.Add("OnChange", "GetAct3Code();");
            //txtAct3Code.Attributes.Add("OnBlur", "return SelectAct3ddl();");

        }
        if (Session.SessionID == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            _userid = Session["vcUserName"].ToString();
        }

        if (!IsPostBack)
        {
            try
            {

            }
            catch
            {
                Response.Redirect("error.aspx");
            }
            bindStateName();
            bindActName();
            bindStatus();
            bindVehicleType();
            bindColName();
            bindddyear();
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddModelName.Items.Insert(0, new ListItem("--Select--", "0"));
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
            if (Session["vcStateCode1"].ToString() != "0" && Session["vcStateCode1"].ToString() != "000" && Session["vcStateCode1"].ToString() != "" && Session["vcStateCode1"].ToString() != null)
            {
                bindStateName(Session["vcStateCode1"].ToString());
                txtStateCode.Text = Session["vcStateCode1"].ToString();
                txtStateCode.Enabled = false;
                rqdddStateName.Enabled = false;
            }
            if (Session["vcDistrict"].ToString() != "0" && Session["vcDistrict"].ToString() != "" && Session["vcDistrict"].ToString() != null)
            {
                bindDistrictName(Session["vcDistrict"].ToString());
                txtDistrictCode.Text = Session["vcDistrict"].ToString();
                txtDistrictCode.Enabled = false;
                ddDistrictName.Enabled = false;
                rqdddDistrictName.Enabled = false;
            }
            else
            {
                bindDistrictName();
            }
            if (Session["vcPS"].ToString() != "0" && Session["vcPS"].ToString() != "" && Session["vcPS"].ToString() != null)
            {
                bindPsName_new();
                txtPsCode.Text = Session["vcPS"].ToString();
                rqdddPsName.Enabled = false;
            }
            else
            {
                if (txtDistrictCode.Text.ToString().Trim() != "")
                {
                    ddPsName.Enabled = true;
                    bindPsNameDistrictWise();
                }
            }
        }

        lblmsg.Text = "";
        string value = Session["inRoleid"].ToString();
        switch (value)
        {
            case "1": //admin
                break;
            case "3": //police station
                break;
            case "6": //state
                break;
            case "7": //district
                break;
            case "8": //ncrb
                break;
            case "10": //ncrb
                break;
            case "11": //crimerecords executive
                break;
            case "12": //ncrb executive
                break;
            default:
                Response.Redirect("LogoutModule.aspx");
                break;
        }
    }
    protected void refresh1()
    {
        btnAdd.Visible = true;
        btnReset.Visible = true;
        //getmaxid();
        txta1sec1.Text = "";
        txta1sec2.Text = "";
        txta1sec3.Text = "";
        txta1sec4.Text = "";
        txta2sec1.Text = "";
        txta2sec2.Text = "";
        txta2sec3.Text = "";
        txta2sec4.Text = "";
        txta3sec1.Text = "";
        txta3sec2.Text = "";
        txta3sec3.Text = "";
        txta3sec4.Text = "";
        txtAct1Code.Text = "";
        txtAct2Code.Text = "";
        txtAct3Code.Text = "";
        txtChasisno.Text = "";
        txtColCode.Text = "";
        txtFIRDt.Text = "";
        txtFIRNo.Text = "";
        txtStolenDt.Text = "";
        txtEngineno.Text = "";

        txtMakeCode.Text = "";

        txtRegistrationno.Text = "";

        txtVehTypeCode.Text = "";
        ddAct1Name.SelectedIndex = 0;
        ddAct2Name.SelectedIndex = 0;
        ddAct3Name.SelectedIndex = 0;
        ddColName.SelectedIndex = 0;

        ddMakeName.Items.Clear();
        ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddMakeName.SelectedIndex = 0;
        ddModelName.Items.Clear();

        ddVehTypeName.SelectedIndex = 0;
    }
    protected void refresh()
    {
        btnAdd.Visible = true;
        btnReset.Visible = true;
        txta1sec1.Text = "";
        txta1sec2.Text = "";
        txta1sec3.Text = "";
        txta1sec4.Text = "";
        txta2sec1.Text = "";
        txta2sec2.Text = "";
        txta2sec3.Text = "";
        txta2sec4.Text = "";
        txta3sec1.Text = "";
        txta3sec2.Text = "";
        txta3sec3.Text = "";
        txta3sec4.Text = "";
        txtAct1Code.Text = "";
        txtAct2Code.Text = "";
        txtAct3Code.Text = "";
        txtChasisno.Text = "";
        txtColCode.Text = "";
        txtDistrictCode.Text = "";
        txtEngineno.Text = "";
        txtFIRDt.Text = "";
        txtFIRNo.Text = "";
        txtMakeCode.Text = "";
        txtPsCode.Text = "";
        txtRegistrationno.Text = "";
        txtStateCode.Text = "";
        txtStolenDt.Text = "";
        txtVehTypeCode.Text = "";
        ddAct1Name.SelectedIndex = 0;
        ddAct2Name.SelectedIndex = 0;
        ddAct3Name.SelectedIndex = 0;
        ddColName.SelectedIndex = 0;
        ddDistrictName.Items.Clear();
        ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddDistrictName.SelectedIndex = 0;
        ddPsName.Items.Clear();
        ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddPsName.SelectedIndex = 0;
        ddMakeName.Items.Clear();
        ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        ddMakeName.SelectedIndex = 0;
        ddModelName.SelectedIndex = 0;
        ddStateName.SelectedIndex = 0;
        ddVehTypeName.SelectedIndex = 0;
    }
    public void getmaxid()
    {
        try
        {
            hfLostId.Value = eform.getEntry_LostMaxId().ToString();
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindStateName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStateTypeNew();
            ddStateName.DataSource = ds;
            ddStateName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["statecode"].ColumnName);
            ddStateName.DataBind();
            ddStateName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindStateName(string stateCode)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStatecode(stateCode);
            ddStateName.DataSource = ds;
            ddStateName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["statename"].ColumnName);
            ddStateName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["statecode"].ColumnName);
            ddStateName.DataBind();
            ddStateName.Enabled = false;

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindddyear()
    {
        ListItem li;
        int val = 1950;
        for (int i = System.DateTime.Now.Year; i >= val; i--)
        {
            li = new ListItem(Convert.ToString(i), Convert.ToString(i));
            txtYear.Items.Add(li);
        }
        txtYear.Items.Insert(0, "--Select--");
    }
    public void bindDistrictName()
    {
        try
        {
            DataTable dt = new DataTable();
            string[] temp = ddStateName.SelectedValue.Split('-');
            dt = mm.getDistrictType_accstatetype(Convert.ToInt32(temp[0]));
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = GenericMethods.check(dt.Columns["districtname"].ColumnName);
            ddDistrictName.DataValueField = GenericMethods.check(dt.Columns["districtcode"].ColumnName);
            ddDistrictName.DataBind();
            ddDistrictName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindDistrictName(string districtCode)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = mm.getDistrictname(Session["vcDistrict"].ToString());
            ddDistrictName.DataSource = dt;
            ddDistrictName.DataTextField = GenericMethods.check(dt.Columns["districtname"].ColumnName);
            ddDistrictName.DataValueField = GenericMethods.check(dt.Columns["districtcode"].ColumnName);
            ddDistrictName.DataBind();


        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindPsName_new()
    {
        try
        {
            DataTable ds = new DataTable();
            ds = mm.getPSname(Session["vcPS"].ToString(), Session["vcDistrict"].ToString(), Session["vcStateCode1"].ToString());
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = ds.Columns["Psname"].ColumnName;
            ddPsName.DataValueField = ds.Columns["psidpscode"].ColumnName;
            ddPsName.DataBind();
            ddPsName.Enabled = false;
            txtPsCode.Enabled = false;

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindPsName()
    {
        try
        {
            DataSet ds = new DataSet();
            int tempState = Convert.ToInt32(ddStateName.SelectedValue);
            string[] temp = ddDistrictName.SelectedValue.Split('-');

            ds = mm.getPsType_accDistricttypeNew(Convert.ToInt32(temp[0]),tempState);
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["Psname"].ColumnName);
            ddPsName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["psidpscode"].ColumnName);
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }


    public void bindPsNameDistrictWise()
    {
        try
        {
            DataTable ds = new DataTable();
            ds = mm.getPSname_districtWise(txtStateCode.Text.ToString().Trim(), txtDistrictCode.Text.ToString().Trim());
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = GenericMethods.check(ds.Columns["Psname"].ColumnName);
            ddPsName.DataValueField = GenericMethods.check(ds.Columns["psidpscode"].ColumnName);
            ddPsName.DataBind();
            ddPsName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindDistrict_StateWise()
    {
        try
        {
            DataTable ds = new DataTable();
            ds = mm.getDistrictType(Session["vcStateCode1"].ToString()).Tables[0];
            ddPsName.DataSource = ds;
            ddPsName.DataTextField = GenericMethods.check(ds.Columns["Psname"].ColumnName);
            ddPsName.DataValueField = GenericMethods.check(ds.Columns["psidpscode"].ColumnName);
            ddPsName.DataBind();


        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindActName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getActNameNew();
            ddAct1Name.DataSource = ds;
            ddAct1Name.DataTextField = GenericMethods.check(ds.Tables[0].Columns["actname"].ColumnName);
            ddAct1Name.DataValueField = GenericMethods.check(ds.Tables[0].Columns["actidactcode"].ColumnName);
            ddAct1Name.DataBind();
            ddAct1Name.Items.Insert(0, new ListItem("--Select--", "0"));
            ddAct2Name.Items.Insert(0, new ListItem("--Select--", "0"));
            ddAct3Name.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindActName2()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getActNameNew();
            ddAct2Name.Items.Clear();
            ddAct2Name.DataSource = ds;
            ddAct2Name.DataTextField = GenericMethods.check(ds.Tables[0].Columns["actname"].ColumnName);
            ddAct2Name.DataValueField = GenericMethods.check(ds.Tables[0].Columns["actidactcode"].ColumnName);
            ddAct2Name.DataBind();
            ddAct2Name.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindActName3()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getActNameNew();
            ddAct3Name.Items.Clear();
            ddAct3Name.DataSource = ds;
            ddAct3Name.DataTextField = GenericMethods.check(ds.Tables[0].Columns["actname"].ColumnName);
            ddAct3Name.DataValueField = GenericMethods.check(ds.Tables[0].Columns["actidactcode"].ColumnName);
            ddAct3Name.DataBind();
            ddAct3Name.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindVehicleType()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehicleType();
            ddVehTypeName.DataSource = ds;
            ddVehTypeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vehicletypename"].ColumnName);
            ddVehTypeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vehicletypecode"].ColumnName);
            ddVehTypeName.DataBind();
            ddVehTypeName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message + "test7"; }
    }

    public void bindStatus()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getStatus();
            drpStatus.DataSource = ds;
            drpStatus.DataTextField = GenericMethods.check(ds.Tables[0].Columns["vcStatusDescription"].ColumnName);
            drpStatus.DataValueField = GenericMethods.check(ds.Tables[0].Columns["vcStatusCode"].ColumnName);
            drpStatus.DataBind();
            drpStatus.Items.Insert(0, "--Select--");
        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    public void bindMakeName()
    {
        {
            try
            {
                DataSet ds = new DataSet();
                ddMakeName.Items.Clear();
                ds = mm.getVehMake_accVehTypeNew2(Convert.ToInt32(ddVehTypeName.SelectedValue));
                ddMakeName.DataSource = ds;
                ddMakeName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["makename"].ColumnName);
                ddMakeName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["Makecode"].ColumnName);

                ddMakeName.DataBind();
                ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            catch (SqlException)
            { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
            catch (HttpCompileException)
            { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
            catch (HttpParseException)
            { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
            catch (Exception ex)
            { lblmsg.Text = ex.Message + "test8"; }
        }
    }
    public void bindColName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getColourNameNew();
            ddColName.DataSource = ds;
            ddColName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["colourname"].ColumnName);
            ddColName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["colidcolcode"].ColumnName);
            ddColName.DataBind();
            ddColName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }
    public void bindModelName()
    {
        try
        {
            DataSet ds = new DataSet();
            ds = mm.getVehModel_accMake(Convert.ToInt32(ddMakeName.SelectedValue));
            ddModelName.DataSource = ds;
            ddModelName.DataTextField = GenericMethods.check(ds.Tables[0].Columns["modelname"].ColumnName);
            ddModelName.DataValueField = GenericMethods.check(ds.Tables[0].Columns["modelid"].ColumnName);
            ddModelName.DataBind();
            ddModelName.Items.Insert(0, "--Select--");

        }
        catch (SqlException)
        { lblmsg.Text = "<font color=red>Occuring Problem in connectivity to database.</font>"; }
        catch (HttpCompileException)
        { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
        catch (HttpParseException)
        { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }
        catch (Exception ex)
        { lblmsg.Text = ex.Message; }
    }

    private bool SaveForm(bool blSave)
    {
        try
        {

            if (txtRegistrationno.Text == "")
            {
                Alert("Please enter the Registration No.");
                txtRegistrationno.Focus();
                return false;
            }

            if (txtChasisno.Text == "")
            {
                Alert("Please enter the Chasis No.");
                txtChasisno.Focus();
                return false;
            }

            if (txtEngineno.Text == "")
            {
                Alert("Please enter the Engine No.");
                txtEngineno.Focus();
                return false;
            }

            if (drpStatus.SelectedIndex == 0)
            {
                Alert("Please select the Status to be entered");
                drpStatus.Focus();
                return false;
            }

            if (ddStateName.SelectedIndex == 0)
            {
                if (txtStateCode.Text == "")
                {
                    Alert("Please select the State to be entered");
                    ddStateName.Focus();
                    return false;
                }
            }

            if (ddDistrictName.SelectedIndex == 0)
            {
                if (txtDistrictCode.Text == "")
                {
                    Alert("Please select the District to be entered");
                    ddDistrictName.Focus();
                    return false;
                }
            }

            if (ddPsName.SelectedIndex == 0)
            {
                if (txtPsCode.Text == "")
                {
                    Alert("Please select the PS to be entered");
                    ddPsName.Focus();
                    return false;
                }
            }

            if (ddVehTypeName.SelectedIndex == 0)
            {
                Alert("Please select the Vehicle Type to be entered");
                ddVehTypeName.Focus();
                return false;
            }

            if (ddMakeName.SelectedIndex == 0)
            {
                if (ddMakeName.Items.Count >= 2)
                {
                    Alert("Please select the Make Type to be entered.");
                    ddMakeName.Focus();
                    return false;
                }
            }

            if (txtFIRNo.Text == "")
            {
                Alert("Please enter the FIR No.");
                txtFIRNo.Focus();
                return false;
            }

            if (txtFIRDt.Text == "")
            {
                Alert("Please enter the FIR Date.");
                txtFIRDt.Focus();
                return false;
            }
            if (txtFIRDt.Text.ToString().Trim().Contains("'") == true)
            {
                Alert("Please enter the FIR Date Properly.");
                txtFIRDt.Focus();
                return false;
            }


            return true;

        }
        catch (Exception ex)
        {
            return false;
        }

        finally
        {
        }
    }

    public string GetIPAddress()
    {
        string strHostName = System.Net.Dns.GetHostName();
        IPHostEntry ipHostInfo = Dns.Resolve(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        HttpRequest currentRequest = HttpContext.Current.Request;
        String clientIP1 = currentRequest.ServerVariables["REMOTE_HOST"];
        return clientIP1;
    }
    private Boolean checkillegalDates(string firdt, string occdt)
    {
        bool illegal;
        illegal = false;
        if (firdt.IndexOfAny(new char[] { '[', ';', '\\', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (occdt.ToString().IndexOfAny(new char[] { '[', ';', '\\', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }


        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }
    private Boolean checkillegal(string fir, string fdt, string firno, string act1, string act2, string act3
        , string a1sec1, string a1sec2, string a1sec3, string a1sec4, string a2sec1, string a2sec2
        , string a2sec3, string a2sec4, string a3sec1, string a3sec2, string a3sec3, string a3sec4,
        string stolendate, string modelno, string color, string regno, string chasis, string enginno)
    {
        bool illegal;
        illegal = false;
        if (fir.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (fdt.ToString().IndexOfAny(new char[] { '[', ';', '\\', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (firno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (act1.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (act2.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (act3.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a1sec1.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a1sec2.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a1sec3.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a1sec4.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a2sec1.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a2sec2.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a2sec3.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a2sec4.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a3sec1.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a3sec2.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a3sec3.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (a3sec4.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (stolendate.ToString().IndexOfAny(new char[] { '[', ';', '\\', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (modelno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (color.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (regno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (chasis.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (enginno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
        {
            illegal = true;
        }
        if (illegal)
        {
            return true;
        }
        {
            return false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {

        bool illegaldt = checkillegalDates(txtFIRDt.Text.ToString().Trim(), txtStolenDt.Text.ToString().Trim());
        if (illegaldt == false)
        {
            if (this.IsValid)
            {

                try
                {


                    string StolenDate = "";

                    bool blnIsSaved = SaveForm(false);
                    if (blnIsSaved == true)
                    {
                        DataSet ds = new DataSet();
                        string fir = "F";
                        if (RbtnFir.Checked == true)
                        {
                            fir = "F";
                        }
                        else if (RbtnGdsde.Checked == true)
                        {
                            fir = "G";
                        }

                        string strHostName = System.Net.Dns.GetHostName();
                        string clientName = GetIPAddress().ToString();
                        string crimeno;


                        if (txtFIRDt.Text != "")
                        {
                            string[] tempFirdate = txtFIRDt.Text.Trim().Split('/');
                            crimeno = ddStateName.SelectedValue.Split('-')[0].Trim() + ddDistrictName.SelectedValue.Split('-')[0].Trim() + ddPsName.SelectedValue.Split('-')[0].Trim() + tempFirdate[2] + txtFIRNo.Text.Trim();
                        }
                        else
                        {
                            string[] tempFirdate = DateTime.Now.ToShortDateString().ToString().Trim().Split('/');
                            crimeno = ddStateName.SelectedValue.Split('-')[0].Trim() + ddDistrictName.SelectedValue.Split('-')[0].Trim() + ddPsName.SelectedValue.Split('-')[0].Trim() + tempFirdate[2] + txtFIRNo.Text.Trim();
                        }

                        if (txtStolenDt.Text != "")
                        {
                            string[] tempStolendate = txtStolenDt.Text.Trim().Split('/');
                            StolenDate = GenericMethods.decodecheck(GenericMethods.check(txtStolenDt.Text.Trim()));
                        }
                        else
                        {

                            string[] tempStolendate = txtFIRDt.Text.Trim().Split('/');
                            StolenDate = GenericMethods.decodecheck(GenericMethods.check(txtFIRDt.Text.Trim()));
                        }
                        ddMakeName.SelectedValue = GenericMethods.check(ddMakeName.SelectedValue);
                        ddVehTypeName.SelectedValue = GenericMethods.check(ddVehTypeName.SelectedValue);
                        int loop = eform.getEntry_LostDetailsfast(GenericMethods.check(txtRegistrationno.Text), GenericMethods.check(txtChasisno.Text), GenericMethods.check(txtEngineno.Text), (int.Parse(ddVehTypeName.SelectedValue.Split('-')[0])), GenericMethods.check(crimeno), drpStatus.SelectedValue.ToString().Trim().Split('-')[0]);
                        if (loop >= 1)
                        {
                            lblmsg.Text = "<font color=red>Registration No.,Engine No.,Chassis No. or Fir No one of these 4 parameter is matching with insert Data...</font>";
                            return;

                        }
                        string Act1 = "";
                        string Act2 = "";
                        string Act3 = "";
                        string Color = "";
                        if (ddAct1Name.SelectedIndex == 0)
                        {
                            Act1 = "0";
                        }
                        else
                        {
                            Act1 = ddAct1Name.SelectedValue.ToString().Split('-')[1];
                        }

                        if (ddAct2Name.Items.Count >= 0)
                        {
                            if (ddAct2Name.SelectedIndex == 0)
                            {
                                Act2 = "0";
                            }
                            else
                            {
                                Act2 = ddAct2Name.SelectedValue.ToString().Split('-')[1];
                            }
                        }
                        else
                        {
                            {
                                Act2 = "0";
                            }
                        }
                        if (ddAct3Name.Items.Count >= 0)
                        {
                            if (ddAct3Name.SelectedIndex == 0)
                            {
                                Act3 = "0";
                            }
                            else
                            {
                                Act3 = ddAct3Name.SelectedValue.ToString().Split('-')[1];
                            }
                        }
                        else
                        {
                            Act3 = "0";

                        }
                        if (ddColName.SelectedIndex == 0)
                        {
                            Color = "0";
                        }
                        else
                        {
                            Color = ddColName.SelectedValue.ToString().Split('-')[1];
                        }
                        string[] PSCode = ddPsName.SelectedValue.ToString().Split('-');
                        string[] MakeCode = ddMakeName.SelectedValue.ToString().Split('-');
                        string ModelNo = ddModelName.SelectedValue.ToString();
                        string color;
                        if (ddColName.SelectedIndex == 0)
                        {
                            color = "N.K";
                        }
                        else
                        {
                            color = ddColName.SelectedItem.Text.ToString().Trim();
                        }

                        string _capcha = Session["strRandom"].ToString();
                        string _Hostaddr = strHostName;
                        string _Clientaddr = clientName;
                        string tyear;
                        if (txtYear.Text != "--Select--")
                        {
                            tyear = txtYear.Text;


                        }
                        else
                        {
                            tyear = "";
                        }
                        ddMakeName.SelectedValue = GenericMethods.check(ddMakeName.SelectedValue);
                        ddVehTypeName.SelectedValue = GenericMethods.check(ddVehTypeName.SelectedValue);

                        String stat = ddStateName.SelectedValue.Split('-')[0];

                        String dist = ddDistrictName.SelectedValue.Split('-')[0];
                        String pols = PSCode[0];
                        String vehc = ddVehTypeName.SelectedValue.Split('-')[0];
                        String makv = MakeCode[0];
                        DateTime fdt = Convert.ToDateTime(GenericMethods.decodecheck(GenericMethods.check(txtFIRDt.Text.ToString())));
                        DateTime stdate = Convert.ToDateTime(StolenDate);
                        bool illegal = checkillegal(fir, fdt.ToString(), txtFIRNo.Text, Act1, Act2, Act3, txta1sec1.Text, txta1sec2.Text, txta1sec3.Text, txta1sec4.Text, txta2sec1.Text, txta2sec2.Text, txta2sec3.Text, txta2sec4.Text, txta3sec1.Text, txta3sec2.Text, txta3sec3.Text, txta3sec4.Text, stdate.ToString(), ModelNo, Color, txtRegistrationno.Text.Trim().ToUpper(), txtChasisno.Text.Trim().ToUpper(), txtEngineno.Text.Trim().ToUpper());
                        if (illegal == false)
                        {
                            ds = eform.AddEntry_LostRecovered((ddStateName.SelectedValue.Split('-')[0]), (ddDistrictName.SelectedValue.Split('-')[0]), (PSCode[0]), drpStatus.SelectedValue.ToString().Split('-')[1], GenericMethods.check(fir), GenericMethods.check(txtFIRNo.Text), fdt, (Act1), (Act2), (Act3), txta1sec1.Text, txta1sec2.Text, txta1sec3.Text, txta1sec4.Text, txta2sec1.Text, txta2sec2.Text, txta2sec3.Text, txta2sec4.Text, txta3sec1.Text, txta3sec2.Text, txta3sec3.Text, txta3sec4.Text, Convert.ToDateTime(StolenDate), (ddVehTypeName.SelectedValue.Split('-')[0]), (MakeCode[0]), ModelNo, (Color), GenericMethods.check(txtRegistrationno.Text.Trim().ToUpper()), GenericMethods.check(txtChasisno.Text.Trim().ToUpper()), GenericMethods.check(txtEngineno.Text.Trim().ToUpper()), tyear, crimeno, _userid, _capcha, _Hostaddr, _Clientaddr);
                            Regex re = new Regex("[;\\/:*?\"<>|&'%^]");
                            //if (drpStatus.SelectedItem.Text == "Recovered")
                            //{
                            //    Connection mycon = new Connection();
                            //    SqlConnection con = mycon.MakeConnection();
                            //    con.Open();
                            //    string msql = "UPDATE TblTrn_EnterLostRecoveredVehicle SET TempDeleted=1 where vcRegistrationNo = '" + txtRegistrationno.Text.ToString().Trim() + "' and vcChasisNo = '" + txtChasisno.Text.ToString().Trim() + "' and vcEngineNo = '" + txtEngineno.Text.ToString().Trim() + "' and FirDt<='" + Convert.ToDateTime(txtFIRDt.Text.ToString().Trim()).ToString("yyyy-MM-dd") + "'";
                            //    SqlCommand cmd = new SqlCommand(msql, con);
                            //    int i = cmd.ExecuteNonQuery();
                            //    con.Close();
                            //}
                            if (ds.Tables[0].Rows.Count > 0)
                            {

                                Session["Data"] = (DataTable)(ds.Tables[0]);
                                Session["VehicleTypeDesc"] = re.Replace(ddVehTypeName.SelectedItem.Text.ToString().Trim(), "");
                                Session["MakeDesc"] = re.Replace(ddMakeName.SelectedItem.Text.ToString().Trim(), "");
                                Session["Registration"] = re.Replace(txtRegistrationno.Text.ToString().Trim(), "");
                                Session["Chasis"] = re.Replace(txtChasisno.Text.ToString().Trim(), "");
                                Session["Engine"] = re.Replace(txtEngineno.Text.ToString().Trim(), "");
                                Session["Color"] = re.Replace(color, "");
                                Session["Model"] = re.Replace(ddModelName.Text.ToString().Trim(), "");
                                if (txtYear.Text != "--Select--")
                                {
                                    Session["Model"] = txtYear.Text.ToString();

                                }
                                else
                                {
                                    Session["Model"] = "";
                                }
                                Session["PSDesc"] = ddPsName.SelectedItem.Text.ToString().Trim();
                                Session["StateDesc"] = ddStateName.SelectedItem.Text.ToString().Trim();
                                Session["StateCode"] = txtStateCode.Text.ToString().Trim();

                                Session["HostAddr"] = strHostName.ToString().Trim();
                                Session["ClientAddr"] = clientName.ToString().Trim() + " ";

                                Session["MatchStatus"] = "1";

                                Response.Write("<script>window.open('WirelessMessage.aspx','_blank')</script>");
                                lblmsg.Text = "<font color=green>1 Record inserted Successfully</font>";



                            }
                            else
                            {
                                Session["VehicleTypeDesc"] = re.Replace(ddVehTypeName.SelectedItem.Text.ToString().Trim(), "");
                                Session["MakeDesc"] = re.Replace(ddMakeName.SelectedItem.Text.ToString().Trim(), "");
                                Session["Registration"] = re.Replace(txtRegistrationno.Text.ToString().Trim(), "");
                                Session["Chasis"] = re.Replace(txtChasisno.Text.ToString().Trim(), "");
                                Session["Engine"] = re.Replace(txtEngineno.Text.ToString().Trim(), "");
                                Session["Color"] = re.Replace(color, "");
                                Session["Model"] = re.Replace(ddModelName.Text.ToString().Trim(), "");
                                if (txtYear.Text != "--Select--")
                                {
                                    Session["Model"] = txtYear.Text.ToString();
                                }
                                else
                                {
                                    Session["Model"] = "";
                                }
                                Session["PSDesc"] = ddPsName.SelectedItem.Text.ToString().Trim();
                                Session["StateDesc"] = ddStateName.SelectedItem.Text.ToString().Trim();
                                Session["StateCode"] = txtStateCode.Text.ToString().Trim();

                                Session["HostAddr"] = strHostName.ToString().Trim();
                                Session["ClientAddr"] = clientName.ToString().Trim() + " ";
                                string dr = drpStatus.SelectedValue.ToString().Trim().Split('-')[0];
                                if (dr == "L")
                                {
                                    Session["MatchStatus"] = "2";
                                }
                                else
                                {
                                    Session["MatchStatus"] = "4";
                                }

                                Response.Write("<script>window.open('WirelessMessage.aspx','_blank')</script>");
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    lblmsg.Text = ex.Message;
                }
                catch (HttpCompileException)
                { lblmsg.Text = "<font color=red>System Occuring Problem.</font>"; }
                catch (HttpParseException)
                { lblmsg.Text = "<font color=red>Internet Browser Occuring Problem.</font>"; }

                catch (Exception ex)
                { lblmsg.Text = ex.Message; }
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "YourUniqueScriptKey", "alert('Illegal Character Found...');", true);
        }

    }

    private void Alert(string strErrMsg)
    {

        string strScript = "<script language='javascript'>alert('" + strErrMsg + "')</script>";

        if (!ClientScript.IsClientScriptBlockRegistered("ScriptAlert"))
        {

            ClientScript.RegisterStartupScript(typeof(Entry_Lost), "ScriptAlert", strScript);

        }

    }

    protected string convertToDateTime(string old)
    {
        string date1 = "";
        string month1 = "";
        string year1 = "";
        char[] bCharArray = old.ToCharArray();
        for (int i = 0; i < bCharArray.Length; i++)
        {
            if (i < 2)
            {
                date1 += bCharArray[i].ToString();
            }
            else if (i > 2 && i < 5)
            {
                month1 += bCharArray[i].ToString();
            }
            else if (i > 5 && i < bCharArray.Length)
            {
                year1 += bCharArray[i].ToString();
            }

        }
        string newdate = month1 + "/" + date1 + "/" + year1;
        return newdate;
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
    private void afterSelectState()
    {
        if (ddStateName.SelectedIndex != 0)
        {
            bindDistrictName();
            ddPsName.Items.Clear();
            txtPsCode.Text = "";
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        else
        {
            txtStateCode.Text = "";
            ddDistrictName.Items.Clear();
            txtDistrictCode.Text = "";
            ddDistrictName.Items.Insert(0, new ListItem("--Select--", "0"));

        }
    }
    protected void ddStateName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectState();
            PostCount++;
        }

    }
    protected void txtStateCode_TextChanged(object sender, EventArgs e)
    {

        //if (PostCount == 0)
        //{
        //    afterSelectState();
        //    PostCount++;
        //}

    }
    private void afterSelectDistrict()
    {
        if (ddDistrictName.SelectedIndex != 0)
        {
            bindPsName();
        }
        else
        {
            txtDistrictCode.Text = "";
            ddPsName.Items.Clear();
            txtPsCode.Text = "";
            ddPsName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }
    protected void ddDistrictName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PostCount == 0)
        {
            afterSelectDistrict();
            PostCount++;
        }
    }
    protected void txtDistrictCode_TextChanged(object sender, EventArgs e)
    {

    }

    protected void ddVehTypeName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (PostCount == 0)
        {
            if (ddVehTypeName.SelectedIndex != 0)
            {
                afterSelectVehicleType();
                PostCount++;
            }
        }
    }



    //}

    protected void RbtnNewEntry_CheckedChanged(object sender, EventArgs e)
    {
        btnAdd.Visible = true;
        btnReset.Visible = true;
    }
    protected void ddModelName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtVehTypeCode_TextChanged(object sender, EventArgs e)
    {

    }
    private void afterSelectVehicleType()
    {
        if (ddVehTypeName.SelectedIndex != 0)
        {

            bindMakeName();
        }
        else
        {
            ddMakeName.Items.Clear();
            txtMakeCode.Text = "";
            ddMakeName.Items.Insert(0, new ListItem("--Select--", "0"));
        }
    }

    private string GetMAC()
    {
        string macAddresses = "";

        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }
    private string SafeSqlLiteral(string inputSQL)
    {
        return inputSQL.Replace("'", "''");

    }

    protected void Loadact2_Click(object sender, EventArgs e)
    {
        bindActName2();
    }

    protected void LoadAct3_Click(object sender, EventArgs e)
    {
        bindActName3();
    }
}
